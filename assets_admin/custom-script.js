(function() {
    document.getElementById('edit-address').addEventListener('click', function() {
        showAddressForm();
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#save-address').click(function() {
        var data={
            ordershipping_id:$('#address-ordershipping-id').val(),
            destination_id:$('#address-destination-id').val(),
            firstname:$('#address-fname').val(),
            lastname:$('#address-lname').val(),
            address:$('#address-address').val(),
            /*
            destination:$('#address-destination').val(),
            */
            city:$('#address-city').val(),
            zip:$('#address-zip').val(),
            phone:$('#address-phone').val()
        };
        $.ajax({
            type: 'POST',
            dataType : 'text', /*Default data type; Returned from server*/
            url: base_url+'/order-details/update-address',
            data: data,
            success: function(d) {
                $('#text-name').text($('#address-fname').val()+' '+$('#address-lname').val());
                $('#text-address').text($('#address-address').val());
                $('#text-city').text($('#address-city').val());
                $('#text-zip').text($('#address-zip').val());
                $('#text-phone').text($('#address-phone').val());
                
                hideAddressForm();
            }
        });
    });
})();
function showAddressForm() {
    var address_text = document.getElementById('address-text');
    var address_field = document.getElementById('address-field');
    var edit_address = document.getElementById('edit-address');
    var save_address = document.getElementById('save-address');
    address_text.classList.remove('show');
    address_text.classList.add('hidden');
    address_field.classList.remove('hidden');
    address_field.classList.add('show');
    edit_address.classList.remove('show');
    edit_address.classList.add('hidden');
    save_address.classList.remove('hidden');
    save_address.classList.add('show');
}
function hideAddressForm() {
    var address_text = document.getElementById('address-text');
    var address_field = document.getElementById('address-field');
    var edit_address = document.getElementById('edit-address');
    var save_address = document.getElementById('save-address');
    address_text.classList.remove('hidden');
    address_text.classList.add('show');
    address_field.classList.remove('show');
    address_field.classList.add('hidden');
    edit_address.classList.remove('hidden');
    edit_address.classList.add('show');
    save_address.classList.remove('show');
    save_address.classList.add('hidden');
}