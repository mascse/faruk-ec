<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subprocat extends Model
{
    //subprocat
	protected $primaryKey = 'subprocat_id';
    protected $table = 'subprocat';
}
