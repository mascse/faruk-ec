<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Registeruser extends Model {

    protected $primaryKey = 'registeruser_id';
    protected $table = 'registeruser';

    public function GetLoginUserData($login_id) {
        $get_register_id = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_login_id', '=', 'registeruserdetails.registeruser_login_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_firstname', 'registeruser.offer', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_login_id', $login_id)
                ->first();
        return $get_register_id;
    }

}
