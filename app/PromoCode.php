<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model {

    //
    public function scopeIsExists($query, $code) {
        $row = $query->where('promo_code', $code)->first();
        if ($row)
            return $row->promo_code;
        return false;
    }

    public function scopeDeactivate($query, $code) {
        $row = $query->where('promo_code', $code)->update(['is_active' => 1]);

        return $row;
    }

}
