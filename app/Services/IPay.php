<?php
namespace App\Services;

use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

//define("IPAY_AUTHORIZATION", "Bearer MDcxMDRlNGItOTM5OC00NTllLTg5YjItNzkzODA2ZGZkZjY5");
define("IPAY_AUTHORIZATION", "Bearer 3db2fcf5-f6f6-4feb-9e71-df081110933e");

# IF SANDBOX TRUE, THEN IT WILL CONNECT WITH SSLCOMMERZ SANDBOX (TEST) SYSTEM
define("IPAY_SANDBOX", true);

class IPay {
    /*protected $ipay_submit_url;
    protected $ipay_validation_url;
    protected $ipay_mode;
    protected $ipay_data;
    protected $store_id;
    protected $store_pass;
    public $error = '';*/
    protected $base_url;

    public function __construct()
    {
        //$this->setSSLCommerzMode((SSLCZ_IS_SANDBOX) ? 1 : 0);
        //$this->store_id = SSLCZ_STORE_ID;
        //$this->store_pass = SSLCZ_STORE_PASSWD;
        //$this->ipay_submit_url = "https://" . $this->ipay_mode . ".ipayommerz.com/gwprocess/v3/api.php";
        //$this->ipay_validation_url = "https://" . $this->ipay_mode . ".sslcommerz.com/validator/api/validationserverAPI.php";
        $this->base_url = url('/');;
    }
    
    public function submitpayment($request, $referencedId) {
		//dd($request);
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://demo.ipay.com.bd/api/pg/order',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => '{"amount" : '.$request->shoppingcart_total.', "referenceId": "'.$referencedId.'", "description" : "Buy '.implode(', ', $request->product_name).' from '.$this->base_url.'", "successCallbackUrl" : "'.$this->base_url.'/iPay/success", "failureCallbackUrl" : "'.$this->base_url.'/iPay/failure", "cancelCallbackUrl" : "'.$this->base_url.'/iPay/cancel"}',
          CURLOPT_HTTPHEADER => array(
            "Authorization: ".IPAY_AUTHORIZATION,
            "Content-Type: application/json",
            "Postman-Token: c83eb032-1e5f-4950-94a7-7f1b13c148ee",
            "cache-control: no-cache"
          ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($response,true);
        $orderId = $json["orderId"];
       // Session::put('orderId', $orderId);
		$url = $json["paymentUrl"];
      //  dd($url);
        
        //Redirect user to iPay website for payment.
        return $url;
        //return redirect($json->paymentUrl);
    }
    public function ipay_success() {
        $orderId = session('orderId');
        $response = $this->orderValidate($orderId);
	    return $response;
    }
    
    public function orderValidate($orderId) {
	    $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://demo.ipay.com.bd/api/pg/order/'.$orderId.'/status',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => array(
                "Authorization: ".IPAY_AUTHORIZATION,
                "Content-Type: application/json"
                ),
            )
        );
        $response = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($response);
        if(isset($json->statusCode) && ($json->statusCode == 200)) {
            return $json;
        }
        return false;
    }
}