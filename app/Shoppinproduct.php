<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Shoppinproduct extends Model {

    //shoppinproduct
    protected $primaryKey = 'shoppinproduct_id';
    protected $table = 'shoppinproduct';

    public function SaveShoppingInfo($data) {
        //	dd($data);
        $save = DB::table('shoppinproduct')->insert($data);
        return $save;
    }

    public function SaveData($request, $shoppingcart_id) {
        $total_pro = $request->product_id;
        $s_charge = $request->Shipping_Charge;
        $disti = $request->region;
        for ($i = 0; $i < count($total_pro); $i++) {
            $shopping_product_data['shoppingcart_id'] = $shoppingcart_id;
            $shopping_product_data['product_id'] = $request->product_id[$i];
            $shopping_product_data['product_barcode'] = $request->product_barcode[$i];
            $shopping_product_data['productalbum_name'] = $request->productalbum_name[$i];
            $shopping_product_data['prosize_name'] = $request->product_size[$i];
            $shopping_product_data['shoppinproduct_quantity'] = $request->shoppinproduct_quantity[$i];
            $shopping_product_data['product_price'] = $request->product_price[$i];
            $shopping_product_data['cart_image'] = $request->image_link[$i];
            $shopping_product_data['shoppingcart_total'] = $request->shoppingcart_total;
            $shopping_product_data['Shipping_Charge'] = $s_charge;
            $shopping_product_data['deliveryMethod'] = $request->dmselect;
            $shopping_product_data['shipping_area'] = $disti;
            DB::table('shoppinproduct')->insert($shopping_product_data);
            // Reduce product quantity 
            $pro_qty = DB::table('productsize')->where('product_id', $request->product_id[$i])->where('color_name', $request->productalbum_name[$i])->where('productsize_size', $request->product_size[$i])->first();
            $qty_product_id = $pro_qty->product_id;
            $productsize_size = $pro_qty->productsize_size;
            $color_name = $pro_qty->color_name;
            $SizeWiseQty = $pro_qty->SizeWiseQty;
            $qty = $request->shoppinproduct_quantity[$i];
            $reduce_data['SizeWiseQty'] = $SizeWiseQty - $qty;
            DB::table('productsize')->where('product_id', $pro_qty->product_id)->where('color_name', $pro_qty->color_name)->where('productsize_size', $pro_qty->productsize_size)->update($reduce_data);
            //cart destory
            DB::table('product')->where('product_id', $request->product_id[$i])->increment('sale');
        }
    }

}
