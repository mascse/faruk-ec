<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $table='role_permission';
	
	protected $fillable = [
        'role_id', 'permission',
    ];
	
	public function get_permission($role_id){
		$role_permission=RolePermission::where('role_id',$role_id)->get();
		$permission=[];
		foreach($role_permission as $p_list){
			$permission[]=$p_list->permission;
		}
		return $permission;
	}
}
