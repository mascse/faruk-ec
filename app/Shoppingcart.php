<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Shoppingcart extends Model {

    //shoppingcart
    protected $primaryKey = 'shoppingcart_id';
    protected $table = 'shoppingcart';

    public function SaveData($request, $user_id, $get_offer) {
      //  $data['registeruser_id'] = $user_id;
        $data['shoppingcart_date'] = date('Y-m-d');
        $data['shipping_charge'] = $request->Shipping_Charge;
        $data['shoppingcart_subtotal'] = $request->shoppingcart_subtotal;
        $data['shoppingcart_total'] = $request->shoppingcart_total;
        $data['used_promo'] = 0;
        $data['get_offer'] = $get_offer;
        $shoppingcart_id = DB::table('shoppingcart')->insertGetId($data);
        return $shoppingcart_id;
    }

}
