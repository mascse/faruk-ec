<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model {

    protected $primaryKey = 'product_id';
    protected $table = 'product';
    
    public static function GetProductCategorySubcategory($procat_id, $subprocat_id) {
        $get_title = DB::table('product')
                ->select('procat.procat_name', 'subprocat.subprocat_name')
                ->join('procat', 'product.procat_id', '=', 'procat.procat_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->where('product.procat_id', $procat_id)
                ->where('product.subprocat_id', $subprocat_id)
                ->first();
        return $get_title;
    }
    
    public function NewArriavl(){
        $new=DB::table('product')
        ->join('subprocat','subprocat.subprocat_id','=','product.subprocat_id')
        ->join('productalbum','productalbum.product_id','=','product.product_id')
        ->join('productimg','productimg.productalbum_id','=','productalbum.productalbum_id')
        ->select('product.product_id','product_price','product_code','product.product_img_thm','product_name','productimg.productimg_img_medium','productalbum_name')
        ->where('product_active_deactive',0)
        ->where('product_insertion_date','<',45)
        ->groupBy('product.product_id')
        ->orderBy('product.product_id','DESC')
        ->limit(10)
        ->get();
        return $new;
    }
    
    //eid collection 
    
    public function EidCollection(){
        $product = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('isSpecial', 2)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(21);
        return $product;
    }
    
    public function EidCollection2019ByCotegory($pro_id) {
        //dd($pro_id);
        if ($pro_id == 17) {
            $procat_id = [17, 18];
        } elseif ($pro_id == 9) {
            $procat_id = [5, 9, 10, 11, 12, 13, 20, 7,16];
        } else {
            $procat_id = [$pro_id];
        }
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->whereIn('product.procat_id', $procat_id)
                ->where('isSpecial', 2)
                ->where('product_active_deactive', 0)
                ->orderBy('product.sold', 'DESC')
                ->groupBy('product.product_id')
                ->paginate(21);
        return $productlist;
    }
    
    public function SummerCollection(){
         $product = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('isSpecial', 8)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(21);
        return $product;
    }
    
    public function SummerCollection2019ByCotegory($pro_id) {
        //dd($pro_id);
        if ($pro_id == 17) {
            $procat_id = [17, 18];
        } elseif ($pro_id == 9) {
            $procat_id = [5, 9, 10, 11, 12, 13, 20, 7,16];
        } else {
            $procat_id = [$pro_id];
        }
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->whereIn('product.procat_id', $procat_id)
                ->where('isSpecial', 8)
                ->where('product_active_deactive', 0)
                ->orderBy('product.sold', 'DESC')
                ->groupBy('product.product_id')
                ->paginate(21);
        return $productlist;
    }
    
    public function CapsuleCollection(){
         $product = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('isSpecial', 9)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(21);
        return $product;
    }
    
    public function CapsuleCollection2019ByCotegory($pro_id) {
        //dd($pro_id);
        if ($pro_id == 17) {
            $procat_id = [17, 18];
        } elseif ($pro_id == 9) {
            $procat_id = [5, 9, 10, 11, 12, 13, 20, 7,16];
        } else {
            $procat_id = [$pro_id];
        }
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->whereIn('product.procat_id', $procat_id)
                ->where('isSpecial', 9)
                ->where('product_active_deactive', 0)
                ->orderBy('product.sold', 'DESC')
                ->groupBy('product.product_id')
                ->paginate(21);
        return $productlist;
    }

}
