<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Registeruserdetails extends Model {

    //registeruserdetails
    protected $primaryKey = 'registeruserdetails_id';
    protected $table = 'registeruserdetails';

    public function ShippingUpdate($request, $user_id) {
        $update_register_details['registeruser_address'] = $request->address;
        $update_register_details['registeruser_phone'] = $request->mobile_no;
        $update_register_details['registeruser_city'] = $request->registeruser_city;
        $update_register_details['registeruser_zipcode'] = $request->registeruser_zipcode;
        $update = DB::table('registeruserdetails')->where('registeruser_id', $user_id)->update($update_register_details);
        return $update;
    }

    public function GetUserDetailsByUserId($register_id) {
        $check_user_details = DB::table('registeruserdetails')->select('registeruser_id', 'registeruserdetails_id')->where('registeruser_id', $register_id)->first();
        return $check_user_details;
    }

}
