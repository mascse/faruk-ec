<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class procat extends Model
{
    protected $primaryKey = 'procat_id';
    protected $table = 'procat';
}
