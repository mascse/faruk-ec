<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productsize extends Model
{
    //
	protected $primaryKey = 'productsize_id';
    protected $table = 'productsize';
}
