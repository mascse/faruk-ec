<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;

class CampaignController extends Controller {

    public function Boishak() {
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('spcollection', 1)
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_order', 'ASC')
                ->orderBy('product.product_id', 'desc')
                ->groupBy('product.product_id')
                ->get();
        $data['product_list'] = $productlist;
        return view('boishak_1425_all', $data);
    }

    public function BoishakProduct($pro_id) {
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('product.procat_id', $pro_id)
                ->where('spcollection', 1)
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_order', 'ASC')
                ->orderBy('product.product_id', 'desc')
                ->groupBy('product.product_id')
                ->get();
        $data['product_list'] = $productlist;
        $data['procat_id'] = $pro_id;
        return view('boishak_1425', $data);
    }

    public function IndependenceDay($title = null, $pro_id = null) {
        if ($pro_id == null) {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->where('product_active_deactive', 0)
                    ->where('isSpecial', 6)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->paginate(21);
        } else {
            if ($pro_id == 17) {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->whereIn('product.procat_id', [17, 18])
                        ->where('isSpecial', 6)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.sold', 'DESC')
                        ->groupBy('product.product_id')
                        ->paginate(21);
            } elseif ($pro_id == 9) {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->whereIn('product.procat_id', [5, 9, 10, 11, 12, 13, 20, 7])
                        ->where('isSpecial', 6)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.sold', 'DESC')
                        // ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->paginate(12);
            } else {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->where('product.procat_id', $pro_id)
                        ->where('isSpecial', 6)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.product_order', 'ASC')
                        ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->get();
            }
        }
        $data['title'] = $title;
        $data['product_list'] = $productlist;
        return view('collection.independence-day', $data);
    }

    public function EidCollection2019($title = null, $pro_id = null) {
        $list = new Product();
        if ($pro_id == null) {
            $productlist = $list->EidCollection2019();
        } else {
            $productlist = $list->EidCollection2019ByCotegory($pro_id);
        }
        $data['title'] = $title;
        $data['product_list'] = $productlist;
        //dd($data);
        return view('collection.eid-collection', $data);
    }

}
