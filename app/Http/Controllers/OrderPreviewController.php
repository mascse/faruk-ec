<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Validator;
use Mail;
use Cart;
use Carbon\Carbon;
use App\Product;
use App\Productalbum;
use App\Shoppinproduct;
use App\Shoppingcart;
use App\Conforder;
use App\Registeruser;
use App\Registeruserdetails;
use App\Ordershipping;
use App\PromoCode;
// payment gateway
use App\Services\IPay;
use App\Services\SSLCommerz;

class OrderPreviewController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['except' => 'OrderConfirmed','GetCityList','paymentMethodHandle','success','SendInvoice','ipn']);
        date_default_timezone_set("Asia/Dhaka");
        $this->ipay = new IPay();
        $this->sslc = new SSLCommerz();
    }

    public function index() {
        $user_id = Auth::user()->id;
        $registeruser_info = new Registeruser();
        $get_register_id = $registeruser_info->GetLoginUserData($user_id);
        $registeruserdetails = new Registeruserdetails();
        $check_user_details = $registeruserdetails->GetUserDetailsByUserId($get_register_id->registeruser_id);
        if ($check_user_details) {
            $data['user_info'] = $registeruser_info->GetLoginUserData($user_id);
            return view('orderpreview', $data);
        } else {
            session()->flash('save', 'Please enter your address first.');
            return redirect()->route('user-details', ['id' => $user_id]);
        }
    }

    public function SaveShippingProductInfo(Request $request) {
        $vaildation = Validator::make($request->all(), [
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'address' => 'required',
                    'mobile_no' => 'required',
                    'RegionList' => 'required',
                    'region' => 'required',
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $login_id = Auth::user()->id;

            // promo code
            $shoppingcart_total = $request->shoppingcart_total;
            $promo_used = 0;
            //end promo code

            $registeruser_info = new Registeruser();
            $user_details = $registeruser_info->GetLoginUserData($login_id);
            $user_id = $user_details->registeruser_id;
            //registerdetails update

            $registeruserdetails = new Registeruserdetails();
            $registeruserdetails->ShippingUpdate($request, $user_id);
            if ($user_details->offer == 0) {
                $get_offer = 1;
            } else {
                $get_offer = 0;
            }

            //shopping cart save data
            $shoppingcart_info = new Shoppingcart();
            $shoppingcart_id = $shoppingcart_info->SaveData($request, $user_id, $get_offer);

            //shoppinproduct save data
            $shoppinproduct = new Shoppinproduct();
            $shoppinproduct->SaveData($request, $shoppingcart_id);
            //conforder save data
            $conforder_placed_date = Carbon::now();
            $conforder = new Conforder();
            $conforder_id = $conforder->SaveData($shoppingcart_id, $user_id, $request, $conforder_placed_date);
            $tracknumber = "PLORDER#100-" . $conforder_id;
            PromoCode::deactivate($request->promo_code);
            //Ordershipping data save
            $rdershipping = new Ordershipping();
            $rdershipping->SaveData($conforder_id, $shoppingcart_id, $user_id, $request);



            //update register offer
            $update_offer['offer'] = 1;
            DB::table('registeruser')->where('registeruser_login_id', $login_id)->update($update_offer);

            //$conforder_id = time().'abcd';
            $url = $this->paymentMethodHandle($request, $conforder_id);
            if ($url != false && $request->dmselect == 'iPay') {
                return redirect($url);
            }

            //order confirmation mail send
            $email_data = $this->SendInvoice($conforder_id);

            //redirect order confirm page
            return redirect('/order-confirmed')->with('email_data', $email_data);
        }
    }

    public function SendInvoice($tran_id) {
        //conforder details
        $order_detials = Conforder::find($tran_id);

        //get customer details
        $login_id = Auth::user()->id;
        $registeruser = new Registeruser();
        $user_details = $registeruser->GetLoginUserData($login_id);

        //order confirmation mail send
        $email_data['email'] = $user_details->registeruser_email;
        $email_data['track_number'] = $order_detials->conforder_tracknumber;
        $email_data['first_name'] = $user_details->registeruser_firstname;
        $email_data['last_name'] = $user_details->registeruser_lastname;
        $email_data['address'] = $user_details->registeruser_address;
        $email_data['city'] = $user_details->registeruser_city;
        $email_data['zip'] = $user_details->registeruser_zipcode;
        $email_data['country'] = $user_details->registeruser_country;
        $email_data['phone'] = $user_details->registeruser_phone;
        $email_data['order_date'] = date('Y-m-d');

        //shopping cart info
        $shoppingcart_info = Shoppingcart::find($order_detials->shoppingcart_id);
        $email_data['shipping_Charge'] = $shoppingcart_info->shipping_charge;
        $email_data['shoppingcart_subtotal'] = $shoppingcart_info->shoppingcart_subtotal;
        $email_data['shoppingcart_total'] = $shoppingcart_info->shoppingcart_total;
        $email_data['get_offer'] = $shoppingcart_info->get_offer;

        //shopping product info
        $email_data['order_product'] = DB::table('shoppinproduct')
                ->join('product', '.shoppinproduct.product_id', '=', 'product.product_id')
                ->select('shoppinproduct.cart_image', 'shoppinproduct.productalbum_name', 'shoppinproduct.prosize_name', 'shoppinproduct.shoppinproduct_quantity', 'shoppinproduct.product_price', 'product.product_name', 'product.product_styleref')
                ->where('shoppingcart_id', $order_detials->shoppingcart_id)
                ->get();

        // send email to the customer
        /* Mail::send('emails.order_email', $email_data, function($message) use($email_data) {
          $message->from('order@pride-limited.com', 'Pride Limited');
          $message->to($email_data['email']);
          $message->bcc('pride.orderlog@gmail.com', 'Pride Group');
          $message->subject('Order Comfirmation');
          }); */

        $email_data['conforder_tracknumber'] = $order_detials->conforder_tracknumber;
        $email_data['shoppingcart_id'] = $order_detials->shoppingcart_id;

        //cart item delete
        Cart::instance('products')->destroy();

        return $email_data;
    }

    public function OrderConfirmed() {
        $data['shoppingcart_total'] = session("email_data")['shoppingcart_total'];
        $data['shipping_Charge'] = session("email_data")['shipping_Charge'];
        $data['track_number'] = session("email_data")['track_number'];
        $data['conforder_tracknumber'] = session("email_data")['conforder_tracknumber'];
        $shoppingcart_id = session("email_data")['shoppingcart_id'];
        $data['order_product'] = DB::table('shoppinproduct')
                ->join('product', '.shoppinproduct.product_id', '=', 'product.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->select('product.product_code as sku', 'product.product_name as name', 'subprocat.subprocat_name as category', 'shoppinproduct.product_price as price', 'shoppinproduct.shoppinproduct_quantity as quantity')
                ->where('shoppingcart_id', $shoppingcart_id)
                ->get();
        return view('orderconfirmed', $data);
    }

    public function paymentMethodHandle($request, $referencedId) {
        if ($request->dmselect == 'iPay') {
            $url = $this->ipay->submitpayment($request, $referencedId);
            return $url;
        } elseif ($request->dmselect == 'ssl') {
			
            $this->sslc->submitpayment($request, $referencedId);
        }
        return false;
    }

    public function success(Request $request, $payment_method) {
        if ($payment_method == 'iPay') {
            $response = $this->ipay->ipay_success();
            if ($response != false) {
                //dd($response->referenceId);
                $update_product = DB::table('conforder')->where('conforder_id', $response->referenceId)->update(['conforder_status' => 'Processing']);
                $email_data = $this->SendInvoice($response->referenceId);
                return redirect('/order-confirmed')->with('email_data', $email_data);
            }
        } elseif ($payment_method == 'ssl') {
            $conforder_id = $this->sslc->ssl_success($request);
            if ($conforder_id != false) {
                $update_product = DB::table('conforder')->where('conforder_id', $conforder_id)->update(['conforder_status' => 'Processing']);
                $email_data = $this->SendInvoice($conforder_id);
                return redirect('/order-confirmed')->with('email_data', $email_data);
            }
        } else {
            return 'Unknown payment method';
        }
        return false;
    }

    //payment gateway fail
    public function failure() {
        return redirect('/checkout')->with('payment-msg', 'Your payment failure.');
    }

    //payment gateway cancel
    public function cancel() {
        return redirect('/checkout')->with('payment-msg', 'You cancel.');
    }

    //payment gateway ssl ipn
    public function ipn() {
        mail("ikshimuluits@gmail.com", "IPN SSL", 'IPN');
    }

    public function iPay() {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://demo.ipay.com.bd/api/pg/order",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{
          \n\"amount\" : 120,
          \n\"referenceId\" : \"abdussalam\",
          \n\"description\" : \"Buy x,y,z from XYZ.com\",
          \n\"successCallbackUrl\" : \"https://xyz.com/success/reference/abcd1234\",
          \n\"failureCallbackUrl\" : \"https://xyz.com/failure/reference/abcd1234\",
          \n\"cancelCallbackUrl\" : \"https://xyz.com/cancel/reference/abcd1234\"\n
          }",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer 3db2fcf5-f6f6-4feb-9e71-df081110933e",
                "Content-Type: application/json",
                "Postman-Token: c83eb032-1e5f-4950-94a7-7f1b13c148ee",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

    public function bKashVerification($TransactionId) {
        if ($TransactionId != '') {
            $USER = 'PRIDELIMITED';
            $PASS = 'pR!d31imIt3d';
            $MSISDN = '01990409336';
            $TX_ID = '4FI981401Z';

            $query = 'https://www.bkashcluster.com:9081/dreamwave/merchant/trxcheck/sendmsg'
                    . '?user=' . $USER
                    . '&pass=' . $PASS
                    . '&msisdn=' . $MSISDN
                    . '&trxid=' . $TX_ID;
            //echo "API LINK: <a href='" . $query . "' target='_blank'>" . $query . "</a>";
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_PORT => "9081",
                CURLOPT_URL => $query,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 500,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "accept:application/xml"
                )
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $data = simplexml_load_string($response);
                //print_r($data);
                //$totalamount = 470;
                $trxStatus = $data->transaction->trxStatus;
                //echo '======================='.$trxStatus;;

                if ($trxStatus == '0000') {
                    $trxStatus = $data->transaction->trxStatus;
                    $trxId = $data->transaction->trxId;
                    $receiver = $data->transaction->receiver;
                    $amount = $data->transaction->amount;
                    //&& $amount=$totalamount
                    if ($trxId == $TransactionId && $receiver == '01990409336') {
                        // echo $amount;
                        echo "ok";
                    } else {
                        echo '420';
                    }
                } else if ($trxStatus == '4001') {
                    echo '2'; // duplicate
                } else {
                    echo '420';
                }
            }
        }
    }

    public function GetCityList($region_id) {
        $city = DB::select(DB::raw("SELECT CityId,CityName FROM t_city WHERE RegionId = '$region_id' OR RegionId ='$region_id'"));
        return json_encode($city);
    }

}
