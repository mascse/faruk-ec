<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Coupon;

class CuponController extends Controller
{
	public function __construct() {
        date_default_timezone_set("Asia/Dhaka");
        $this->Coupon = new Coupon();
    }
	
	public function GenerateCoupon(){
		return view('admin.coupon.generate');
	}

	public function StorePromo(Request $request){
		$no_of_coupons = $request->no_of_coupons;
        $coupons = [];
        for ($i = 0; $i < $no_of_coupons; $i++) {
            $temp = $this->generate($request);
            $coupons[] = $temp;
        }
		foreach ($coupons as $key => $value) {
			echo $value."<br>";
		}
	}

    public function generate($request){
		$coupon = '';
		$uppercase    = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'];
        $lowercase    = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
        $numbers      = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $symbols      = ['`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '\\', '|', '/', '[', ']', '{', '}', '"', "'", ';', ':', '<', '>', ',', '.', '?'];
        $characters   = [];
		$characters = array_merge($characters, $lowercase, $uppercase);
		$characters = array_merge($characters, $numbers);
		//$characters = array_merge($characters, $symbols);
		for ($i = 0; $i < $request->length; $i++) {
					$coupon .= $characters[mt_rand(0, count($characters) - 1)];
				}
				return $request->prefix . $coupon . $request->suffix;
			}
	}  

