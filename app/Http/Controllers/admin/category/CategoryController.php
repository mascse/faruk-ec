<?php

namespace App\Http\Controllers\admin\category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Storage;

class CategoryController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
    }

    //
    public function ManageCategory() {
        $main_category_list = DB::table('procat')
                ->join('employe', 'procat.employe_id', '=', 'employe.employe_id')
                ->select('procat.procat_id', 'procat.procat_name', 'procat.procat_lastupdate', 'employe.employe_name')
                ->orderBy('procat_id', 'ASC')
                ->get();
        $data['main_category'] = $main_category_list;
        return view('admin.category.manage_category', $data);
    }

    public function EditProcat($procat_id) {
        $procat = DB::table('procat')->where('procat_id', $procat_id)->first();
        $data['procat'] = $procat;
        return view('admin.category.edit_procat', $data);
    }

    public function UpdateProcat(Request $request) {
        //  dd($request);
        if ($request->hasFile('filebanner')) {
            $file = $request->file('filebanner');
            $originalimage = time() . '_' . $file->getClientOriginalName();
            Storage::put('productbanner/' . $originalimage, file_get_contents($request->file('filebanner')->getRealPath()));
            $updatedata = [
                'procat_name' => $request->procat_name,
                'procat_banner' => $originalimage,
                'employe_id' => 2
            ];
            $result = DB::table('procat')->where('procat_id', $request->procat_id)->update($updatedata);
            if ($result) {
                return redirect()->back()->with('update', 'Procat update successfully');
            } else {
                return redirect()->back()->with('error', 'Procat not update');
            }
        } else {
            $updatedata = [
                'procat_name' => $request->procat_name,
                'employe_id' => 2
            ];
            $result = DB::table('procat')->where('procat_id', $request->procat_id)->update($updatedata);
            if ($result) {
                return redirect()->back()->with('update', 'Procat update successfully');
            } else {
                return redirect()->back()->with('error', 'Procat not update');
            }
        }
    }

    public function AddSubCat() {
        $main_category = DB::table('procat')->get();
        $data['main_category'] = $main_category;
        return view('admin.category.add_subcat', $data);
    }

    public function SaveSubPro(Request $request) {
        if ($request->hasFile('filename')) {
            $file = $request->file('filename');
            $savename = time() . '_' . $file->getClientOriginalName();
            Storage::put('productbanner/subcategory/' . $savename, file_get_contents($request->file('filename')->getRealPath()));
            $data = [
                'procat_id' => $request->procat_id,
                'subprocat_name' => $request->txtsubcategoryname,
                'subprocat_order' => $request->txtorder,
                'subprocat_img' => $savename
            ];
            $result = DB::table('subprocat')->insert($data);
            if ($result) {
                return redirect('/add-subcat')->with('save', 'Subcate insert successfully');
            } else {
                return redirect('/add-subcat')->with('error', 'data not inserted');
            }
        } else {
            return redirect('/add-subcat')->with('error', 'please select image');
        }
    }

    public function ManageSubPro() {
        $main_category = DB::table('procat')->get();
        $sub_pro = DB::table('subprocat')
                ->join('procat', 'procat.procat_id', '=', 'subprocat.procat_id')
                ->select('subprocat.subprocat_name', 'subprocat.subprocat_order', 'subprocat.subprocat_img', 'procat.procat_id', 'subprocat.procat_id', 'procat.procat_name')
                ->get();
        $data['main_category'] = $main_category;
        $data['sub_category'] = $sub_pro;
        return view('admin.category.manage_subcat', $data);
    }

    public function GetProWiseSubpro($id) {
        $subpro = DB::table('subprocat')
                ->join('procat', 'procat.procat_id', '=', 'subprocat.procat_id')
                ->select('subprocat.subprocat_id', 'subprocat.subprocat_name', 'subprocat.subprocat_order', 'subprocat.subprocat_img', 'procat.procat_id', 'subprocat.procat_id', 'procat.procat_name')
                ->where('subprocat.procat_id', $id)
                ->get();
        return json_encode($subpro);
    }

}
