<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Conforder;
use App\Registeruser;
use App\Productsize;
use App\Ordershipping;
use App\Shoppinproduct;
use App\Shoppingcart;
use DB;
use App\Rider;
use Carbon\Carbon;
use Mail;

class ManageOrderController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
    }

    public function manageIncompleteOrder() {
        $total_incomplete_order_info = DB::table('conforder')
                ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                ->leftJoin('riders','conforder.delivery_by','=','riders.id')
                ->select('conforder.conforder_id', 'conforder.conforder_status', 'conforder.conforder_lastupdte','conforder.conforder_placed_date','conforder.created_at','conforder.delivery_by','conforder.conforder_deliverydate', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname','riders.name')
                ->where('conforder_completed', '=', 0)
                ->where('conforder_status', '!=', 'Closed')
                ->where('conforder_status', '!=', 'Cancelled')
                ->where('conforder_status', '!=', 'Exchanged')
                ->where('conforder_status', '!=', 'Invalidate')
                ->where('conforder_status', '!=', 'Returned')
                ->orderBy('conforder.conforder_id', 'desc')
                ->get();
        $data['total_incomplete_order_info'] = $total_incomplete_order_info;
        return view('admin.manage-incomplete-order', $data);
    }

    public function manageAllOrder() {
        $all_order_info = DB::table('conforder')
                ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                ->leftJoin('riders','conforder.delivery_by','=','riders.id')
                ->select('conforder.conforder_id', 'conforder.conforder_status', 'conforder.conforder_lastupdte','conforder.conforder_placed_date','conforder.delivery_by','conforder.conforder_deliverydate', 'conforder.conforder_tracknumber', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname','riders.name')
                ->orderBy('conforder.conforder_id', 'desc')
                ->get();
        $data['form_date']='';
        $data['to_date']='';
        $data['tracking_number']='';
        $data['all_order_info'] = $all_order_info;
        return view('admin.manage-all-order', $data);
    }

    public static function getTotlaIncomplte_order() {
        $total_incomplete_order = DB::table('conforder')
                ->where('conforder_completed', 0)
                ->where('conforder_status', '!=', 'Closed')
                ->where('conforder_status', '!=', 'Cancelled')
                ->where('conforder_status', '!=', 'Exchanged')
                ->where('conforder_status', '!=', 'Invalidate')
                ->where('conforder_status', '!=', 'Returned')
                ->count();

        return $total_incomplete_order;
    }

    public function OrderDetails($order_id) {

        $data['shipping_address_details'] = $this->getShippingAddressDetails($order_id);
        $data['total_incomplete_order_info'] = $this->getTotalOrderInfo($this->getShippingAddressDetails($order_id)->shoppingcart_id);
        $data['riders'] = Rider::all();
        $data['conforder'] = DB::table('conforder')
            ->join('cash_expenses', 'conforder.conforder_id', '=', 'cash_expenses.conforder_id')
            ->select('conforder.delivery_by', 'cash_expenses.amount')
            ->where('conforder.conforder_id', $order_id)
            ->first();
        $data['delivery_by'] = DB::table('conforder')
            ->select('delivery_by')
            ->where('conforder_id', $order_id)
            ->first();
        $data['order_id'] = $order_id;
        return view('admin.order-details', $data);
    }

    public function InvoicePrint($order_id) {
        $data['shipping_address_details'] = $this->getShippingAddressDetails($order_id);
        $data['total_incomplete_order_info'] = $this->getTotalOrderInfo($this->getShippingAddressDetails($order_id)->shoppingcart_id);
        $data['order_id'] = $order_id;
        return view('admin.invoice_print', $data);
    }

    public function UpdateOrderStatus(Request $request) {
        $confoorderid=$request->conforder_id;
        $order_status=$request->ddlstatus;
        $totalamount=$request->total_amount;
        $orderstatusnote=$request->orderstatusnote;
        $data['conforder_status'] = $order_status;
        $data['conforder_statusdetails']=$orderstatusnote;
        $data['updated_at']=Carbon::now();
        $data['delivery_by'] = $request->delivered_by;
        $data['conforder_deliverydate'] = $request->delivery_date;
        $data['order_threepldlv'] = $request->ddlthreepldlv;
        $order_status_result = DB::table("conforder")->where('conforder_id', $confoorderid)->update($data);
        DB::table("registeruser")->where('registeruser_id', $request->registeruser_id)->update(['admin_comment' => $request->admin_comment]);
        $data['conforder'] = DB::table('conforder')
            ->join('cash_expenses', 'conforder.conforder_id', '=', 'cash_expenses.conforder_id')
            ->select('conforder.delivery_by', 'cash_expenses.amount')
            ->where('conforder.conforder_id', $confoorderid)
            ->first();
        $data['delivery_by'] = DB::table('conforder')
            ->select('delivery_by')
            ->where('conforder_id', $confoorderid)
            ->first();
        if ($order_status_result) {
            $shipping_address_details = $this->getShippingAddressDetails($confoorderid);
            $total_incomplete_order_info = $this->getTotalOrderInfo($shipping_address_details->shoppingcart_id);
            if ($order_status == 'Invalidate' || $order_status == 'Returned' || $order_status == 'Cancelled') {
                foreach ($total_incomplete_order_info as $order_info) {
                    $result = $this->getProductInfoByProductSize($order_info->product_id, $order_info->prosize_name, $order_info->productalbum_name);
                    $pro_SizeWiseQty = $result->SizeWiseQty + $order_info->shoppinproduct_quantity;
                    //echo $pro_SizeWiseQty.'==========';exit;
                    
                    $update_qty = $this->update_qty($order_info->product_id, $result->color_name, $result->productsize_size, $pro_SizeWiseQty);
                    if ($update_qty) {
                      //  echo "Success";
                    } else {
                     //   echo "Failed!!";
                    }
                }
            } else {
                
           /* foreach ($total_incomplete_order_info as $order_info) {
                    $result = $this->getProductInfoByProductSize($order_info->product_id, $order_info->prosize_name, $order_info->productalbum_name);
                    $pro_SizeWiseQty = $result->SizeWiseQty - $order_info->shoppinproduct_quantity;
                    $update_qty = $this->update_qty($order_info->product_id, $result->color_name, $result->productsize_size, $pro_SizeWiseQty);
                    if ($update_qty) {
                        echo "Success";
                    } else {
                        echo "Failed!!";
                    }
                } */
            }
        } else {
            
        }
        
        if ($order_status == 'Bkash_Payment_Receive'){
            $e_data=DB::table('conforder')
            ->join('registeruser','conforder.registeruser_id','=','registeruser.registeruser_id')
            ->select('registeruser.registeruser_email','registeruser.registeruser_firstname','registeruser.registeruser_lastname','conforder.conforder_tracknumber')
            ->where('conforder_id',$confoorderid)
            ->first();
            $email_data['registeruser_email']=$e_data->registeruser_email;
            $email_data['registeruser_firstname']=$e_data->registeruser_firstname;
            $email_data['registeruser_lastname']=$e_data->registeruser_lastname;
            $email_data['conforder_tracknumber']=$e_data->conforder_tracknumber;
            Mail::send('emails.bkash_payment', $email_data, function($message) use($email_data) {
            $message->from('admin@pride-limited.com','Pride Limited');
            $message->to($email_data['registeruser_email']);
            $message->bcc('pride.orderlog@gmail.com', 'Pride Limited');
            $message->subject('bKash Payment Comfirmation');
            });
         }
         
        $tplorderid=$request->ddlthreepldlv;
        if ($tplorderid == 'Pathao') {
        $get_pathao_check=DB::table('conforder')->select('pathao_check')->where('conforder_id', $confoorderid)->first();
        $pathao_check=$get_pathao_check->pathao_check;
        //echo $pathao_check;exit;
        if($pathao_check!=1){
        $tplorderid=$request->ddlthreepldlv;
        $p_data['order_threepldlv'] = $tplorderid;
        $p_data['pathao_check'] = 1;
        $tplstatus_result = DB::table('conforder')->where('conforder_id', $confoorderid)->update($p_data);
        $shipping_address_details = $this->getShippingAddressDetails($confoorderid);
       // $data['order_threepldlv'] = $tplorderid;
     //   $tplstatus_result = DB::table('conforder')->where('conforder_id', $confoorderid)->update($data);
            $curl = curl_init();
            $token_postdata = [
                'client_id' => 'a853162dbc0b3d4a1cd0c63fd8f75073',
                'client_secret' => '9d5e877c280325a270291a3da60eb924',
                'username' => 'farwah_tasnim@pride-grp.com',
                'password' => 'asdf7890',
                'scope' => 'create_user_delivery',
                'grant_type' => 'password'
            ];
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.pathao.com/v1/oauth/access_token", //api url call here
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($token_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json"
                ),
            ));

            $token_response = curl_exec($curl);
            $result = json_decode($token_response);
            $access_token = $result->access_token;
            $delivery_postdata = [
                'receiver_name' => $shipping_address_details->Shipping_txtfirstname . ' ' . $shipping_address_details->Shipping_txtlastname,
                'receiver_address' => $shipping_address_details->Shipping_txtaddressname,
                'receiver_number' => $shipping_address_details->Shipping_txtphone,
                'recipient_email' => 'ikshimuluits@gmail.com',
                'cost' => $totalamount,
                'instructions' => $shipping_address_details->conforder_tracknumber,
                'package_description' => 'Medium Packet'
            ];

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.pathao.com/v1/me/deliveries",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($delivery_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "Authorization: Bearer " . $access_token
                ),
            ));

            $delivery_response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                 return redirect("/pride-admin/order-details/$confoorderid")->with('update', 'status  updated');
            }
        }
		}elseif($tplorderid == 'eCourier'){
			$curl = curl_init();
			$delivery_postdata = [
                'parcel' => 'insert',
                'recipient_name' => 'inzamamul',
                'recipient_mobile' => '01722072901',
                'recipient_city' => 'Dhaka',
				'recipient_area' => 'Mirpur',
                'recipient_address' => 'Dhokhin bishil',
                'package_code' => '#2426',
                'product_price' => 1500,
				'payment_method' => 'COD',
            ];
			
			curl_setopt_array($curl, array(
                CURLOPT_URL => "http://ecourier.com.bd/apiv2/",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $delivery_postdata,
                CURLOPT_HTTPHEADER => array(
                    "API_SECRET:zReJG",
                    "API_KEY: 66eY",
                    "USER_ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			$delivery_response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
				 dd($delivery_response);
			}
			
			//echo 'eCourier';
		} else {
            return redirect("/pride-admin/order-details/$confoorderid")->with('update', 'status updated');
        }
		
       // return redirect("/pride-admin/order-details/$confoorderid")->with('update', 'status updated');
    }

    public function UpdateTPLStatus(Request $request) {
        $tplorderid=$request->ddlthreepldlv;
        $data['order_threepldlv'] = $tplorderid;
        $tplstatus_result = DB::table('conforder')->where('conforder_id', $confoorderid)->update($data);
        $shipping_address_details = $this->getShippingAddressDetails($confoorderid);
        $data['order_threepldlv'] = $tplorderid;
        $tplstatus_result = DB::table('conforder')->where('conforder_id', $confoorderid)->update($data);

        if ($tplorderid == 'Pathao') {
            $curl = curl_init();
            $token_postdata = [
                'client_id' => 'a853162dbc0b3d4a1cd0c63fd8f75073',
                'client_secret' => '9d5e877c280325a270291a3da60eb924',
                'username' => 'farwah_tasnim@pride-grp.com',
                'password' => 'asdf7890',
                'scope' => 'create_user_delivery',
                'grant_type' => 'password'
            ];
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.pathao.com/v1/oauth/access_token", //api url call here
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($token_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json"
                ),
            ));

            $token_response = curl_exec($curl);
            $result = json_decode($token_response);
            $access_token = $result->access_token;
            $delivery_postdata = [
                'receiver_name' => $shipping_address_details->Shipping_txtfirstname . ' ' . $shipping_address_details->Shipping_txtlastname,
                'receiver_address' => $shipping_address_details->Shipping_txtaddressname,
                'receiver_number' => $shipping_address_details->Shipping_txtphone,
                'recipient_email' => 'salam.pustcse@gmail.com',
                'cost' => $totalamount,
                'instructions' => $shipping_address_details->conforder_tracknumber,
                'package_description' => 'Medium Packet'
            ];

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.pathao.com/v1/me/deliveries",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($delivery_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "Authorization: Bearer " . $access_token
                ),
            ));

            $delivery_response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                
            }
        } else {
            echo 0;
        }
    }

    public function CanceIndivisuallQty($product_id, $ProColor, $ProSize, $ProQty, $shoppinproduct_id) {
        $get_shoppingcart_id=DB::table('shoppinproduct')->where('shoppinproduct_id', $shoppinproduct_id)->first();
        $shopping_cart_id=$get_shoppingcart_id->shoppingcart_id;
        $dmselect=$get_shoppingcart_id->deliveryMethod;
        $data['shoppinproduct_quantity'] = 0;
        $update_result = DB::table("shoppinproduct")->where('shoppinproduct_id', $shoppinproduct_id)->where('productalbum_name', $ProColor)->where('prosize_name', $ProSize)->update($data);
        if ($update_result) {
            
             $subtotal_sql = DB::select(DB::raw("select SUM(product_price*shoppinproduct_quantity)as total from shoppinproduct where shoppingcart_id='$shopping_cart_id' and shoppinproduct_quantity!=0"));
              foreach ($subtotal_sql as $cart_update_total) {
                         $subtotal = $cart_update_total->total;
             }
             
            $product_info = DB::table('productsize')
                    ->select('productsize.SizeWiseQty')
                    ->where('productsize.product_id', '=', $product_id)
                    ->where('productsize.productsize_size', '=', $ProSize)
                    ->where('productsize.color_name', '=', $ProColor)
                    ->first();

            $data2['SizeWiseQty'] = $ProQty + $product_info->SizeWiseQty;
            $update_Prosize_result = DB::table("productsize")->where('product_id', $product_id)->where('color_name', $ProColor)->where('productsize_size', $ProSize)->update($data2);
            
            if($subtotal < 3000){
                if($dmselect=='cs'){
                   $shiping_charge=70; 
                }elseif($dmselect=='bk'){
                    $shiping_charge=150; 
                }
                
            }else{
                $shiping_charge=0;
            }
            $update_Shipping_Charge['Shipping_Charge']=$shiping_charge;
            DB::table("shoppinproduct")->where('shoppingcart_id', $shopping_cart_id)->update($update_Shipping_Charge);
            
            if ($update_Prosize_result) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    /* public function CanceIndivisuallQty($product_id,$ProColor,$ProSize,$ProQty,$shoppinproduct_id){

      //$data['productalbum_name'] = $ProColor;
      //$data['prosize_name'] = $ProSize;

      $data['shoppinproduct_quantity'] = 0;
      $update_result = DB::table("shoppinproduct")->where('shoppinproduct_id',$shoppinproduct_id)->update($data);
      if($update_result){
      echo 1;
      }else{
      echo 0;
      }

      }

      $cancle_shoppinproduct_id=$_POST['shoppinproduct_id'];
      $cancle_product_id=$_POST['product_id'];
      $cancle_color=$_POST['pre_color'];
      $cancle_size=$_POST['pre_size'];
      $quantity=$_POST['qty'];
      $cancle_quantity=0;
      $sqlcancleupdate = "UPDATE shoppinproduct SET productalbum_name='$cancle_color', prosize_name ='$cancle_size',shoppinproduct_quantity='$cancle_quantity' WHERE shoppinproduct_id='$cancle_shoppinproduct_id'";
      $cancleupdate = mysql_query($sqlcancleupdate, $con) or die("Cannot Update: " . mysql_error());
      if($cancleupdate){
      $pre_change_sql="select productsize_size,SizeWiseQty,color_name,product_id  FROM productsize WHERE product_id='$cancle_product_id'
      AND color_name='$cancle_color' AND productsize_size='$cancle_size'";
      $pre_change_res=mysql_query($pre_change_sql,$con);
      $pre_result=mysql_fetch_assoc($pre_change_res);
      $pre_product_id= $pre_result['product_id'];
      $pre_productsize_size = $pre_result['productsize_size'];
      $pre_color_name = $pre_result['color_name'];
      $pre_SizeWiseQty = $pre_result['SizeWiseQty'];
      $pre_new_qty =$pre_SizeWiseQty + $quantity;
      $pre_sql = "UPDATE productsize SET SizeWiseQty='$pre_new_qty' WHERE product_id='$pre_product_id' AND color_name='$pre_color_name' AND productsize_size='$pre_productsize_size'";
      $pre_res = mysql_query($pre_sql,$con) or die("<br>Error: ".mysql_error($con));
      }

     */


    /* public function CanceIndivisuallQty($product_id, $ProColor, $ProSize, $ProQty, $shoppinproduct_id) {

      //$data['productalbum_name'] = $ProColor;
      //$data['prosize_name'] = $ProSize;

      $data['shoppinproduct_quantity'] = 0;
      $update_result = DB::table("shoppinproduct")->where('shoppinproduct_id', $shoppinproduct_id)->update($data);
      if ($update_result) {
      echo 1;
      } else {
      echo 0;
      }
      } */


    /*
      $cancle_shoppinproduct_id=$_POST['shoppinproduct_id'];
      $cancle_product_id=$_POST['product_id'];
      $cancle_color=$_POST['pre_color'];
      $cancle_size=$_POST['pre_size'];
      $quantity=$_POST['qty'];
      $cancle_quantity=0;
      $sqlcancleupdate = "UPDATE shoppinproduct SET productalbum_name='$cancle_color', prosize_name ='$cancle_size',shoppinproduct_quantity='$cancle_quantity' WHERE shoppinproduct_id='$cancle_shoppinproduct_id'";
      $cancleupdate = mysql_query($sqlcancleupdate, $con) or die("Cannot Update: " . mysql_error());
      if($cancleupdate){
      $pre_change_sql="select productsize_size,SizeWiseQty,color_name,product_id  FROM productsize WHERE product_id='$cancle_product_id'
      AND color_name='$cancle_color' AND productsize_size='$cancle_size'";
      $pre_change_res=mysql_query($pre_change_sql,$con);
      $pre_result=mysql_fetch_assoc($pre_change_res);
      $pre_product_id= $pre_result['product_id'];
      $pre_productsize_size = $pre_result['productsize_size'];
      $pre_color_name = $pre_result['color_name'];
      $pre_SizeWiseQty = $pre_result['SizeWiseQty'];
      $pre_new_qty =$pre_SizeWiseQty + $quantity;
      $pre_sql = "UPDATE productsize SET SizeWiseQty='$pre_new_qty' WHERE product_id='$pre_product_id' AND color_name='$pre_color_name' AND productsize_size='$pre_productsize_size'";
      $pre_res = mysql_query($pre_sql,$con) or die("<br>Error: ".mysql_error($con));
      }

     */

    /* Function Description here */

    public function getShippingAddressDetails($order_id) {
        $shipping_address_details = DB::table('conforder')
                ->select('conforder.conforder_id','conforder.employee_status', 'conforder.conforder_tracknumber', 'conforder.conforder_completed', 'conforder.conforder_status', 'conforder.order_threepldlv', 'conforder.conforder_deliverynotes','conforder.conforder_statusdetails', 'conforder.conforder_placed_date','conforder.created_at', 'conforder.conforder_deliverydate', 'shoppingcart.shoppingcart_id','shoppingcart.used_promo','shoppingcart.get_offer','shoppingcart.shoppingcart_total', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname', 'ordershipping.Shipping_txtaddressname', 'ordershipping.Shipping_txtcity', 'ordershipping.Shipping_txtzipcode', 'ordershipping.Shipping_txtphone', 'shoppingcart.shoppingcart_id', 'ordershipping.ordershipping_id', 't_city.CityId', 't_city.CityName', 'shoppinproduct.shoppinproduct_id')
                ->join('shoppingcart', 'conforder.shoppingcart_id', '=', 'shoppingcart.shoppingcart_id')
                ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', '=', 'shoppinproduct.shoppingcart_id')
                ->join('t_city', 'shoppinproduct.shipping_area', '=', 't_city.CityId')
               // ->join('registeruser', 'ordershipping.registeruser_id', '=', 'registeruser.registeruser_id')
                ->where('conforder.conforder_id', '=', $order_id)
                ->first();
        return $shipping_address_details;
    }

    public function getTotalOrderInfo($shoppingcartid) {

        $total_incomplete_order_info = DB::table('product')
                ->join('shoppinproduct', 'product.product_id', '=', 'shoppinproduct.product_id')
                ->select('product.product_name', 'product.product_img_thm','product.product_id', 'product.product_styleref', 'shoppinproduct.shoppinproduct_id', 'shoppinproduct.shoppinproduct_quantity', 'shoppinproduct.prosize_name', 'shoppinproduct.cart_image', 'shoppinproduct.productalbum_name', 'shoppinproduct.Shipping_Charge','shoppinproduct.shipping_area','shoppinproduct.deliveryMethod', 'shoppinproduct.product_price', 'shoppinproduct.product_id', 'shoppinproduct.productalbum_name')
                ->where('shoppinproduct.shoppingcart_id', '=', $shoppingcartid)
                ->get();
        return $total_incomplete_order_info;
    }

    public function getProductInfoByProductSize($product_id, $prosize, $color) {
        $product_info = DB::table('productsize')
                ->select('productsize.color_name', 'productsize.SizeWiseQty', 'productsize.productsize_size')
                ->where('productsize.product_id', '=', $product_id)
                ->where('productsize.productsize_size', '=', $prosize)
                ->where('productsize.color_name', '=', $color)
                ->first();
        return $product_info;
    }

    public function update_qty($product_id, $pro_color, $prosize, $pro_SizeWiseQty) {
        $data['SizeWiseQty'] = $pro_SizeWiseQty;
        $update_result = DB::table('productsize')->where('product_id', $product_id)->where('color_name', $pro_color)->where('productsize_size', $prosize)->update($data);
        if ($update_result) {
            return 1;
        } else {
            return 0;
        }
    }

    //SELECT productalbum_id, productalbum_name FROM productalbum WHERE product_id='$pid' ORDER BY productalbum_id ASC
    public static function GetColorListByProductId($product_id) {
        $color_list = DB::table('productsize')
                ->select('productsize.color_name')
                ->where('productsize.product_id', '=', $product_id)
                ->get();
        return $color_list;
    }

    public static function GetSizeListByProductId($product_id, $productalbum_name) {
        $Size_list = DB::table('productsize')
                ->select('productsize.productsize_size')
                ->where('productsize.product_id', '=', $product_id)
                ->where('productsize.color_name', '=', $productalbum_name)
                ->get();
        return $Size_list;
    }

    public static function GetQtyProductId($product_id, $productalbum_name, $prosize_name) {
        $qtys = DB::table('productsize')
                ->where('productsize.product_id', '=', $product_id)
                ->where('productsize.color_name', '=', $productalbum_name)
                ->where('productsize.productsize_size', '=', $prosize_name)
                ->sum('productsize.SizeWiseQty');
        return $qtys;
    }

    public static function GetShippingDestinationName($city_id) {
		if (is_numeric($city_id)) {
		   $city_name = DB::table('t_city')->where('CityId', $city_id)->first();
          $name=$city_name->CityName;
        return $name;
		} else {
		   return $city_id;
		}
        
    }

    public function ExchangeUpdateSave(Request $request) {
        $product_id = $request->product_id;
        $shoppinproduct_id = $request->shoppinproduct_id;
        $shoppinproduct['productalbum_name'] = $request->new_productalbum_name;
        $shoppinproduct['prosize_name'] = $request->new_prosize_name;
        $shoppinproduct['shoppinproduct_quantity'] = $request->new_shoppinproduct_quantity;
        //UPDATE shoppinproduct SET productalbum_name='$color', prosize_name ='$size',shoppinproduct_quantity='$quantity' WHERE shoppinproduct_id='$exchange_shoppinproduct_id'
        $exchange = DB::table('shoppinproduct')->where('shoppinproduct_id', $shoppinproduct_id)->update($shoppinproduct);
        if ($exchange) {
            //select productsize_size,SizeWiseQty,color_name,product_id  FROM productsize WHERE product_id='$product_id_exchange'
            //  AND color_name='$precolor' AND productsize_size='$presize'
            $productalbum_name = $request->productalbum_name;
            $productsize_size = $request->prosize_name;
            $pre_change = DB::table('productsize')
                    ->where('product_id', $product_id)
                    ->where('color_name', $productalbum_name)
                    ->where('productsize_size', $productsize_size)
                    ->first();
            $shoppinproduct_quantity = $request->shoppinproduct_quantity;
            $product_id = $pre_change->product_id;
            $pre_productsize_size = $pre_change->productsize_size;
            $pre_color_name = $pre_change->color_name;
            $pre_SizeWiseQty = $pre_change->SizeWiseQty;
            $update_qty = $pre_SizeWiseQty + $shoppinproduct_quantity;
            $pre_change_update_data['SizeWiseQty'] = $update_qty;
            //UPDATE productsize SET SizeWiseQty='$pre_new_qty' WHERE product_id='$pre_product_id' AND color_name='$pre_color_name' AND productsize_size='$pre_productsize_size'
            $pre_change_update = DB::table('productsize')
                    ->where('product_id', $product_id)
                    ->where('color_name', $pre_color_name)
                    ->where('productsize_size', $pre_productsize_size)
                    ->update($pre_change_update_data);
            if ($pre_change_update) {
                $new_change = DB::table('productsize')
                        ->where('product_id', $product_id)
                        ->where('color_name', $productalbum_name)
                        ->where('productsize_size', $productsize_size)
                        ->first();
                $new_shoppinproduct_quantity = $request->new_shoppinproduct_quantity;
                $product_id = $new_change->product_id;
                $new_productsize_size = $new_change->productsize_size;
                $new_color_name = $new_change->color_name;
                $new_SizeWiseQty = $new_change->SizeWiseQty;
                $new_update_qty = $new_SizeWiseQty - $new_shoppinproduct_quantity;
                $new_change_update_data['SizeWiseQty'] = $new_update_qty;
                $new_change_update = DB::table('productsize')
                        ->where('product_id', $product_id)
                        ->where('color_name', $new_color_name)
                        ->where('productsize_size', $new_productsize_size)
                        ->update($new_change_update_data);
                if ($new_change_update) {
                    return redirect()->back()->with('update', 'Exchange success');
                } else {
                    return redirect()->back()->with('update', 'Exchange not success');
                }
            }
            
        }
         return redirect()->back()->with('update', 'Exchange not success');
    }
    
    public function updateAddress(Request $request) {
        DB::table('ordershipping')->where('ordershipping_id', $request->ordershipping_id)->update([
            'Shipping_txtfirstname' => $request->firstname,
            'Shipping_txtlastname' => $request->lastname,
            'Shipping_txtaddressname' => $request->address,
            'Shipping_txtcity' => $request->city,
            'Shipping_txtzipcode' => $request->zip,
            'Shipping_txtphone' => $request->phone
        ]);
        /*
        DB::table('t_city')
                ->where('CityId', $request->destination_id)
                ->update(['CityName' => $request->destination]);
        */
    }
    
   public function OrderFiltering(){
       $order_status = Input::get('order_status');
       if($order_status !=''){
           $all_order_info = DB::table('conforder')
                 ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                 ->leftJoin('riders','conforder.delivery_by','=','riders.id')
                ->select('conforder.conforder_id', 'conforder.conforder_status', 'conforder.conforder_lastupdte','conforder.conforder_placed_date','conforder.delivery_by','conforder.conforder_deliverydate', 'conforder.conforder_tracknumber', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname','riders.name')
                ->orderBy('conforder.conforder_deliverydate', 'ASC')
                ->where('conforder.conforder_status',$order_status)
                ->get();
        $data['all_order_info'] = $all_order_info;
        $data['filter_by']=$order_status;
       } else {
            $all_order_info = DB::table('conforder')
                 ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                 ->leftJoin('riders','conforder.delivery_by','=','riders.id')
                ->select('conforder.conforder_id', 'conforder.conforder_status', 'conforder.conforder_lastupdte','conforder.conforder_placed_date','conforder.delivery_by','conforder.conforder_deliverydate', 'conforder.conforder_tracknumber', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname','riders.name')
                ->orderBy('conforder.conforder_id', 'DESC')
                ->where('conforder.conforder_status','Order_Verification_Pending')
                ->get();
           $data['all_order_info'] = $all_order_info;
           $data['filter_by']='Order_Verification_Pending';
       }
       return view('admin.order_management.order_filter',$data);
   }
   
   public function updateCityInProductDetails(Request $request) {
       
        DB::table('shoppinproduct')
                ->where('shoppingcart_id', $request->shoppingcart_id)
                ->update([
                    'shipping_area' => $request->route_city
                ]);
        return $request->shippingcart_id;
   }
   public function orderLog() {
        $data['order_log'] = DB::select(DB::raw("SELECT shoppinproduct.shoppinproduct_id,conforder.conforder_id,conforder.conforder_status,conforder.conforder_placed_date,conforder.conforder_tracknumber,conforder.conforder_deliverydate,ordershipping.Shipping_txtfirstname,ordershipping.Shipping_txtlastname,ordershipping.Shipping_txtaddress1,ordershipping.Shipping_txtaddress2,ordershipping.Shipping_txtphone,product.product_name,product.product_styleref,shoppinproduct.productalbum_name,shoppinproduct.prosize_name,shoppinproduct.shoppinproduct_quantity,shoppinproduct.Shipping_Charge,product.product_price,product.product_pricediscounted,riders.name,shoppinproduct.dmselect
            FROM conforder
            LEFT JOIN ordershipping
            ON conforder.conforder_id=ordershipping.conforder_id
            LEFT JOIN shoppingcart
            ON conforder.shoppingcart_id=shoppingcart.shoppingcart_id
            LEFT JOIN shoppinproduct
            ON shoppingcart.shoppingcart_id=shoppinproduct.shoppingcart_id
            LEFT JOIN product
            ON shoppinproduct.product_id=product.product_id
            LEFT JOIN riders
            ON conforder.delivery_by=riders.id
            WHERE conforder.conforder_status !='Invalidate' AND conforder.conforder_status !='Cancelled' AND conforder.conforder_status !='Order_Verification_Pending'
            GROUP BY shoppinproduct.shoppinproduct_id ORDER BY shoppinproduct.shoppinproduct_id DESC"));
       return view('admin.order_management.order_log',$data);
   }

}