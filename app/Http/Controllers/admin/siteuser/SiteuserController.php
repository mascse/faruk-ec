<?php

namespace App\Http\Controllers\admin\siteuser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SiteuserController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
    }

    public function SiteUserList() {
        $data['site_user'] = DB::table('users')
                ->join('registeruser', 'users.id', '=', 'registeruser.registeruser_login_id')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('users.id', 'registeruser.registeruser_id', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname', 'registeruser.registeruser_email', 'registeruser.created_at', 'registeruserdetails.registeruser_phone', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_address1', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode')
                ->where('registeruser.registeruser_active', 1)
                ->groupBy('users.id')
                ->orderBy('registeruser.registeruser_id', 'DESC')
                ->get();
        return view('admin.siteuser.site_user_list', $data);
    }

    public function DeleteUser($user_id) {
        $data['registeruser_active'] = 0;
        DB::table('registeruser')->where('registeruser_id', $user_id)->update($data);
        return redirect()->back()->with('success', 'User deleted!');
    }

}
