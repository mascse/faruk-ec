<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ReportController extends Controller
{
    public static function getOfferFlag($shopping_cart_id) {
        $offer=DB::table('shoppingcart')
        ->select('get_offer')
        ->where('shoppingcart_id',$shopping_cart_id)
        ->first();
        return $offer;
        
    }
    public function saleReport(Request $request) {
        $query_scope = DB::table('conforder')
                ->join('shoppingcart', 'conforder.shoppingcart_id', '=', 'shoppingcart.shoppingcart_id')
                ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', 'shoppinproduct.shoppingcart_id', '=')
                ->join('registeruser', 'conforder.registeruser_id', '=', 'registeruser.registeruser_id')
                ->select('conforder.conforder_tracknumber', 'conforder.conforder_placed_date', 'registeruser.registeruser_firstname', 'registeruser_lastname', 'shoppinproduct.Shipping_Charge','shoppingcart.get_offer', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total', 'shoppingcart.shoppingcart_total')
                ->where('conforder.conforder_status','=', 'Closed');
        if(isset($request->from) && isset($request->to)) {
            $from = $request->from;
            $to = $request->to;
            $sale_reports = $query_scope
                ->whereBetween('conforder.conforder_placed_date', array($from, $to))
                ->orderBy('conforder.conforder_placed_date', 'desc')
                ->get();
            return view('admin.sale_report', compact('sale_reports', 'from', 'to'));
        } elseif(isset($request->from)) {
            $from = $request->from;
            $sale_reports = $query_scope
                ->where('conforder.conforder_placed_date', '>=', $from)
                ->orderBy('conforder.conforder_placed_date', 'desc')
                ->get();
            return view('admin.sale_report', compact('sale_reports', 'from', 'to'));
        } else {
            $sale_reports = $query_scope
                ->orderBy('conforder.conforder_placed_date', 'desc')
                ->get();
            return view('admin.sale_report', compact('sale_reports'));
        }
        
        
    }
}
