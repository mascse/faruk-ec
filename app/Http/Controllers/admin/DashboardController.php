<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Conforder;
use App\Registeruser;
use App\Productsize;
use App\Browser;
use DB;

class DashboardController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
    }

    public function index() {
        $total_incomplete_order = DB::table('conforder')
                ->where('conforder_completed', 0)
                ->where('conforder_status', '!=', 'Closed')
                ->where('conforder_status', '!=', 'Cancelled')
                ->where('conforder_status', '!=', 'Exchanged')
                ->where('conforder_status', '!=', 'Invalidate')
                ->where('conforder_status', '!=', 'Returned')
                ->count();
        $totalorder = DB::table('conforder')
                ->where('conforder_completed', 0)
                ->where('conforder_status', '!=', 'Invalidate')
                ->count();
        $totalregisteruser = DB::table('registeruser')
                ->count();
        $totalnumberoflistings = DB::table('product')
                ->where('product_active_deactive', 0)
                ->count();
        $totalcompleteorder = DB::table('conforder')
                ->where('conforder_status', '=', 'Closed')
                ->count();
        $gettotalbounceorder = DB::table('conforder')
                ->where('conforder_status', '=', 'Cancelled')
                ->count();
        $totalbouncedorderrate = ($gettotalbounceorder * 100) / $totalorder;
        $total_qty = DB::table('productsize')
                ->sum('SizeWiseQty');
        $total_incomplete_order_info = DB::table('conforder')
                ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                ->select('conforder.conforder_id', 'conforder.conforder_status', 'conforder.conforder_lastupdte','conforder.conforder_placed_date', 'conforder.conforder_tracknumber', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname')
                ->where('conforder_completed', '=', 0)
                ->where('conforder_status', '!=', 'Closed')
                ->where('conforder_status', '!=', 'Cancelled')
                ->where('conforder_status', '!=', 'Exchanged')
                ->where('conforder_status', '!=', 'Invalidate')
                ->where('conforder_status', '!=', 'Returned')
                ->orderBy('conforder.conforder_id', 'desc')
                ->get();
        // dd($total_incomplete_order_info);
        //dd($series);
        
        //Amount of sold product
        $total_sale_product = DB::table('shoppinproduct')
                ->join('conforder', 'shoppinproduct.shoppingcart_id', '=', 'conforder.shoppingcart_id')
                ->where('conforder.conforder_status', 'Closed')
                ->orWhere('conforder.conforder_status', 'Exchanged')
                ->sum('shoppinproduct_quantity');
                
        $data['total_sale_product'] = $total_sale_product;
        $data['total_incomplete_order'] = $total_incomplete_order;
        $data['totalorder'] = $totalorder;
        $data['totalregisteruser'] = $totalregisteruser;
        $data['totalnumberoflistings'] = $totalnumberoflistings;
        $data['totalcompleteorder'] = $totalcompleteorder;
        $data['totalbounceorder'] = $gettotalbounceorder;
        $data['totalbouncedorderrate'] = $totalbouncedorderrate;
        $data['total_qty'] = $total_qty;
        $data['total_incomplete_order_info'] = $total_incomplete_order_info;
        //$data['browser_data_count'] = $browser_data_count;
        //$data['browser_name'] = $browser_name;
        //$data['series'] = $series;
        return view('admin.dashboard', $data);
    }
    
    public function SiteUserList() {
        $data['site_user'] = DB::table('users')
                ->join('registeruser', 'users.id', '=', 'registeruser.registeruser_login_id')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('users.id', 'registeruser.registeruser_id', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname', 'registeruser.registeruser_email', 'registeruser.created_at', 'registeruserdetails.registeruser_phone', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_address1', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode')
                ->groupBy('users.id')
                ->orderBy('registeruser.registeruser_id', 'DESC')
                ->get();
        return view('admin.siteuser.site_user_list', $data);
    }

    public function DeleteUser($user_id) {
        $data['registeruser_active'] = 0;
        DB::table('registeruser')->where('registeruser_id', $user_id)->update($data);
        return redirect()->back()->with('success', 'User deleted!');
    }
    
    public function EditSiteUser($site_user_id){
        $data['site_user'] = DB::table('users')
                ->join('registeruser', 'users.id', '=', 'registeruser.registeruser_login_id')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('users.id', 'registeruser.registeruser_id', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname', 'registeruser.registeruser_email', 'registeruser.created_at', 'registeruserdetails.registeruser_phone', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_address1', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode')
                ->where('registeruser.registeruser_active', 1)
                ->where('registeruser.registeruser_id',$site_user_id)
                ->first();
        return view('admin.siteuser.edit_site_user', $data);
    }
    
    public function UpdateSiteUser(Request $request){
         $data['registeruser_firstname'] = $request->registeruser_firstname;
         $data['registeruser_lastname'] = $request->registeruser_lastname;
         $data['registeruser_email'] = $request->registeruser_email;
         DB::table('registeruser')
         ->where('registeruser_login_id',$request->registeruser_login_id)
         ->where('registeruser_id',$request->registeruser_id)
         ->update($data);
         $data_userdetails['registeruser_phone'] = $request->registeruser_phone;
         $data_userdetails['registeruser_address'] = $request->registeruser_address;
         $data_userdetails['registeruser_address1'] = $request->registeruser_address1;
         $data_userdetails['registeruser_city'] = $request->registeruser_city;
         $data_userdetails['registeruser_zipcode'] = $request->registeruser_zipcode;
         $data_userdetails['updated_at'] = date('Y-m-d H:i:s');
         $result=DB::table('registeruserdetails')->where('registeruser_id',$request->registeruser_id)->update($data_userdetails);
         if($result){
         return redirect()->back()->with('success', 'User updated');
         }else{
            return redirect()->back()->with('eroor', 'User not updated'); 
         }
    }


}
