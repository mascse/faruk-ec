<?php

namespace App\Http\Controllers\admin\product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;
use Carbon\Carbon;

class ProductController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
    }

    public function ProductAddForm() {
        $value = session('admin_id');
        $procat_list = DB::table('procat')->get();
        $totaladd = DB::table('product')->count();
        $total_size = DB::table('prosize')->get();
        $data['total_product'] = $totaladd;
        $data['procat_list'] = $procat_list;
        $data['avail_size'] = $total_size;
        return view('admin/product/add_product_form', $data);
    }

    public function ProductSave(Request $request) {
		dd($request);
        $created_at = Carbon::now();
        $created_by=session('pride_admin_id');
        $totalnumberofinserteddata = 0;
        $numberofboxedchecked = 0;
        $current_date = date("d-m-Y");
        $vaildation = Validator::make($request->all(), [
                    'txtproductname' => 'required|min:4',
                    'txtprice' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $discount = $request->txtpricediscounted;
            $original_price = $request->txtprice;
            if ($discount >= 1) {
                $discount_price = $original_price - $discount;
                $discount_price = ($discount_price * 100) / $original_price;
            } else {
                $discount_price = 0;
            }
            if ($request->campaign) {
                $specialcollection = $request->campaign;
            } else {
                $specialcollection = 0;
            }
            $data['procat_id'] = $request->ddlprocat;
            $data['subprocat_id'] = $request->ddlprosubcat1;
            $data['product_name'] = $request->txtproductname;
            $data['product_barcode'] = $request->product_barcode;
            $data['product_price'] = $request->txtprice;
            //   $data['discount_product_price'] = $request->txtpricediscounted product_barcode;
            $data['discount_product_price'] = $discount_price;
            $data['product_pricefilter'] = $request->ddlfilter;
            $data['product_insertion_date'] = $current_date;
            $data['product_code'] = $request->txtstyleref;
            $data['product_styleref'] = $request->txtstyleref;
            $data['product_order'] = $request->txtorder;
            $data['product_description'] = $request->txtproductdetails;
            $data['product_care'] = $request->txtproductcare;
            $data['created_at'] = $created_at;
            $data['created_by']=$created_by;
            $data['isSpecial'] = $specialcollection;
            $product_id = DB::table('product')->insertGetId($data);
            $sold['sold']=$product_id;
            DB::table('product')->where('product_id',$product_id)->update($sold);
            if ($product_id) {
                if ($request->hasFile('sizeguide_image')) {
                    $file = $request->file('sizeguide_image');
                    $savename = time() . '_' . $file->getClientOriginalName();
                    //  Storage::put('productbanner/subcategory/' . $savename, file_get_contents($request->file('sizeguide_image')->getRealPath()));
                    $image = Image::make($request->file('sizeguide_image'))->resize(255, 383)->insert('assets/image/logo.png')->save('storage/app/public/prothm/' . $savename);
                    $image_data = [
                        'product_sizeguide_img' => $product_id . '_sizeguide_' . $savename
                    ];
                    DB::table('product')->where('product_id', $product_id)->update($image_data);
                } else {
                    
                }
            }
            $total_grp = $request->total_grp;
            for ($loopcountermain = 1; $loopcountermain <= $total_grp; $loopcountermain++) {
                $txtcolorvalname = "txtcolorname_" . $loopcountermain;
                $color_name_post = "txtcolorname_" . $loopcountermain;
                $txtcolovalue = $request->$color_name_post;
                $colorfilethm = "file_colorthm_" . $loopcountermain;
                $totalnumberofavailabesize = session('numberofavailabesize');
                for ($loopcountersize = 1; $loopcountersize <= $totalnumberofavailabesize; $loopcountersize++) {
                    $checkboxvarname = "size" . $loopcountersize . "_" . $loopcountermain;
                    $checkboxvarvalename = "input_size" . $loopcountersize . "_" . $loopcountermain;
                    if ($request->$checkboxvarname == TRUE) {
                        $productsizaname = $request->$checkboxvarname;
                        $productsizquantity = $request->$checkboxvarvalename;
                        $numberofboxedchecked++;
                        if ($productsizquantity > 0) {
                            
                        } else {
                            $productsizquantity = 0;
                        }
                        $product_size['product_id'] = $product_id;
                        $product_size['productsize_size'] = $productsizaname;
                        $product_size['SizeWiseQty'] = $productsizquantity;
                        $product_size['color_name'] = $txtcolovalue;
                        $productsize_id = DB::table('productsize')->insertGetId($product_size);
                        if ($productsize_id) {
                            $totalnumberofinserteddata++;
                        } else {
                            
                        }
                    } else {
                        
                    }
                }
                if ($totalnumberofinserteddata == $numberofboxedchecked) {
                    $productalbum['product_id'] = $product_id;
                    $productalbum['productalbum_name'] = $txtcolovalue;
                    $productalbum['ProductSizeId'] = 0;
                    $productalbum['productalbum_order']=$loopcountermain;
                    $productalbum_id = DB::table('productalbum')->insertGetId($productalbum);
                    if ($productalbum_id) {
                        if ($request->hasFile($colorfilethm)) {
                            $file = $request->file($colorfilethm);
                            $savename = $productalbum_id . '_album_thm_' . $file->getClientOriginalName();
                            $image = Image::make($request->file($colorfilethm))->save('storage/app/public/pgallery/' . $savename);
                            if ($image) {
                                $productablumimage['productalbum_img'] = $savename;
                                DB::table('productalbum')->where('productalbum_id', $productalbum_id)->update($productablumimage);
                                for ($filecounter = 1; $filecounter <= 6; $filecounter++) {
                                    $imageid = NULL;
                                    $imgfieldname = "file_im" . $filecounter . "_" . $loopcountermain;
                                    if ($request->hasFile($imgfieldname)) {
                                        $file = $request->file($imgfieldname);
                                        $filename = $product_id . "_product_image_" . $filecounter . "_" . $file->getClientOriginalName();
                                        $image = Image::make($request->file($imgfieldname))->resize(1600, 2400)->save('storage/app/public/pgallery/' . $filename);
                                        $productimg['productimg_title'] = '';
                                        $productimg['productalbum_id'] = $productalbum_id;
                                        $productimg['productimg_order'] = '';
                                        $productimg_id = DB::table('productimg')->insertGetId($productimg);
                                        if ($image) {
                                            $width = Image::make($request->file($imgfieldname))->width();
                                            $height = Image::make($request->file($imgfieldname))->height();
                                            if ($width == 1600 && $height == 2400) {
                                                //upload thm file
                                                $filenamethmb = $product_id . "_product_image_" . $filecounter . "_thm_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(200, 256)->save('storage/app/public/pgallery/' . $filenamethmb);
                                                //upload tiny file
                                                $filenametiny = $product_id . "_product_image_" . $filecounter . "_tiny_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(64, 82)->save('storage/app/public/pgallery/' . $filenametiny);
                                                //upload medium file
                                                $filenamemedium = $product_id . "_product_image_" . $filecounter . "_medium_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(370, 474)->save('storage/app/public/pgallery/' . $filenamemedium);
                                                $productimg_update['productimg_img'] = $filename;
                                                $productimg_update['productimg_img_tiny'] = $filenametiny;
                                                $productimg_update['productimg_img_medium'] = $filenamemedium;
                                                $productimg_update['productimg_img_thm'] = $filenamethmb;
                                                $productimg_update['productimg_order']=$filecounter;
                                              //  $product_id
                                                $productimg_query = DB::table('productimg')->where('productimg_id', $productimg_id)->update($productimg_update);
                                                if ($filecounter == 1 && $loopcountermain == 1) {
                                                    $product_update['product_img_big']=$filename;
                                                    $product_update['product_img_thm'] = $filenamemedium;
                                                    DB::table('product')->where('product_id', $product_id)->update($product_update);
                                                } else {
                                                    
                                                }
                                            } else {
                                                DB::table('productimg')->where('productimg_id', $productimg_id)->delete();
                                                DB::table('productalbum')->where('productalbum_id',$productalbum_id)->delete();
                                                $p['product_active_deactive']=1;
                                                $p['trash']=1;
                                                DB::table('product')->where('product_id',$product_id)->update($p);
                                                return redirect()->back()->with('error', 'Image size problem.Upload image size must be equal 1600 X 2400');
                                            }
                                        }
                                    }
                                }
                            } else {
                                
                            }
                        } else {
                            
                        }
                    } else {
                        
                    }
                } else {
                    
                }
            }
            return redirect()->back()->with('save', 'Product upload successfully!');
        }
    }

    public function ViewAllProduct($subpro_id = null) {
        $subpro_id = Input::get('scid');
        $sub_pro = DB::table('subprocat')
                ->join('procat', 'procat.procat_id', '=', 'subprocat.procat_id')
                ->select('subprocat.subprocat_id', 'subprocat.subprocat_name', 'subprocat.subprocat_order', 'subprocat.subprocat_img', 'procat.procat_id', 'subprocat.procat_id', 'procat.procat_name')
                ->get();

     $product_list1 = DB::select(DB::raw("select product.*,subprocat.*,procat.*,product.subprocat_id,product.product_id,product.product_active_deactive,s.productimg_img_tiny from product 
          inner join subprocat on product.subprocat_id = subprocat.subprocat_id inner join procat on product.procat_id = procat.procat_id 
          inner join( select productalbum.product_id,productimg.productimg_img_tiny from productalbum inner join productimg on productalbum.productalbum_id = productimg.productalbum_id order by productimg.productimg_id asc limit 1 ) s 
          on s.product_id = product.product_id where product.subprocat_id='$subpro_id'"));    
                $product_list = DB::table('product')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('procat', 'product.procat_id', '=', 'procat.procat_id')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productimg.productalbum_id', '=', 'productalbum.productalbum_id')
                ->select('product.*','productimg.*', 'subprocat.subprocat_name', 'procat.procat_name')
                ->where('product.subprocat_id', $subpro_id)
                ->where('product.trash', 0)
                ->orderBy('product.product_id', 'DESC')
                ->groupBy('product.product_id')
                ->get();
		$data['sub_pro_selected']=$subpro_id;
        $data['sub_category'] = $sub_pro;
        $data['subpro_id'] = $subpro_id;
        $data['product_list'] = $product_list;
        return view('admin.product.product_manage', $data);
    }
    
    public function ProductInTrash(){
                 $product_list = DB::table('product')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('procat', 'product.procat_id', '=', 'procat.procat_id')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productimg.productalbum_id', '=', 'productalbum.productalbum_id')
                ->join('admin','product.deleted_by','admin.admin_id')
                ->select('product.*','productimg.*', 'subprocat.subprocat_name', 'procat.procat_name','admin.admin_username')
                ->where('product.trash', 1)
                ->orderBy('product.product_id', 'DESC')
                ->groupBy('product.product_id')
                ->get();
        $data['product_list'] = $product_list;
        return view('admin.product.view_trash_product', $data);
    }


    public function EditProduct($product_id) {
        $product_info = DB::table('product')->where('product_id', $product_id)->first();
        $procat_id = $product_info->procat_id;
        $procat_list = DB::table('procat')->get();
        $subprocat_list = DB::table('subprocat')->where('procat_id', $procat_id)->get();
        $totaladd = DB::table('product')->count();
        $data['total_product'] = $totaladd;
        $data['procat_list'] = $procat_list;
        $data['subprocat_list'] = $subprocat_list;
        $data['product_info'] = $product_info;
        return view('admin.product.edit_product', $data);
    }

    public function UpdateProduct(Request $request) {
        if ($request->has('campaign')) {
            $isSpecial = $request->campaign;
        } else {
            $isSpecial = 0;
        }
        $product_id = $request->product_id;
        $data['procat_id'] = $request->ddlprocat;
        $data['subprocat_id'] = $request->ddlprosubcat1;
        $data['product_name'] = $request->txtproductname;
        //$data['product_barcode'] = $request->barcode;
        $data['product_code'] = $request->txtstyleref;
        $data['product_styleref'] = $request->txtstyleref;
        $data['product_price'] = $request->txtprice;
        $data['discount_product_price'] = $request->txtpricediscounted;
        $data['product_pricefilter'] = $request->ddlfilter;
        $data['product_order'] = $request->txtorder;
        $data['product_care'] = $request->txtproductcare;
        $data['product_description'] = $request->txtproductdetails;
        $data['isSpecial'] = $isSpecial;
        DB::table('product')->where('product_id', $product_id)->update($data);
        if ($request->has('sizeguide_image')) {
            $file = $request->file('sizeguide_image');
            $savename = $product_id . '_sizeguide_' . $file->getClientOriginalName();
            $image = Image::make($request->file('sizeguide_image'))->resize(255, 383)->save('storage/app/public/prothm/' . $savename);
            $image_data = [
                'product_sizeguide_img' => $savename
            ];
            DB::table('product')->where('product_id', $product_id)->update($image_data);
        }
        return redirect()->back()->with('update', 'Updated successfully !');
    }

    public function ManageProductAlbum($product_id, $subprocat_id) {
        //SELECT product_name, product_styleref FROM product WHERE product_id='$pid' AND subprocat_id='$spcatid'
        $title = DB::table('product')
                ->select('product_name', 'product_styleref')
                ->where('product_id', $product_id)
                ->where('subprocat_id', $subprocat_id)
                ->first();
        //SELECT * FROM productalbum WHERE product_id='$pid' ORDER BY productalbum_order ASC
        $product_album = DB::table('productalbum')
                ->where('product_id', $product_id)
                ->orderBy('productalbum_order', 'ASC')
                ->get();
        $data['title'] = $title;
        $data['product_id'] = $product_id;
        $data['album'] = $product_album;
        $data['subpro_id'] = $subprocat_id;
        return view('admin.product.manage_album', $data);
    }

    public function ManageProductGallery($album_id, $product_id) {
        //SELECT productalbum_name FROM productalbum WHERE productalbum_id='$albumid' AND product_id='$pid'
        $album_name = DB::table('productalbum')
                ->select('productalbum_name')
                ->where('productalbum_id', $album_id)
                ->first();
        //SELECT product_name FROM product WHERE product_id='$pid'
        $product_name = DB::table('product')
                ->select('product_name')
                ->where('product_id', $product_id)
                ->first();
        $product_img = DB::table('productimg')
                ->where('productalbum_id', $album_id)
                ->orderBy('productimg.productimg_id','ASC')
                ->get();
        $data['albumid'] = $album_id;
        $data['pid'] = $product_id;
        $data['album_name'] = $album_name;
        $data['product_name'] = $product_name;
        $data['product_img'] = $product_img;
        return view('admin.product.manage_gallery', $data);
    }

    public function EditAlbumImage($product_img_id, $product_id, $album_id, $serial) {
        //SELECT productalbum_name FROM productalbum WHERE productalbum_id='$albumid' AND product_id='$pid'
        $album_name = DB::table('productalbum')
                ->select('productalbum_name')
                ->where('productalbum_id', $album_id)
                ->where('product_id', $product_id)
                ->first();
        //SELECT subprocat_id FROM product WHERE product_id='$pid'
        $subpro_id = DB::table('product')
                ->select('subprocat_id')
                ->where('product_id', $product_id)
                ->first();
        //SELECT * FROM productimg WHERE productimg_id='$imgid' order by productimg_id ASC
        $product_image = DB::table('productimg')
                ->where('productimg_id', $product_img_id)
                ->first();
        $product_img_all = DB::table('productimg')
                ->where('productalbum_id', $album_id)
                ->get();
        $data['productimg'] = $product_image;
        $data['subcatid'] = $subpro_id->subprocat_id;
        $data['album_name'] = $album_name->productalbum_name;
        $data['albumid'] = $album_id;
        $data['product_img'] = $product_img_all;
        $data['pid'] = $product_id;
        $data['fid'] = $serial;
        return view('admin.product.edit_album_image', $data);
    }

    public function UpdateAlbumImage(Request $request) {
        //  UPDATE productimg SET productimg_title='".$_POST['txtimagename']."', productalbum_id='$albumid', 
        //productimg_order='".$_POST['txtorder']."' WHERE productimg_id='$imgid'
        $image_id = $request->productimg_id;
        $product_id = $request->product_id;
        $updatedata['productimg_title'] = $request->txtimagename;
        $updatedata['productalbum_id'] = $request->album_id;
        $updatedata['productimg_order'] = $request->txtorder;
        DB::table('productimg')->where('productimg_id', $image_id)->update($updatedata);
        if ($request->hasFile('filename')) {
            $get_productimg = DB::table('productimg')
                    ->where('productimg_id', $image_id)
                    ->first();
            $product_tiny_image = $get_productimg->productimg_img_tiny;
            $product_medium_image = $get_productimg->productimg_img_medium;
            $productimg_img_thm = $get_productimg->productimg_img_thm;
            $productimg_img = $get_productimg->productimg_img;
            Storage::delete("public/pgallery/$product_tiny_image");
            Storage::delete("public/pgallery/$product_medium_image");
            Storage::delete("public/pgallery/$productimg_img_thm");
            Storage::delete("public/pgallery/$productimg_img");
            $file = $request->file('filename');
            $filename = $image_id . "_product_image_" . $file->getClientOriginalName();
            $image = Image::make($request->file('filename'))->resize(1600, 2400)->save('storage/app/public/pgallery/' . $filename);
            $width = Image::make($request->file('filename'))->width();
            $height = Image::make($request->file('filename'))->height();
            if ($width == 1600 && $height == 2400) {
                //upload thm file
                $filenamethmb = $product_id . "_product_image_thm_" . $file->getClientOriginalName();
                $image = Image::make($request->file('filename'))->resize(200, 256)->save('storage/app/public/pgallery/' . $filenamethmb);
                //upload tiny file
                $filenametiny = $product_id . "_product_image_tiny_" . $file->getClientOriginalName();
                $image = Image::make($request->file('filename'))->resize(64, 82)->save('storage/app/public/pgallery/' . $filenametiny);
                //upload medium file
                $filenamemedium = $product_id . "_product_image_medium_" . $file->getClientOriginalName();
                $image = Image::make($request->file('filename'))->resize(370, 474)->save('storage/app/public/pgallery/' . $filenamemedium);
                $productimg_update['productimg_img'] = $filename;
                $productimg_update['productimg_img_tiny'] = $filenametiny;
                $productimg_update['productimg_img_medium'] = $filenamemedium;
                $productimg_update['productimg_img_thm'] = $filenamethmb;
                $productimg_query = DB::table('productimg')->where('productimg_id', $image_id)->update($productimg_update);
                if ($request->fid == 1) {
                    $product_update['product_img_big']=$filename;
                    $product_update['product_img_thm'] = $filenamemedium;
                    DB::table('product')->where('product_id', $product_id)->update($product_update);
                } elseif ($request->fid == 3) {
                   // $product_update['productimg_img_medium'] = $filenamemedium;
                  //  $product_id = $request->product_id;
                  //  DB::table('product')->where('product_id', $product_id)->update($product_update);
                }
            } else {
                return redirect()->back()->with('image_error', 'Image size problem.Upload image size must be equal 1600 X 2400');
            }
            return redirect()->back()->with('success', 'Image edit successfully!');
        }
        //   dd($request);
    }

    public function DeleteAlbumImage($album_id, $pro_id, $productimg_id) {
        $get_productimg = DB::table('productimg')
                ->where('productimg_id', $productimg_id)
                ->first();
        $product_tiny_image = $get_productimg->productimg_img_tiny;
        $product_medium_image = $get_productimg->productimg_img_medium;
        $productimg_img_thm = $get_productimg->productimg_img_thm;
        $productimg_img = $get_productimg->productimg_img;
        Storage::delete("public/pgallery/$product_tiny_image");
        Storage::delete("public/pgallery/$product_medium_image");
        Storage::delete("public/pgallery/$productimg_img_thm");
        Storage::delete("public/pgallery/$productimg_img");
        //DELETE FROM productimg WHERE productimg_id='".$_GET['imgid']."'
        $result = DB::table('productimg')->where('productimg_id', $productimg_id)->delete();
        if ($result) {
            return redirect()->back()->with('success', 'Image deleted successfully!');
        } else {
            return redirect()->back()->with('error', 'Image not deleted!');
        }
    }

    public function ProductActiveDeactive($procat_id, $subpro_id, $product_id, $active_deactive) {
        if ($active_deactive == 1) {
            $data['product_active_deactive'] = 0;
        } else {
            $data['product_active_deactive'] = 1;
        }
        //UPDATE product SET product_active_deactive= $status where product_id = '$pid'
        DB::table('product')->where('product_id', $product_id)->update($data);
        if ($active_deactive == 1) {
            return redirect()->back()->with('update', 'Product active successfully!.');
            $data['product_active_deactive'] = 0;
        } else {
            return redirect()->back()->with('update', 'Product deactive successfully!.');
        }
    }
    
    public function ProductDeleteTrash($product_id, $subpro_id){
        $data['product_active_deactive'] = 1;
        $data['trash'] = 1;
        $data['deleted_by'] =session('pride_admin_id');;
        $data['deleted_at'] = Carbon::now();
        $result = DB::table('product')->where('product_id', $product_id)->update($data);
        return redirect()->back()->with('delete', 'Product delete successfully !.');
    }
    
    public function ProductRestore($product_id, $subpro_id){
        $data['product_active_deactive'] = 0;
        $data['trash'] = 0;
        $result = DB::table('product')->where('product_id', $product_id)->update($data);
        return redirect()->back()->with('update', 'Product restore successfully !.');
    }

    public function ProductDelete($product_id, $subpro_id) {
        $product_table_image = DB::table('product')->where('product_id', $product_id)->first();
        //    Storage::delete("public/prothm/$product_table_image->product_sizeguide_img");
        $product_album_image = DB::table('productalbum')->where('product_id', $product_id)->get();
        foreach ($product_album_image as $album_image) {
            Storage::delete("public/pgallery/$album_image->productalbum_img");
            $album_id = $album_image->productalbum_id;
            //SELECT productimg_img_thm, productimg_img FROM productimg WHERE productalbum_id ='$albumid'
            $productimg = DB::table('productimg')->where('productalbum_id', $album_id)->get();
            foreach ($productimg as $img) {
                //  Storage::delete("public/pgallery/$img->productimg_img_tiny");
                Storage::delete("public/pgallery/$img->productimg_img_thm");
                Storage::delete("public/pgallery/$img->productimg_img_medium");
                Storage::delete("public/pgallery/$img->productimg_img");
            }
            DB::table('productimg')->where('productalbum_id', $album_id)->delete();
            DB::table('productalbum')->where('productalbum_id', $album_id)->delete();
        }
        DB::table('product')->where('product_id', $product_id)->delete();
        return redirect()->back()->with('delete', 'Product delete successfully !.');
    }

    public function AddRelatedProduct($product_id) {
		
		$data['procat_list'] = DB::table('procat')->get();
		$data['product_info'] = DB::table('product')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('procat', 'product.procat_id', '=', 'procat.procat_id')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productimg.productalbum_id', '=', 'productalbum.productalbum_id')
                ->select('product.*','productimg.*', 'subprocat.subprocat_name', 'procat.procat_name')
                ->where('product.product_id', $product_id)
                ->first();
		 $data['related_product'] = DB::table('related_product')
                ->join('product', 'product.product_id', '=', 'related_product.relatedproductid')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('procat', 'product.procat_id', '=', 'procat.procat_id')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productimg.productalbum_id', '=', 'productalbum.productalbum_id')
                ->select('product.*','productimg.*', 'subprocat.subprocat_name', 'procat.procat_name')
                ->where('related_product.productid', $product_id)
                ->groupBy('related_product.relatedproductid')
                ->get();
        return view('admin.product.related_product',$data);
    }
	
	public function AddRelatedProductSave(Request $request)
	{
		$product_id = $request->product_id;
		$related_product_id = $request->related_product_id;
        foreach ($related_product_id as $r_p_id) {
			$data['relatedproductid']=$r_p_id;
			$data['productid']=$product_id;
			DB::table('related_product')->insert($data);
        }
		return redirect()->back()->with('save', 'Related Product add successfully !.');
	}
	
	

}
