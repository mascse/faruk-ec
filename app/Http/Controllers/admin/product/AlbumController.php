<?php

namespace App\Http\Controllers\admin\product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use DB;

class AlbumController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
    }

    //
    public function AddAlbumForm($product_id, $album_id) {
        $total_size = DB::table('prosize')->where('product_id', 0)->get();
        $data['avail_size'] = $total_size;
        // dd($total_size);
        return view('admin.product.add_album_form', $data);
    }

    public function SaveAlbum(Request $request) {
        if ($request->hasFile('filename')) {
            $file = $request->file('filename');
            $file = "file_colorthm_" . $file->getClientOriginalName();
            dd($request);
        }
    }

    public function AddProductStyle($product_id) {
        $total_size = DB::table('prosize')->get();
        $data['avail_size'] = $total_size;
        $data['product_id'] = $product_id;
        return view('admin.product.add_product_style', $data);
    }

    public function SaveProductStyle(Request $request) {

        $totalnumberofinserteddata = 0;
        $numberofboxedchecked = 0;
        $totalnumberofavailabesize = session('numberofavailabesize');
        $product_id = $request->product_id;
        //  $txtcolovalue = $request->txtcolorname;
        $total_grp = $request->total_grp;
        for ($loopcountermain = 1; $loopcountermain <= $total_grp; $loopcountermain++) {
            $txtcolorvalname = "txtcolorname_" . $loopcountermain;
            $color_name_post = "txtcolorname_" . $loopcountermain;
            $txtcolovalue = $request->$color_name_post;
            $colorfilethm = "file_colorthm_" . $loopcountermain;
            for ($loopcountersize = 1; $loopcountersize <= $totalnumberofavailabesize; $loopcountersize++) {
                $checkboxvarname = "size" . $loopcountersize . "_" . $loopcountermain;
                $checkboxvarvalename = "input_size" . $loopcountersize . "_" . $loopcountermain;
                if ($request->$checkboxvarname == TRUE) {
                    $productsizaname = $request->$checkboxvarname;
                    $productsizquantity = $request->$checkboxvarvalename;
                    $numberofboxedchecked++;
                    if ($productsizquantity > 0) {
                        
                    } else {
                        $productsizquantity = 0;
                    }
                    $product_size['product_id'] = $product_id;
                    $product_size['productsize_size'] = $productsizaname;
                    $product_size['SizeWiseQty'] = $productsizquantity;
                    $product_size['color_name'] = $txtcolovalue;
                    $productsize_id = DB::table('productsize')->insertGetId($product_size);
                    if ($productsize_id) {
                        $totalnumberofinserteddata++;
                    } else {
                        
                    }
                } else {
                    
                }
            }
            if ($totalnumberofinserteddata == $numberofboxedchecked) {
                $productalbum['product_id'] = $product_id;
                $productalbum['productalbum_name'] = $txtcolovalue;
                $productalbum['ProductSizeId'] = 0;
               // $productalbum['ProductQty'] = 0;
                $productalbum_id = DB::table('productalbum')->insertGetId($productalbum);
                if ($productalbum_id) {
                    if ($request->hasFile('file_colorthm')) {
                        $file = $request->file('file_colorthm');
                        $savename = $productalbum_id . '_album_thm_' . $file->getClientOriginalName();
                        $image = Image::make($request->file('file_colorthm'))->save('storage/app/public/pgallery/' . $savename);
                        if ($image) {
                            $productablumimage['productalbum_img'] = $savename;
                            DB::table('productalbum')->where('productalbum_id', $productalbum_id)->update($productablumimage);
                            for ($filecounter = 1; $filecounter <= 6; $filecounter++) {
                                $imageid = NULL;
                                echo $imgfieldname = "file_im" . $filecounter;
                                if ($request->hasFile($imgfieldname)) {
                                    $file = $request->file($imgfieldname);
                                    $filename = $product_id . "_product_image_" . $filecounter . "_" . $file->getClientOriginalName();
                                    $image = Image::make($request->file($imgfieldname))->resize(1600, 2400)->save('storage/app/public/pgallery/' . $filename);
                                    $productimg['productimg_title'] = '';
                                    $productimg['productalbum_id'] = $productalbum_id;
                                    $productimg['productimg_order'] = '';
                                    $productimg_id = DB::table('productimg')->insertGetId($productimg);
                                    if ($image) {
                                        $width = Image::make($request->file($imgfieldname))->width();
                                        $height = Image::make($request->file($imgfieldname))->height();
                                        if ($width <= 1600 && $height <= 2400) {
                                            //upload thm file
                                            $filenamethmb = $product_id . "_product_image_" . $filecounter . "_thm_" . $file->getClientOriginalName();
                                            $image = Image::make($request->file($imgfieldname))->resize(200, 256)->save('storage/app/public/pgallery/' . $filenamethmb);
                                            //upload tiny file
                                            $filenametiny = $product_id . "_product_image_" . $filecounter . "_tiny_" . $file->getClientOriginalName();
                                            $image = Image::make($request->file($imgfieldname))->resize(64, 82)->save('storage/app/public/pgallery/' . $filenametiny);
                                            //upload medium file
                                            $filenamemedium = $product_id . "_product_image_" . $filecounter . "_medium_" . $file->getClientOriginalName();
                                            $image = Image::make($request->file($imgfieldname))->resize(370, 474)->save('storage/app/public/pgallery/' . $filenamemedium);
                                            $productimg_update['productimg_img'] = $filename;
                                            $productimg_update['productimg_img_tiny'] = $filenametiny;
                                            $productimg_update['productimg_img_medium'] = $filenamemedium;
                                            $productimg_update['productimg_img_thm'] = $filenamethmb;
                                            $productimg_query = DB::table('productimg')->where('productimg_id', $productimg_id)->update($productimg_update);
                                            if ($filecounter == 1 && $loopcountermain == 1) {
                                                $product_update['product_img_thm'] = $filenamethmb;
                                                DB::table('product')->where('product_id', $product_id)->update($product_update);
                                            } else {
                                                
                                            }
                                        } else {
                                            DB::table('productalbum')->where('productalbum_id',$productalbum_id)->delete();
                                            $result = DB::table('productimg')->where('productimg_id', $productimg_id)->delete();
                                            return redirect()->back()->with('error', 'Image Size Do not match');
                                        }
                                    }
                                }
                            }
                        } else {
                            
                        }
                    } else {
                        
                    }
                } else {
                    
                }
            }
        }
        return redirect()->back()->with('success', 'New style add successfully !.');
    }
    
    public function EditSwatch($productalbum_id,$product_id,$subpro_id){
        $data['album_info']=DB::table('productalbum')->where('productalbum_id',$productalbum_id)->first();
        return view('admin.product.edit_productalbum',$data);
    }
    
    public function UpdateSwatch(Request $request){
        if ($request->hasFile('filename')) {
            $productalbum_id=$request->productalbum_id;
            $file = $request->file('filename');
            $savename = $productalbum_id . 'update_album_thm_' . $file->getClientOriginalName();
            $image = Image::make($request->file('filename'))->save('storage/app/public/pgallery/' . $savename);
            if ($image) {
                $productablumimage['productalbum_name'] = $request->productalbum_name;
                $productablumimage['productalbum_img'] = $savename;
                $productablumimage['productalbum_order'] = $request->productalbum_order;
                $result=DB::table('productalbum')->where('productalbum_id', $productalbum_id)->update($productablumimage);
                if($result){
                    return redirect()->back()->with('success', 'Update album image successfully');
                }else{
                    return redirect()->back()->with('error', 'Not updated');
                }
            }
        }else{
            $productalbum_id=$request->productalbum_id;
            $productablumimage['productalbum_name'] = $request->productalbum_name;
            $productablumimage['productalbum_order'] = $request->productalbum_order;
            $result=DB::table('productalbum')->where('productalbum_id', $productalbum_id)->update($productablumimage);
            if($result){
                return redirect()->back()->with('success', 'Update album image successfully');
            }else{
                return redirect()->back()->with('error', 'Not updated');
            }
        }
    }

}
