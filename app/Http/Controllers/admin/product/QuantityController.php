<?php

namespace App\Http\Controllers\admin\product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class QuantityController extends Controller {

    public function __construct() {
        $this->middleware('AdminAuth');
    }

    public static function GetExpectSize($product_id, $color_name) {
        $prosizes = DB::select("SELECT *  FROM prosize where prosize_name not in(select productsize_size  FROM productsize where product_id='$product_id' and color_name='$color_name')");
        return $prosizes;
    }

    public function UpdateQuantitySearchForm() {
        return view('admin.product_quantity.update_qty_search');
    }

    public function UpdateQuantitySearch(Request $request) {
        $productStyleCode = $request->product_style_code;
        $product_info = DB::table('product')
                ->join('procat', 'product.procat_id', '=', 'procat.procat_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->select('procat.procat_name', 'subprocat.subprocat_name', 'product.product_name', 'product.product_id', 'product.product_styleref')
                ->where('product.product_styleref', $productStyleCode)
                ->first();
        if ($product_info) {
            $product_id = $product_info->product_id;
            $size_qty = DB::table('productsize')->where('product_id', $product_id)->orderBy('productsize_id', 'DESC')->get();
            $color_wise_size = DB::table('productsize')->where('product_id', $product_id)->groupBy('color_name')->get();
            $data['color_wise_prosize'] = $color_wise_size;
            $data['product_info'] = $product_info;
            $data['size_qty'] = $size_qty;
            return view('admin.product_quantity.update_qty', $data);
        } else {
            return redirect()->back()->with('error', 'Product not found');
        }
    }

    public function UpdateQuantitySave(Request $request) {
        $prosize_id = $request->prosize_id;
        foreach ($prosize_id as $prosize) {
            echo $prosize;
        }
    }

    public function UpdateQuantitySaveByAjax($prosize_id, $qty) {
        $data['SizeWiseQty'] = $qty;
        $update = DB::table('productsize')->where('productsize_id', $prosize_id)->update($data);
        if ($update) {
            echo "Product quantity updated successfully!";
        } else {
            echo "Product quantity not updated.";
        }
    }

    public function SaveNewSizeQty(Request $request) {
        $data['product_id'] = $request->product_id;
        $data['color_name'] = $request->color_name;
        $data['productsize_size'] = $request->productsize_size;
        $data['SizeWiseQty'] = $request->SizeWiseQty;
        $productsize = DB::table('productsize')->insert($data);
        if ($productsize) {
            return redirect('/update-quantity-search')->with('success', 'New Size and Quantity Add Successfully!.');
        } else {
            return redirect('/update-quantity-search')->with('error', 'Data not insterted.');
        }
        //  dd($data);
    }

}
