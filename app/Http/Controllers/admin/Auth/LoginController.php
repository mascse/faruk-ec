<?php

namespace App\Http\Controllers\admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Carbon\Carbon;

class LoginController extends Controller {

    public function Login() {
        return view('admin.Auth.login');
    }

    public function LoginCheck(Request $request) {
        $update['admin_ip'] = request()->ip();
        $update['admin_lastlogin'] = Carbon::now()->toDateTimeString();
        $user_name = $request->txtusername;
        $password = md5($request->txtpassword);
        $check = DB::table('admin')
                ->where('admin_username', $user_name)
                ->where('admin_password', $password)
                ->first();
        if ($check) {
            Session::put('pride_admin_id', $check->admin_id);
            Session::put('role_id', $check->role_id);
            Session::put('pride_admin_username', $check->admin_username);
            Session::save();
            DB::table('admin')->where('admin_id', $check->admin_id)->update($update);
            return redirect('/pride-admin');
        } else {
            return redirect()->back()->with('error', 'Invalid user name or password.');
        }
    }

    public function AdminLogout(Request $request) {
        $request->session()->forget('pride_admin_id');
        $request->session()->forget('pride_admin_username');
        return redirect('/pride-login');
    }

}
