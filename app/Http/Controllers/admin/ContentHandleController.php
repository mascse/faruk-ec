<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;
use Carbon\Carbon;

class ContentHandleController extends Controller
{
    
    public function AddSlider(){
        return view('admin.content.add_slider');
    }
    
    public function SaveSlider(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'slider_caption' => 'required',
                    'slider_occasion' => 'required',
                    'slider_order' => 'required|unique:p_slider,slider_order',
                    'slider_image' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            if ($request->hasFile('slider_image')) {
                $width = Image::make($request->file('slider_image'))->width();
                $height = Image::make($request->file('slider_image'))->height();
                if ($width >= 1110 && $height >= 550) {
                $file = $request->file('slider_image');
                $filename = time(). "_" . $file->getClientOriginalName();
                $image = Image::make($request->file('slider_image'))->resize(1110, 550)->save('storage/app/public/slider/' . $filename);
                $last_ip=$request->ip();
                $created_by=session('pride_admin_id');
                $created_at = Carbon::now();
                $data['slider_caption']=$request->slider_caption;
                $data['slider_occasion']=$request->slider_occasion;
                $data['slider_order']=$request->slider_order;
                $data['slider_image']=$filename;
                $data['url_link']=$request->url_link;
                $data['created_at']=$created_at;
                $data['created_by']=$created_by;
                $data['created_ip']=$last_ip;
                $result=DB::table('p_slider')->insert($data);
                if($result){
                     return redirect()->back()->with('save', 'New slider image added.');
                }else{
                    return redirect()->back()->with('error', 'Data not added.'); 
                }
            }else{
               return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 1110px X Height: 550px.New image size Width: $width px X Height: $height px");  
            }
            }else{
               return redirect()->back()->with('error', 'Please select an image.'); 
            }
        }
    }
    
    public function ManageSlider(){
        $data['slider_list']=DB::table('p_slider')
        ->join('admin','admin.admin_id','=','p_slider.created_by')
        ->select('p_slider.*','admin.*')
        ->where('deleted_status',0)
        ->orderBy('slider_id','DESC')
        ->get();
        return view('admin.content.manage_slider',$data);
    }
    
    public function EditSlider($slider_id){
         $data['slider']=DB::table('p_slider')->where('slider_id',$slider_id)->first();
         return view('admin.content.edit_slider',$data);
    }
    
    public function UpdateSlider(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'slider_caption' => 'required',
                    'slider_occasion' => 'required',
                    'slider_order' => 'required',
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            if ($request->hasFile('slider_image')) {
                $width = Image::make($request->file('slider_image'))->width();
                $height = Image::make($request->file('slider_image'))->height();
                if ($width >= 1110 && $height >= 550) {
                $file = $request->file('slider_image');
                $filename = time(). "_" . $file->getClientOriginalName();
                $image = Image::make($request->file('slider_image'))->resize(1110, 550)->save('storage/app/public/slider/' . $filename);
                $last_ip=$request->ip();
                $created_by=session('pride_admin_id');
                $created_at = Carbon::now();
                $slider_id=$request->slider_id;
                $data['slider_caption']=$request->slider_caption;
                $data['slider_occasion']=$request->slider_occasion;
                $data['slider_order']=$request->slider_order;
                $data['url_link']=$request->url_link;
                $data['slider_image']=$filename;
                $data['updated_at']=$created_at;
                $data['updated_by']=$created_by;
                $data['updated_ip']=$last_ip;
                $result=DB::table('p_slider')->where('slider_id',$slider_id)->update($data);
                if($result){
                     return redirect()->back()->with('save', 'Update slider image.');
                }else{
                    return redirect()->back()->with('error', 'Data not updated.'); 
                }
            }else{
               return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 1110px X Height: 550px.New image size Width: $width px X Height: $height px");  
            }
         }else{
            $last_ip=$request->ip();
            $created_by=session('pride_admin_id');
            $created_at = Carbon::now();
            $slider_id=$request->slider_id;
            $data['slider_caption']=$request->slider_caption;
            $data['slider_occasion']=$request->slider_occasion;
            $data['slider_order']=$request->slider_order;
            $data['url_link']=$request->url_link;
            $data['updated_at']=$created_at;
            $data['updated_by']=$created_by;
            $data['updated_ip']=$last_ip;
            $result=DB::table('p_slider')->where('slider_id',$slider_id)->update($data);
              return redirect()->back()->with('save', 'Update slider image.');  
         }
        }
    }
    
    public function DeleteSlider($slider_id){
        $image=DB::table('p_slider')->where('slider_id',$slider_id)->first();
        $slider_image=$image->slider_image;
        Storage::delete("public/slider/$slider_image");
        $result=DB::table('p_slider')->where('slider_id',$slider_id)->delete();
        if($result){
             return redirect()->back()->with('save', 'Slider image deleted.');
        }else{
          return redirect()->back()->with('error', 'Data not deleted.');  
        }
    }
    
    public function ActiveSlider($slider_id){
        $data['slider_status']=1;
        $result=DB::table('p_slider')->where('slider_id',$slider_id)->update($data);
        if($result){
             return redirect()->back()->with('save', 'Slider Actived.');
        }else{
            return redirect()->back()->with('error', 'Data not updated.'); 
        }
    }
    
    public function DeactiveSlider($slider_id){
        $data['slider_status']=0;
        $result=DB::table('p_slider')->where('slider_id',$slider_id)->update($data);
        if($result){
             return redirect()->back()->with('save', 'Slider Deactived.');
        }else{
            return redirect()->back()->with('error', 'Data not updated.'); 
        } 
    }
    
    public function AddBanner(){
        return view('admin.content.add_banner'); 
    }
    
    public function SaveBanner(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'banner_title' => 'required',
                    'banner_pos' => 'required',
                    'banner_order' => 'required',
                    'banner_image' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            if ($request->hasFile('banner_image')) {
                $width = Image::make($request->file('banner_image'))->width();
                $height = Image::make($request->file('banner_image'))->height();
                if ($width >= 380 && $height >= 507) {
                $file = $request->file('banner_image');
                $filename = time(). "_" . $file->getClientOriginalName();
                $image = Image::make($request->file('banner_image'))->resize(380, 507)->save('storage/app/public/banner/' . $filename);
                $last_ip=$request->ip();
                $created_by=session('pride_admin_id');
                $created_at = Carbon::now();
                $data['banner_title']=$request->banner_title;
                $data['banner_pos']=$request->banner_pos;
                $data['banner_order']=$request->banner_order;
                $data['banner_image']=$filename;
                $data['banner_link']=$request->banner_link;
                $data['created_at']=$created_at;
                $data['created_by']=$created_by;
                $data['created_ip']=$last_ip;
                $result=DB::table('banner')->insert($data);
                if($result){
                     return redirect()->back()->with('save', 'New banner image added.');
                }else{
                    return redirect()->back()->with('error', 'Data not added.'); 
                }
            }else{
                return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 380px X Height: 507px.New image size Width: $width px X Height: $height px");
            }
        }else{
           return redirect()->back()->with('error', 'Please select an image.'); 
        }
     }
  }
  
  public function ManageBanner(){
       $data['banner_list']=DB::table('banner')
        ->join('admin','admin.admin_id','=','banner.created_by')
        ->select('banner.*','admin.*')
        ->where('banner_deleted_status',0)
        ->orderBy('banner_id','DESC')
        ->get();
      return view('admin.content.manage_banner',$data); 
  }
  
  public function ActiveBanner($banner_id){
      $data['banner_status']=1;
        $result=DB::table('banner')->where('banner_id',$banner_id)->update($data);
        if($result){
             return redirect()->back()->with('save', 'Banner Actived.');
        }else{
            return redirect()->back()->with('error', 'Data not updated.'); 
        }
  }
  
  public function DeactiveBanner($banner_id){
        $data['banner_status']=0;
        $result=DB::table('banner')->where('banner_id',$banner_id)->update($data);
        if($result){
             return redirect()->back()->with('save', 'Banner Deactived.');
        }else{
            return redirect()->back()->with('error', 'Data not updated.'); 
        } 
  }
  
  public function EditBanner($banner_id){
      $data['banner']=DB::table('banner')->where('banner_id',$banner_id)->first();
      return view('admin.content.edit_banner',$data);
  }
  
  public function UpdateBanner(Request $request){
      $vaildation = Validator::make($request->all(), [
                    'banner_title' => 'required',
                    'banner_pos' => 'required',
                    'banner_order' => 'required',
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            if ($request->hasFile('banner_image')) {
                $width = Image::make($request->file('banner_image'))->width();
                $height = Image::make($request->file('banner_image'))->height();
                if ($width == 380 && $height == 507) {
                $file = $request->file('banner_image');
                $filename = time(). "_" . $file->getClientOriginalName();
                $image = Image::make($request->file('banner_image'))->resize(380, 507)->save('storage/app/public/banner/' . $filename);
                $last_ip=$request->ip();
                $created_by=session('pride_admin_id');
                $created_at = Carbon::now();
                $data['banner_title']=$request->banner_title;
                $data['banner_pos']=$request->banner_pos;
                $data['banner_order']=$request->banner_order;
                $data['banner_image']=$filename;
                $data['banner_link']=$request->banner_link;
                $data['updated_at']=$created_at;
                $data['updated_by']=$created_by;
                $data['updated_ip']=$last_ip;
                $result=DB::table('banner')->where('banner_id',$request->banner_id)->update($data);
                if($result){
                     return redirect()->back()->with('save', 'Banner updated.');
                }else{
                    return redirect()->back()->with('error', 'Data not updated.'); 
                }
            }else{
                return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 380px X Height: 507px.New image size Width: $width px X Height: $height px");
            }
        }else{
            $last_ip=$request->ip();
            $created_by=session('pride_admin_id');
            $created_at = Carbon::now();
            $data['banner_title']=$request->banner_title;
            $data['banner_pos']=$request->banner_pos;
            $data['banner_order']=$request->banner_order;
            $data['banner_link']=$request->banner_link;
            $data['updated_at']=$created_at;
            $data['updated_by']=$created_by;
            $data['updated_ip']=$last_ip;
            $result=DB::table('banner')->where('banner_id',$request->banner_id)->update($data);
            return redirect()->back()->with('save', 'Banner updated.'); 
        }
     }
  }
  
  public function DeleteBanner($banner_id){
        $image=DB::table('banner')->where('banner_id',$banner_id)->first();
        $slider_image=$image->slider_image;
        Storage::delete("public/banner/$slider_image");
        $result=DB::table('banner')->where('banner_id',$banner_id)->delete();
        if($result){
             return redirect()->back()->with('save', 'Banner image deleted.');
        }else{
          return redirect()->back()->with('error', 'Data not deleted.');  
        }
  }
  
}
