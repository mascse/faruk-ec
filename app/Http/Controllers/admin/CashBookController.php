<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use App\CashTransaction;
use App\CashExpenses;
use Illuminate\Support\Facades\Auth;

class CashBookController extends Controller
{
    
    public function __construct() {
        $this->middleware('AdminAuth');
     //   $this->middleware('permission');
    }
    //
    
    public function CashbookHistrory(){
      //  $data['cash_history']=DB::select(DB::raw("SELECT cash_transactions.transaction_type, cash_transactions.amount as transaction_amount, cash_expenses.expense_type, cash_expenses.amount as expenses_amount, cash_transactions.conforder_id as order_id, cash_expenses.remark, cash_transactions.created_at FROM cash_transactions LEFT JOIN cash_expenses on cash_transactions.conforder_id = cash_expenses.conforder_id UNION SELECT cash_transactions.transaction_type, cash_transactions.amount as tranaction_amount, cash_expenses.expense_type, cash_expenses.amount as expenses_amount, cash_expenses.conforder_id as order_id, cash_expenses.remark, cash_expenses.created_at FROM cash_transactions RIGHT JOIN cash_expenses on cash_transactions.conforder_id = cash_expenses.conforder_id"));
       $data['cash_history']=DB::select(DB::raw("SELECT cash_transactions.transaction_type,cash_transactions.created_at,cash_transactions.transfer_mode,conforder.conforder_deliverydate,conforder.conforder_tracknumber,conforder.conforder_id,ordershipping.Shipping_txtfirstname,ordershipping.Shipping_txtlastname,ordershipping.Shipping_txtaddress1,riders.name as delivery_by,cash_transactions.amount as transation_amount,cash_expenses.amount as expense_amount,admin.admin_username
            FROM cash_transactions
            LEFT JOIN cash_expenses
            ON cash_transactions.conforder_id=cash_expenses.conforder_id
            LEFT JOIN conforder
            ON cash_transactions.conforder_id=conforder.conforder_id
            LEFT JOIN ordershipping
            ON conforder.conforder_id=ordershipping.conforder_id
            LEFT JOIN riders
            ON conforder.delivery_by=riders.id
            LEFT JOIN admin
            on cash_transactions.issued_by=admin.admin_id
            order BY cash_transactions.id ASC"));
        /*DB::table('cash_transactions')
        ->join('cash_expenses','cash_transactions.conforder_id','=','cash_expenses.conforder_id')
        ->select('cash_transactions.*')
        ->get();
        */
        return view('admin.cashbook.cashbook_histroy',$data);
    }
    public function cashOut() {
        $data['opening_balance']=DB::table('cash_transactions')->where('transaction_type','opening_balance')->sum('cash_transactions.amount');
        $data['cash_in']=DB::table('cash_transactions')->where('transaction_type','cash_in')->sum('cash_transactions.amount');
        $data['cash_out']=DB::table('cash_transactions')->where('transaction_type','cash_out')->sum('cash_transactions.amount');
        $data['expenses']=DB::table('cash_expenses')->sum('cash_expenses.amount');
        return view('admin.cashbook.cashout',$data);
    }
    public function storeCashout(Request $request) {
        if($request->amount <= $request->available_amount ){
          DB::table('cash_transactions')->insert(
            [
                'amount' => $request->amount,
                'transaction_type' => 'cash_out',
                'transfer_to' => $request->transfer_to,
                'description' => $request->description,
                'website_code' => 'ut',
                'transfer_mode' => 'cash',
                'created_at' => Carbon::now(),
                'issued_by' => session('admin_id')
            ]
        );
        return redirect()->back()->with('save', 'Cash transferred successfully');
      }else{
          return redirect()->back()->with('error', 'Cash not transferred');
      }
    }
}
