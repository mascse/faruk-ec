<?php

namespace App\Http\Controllers\product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Product;
use App\Productalbum;
use App\Productimg;
use App\Productsize;

class ProductController extends Controller {

    public static function GetProductColorAlbum($pro_id) {
        $color_album = DB::table('productalbum')
                ->select('productalbum_id', 'productalbum_name', 'productalbum_img')
                ->where('product_id', $pro_id)
                ->orderby('productalbum_order', 'ASC')
                ->get();
        return $color_album;
    }

    public static function GetUpdatedProductColorAlbumName($pro_id) {
        $color_album = DB::table('productalbum')
                ->select('productalbum_name')
                ->where('product_id', $pro_id)
                ->orderby('productalbum_id', 'DESC')
                ->first();
        return $color_album;
    }

    public static function GetProductColorAlbumPuja($pro_id) {
        $color_album = DB::table('productalbum')
                ->select('productalbum_id', 'productalbum_name', 'productalbum_img')
                ->where('product_id', $pro_id)
                ->orderby('productalbum_order', 'ASC')
                ->first();
        return $color_album;
    }

    public static function GetProductImageByColorAlbum($pro_album_id) {
        $products = DB::table('productimg')
                ->select('productimg_id', 'productimg_img_thm', 'productimg_img_medium', 'productimg_img')
                ->where('productalbum_id', $pro_album_id)
                ->orderBy('productimg_order', 'ASC')
                ->orderBy('productimg_id', 'ASC')
                ->first();
        return $products;
    }
	
	public static function GetProductImage($product_id){
		$color_album = DB::table('productalbum')
                ->select('productalbum_id')
                ->where('product_id', $product_id)
                ->orderby('productalbum_id', 'DESC')
                ->first();
        $products = DB::table('productimg')
                ->select('productimg_id', 'productimg_img_medium', 'productimg_img_thm', 'productimg_img')
                ->where('productalbum_id', $color_album->productalbum_id)
                ->orderBy('productimg_order', 'ASC')
                ->orderBy('productimg_id', 'ASC')
                ->first();
        return $products;        
	}

    public static function ProductWiseQty($product_id) {
        $qty = DB::table('productsize')->where('product_id', $product_id)->sum('SizeWiseQty');
        return $qty;
    }

    public static function GetLatestProduct() {
        $product = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_id', 'DESC')
                ->groupBy('product.product_id')
                ->first();
        return $product;
    }

    public static function GetLatestWomanProduct() {
        $product = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->whereIn('product.procat_id', [5, 9, 10, 11, 12, 13, 20, 7])
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_id', 'DESC')
                ->groupBy('product.product_id')
                ->first();
        return $product;
    }

    public static function GetLatestManProduct() {
        $product = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->whereIn('product.procat_id', [17, 18])
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_id', 'DESC')
                ->groupBy('product.product_id')
                ->first();
        return $product;
    }

    public static function GetLatestKidsProduct() {
        $product = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->whereIn('product.procat_id', [6])
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_id', 'DESC')
                ->groupBy('product.product_id')
                ->first();
        return $product;
    }

    public static function GetLatestHomesProduct() {
        $product = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->whereIn('product.procat_id', [19])
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_id', 'DESC')
                ->groupBy('product.product_id')
                ->first();
        return $product;
    }

    public static function GetLatestAccessoriesProduct() {
        $product = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->whereIn('product.subprocat_id', [78, 89])
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_id', 'DESC')
                ->groupBy('product.product_id')
                ->first();
        return $product;
    }

    public function ProductTotalQty($product_id) {
        $qty = DB::table('productsize')->where('product_id', $product_id)->sum('SizeWiseQty');
        echo $qty;
    }

    public function NewArrival() {
        $productlist = DB::select(DB::raw("SELECT product.product_id,product.product_insertion_date,product_price,product.product_img_thm,product_code,product_name,productimg.productimg_img_medium,productalbum.productalbum_name FROM product 
                    INNER JOIN productalbum ON product.product_id=productalbum.product_id 
                    inner join subprocat on product.subprocat_id = subprocat.subprocat_id 
                    INNER JOIN productimg on productalbum.productalbum_id=productimg.productalbum_id
                    where  product.product_active_deactive=0 AND DATEDIFF(CURRENT_DATE,STR_TO_DATE(product.product_insertion_date, '%d-%m-%Y'))< 65 GROUP BY product.product_id  order by product.product_id desc"));

        $data['product_list'] = $productlist;
        $data['category_name'] = 'All';
        return view('product.new_arrival', $data);
    }

    public function NewIn($name) {
        if ($name == 'woman') {
            $procat_id = "5, 7, 9, 10, 11, 12, 13, 14, 15, 20";
        } elseif ($name == 'man') {
            $procat_id = "16, 17,18";
        } elseif ($name == 'pride-homes') {
            $procat_id = "19";
        } else {
            $procat_id = "6";
        }
        $productlist = DB::select(DB::raw("SELECT product.product_id,product.product_insertion_date,product_price,product.product_img_thm,product_code,product_name,productimg.productimg_img,productimg.productimg_img_medium,productalbum.productalbum_name FROM product 
                    INNER JOIN productalbum ON product.product_id=productalbum.product_id 
                    inner join subprocat on product.subprocat_id = subprocat.subprocat_id 
                    INNER JOIN productimg on productalbum.productalbum_id=productimg.productalbum_id
                    where product.procat_id in($procat_id) and product.product_active_deactive=0 AND DATEDIFF(CURRENT_DATE,STR_TO_DATE(product.product_insertion_date, '%d-%m-%Y'))< 30 GROUP BY product.product_id  order by product.product_id desc"));

        $data['product_list'] = $productlist;
        $data['category_name'] = $name;
        return view('product.new_arrival', $data);
    }

    public function NewInByCategory($name, $category) {
        if ($name == 'woman') {
            if ($category == 'signature') {
                $procat_id = "16,12,9";
            } elseif ($category == 'classic') {
                $procat_id = "7";
            } elseif ($category == 'pride-girls') {
                $procat_id = "5";
            } elseif ($category == 'accessories') {
                $procat_id = "woman-accessories";
            }
        } elseif ($name == 'man') {
            $procat_id = "16, 17,18";
        } elseif ($name == 'pride-homes') {
            $procat_id = "19";
        } elseif ($name == 'kids') {
            $procat_id = "6";
        }
        if ($procat_id == '6') {
            if ($category == 'boys') {
                $subpro_cat_id = "39";
            } elseif ($category == 'girls') {
                $subpro_cat_id = "52,81,82,83,84,85";
            } elseif ($category == 'accessories') {
                $subpro_cat_id = "89";
            }
            $productlist = DB::select(DB::raw("SELECT product.product_id,product_price,product.product_insertion_date,product.product_img_thm,product_code,product_name,productimg.productimg_img,productimg.productimg_img_medium,productalbum.productalbum_name FROM product 
                    INNER JOIN productalbum ON product.product_id=productalbum.product_id 
                    inner join subprocat on product.subprocat_id = subprocat.subprocat_id 
                    INNER JOIN productimg on productalbum.productalbum_id=productimg.productalbum_id
                    where product.subprocat_id in($subpro_cat_id) and product.product_active_deactive=0 AND DATEDIFF(CURRENT_DATE,STR_TO_DATE(product.product_insertion_date, '%d-%m-%Y'))< 30 GROUP BY product.product_id  order by product.product_id desc"));
        } elseif ($procat_id == 'woman-accessories') {
            $subpro_cat_id = "78";
            $productlist = DB::select(DB::raw("SELECT product.product_id,product_price,product.product_insertion_date,product.product_img_thm,product_code,product_name,productimg.productimg_img,productimg.productimg_img_medium,productalbum.productalbum_name FROM product 
                    INNER JOIN productalbum ON product.product_id=productalbum.product_id 
                    inner join subprocat on product.subprocat_id = subprocat.subprocat_id 
                    INNER JOIN productimg on productalbum.productalbum_id=productimg.productalbum_id
                    where product.subprocat_id in($subpro_cat_id) and product.product_active_deactive=0 AND DATEDIFF(CURRENT_DATE,STR_TO_DATE(product.product_insertion_date, '%d-%m-%Y'))< 30 GROUP BY product.product_id  order by product.product_id desc"));
        } else {
            $productlist = DB::select(DB::raw("SELECT product.product_id,product_price,product.product_insertion_date,product.product_img_thm,product_code,product_name,productimg.productimg_img,productimg.productimg_img_medium,productalbum.productalbum_name FROM product 
                    INNER JOIN productalbum ON product.product_id=productalbum.product_id 
                    inner join subprocat on product.subprocat_id = subprocat.subprocat_id 
                    INNER JOIN productimg on productalbum.productalbum_id=productimg.productalbum_id
                    where product.procat_id in($procat_id) and product.product_active_deactive=0 AND DATEDIFF(CURRENT_DATE,STR_TO_DATE(product.product_insertion_date, '%d-%m-%Y'))< 30 GROUP BY product.product_id  order by product.product_id desc"));
        }

        $data['product_list'] = $productlist;
        $data['category_name'] = $name;
        return view('product.new_arrival', $data);
    }

    public function ProductList($categoryid, $subcategory) {
        if ($subcategory == 'signature') {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->where('product.procat_id', $categoryid)
                    ->where('product_active_deactive', 0)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->paginate(18);
            $data['subcategory'] = $subcategory;
            $data['title'] = 'signature';
            $data['last'] = '';
        } else if ($subcategory == 'pride-girls') {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->where('product.procat_id', $categoryid)
                    ->where('product_active_deactive', 0)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->orderBy('productimg.productimg_id', 'ASC')
                    ->groupBy('product.product_id')
                    ->paginate(18);
            $data['subcategory'] = $subcategory;
            $data['title'] = 'Signature';
            $data['last'] = $subcategory;
        } else if ($subcategory == 'all-panjabi') {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.procat_id', [17, 18])
                    ->where('product_active_deactive', 0)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->orderBy('productimg.productimg_order', 'ASC')
                    ->groupBy('product.product_id')
                    ->paginate(18);
            $data['subcategory'] = $subcategory;
            $data['title'] = 'Ethinc Menaswear';
            $data['last'] = '';
        } elseif ($subcategory == 'all-girls') {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.subprocat_id', [52, 81, 82, 83, 84, 85])
                    ->where('product_active_deactive', 0)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->orderBy('productimg.productimg_order', 'ASC')
                    ->groupBy('product.product_id')
                    ->paginate(18);
            $data['subcategory'] = $subcategory;
            $data['title'] = 'Kids';
            $data['last'] = 'Girls';
        } elseif ($subcategory == 72) {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.subprocat_id', [72, 87, 88])
                    ->where('product_active_deactive', 0)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->orderBy('productimg.productimg_order', 'ASC')
                    ->groupBy('product.product_id')
                    ->paginate(18);
            $data['subcategory'] = 'kameez';
            $data['title'] = 'Pride';
            $data['last'] = 'kameez';
        } else if($subcategory == 13){
			$productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name','productalbum.productalbum_id', 'productalbum.productalbum_img')
                    ->whereIn('product.subprocat_id', [13])
                    ->where('product_active_deactive', 0)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->orderBy('productimg.productimg_order', 'ASC')
                    ->groupBy('productalbum.productalbum_id')
                    ->paginate(18);
			$data['subcategory'] = 'Orna';
            $data['title'] = 'Pride';
            $data['last'] = 'Orna';
		} else {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    // ->select(DB::raw('SELECT productimg.productimg_img_medium FROM productimg ORDER BY productimg.productimg_id ASC LIMIT 1) as product_img'))
                    //->select('productimg', DB::raw('productimg_img_medium as product_img'))
                    ->where('product.procat_id', $categoryid)
                    ->where('product.subprocat_id', $subcategory)
                    ->where('product_active_deactive', 0)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->paginate(18);

            $data['subcategory'] = $subcategory;

            $title = DB::table('procat')
                    ->join('subprocat', 'procat.procat_id', '=', 'subprocat.procat_id')
                    ->select('procat.procat_name', 'subprocat.subprocat_name')
                    ->where('subprocat.subprocat_id', $subcategory)
                    ->first();


            $data['title'] = $title->procat_name;
            $data['last'] = $title->subprocat_name;
        }
        if ($categoryid == 6) {
            $data['main_cate'] = 'Kids';
        } else if ($categoryid == 17 || $categoryid == 18) {
            $data['main_cate'] = 'Mens';
        } else {
            $data['main_cate'] = 'Woman';
        }
        $data['procat_id'] = $categoryid;
        $data['subcategory'] = $subcategory;
        $data['product_list'] = $productlist;
        $data['color_names'] = $this->getProductColors();
        $data['fabrics'] = $this->getFabrics();
        return view('product.product_list', $data);
    }

    /* product details live
      public function ProductDetails($product_name, $product_color, $product_id) {
      $product_color=str_replace('-','/',$product_color);
      $data['product_name'] = $product_name;
      $data['select_color'] = $product_color;
      $data['singleproduct'] = Product::where('product_id', $product_id)->first();
      $productalbum = Productalbum::where('product_id', $product_id)->where('productalbum_name', $product_color)->first();
      $productalbum_id = $productalbum->productalbum_id;
      //$sproduct = DB::select(DB::raw("select productalbum.productalbum_id from product inner join productalbum on product.product_id=productalbum.product_id WHERE product.product_id='$product_id' AND productalbum.productalbum_name='$product_color'"));
      //foreach ($sproduct as $sproduct) {
      //    $productalbum_id = $sproduct->productalbum_id;
      // }
      //select productimg.productimg_img_thm,productimg.productimg_img,productimg.productimg_img_medium,productimg.productimg_img_tiny from productimg WHERE productimg.productalbum_id='$productalbum_id'
      // $singleproductmultiplepic = DB::select(DB::raw("select productimg.productimg_img_thm,productimg.productimg_img,productimg.productimg_img_medium,productimg.productimg_img_tiny from productimg WHERE productimg.productalbum_id='$productalbum_id' ORDER BY productimg.productimg_id ASC"));
      $data['singleproductmultiplepic'] = Productimg::where('productalbum_id', $productalbum_id)->orderBy('productimg_id', 'ASC')->get();
      $data['cart_image'] = Productimg::where('productalbum_id', $productalbum_id)->orderBy('productimg_id', 'ASC')->first();
      //$product_color_image = DB::select(DB::raw("SELECT  product_id,productalbum_id,productalbum_name,productalbum_img  from productalbum where product_id='$product_id' order by productalbum_order asc"));
      $data['product_color_image'] = Productalbum::where('product_id', $product_id)->get();
      // $data['product_selected_color'] = DB::table('productalbum')->where('product_id', $product_id)->where('productalbum_name', $product_color)->first();
      $data['product_selected_color'] = Productalbum::where('product_id', $product_id)->where('productalbum_name', $product_color)->first();
      // $product_sizes = DB::select(DB::raw("SELECT  productsize_id, productsize_size, SizeWiseQty as quantity from productsize where product_id='$product_id' AND color_name='$product_color'"));
      $data['product_sizes'] = Productsize::where('product_id', $product_id)->where('color_name', $product_color)->get();
      //$product_qty = DB::select(DB::raw("SELECT SUM(SizeWiseQty) as totalqty from productsize where product_id='$product_id'"));
      $data['product_qty'] = Productsize::where('product_id', $product_id)->sum('SizeWiseQty');
      $data['singleproduct'] = $singleproduct;
      // $data['singleproductmultiplepic'] = $singleproductmultiplepic;
      //$data['product_color_image'] = $product_color_image;
      // $data['product_sizes'] = $product_sizes;
      // $data['product_qty'] = $product_qty;
      $data['product_color'] = $product_color;
      $data['product_id'] = $product_id;
      $data['subprocat_id'] = $data['singleproduct']->subprocat_id;
      $procat_id = $data['singleproduct']->procat_id;
      $subprocat_id = $data['singleproduct']->subprocat_id;
      $get_title=DB::table('product')
      ->select('procat.procat_name','subprocat.subprocat_name')
      ->join('procat','product.procat_id','=','procat.procat_id')
      ->join('subprocat','product.subprocat_id','=','subprocat.subprocat_id')
      ->where('product.procat_id',$procat_id)
      ->where('product.subprocat_id',$subprocat_id)
      ->first();
      if ($singleproduct->procat_id == 6) {
      $data['main_cate'] = 'Kids';
      } else if ($singleproduct->procat_id == 17 || $singleproduct->procat_id == 18) {
      $data['main_cate'] = 'Mens';
      } else {
      $data['main_cate'] = 'Woman';
      }
      $category_name=$get_title->procat_name;
      $subcategory_name=$get_title->subprocat_name;
      $data['title']=$category_name.'-'.$subcategory_name.'-'.$singleproduct->product_name;

      $product=DB::table('product')->where('product_id',$product_id)->first();
      $data['sub_cate_id'] = $singleproduct->subprocat_id;
      $data['alt']=$product->product_name;
      $data['category_name'] = $get_title->procat_name;
      $data['subcategory_name'] = $get_title->subprocat_name;
      $data['title'] = $singleproduct->product_name;
      $data['image']=$product->product_img_thm;
      // $data['district_list'] = DB::table('showroom_cities')->get();
      $data['description']=$product->product_description;
      $data['color_names'] = $this->getProductColors();
      // $data['district_list'] =DB::connection('sqlsrv')->select("SELECT DISTINCT ShowroomDetails.district FROM  ShowroomDetails inner join Showroomwiseitemstock on ShowroomDetails.SalesPointID=Showroomwiseitemstock.salespointid where Showroomwiseitemstock.designref='$singleproduct->product_code'");
      $show_fb = true;
      return view('product.product_details_new', $data)->with('show_fb', $show_fb);
      }
     */

    public function ProductDetails($product_name, $product_color, $product_id) {
        $product_color = str_replace('-', '/', $product_color);
        $data['singleproduct'] = Product::find($product_id);
        $productalbum = Productalbum::where('product_id', $product_id)->where('productalbum_name', $product_color)->first();
        $productalbum_id = $productalbum->productalbum_id;
        Productimg::where('productalbum_id', $productalbum_id)->orderBy('productimg_id', 'ASC')->get();
        $data['singleproductmultiplepic'] = Productimg::where('productalbum_id', $productalbum_id)->orderBy('productimg_id', 'ASC')->get();
        $data['product_color_image'] = Productalbum::where('product_id', $product_id)->get();
        $data['product_selected_color'] = DB::table('productalbum')->where('product_id', $product_id)->where('productalbum_name', $product_color)->first();
        $data['product_sizes'] = Productsize::where('product_id', $product_id)->where('color_name', $product_color)->get();
		$data['cart_image'] = Productimg::where('productalbum_id', $productalbum_id)->orderBy('productimg_id', 'ASC')->first();
        $data['product_qty'] = Productsize::where('product_id', $product_id)->sum('SizeWiseQty');
        $data['subprocat_id'] = $data['singleproduct']->subprocat_id;
        $procat_id = $data['singleproduct']->procat_id;
        $subprocat_id = $data['singleproduct']->subprocat_id;
        if ($data['singleproduct']->procat_id == 6) {
            $data['main_cate'] = 'Kids';
        } else if ($data['singleproduct']->procat_id == 17 || $data['singleproduct']->procat_id == 18) {
            $data['main_cate'] = 'Mens';
        } else {
            $data['main_cate'] = 'Woman';
        }
        $get_title = $value = Product::GetProductCategorySubcategory($procat_id, $subprocat_id);
        $category_name = $get_title->procat_name;
        $subcategory_name = $get_title->subprocat_name;
        //$data['title']=$category_name.'-'.$subcategory_name.'-'.$data['singleproduct']->product_name;
        $product = DB::table('product')->where('product_id', $product_id)->first();
        $data['sub_cate_id'] = $data['singleproduct']->subprocat_id;
        $data['alt'] = $data['singleproduct']->product_name;
        $data['category_name'] = $get_title->procat_name;
        $data['subcategory_name'] = $get_title->subprocat_name;
        $data['title'] = $data['singleproduct']->product_name;
        $data['image'] = $data['singleproduct']->product_img_thm;
        $style_code = $data['singleproduct']->product_code;
       
        $data['description'] = $data['singleproduct']->product_description;
        $data['color_names'] = $this->getProductColors();
        $data['product_color'] = $product_color;
        $data['product_id'] = $product_id;
        $data['product_name'] = $product_name;
        $data['select_color'] = $product_color;
        $show_fb = true;
        return view('product.product_details_new', $data)->with('show_fb', $show_fb);
    }
	
	public function FindInStoreCity($product_code){
        $city_list=DB::connection('sqlsrv')->select("SELECT DISTINCT ShowroomDetails.district FROM  ShowroomDetails inner join Showroomwiseitemstock on ShowroomDetails.SalesPointID=Showroomwiseitemstock.salespointid where Showroomwiseitemstock.designref='$product_code'");
        return $city_list;
    }

    /* Getting product images */

    public static function productImages($album_id) {
        $images = DB::table('productimg')
                ->select('productimg_id', 'productimg_img_medium', 'productimg_img_thm', 'productimg_img')
                ->where('productalbum_id', $album_id)
                ->orderBy('productimg_id', 'ASC')
                ->get();
        //dd($color_album);
        return $images;
    }

    public function productByPrice($categoryid, $subcategory, $lower_price = 0, $upper_price = 0) {
        $title = DB::table('procat')
                ->join('subprocat', 'procat.procat_id', '=', 'subprocat.procat_id')
                ->select('procat.procat_name', 'subprocat.subprocat_name')
                ->where('subprocat.subprocat_id', $subcategory)
                ->first();
        $products = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('product.procat_id', $categoryid)
                ->where('product.subprocat_id', $subcategory)
                ->where('product_active_deactive', 0);
        if ($lower_price <= $upper_price && $lower_price >= 0 && $upper_price >= 0) {
            $products = $products->whereBetween('product_price', [$lower_price, $upper_price]);
        } elseif ($lower_price >= 0) {
            $products = $products->where('product_price', '>=', $lower_price);
        }
        $productlist = $products
                ->orderBy('product.product_id', 'desc')
                ->groupBy('product.product_id')
                ->paginate(18);

        $data['title'] = $title->procat_name;
        $data['last'] = $title->subprocat_name;

        if ($categoryid == 6) {
            $data['main_cate'] = 'Kids';
        } else if ($categoryid == 17 || $categoryid == 18) {
            $data['main_cate'] = 'Mens';
        } else {
            $data['main_cate'] = 'Woman';
        }
        $data['procat_id'] = $categoryid;
        $data['subcategory'] = $subcategory;
        $data['product_list'] = $productlist;
        $data['lower_price'] = $lower_price;
        $data['upper_price'] = $upper_price;
        $data['color_names'] = $this->getProductColors($categoryid, $subcategory);
        $data['fabrics'] = $this->getFabrics($categoryid, $subcategory);
        return view('product.product_list', $data);
    }

    public function SearchByPrice($key, $lower_price = 0, $upper_price = 0) {
        if ($lower_price < 0)
            $lower_price = 0;
        if ($upper_price <= 0)
            $upper_price = 99999;
        $data['search'] = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img', 'productimg.productimg_img_medium')
                ->whereBetween('product.product_price', [$lower_price, $upper_price])
                ->where('product.product_active_deactive', 0)
                ->where(function($q) use ($key) {
                    $q->where('product.product_name', 'like', "%{$key}%")
                    ->orWhere('product.product_description', 'like', "%{$key}%")
                    ->orWhere('product.product_code', 'like', "%{$key}%");
                })
                ->groupBy('product.product_id')
                ->orderBy('product.product_id', 'DESC')
                ->paginate(18);
        $data['key'] = $key;
        $data['lower_price'] = $lower_price;
        $data['upper_price'] = $upper_price;
        $data['color_names'] = $this->getProductColorsForSearch($key);
        return view('search_view', $data);
    }

    public function ProductBySize($categoryid, $subcategory, $size = null) {
        $title = DB::table('procat')
                ->join('subprocat', 'procat.procat_id', '=', 'subprocat.procat_id')
                ->select('procat.procat_name', 'subprocat.subprocat_name')
                ->where('subprocat.subprocat_id', $subcategory)
                ->first();
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->join('productsize', 'product.product_id', '=', 'productsize.product_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('product.procat_id', $categoryid)
                ->where('product.subprocat_id', $subcategory)
                ->where('productsize.productsize_size', $size)
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_id', 'desc')
                ->groupBy('product.product_id')
                ->paginate(18);

        $data['title'] = $title->procat_name;
        $data['last'] = $title->subprocat_name;

        if ($categoryid == 6) {
            $data['main_cate'] = 'Kids';
        } else if ($categoryid == 17 || $categoryid == 18) {
            $data['main_cate'] = 'Mens';
        } else {
            $data['main_cate'] = 'Woman';
        }
        $data['procat_id'] = $categoryid;
        $data['subcategory'] = $subcategory;
        $data['product_list'] = $productlist;
        $data['size'] = $size;
        $data['color_names'] = $this->getProductColors($categoryid, $subcategory);
        $data['fabrics'] = $this->getFabrics($categoryid, $subcategory);
        return view('product.product_list', $data);
    }

    public function SearchBySize($key, $size = 'S') {
        $data['search'] = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->join('productsize', 'product.product_id', '=', 'productsize.product_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img', 'productimg.productimg_img_medium')
                ->where('productsize.productsize_size', $size)
                ->where('product.product_active_deactive', 0)
                ->where(function($q) use ($key) {
                    $q->where('product.product_name', 'like', "%{$key}%")
                    ->orWhere('product.product_description', 'like', "%{$key}%")
                    ->orWhere('product.product_code', 'like', "%{$key}%");
                })
                ->groupBy('product.product_id')
                ->orderBy('product.product_id', 'DESC')
                ->paginate(18);
        $data['key'] = $key;
        $data['size'] = $size;
        $data['color_names'] = $this->getProductColorsForSearch($key);
        return view('search_view', $data);
    }

    public function ProductByColor($categoryid, $subcategory, $color_name = null) {
        $title = DB::table('procat')
                ->join('subprocat', 'procat.procat_id', '=', 'subprocat.procat_id')
                ->select('procat.procat_name', 'subprocat.subprocat_name')
                ->where('subprocat.subprocat_id', $subcategory)
                ->first();
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->join('productsize', 'product.product_id', '=', 'productsize.product_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('product.procat_id', $categoryid)
                ->where('product.subprocat_id', $subcategory)
                ->where('productsize.color_name', 'like', '%' . $color_name . '%')
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_id', 'desc')
                ->groupBy('product.product_id')
                ->paginate(18);

        $data['title'] = $title->procat_name;
        $data['last'] = $title->subprocat_name;

        if ($categoryid == 6) {
            $data['main_cate'] = 'Kids';
        } else if ($categoryid == 17 || $categoryid == 18) {
            $data['main_cate'] = 'Mens';
        } else {
            $data['main_cate'] = 'Woman';
        }
        $data['procat_id'] = $categoryid;
        $data['subcategory'] = $subcategory;
        $data['product_list'] = $productlist;
        $data['color_name'] = $color_name;
        $data['color_names'] = $this->getProductColors($categoryid, $subcategory);
        $data['fabrics'] = $this->getFabrics($categoryid, $subcategory);
        return view('product.product_list', $data);
    }

    public function SearchByColor($key, $color_name = null) {
        $data['search'] = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->join('productsize', 'product.product_id', '=', 'productsize.product_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img', 'productimg.productimg_img_medium')
                ->where('productsize.color_name', $color_name)
                ->where('product.product_active_deactive', 0)
                ->where(function($q) use ($key) {
                    $q->where('product.product_name', 'like', "%{$key}%")
                    ->orWhere('product.product_description', 'like', "%{$key}%")
                    ->orWhere('product.product_code', 'like', "%{$key}%");
                })
                ->groupBy('product.product_id')
                ->orderBy('product.product_id', 'DESC')
                ->paginate(18);
        $data['key'] = $key;
        $data['color_name'] = $color_name;
        $data['color_names'] = $this->getProductColorsForSearch($key);
        return view('search_view', $data);
    }

    public function ProductByFabric($categoryid, $subcategory, $fabric = null) {
        $title = DB::table('procat')
                ->join('subprocat', 'procat.procat_id', '=', 'subprocat.procat_id')
                ->select('procat.procat_name', 'subprocat.subprocat_name')
                ->where('subprocat.subprocat_id', $subcategory)
                ->first();
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->join('productsize', 'product.product_id', '=', 'productsize.product_id')
                ->select('product.product_id', 'product.product_price', 'product.product_insertion_date', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('product.procat_id', $categoryid)
                ->where('product.subprocat_id', $subcategory)
                ->where('product.fabric', 'like', '%' . $fabric . '%')
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_id', 'desc')
                ->groupBy('product.product_id')
                ->paginate(18);

        $data['title'] = $title->procat_name;
        $data['last'] = $title->subprocat_name;

        if ($categoryid == 6) {
            $data['main_cate'] = 'Kids';
        } else if ($categoryid == 17 || $categoryid == 18) {
            $data['main_cate'] = 'Mens';
        } else {
            $data['main_cate'] = 'Woman';
        }
        $data['procat_id'] = $categoryid;
        $data['subcategory'] = $subcategory;
        $data['product_list'] = $productlist;
        $data['fabric'] = $fabric;
        $data['color_names'] = $this->getProductColors($categoryid, $subcategory);
        $data['fabrics'] = $this->getFabrics($categoryid, $subcategory);
        return view('product.product_list', $data);
    }

    public function getProductColors($categoryid = 0, $subcategory = 0) {
        $color_names = DB::table('product')
                ->join('productsize', 'product.product_id', '=', 'productsize.product_id')
                ->where('product.procat_id', $categoryid)
                ->where('product.subprocat_id', $subcategory)
                ->groupBy('color_name')
                ->orderBy('color_name')
                ->pluck('color_name')
                ->toArray();
        if ($categoryid == 0 || $subcategory == 0)
            $color_names = DB::table('productsize')
                    ->where('status', 1)
                    ->groupBy('color_name')
                    ->orderBy('color_name')
                    ->pluck('color_name')
                    ->toArray();
        return $color_names;
    }

    public function getProductColorsForSearch($key) {
        $color_names = DB::table('product')
                ->join('productsize', 'product.product_id', '=', 'productsize.product_id')
                ->where(function($q) use ($key) {
                    $q->where('product.product_name', 'like', "%{$key}%")
                    ->orWhere('product.product_description', 'like', "%{$key}%")
                    ->orWhere('product.product_code', 'like', "%{$key}%");
                })
                ->groupBy('color_name')
                ->orderBy('color_name')
                ->pluck('color_name')
                ->toArray();
        return $color_names;
    }

    public function getFabrics($categoryid = 0, $subcategory = 0) {
        $fabrics = DB::table('product')
                ->where('product.procat_id', $categoryid)
                ->where('product.subprocat_id', $subcategory)
                ->groupBy('fabric')
                ->orderBy('fabric')
                ->pluck('fabric')
                ->toArray();
        if ($categoryid == 0 || $subcategory == 0)
            $fabrics = DB::table('product')
                    ->groupBy('fabric')
                    ->orderBy('fabric')
                    ->pluck('fabric')
                    ->toArray();
        return $fabrics;
    }

}
