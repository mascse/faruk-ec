<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use App\Product;
use App\Slider;
use App\Banner;
use App\Browser;

class HomeController extends Controller {

    public function __construct() {
        //    $this->middleware('auth');
    }

    public function index() {
    //    $data['slides']  = Slider::where('slider_status', 1)->orderBy('slider_order')->get();
      //  $data['banner_list']=Banner::where('banner_status',1)->orderBy('banner_order','ASC')->limit(3)->get();
        //browser history
        $ip=$_SERVER['REMOTE_ADDR'];
        if($ip != "202.79.19.112"){
           // $this->get_browser_name($_SERVER['HTTP_USER_AGENT']);
        }
		
        $product=new Product();
        $data['product_list'] =$product->NewArriavl();
        $data['category_name'] = 'New Arrival';
        return view('home', $data);
    }
    
   public function get_browser_name($user_agent)
        {
            if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')){
                Browser::increment('Opera');
            }
            elseif (strpos($user_agent, 'Edge')){
                Browser::increment('IE');
            }
            elseif (strpos($user_agent, 'Chrome')){  
                Browser::increment('Chrome');
            }
            elseif (strpos($user_agent, 'Safari')) {
                Browser::increment('Safari'); 
            }
            elseif (strpos($user_agent, 'Firefox')){
                 Browser::increment('Firefox');
            }
            elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')){
               Browser::increment('Trident'); 
            }else{
                Browser::increment('others'); 
            }
        }

    
    
    
    public function EidCollection2018(){
        $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('isSpecial', 2)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->get();
            $data['product_list'] = $productlist;
        return view('eid_collection_2018',$data);
    }
    
    public function EidCollection2018Product($pro_id){
        if($pro_id==17){
            $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->whereIn('product.procat_id', [17,18])
            ->where('isSpecial', 2)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_order', 'ASC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->get();
        }elseif($pro_id==12){
            $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->whereIn('product.procat_id', [9,12,13,16,20])
            ->where('isSpecial', 2)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_order', 'ASC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->get();
        }else{
         $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('product.procat_id', $pro_id)
            ->where('isSpecial', 2)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_order', 'ASC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->get();
        }
            $data['product_list'] = $productlist;
            $data['procat_id']=$pro_id;
         return view('eid_product_2018',$data);
    }
    
    
    public function PujaCollection2018($title=null,$pro_id=null){
       /* for($i=1;$i<=487;$i++)
        {
         $da['sold']= $i; 
        DB::table('product')->where('product_id',$i)->update($da);
        } */
        if($pro_id == null){
        $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
           // ->whereIn('product.procat_id', [7,9,10,11,12,13,20,17,18])
            ->where('product_active_deactive', 0)
            ->where('isSpecial',3)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(21);
        }else{
        if($pro_id==17){
            $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->whereIn('product.procat_id', [17,18])
            ->where('isSpecial',3)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->groupBy('product.product_id')
            ->paginate(21);
        }elseif($pro_id==9){
            $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->whereIn('product.procat_id', [9,10,11,12,13,20,7])
            ->where('isSpecial',3)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
           // ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(21);
        }else{
         $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('product.procat_id', $pro_id)
            ->where('isSpecial', 3)
            ->where('product_active_deactive', 0)
            ->orderBy('product.product_order', 'ASC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->get();
         }
        }
        $data['product_list'] = $productlist;
        return view('puja_collection_2018',$data);
    }
    
    public function FlagunCollection2019($title=null,$pro_id=null){
        if($pro_id == null){
        $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name','productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('product_active_deactive', 0)
            ->where('isSpecial',4)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(18);
        }else{
        if($pro_id==17){
            $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img',  'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->whereIn('product.procat_id', [17,18])
            ->where('isSpecial',4)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->groupBy('product.product_id')
            ->paginate(18);
        }elseif($pro_id==9){
            $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img',  'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->whereIn('product.procat_id', [9,10,11,12,13,20,7])
            ->where('isSpecial',4)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
           // ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(18);
        }elseif($pro_id==5){
            $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img',  'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->whereIn('product.procat_id', [5])
            ->where('isSpecial',4)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
           // ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(18);
        }else{
         $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img',  'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('product.procat_id', $pro_id)
            ->where('isSpecial', 4)
            ->where('product_active_deactive', 0)
            ->orderBy('product.product_order', 'ASC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(18);
         }
        }
        $data['title'] = $title;
        $data['procat_id'] = '';
        $data['subcategory'] = '';
        $data['product_list'] = $productlist;
        return view('falgun_collection_2018',$data);
    }
    
    public function AmarEkushayCollection2019($title=null,$pro_id=null){
        if($pro_id == null){
        $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name','productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('product_active_deactive', 0)
            ->where('isSpecial',5)
            ->orderBy('product.sold','DESC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(18);
        }else{
        if($pro_id==17){
            $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img',  'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->whereIn('product.procat_id', [17,18])
            ->where('isSpecial',5)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
            ->groupBy('product.product_id')
            ->paginate(18);
        }elseif($pro_id==9){
            $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img',  'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->whereIn('product.procat_id', [9,10,11,12,13,20,7,5])
            ->where('isSpecial',5)
            ->where('product_active_deactive', 0)
            ->orderBy('product.sold','DESC')
           // ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(18);
        }else{
         $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img',  'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('product.procat_id', $pro_id)
            ->where('isSpecial', 5)
            ->where('product_active_deactive', 0)
            ->orderBy('product.product_order', 'ASC')
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(18);
         }
        }
        $data['title'] = $title;
        $data['procat_id'] = '';
        $data['subcategory'] = '';
        $data['product_list'] = $productlist;
        return view('collection.ekushay_collection_2019',$data);
    }
    
    public function LimitedSari(){
         $productlist = DB::table('product')
            ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
            ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
            ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
            ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
            ->where('product.procat_id', 9)
            ->where('product.subprocat_id', 56)
          //  ->where('spcollection', 1)
            ->where('product_active_deactive', 0)
            ->orderBy('product.product_id', 'desc')
            ->groupBy('product.product_id')
            ->paginate(16);
            $data['product_list'] = $productlist;
         return view('limited_edition_sari',$data);
    }
    
    public function ComingSoonPage(){
        return view('coming_soon');
    }
    
    public function AboutUs(){
        return view('footer_page.about_us');
    }
    
    public function WorkWithUs(){
       return view('footer_page.work_with_us');
    }
    
    public function ContactUs(){
       return view('footer_page.contact_us');
    }
    
    public function ExchangePolicy(){
        return view('footer_page.exchange_policy');
    }
    
    public function Faq(){
        return view('footer_page.faq');
    }
    
    public function StoreLocator(){
        return view('footer_page.store_locator');
    }
    
    public function PrivacyCookies(){
        return view('footer_page.privacy_cookies');
    }
    
    public function HowToOrder(){
        return view('footer_page.how_to_order');
    }
    
    public function LoyaltyTerms(){
        return view('loyalty.terms');
    }
	
    
	public function SiteMap(){
		return view('footer_page.sitemap');
	}
    
    public function search($key=null){
        $search = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img_medium')
                ->where('product.product_active_deactive', 0)
                ->where('product.product_name', 'like', "%{$key}%")
                ->orWhere('product.product_code', 'like', "%{$key}%")
                ->groupBy('product.product_id')
                ->orderBy('product.product_id','DESC')
                ->get();
                echo "<main class='main shop-page'>
                    <div class='container'>
                     <div class='row align-items-start'>
                      <div class='col-12 col-lg-12'>
                      <div class='row'>";
                     if($search->isEmpty()){
                       echo "
                       <div class='shop-st-header text-center'>
                           <span class='text-second'>Sorry, no matches found for"." '$key'</span>
                       </div>";
                     }
                      $i=0;
        foreach($search as $get_result){
            $i++;
            $product_name = str_replace(' ', '-', $get_result->product_name);
            $product_url = strtolower($product_name);
            if(!$search->isEmpty()){
                  echo "<div class='product-grid col-6 col-md-3'>
                        <div class='product-item row js-shop-item-product'>
                            <div class='product-item__grid col-5 col-sm-5 col-lg-4 mb-4'>
                                <div class='product-item__img'>
                                    <a class='product-item__img-link js-shop-img-flip d-block' href='https://www.pride-limited.com/shop/$product_url/color-$get_result->productalbum_name/$get_result->product_id'>
                                        <span class='front'>
                                          <img  src='https://www.pride-limited.com//storage/app/public/pgallery/$get_result->productimg_img_medium' width='255px' height='383px' alt='#'/>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class='product-item__list collapse js-list-show col-7 col-sm-7 col-lg-8'>
                                <div class='product-item__title h4 text-uppercase mb-4'>$get_result->product_name</div>
                                <div class='product-item__price'><span class='currency'>Tk </span><span>  $get_result->product_price</span></div>
                            </div>
                            <div class='product-item__bottom js-grid-show'>
                                <div class='product-item__title h4 text-uppercase mb-2'><a href='https://www.pride-limited.com/shop/$product_url/color-$get_result->productalbum_name/$get_result->product_id'>$get_result->product_name</a></div>
                                <div class='product-item__price mb-0'><span class='currency'>Tk </span><span>$get_result->product_price</span></div>
                            </div>
                        </div>
                    </div>";
                }
        }
              echo  "</div>
                    </div>
                    </div>
                     </div>
                </main>";
    }
    
    public function SearchNew($key) {
        $data['search'] = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img', 'productimg.productimg_img_medium')
                ->where('product.product_active_deactive', 0)
                ->where('product.product_name', 'like', "%{$key}%")
                ->orWhere('product.product_description', 'like', "%{$key}%")
                ->orWhere('product.product_code', 'like', "%{$key}%")
                ->groupBy('product.product_id')
                ->orderBy('product.product_id', 'DESC')
                ->get();
        $data['key']=$key;
        return view('search', $data);
    }
    
    public function SearchResultView($key){
         $data['search'] = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img', 'productimg.productimg_img_medium')
                ->where('product.product_active_deactive', 0)
                ->where('product.product_name', 'like', "%{$key}%")
                ->orWhere('product.product_description', 'like', "%{$key}%")
                ->orWhere('product.product_code', 'like', "%{$key}%")
                ->groupBy('product.product_id')
                ->orderBy('product.product_id', 'DESC')
                ->paginate(18);
        $data['key']=$key;
        return view('search_view', $data);
    }

}
