<?php

namespace App\Http\Controllers\cart;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\Product;
use DB;

class LastseenController extends Controller {

    public static function AddLastSeen($productid, $color) {
        $product_color = $color;
        // $product_size = $request->productsize;
        $productid = $productid;
        // $productimage = $image;
        $sproduct = DB::select(DB::raw("select productalbum.productalbum_id from product inner join productalbum on product.product_id=productalbum.product_id WHERE product.product_id='$productid' AND productalbum.productalbum_name='$product_color'"));
        foreach ($sproduct as $sproduct) {
            $productalbum_id = $sproduct->productalbum_id;
        }
        // $singleproductmultiplepic = DB::select(DB::raw("select productimg.productimg_img_thm,productimg.productimg_img,productimg.productimg_img_medium,productimg.productimg_img_tiny from productimg WHERE productimg.productalbum_id='$productalbum_id'"));
        $image = DB::table('productimg')
                ->where('productalbum_id', $productalbum_id)
                ->orderBy('productimg_id', 'ASC')
                ->limit(1)
                ->first();
        $prduct_imge = $image->productimg_img_medium;
        $product = Product::find($productid);
        $product_name = $product->product_name;
        Cart::instance('lastseen')->add(['id' => $product->product_id, 'qty' => 1, 'name' => $product->product_name, 'price' => $product->product_price, 'options' => ['color' => $product_color, 'lastseen_product_image' => $prduct_imge]]);
        return redirect()->back();
    }

    public static function TopPicks($main_cate, $product_id,$sub_cate_id) {
        if ($main_cate == 'Mens') {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.procat_id', [17, 18])
                    ->where('product.subprocat_id', $sub_cate_id)
                    ->where('product_active_deactive', 0)
                    ->whereNotIn('product.product_id', [$product_id])
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->limit(2)
                    ->get();
            return $productlist;
        } else if ($main_cate == 'Kids') {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.procat_id', [6])
                    ->where('product.subprocat_id', $sub_cate_id)
                    ->where('product_active_deactive', 0)
                    ->whereNotIn('product.product_id', [$product_id])
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->limit(2)
                    ->get();
            return $productlist;
        } else {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.procat_id', [5, 7, 9, 10, 11, 12, 13, 14, 15])
                    ->where('product.subprocat_id', $sub_cate_id)
                    ->where('product_active_deactive', 0)
                    ->whereNotIn('product.product_id', [$product_id])
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->limit(2)
                    ->get();
            return $productlist;
        }
    }
    public static function YouMayLike($main_cate, $product_id,$sub_cate_id) {
        if ($main_cate == 'Mens') {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.procat_id', [17, 18])
                    ->whereNotIn('product.subprocat_id', [$sub_cate_id])
                    ->where('product_active_deactive', 0)
                    ->whereNotIn('product.product_id', [$product_id])
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->limit(10)
                    ->get();
            return $productlist;
        } else if ($main_cate == 'Kids') {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.procat_id', [6])
                     ->whereNotIn('product.subprocat_id', [$sub_cate_id])
                    ->where('product_active_deactive', 0)
                    ->whereNotIn('product.product_id', [$product_id])
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->limit(10)
                    ->get();
            return $productlist;
        } else {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_img_thm', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->whereIn('product.procat_id', [5, 7, 9, 10, 11, 12, 13, 14, 15])
                    ->whereNotIn('product.subprocat_id', [$sub_cate_id])
                    ->where('product_active_deactive', 0)
                    ->whereNotIn('product.product_id', [$product_id])
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->limit(10)
                    ->get();
            return $productlist;
        }
    }
}
