<?php

namespace App\Http\Controllers;

use App\Models\TenderImage;
use Illuminate\Http\Request;
use App\Http\Controllers;
use App\Services\SSLCommerz;
use DB;
use Session;
use Mail;
use Cart;

session_start();

class PaynowController extends Controller {

    public function __construct() {
           $this->middleware('auth');
    }

    public function index() {
        $data['title'] = 'Pay-now';
        return view('paynow', $data);
    }

    public function submitpayment(Request $request) {
        //dd($request);
        $post_data = array();
        $post_data['total_amount'] = $request->shoppingcart_total; # You cant not pay less than 10
        $post_data['currency'] = "BDT";
        $post_data['tran_id'] = $request->tran_id; // tran_id must be unique
        #Start to save these value  in session to pick in success page.
        $_SESSION['payment_values']['tran_id'] = $post_data['tran_id'];
        $_SESSION['payment_values']['amount'] = $post_data['total_amount'];
        Session::put('order_infos', $request);
        #End to save these value  in session to pick in success page.

        $server_name = $request->root() . "/";
        $post_data['success_url'] = $server_name . "ssl_success";
        $post_data['fail_url'] = $server_name . "ssl_fail";
        $post_data['cancel_url'] = $server_name . "ssl_cancel";

        # CUSTOMER INFORMATION
        $user_details = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_email', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_id', $request->user_id)
                ->orderBy('registeruserdetails.registeruserdetails_id', 'DESC')
                ->first();
        $post_data['cus_name'] = $user_details->registeruser_firstname . '' . $user_details->registeruser_lastname;
        $post_data['cus_email'] = $user_details->registeruser_email;
        $post_data['cus_add1'] = $user_details->registeruser_address;
        $post_data['cus_add2'] = "";
        $post_data['cus_city'] = $user_details->registeruser_city;
        $post_data['cus_state'] = "";
        $post_data['cus_postcode'] = $user_details->registeruser_zipcode;
        $post_data['cus_country'] = "Bangladesh";
        $post_data['cus_phone'] = $user_details->registeruser_phone;
        $post_data['cus_fax'] = "";



        # SHIPMENT INFORMATION
        $post_data['ship_name'] = $user_details->registeruser_firstname . '' . $user_details->registeruser_lastname;
        $post_data['ship_add1 '] = $user_details->registeruser_address;
        $post_data['ship_add2'] = "";
        $post_data['ship_city'] = $user_details->registeruser_city;
        $post_data['ship_state'] = "";
        $post_data['ship_postcode'] = $user_details->registeruser_zipcode;
        $post_data['ship_country'] = "Bangladesh";

        # OPTIONAL PARAMETERS
        $post_data['value_a'] = "ref001";
        $post_data['value_b'] = "ref002";
        $post_data['value_c'] = "ref003";
        $post_data['value_d'] = "ref004";

        //Before  going to initiate the payment order status need to update as Pending.
        $update_product = DB::table('conforder')->where('conforder_id', $post_data['tran_id'])->update(['conforder_status' => 'Pending']);

        $sslc = new SSLCommerz();
        # initiate(Transaction Data , false: Redirect to SSLCOMMERZ gateway/ true: Show all the Payement gateway here )
        $payment_options = $sslc->initiate($post_data, false);
        if (!is_array($payment_options)) {
            print_r($payment_options);
            $payment_options = array();
        }
    }

    public function ssl_success(Request $request) {
        $sslc = new SSLCommerz();

        #Start to received these value from session. which was saved in index function.
        $tran_id = $_SESSION['payment_values']['tran_id'];
        $total = $_SESSION['payment_values']['amount'];
        #End to received these value from session. which was saved in index function.
        #Check order status in order tabel against the transaction id or order id.
        $order_detials = DB::table('conforder')->where('conforder_id', $tran_id)->select('conforder_id','conforder_tracknumber','registeruser_id', 'shoppingcart_id', 'conforder_status')->first();

        if ($order_detials->conforder_status == 'Pending') {
            $validation = $sslc->orderValidate($tran_id, $total, 'BDT', $request->all());
            if ($validation == TRUE) {
                $update_product = DB::table('conforder')->where('conforder_id', $tran_id)->update(['conforder_status' => 'Processing']);
				//get customer details
				$user_details = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_email', 'registeruser.registeruser_firstname', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_id', $order_detials->registeruser_id)
                ->orderBy('registeruserdetails.registeruserdetails_id', 'DESC')
                ->first();
				//order confirmation mail send
				$email_data['email'] = $user_details->registeruser_email;
				$email_data['track_number'] = $order_detials->conforder_tracknumber;
				$email_data['first_name'] = $user_details->registeruser_firstname;
				$email_data['last_name'] = $user_details->registeruser_lastname;
				$email_data['address'] = $user_details->registeruser_address;
				$email_data['city'] = $user_details->registeruser_city;
				$email_data['zip'] = $user_details->registeruser_zipcode;
				$email_data['country'] = $user_details->registeruser_country;
				$email_data['phone'] = $user_details->registeruser_phone;
				$email_data['order_date'] = date('Y-m-d');
				//get shopping cart info
				$shoppingcart_info= DB::table('shoppingcart')->where('shoppingcart_id',$order_detials->shoppingcart_id)->first();
				$email_data['shipping_Charge'] = $shoppingcart_info->Shipping_Charge;
				$email_data['shoppingcart_subtotal'] = $shoppingcart_info->shoppingcart_subtotal;
				$email_data['shoppingcart_total'] = $shoppingcart_info->shoppingcart_total;
				$email_data['order_product'] = DB::table('shoppinproduct')
						->join('product', '.shoppinproduct.product_id', '=', 'product.product_id')
						->select('shoppinproduct.cart_image','shoppinproduct.productalbum_name','shoppinproduct.prosize_name', 'shoppinproduct.shoppinproduct_quantity', 'shoppinproduct.product_price', 'product.product_name', 'product.product_styleref')
						->where('shoppingcart_id', $order_detials->shoppingcart_id)
						->get();

				/*Mail::send('emails.order_email', $email_data, function($message) use($email_data) {
					$message->from('order@urban-truth.com', 'Urban Truth');
					$message->to($email_data['email']);
					$message->bcc('pride.orderlog@gmail.com', 'Pride Group');
					$message->subject('Order Comfirmation');
				}); */
				 $email_data['conforder_tracknumber'] = $order_detials->conforder_tracknumber;
				 $email_data['shoppingcart_id']=$order_detials->shoppingcart_id;
				 Cart::instance('products')->destroy();
				 return redirect('/order-confirmed')->with('email_data',$email_data);
            } else {
                $update_product = DB::table('conforder')->where('conforder_id', $tran_id)->update(['conforder_status' => 'Failed']);
                echo "validation Fail";
            }
        } else if ($order_detials->conforder_status == 'Processing' || $order_detials->conforder_status == 'Closed') {
            echo "Transaction is successfully Complete";
        } else {
            #That means something wrong happened. You can redirect customer to your product page.
            echo "Invalid Transaction";
        }
    }

    public function ssl_fail(Request $request) {
        $tran_id = $_SESSION['payment_values']['tran_id'];
        $order_detials = DB::table('conforder')->where('conforder_id', $tran_id)->select('conforder_id', 'conforder_status')->first();
        if ($order_detials->conforder_status == 'Pending') {
            $update_product = DB::table('conforder')->where('conforder_id', $tran_id)->update(['conforder_status' => 'Failed']);
            return redirect('/orderpreview')->with('ssl_cancel','Your Transaction is Falied.please select an aonther payment method.');
        } else if ($order_detials->conforder_status == 'Processing' || $order_detials->conforder_status == 'Closed') {
            return redirect('/order-confirmed')->with('ssl_success','Transaction is already Successful');
        } else {
            echo "Transaction is Invalid";
        }
    }

    public function ssl_cancel(Request $request) {
        $tran_id = $_SESSION['payment_values']['tran_id'];
        $order_detials = DB::table('conforder')->where('conforder_id', $tran_id)->select('conforder_id', 'conforder_status')->first();
        if ($order_detials->conforder_status == 'Pending') {
            $update_product = DB::table('conforder')->where('conforder_id', $tran_id)->update(['conforder_status' => 'Cancelled']);
            return redirect('/orderpreview')->with('ssl_cancel','Your payment process failed.please select an aonther payment method.');
        } else if ($order_detials->conforder_status == 'Processing' || $order_detials->conforder_status == 'Closed') {
			return redirect('/order-confirmed')->with('ssl_success','Transaction is already Successful');
          //  echo "Transaction is already Successful";
        } else {
            echo "Transaction is Invalid";
        }
    }

    public function ssl_ipn(Request $request) {
        if ($request->input('tran_id')) { #Check transation id is posted or not.
            $tran_id = $request->input('tran_id');
            #Check order status in order tabel against the transaction id or order id.
            $order_details = DB::table('conforder')
                            ->where('conforder_id', $tran_id)
                            ->select('conforder_id', 'conforder_status')->first();
            $total = $_SESSION['payment_values']['amount'];
            if ($order_details->conforder_status == 'Pending') {
                $sslc = new SSLCommerz();
                $validation = $sslc->orderValidate($tran_id, $total, 'BDT', $request->all());
                if ($validation == TRUE) {
                    /*
                      That means IPN worked. Here you need to update order status
                      in order table as Processing or Complete.
                      Here you can also sent sms or email for successfull transaction to customer
                     */
                    $update_product = DB::table('conforder')
                            ->where('conforder_id', $tran_id)
                            ->update(['conforder_status' => 'Processing']);

                    echo "Transaction is successfully Complete";
                } else {
                    /*
                      That means IPN worked, but Transation validation failed.
                      Here you need to update order status as Failed in order table.
                     */
                    $update_product = DB::table('conforder')
                            ->where('conforder_id', $tran_id)
                            ->update(['conforder_status' => 'Failed']);
                    echo "validation Fail";
                }
            } else if ($order_details->conforder_status == 'Processing' || $order_details->conforder_status == 'Complete') {

                #That means Order status already updated. No need to udate database.

                echo "Transaction is already successfully Complete";
            } else {
                #That means something wrong happened. You can redirect customer to your product page.

                echo "Invalid Transaction";
            }
        } else {
            echo "Inavalid Data";
        }
    }

}
