<?php

namespace App\Http\Controllers\user;

use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;
use App\Registeruser;
use App\Registeruserdetails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller {

    use RegistersUsers;

//    protected $redirectTo = '/home';
//
//    public function __construct() {
//        $this->middleware('guest');
//    }

    public function create(Request $request) {

        $vaildation = Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    //  'regi_email' => 'required|max:10|unique:users,user_name',
                    'regi_email' => 'required|email|unique:users,email',
                    'confirm_email' => 'required|same:regi_email',
                    'regi_password' => 'required|min:4',
                    'password_confirmation' => 'required|same:regi_password'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $hashedPassword = Hash::make($request->regi_password);
            $data = [
                'name' => $request->first_name,
                'email' => $request->regi_email,
                'password' => $hashedPassword
            ];
            $user_id = DB::table('users')->insertGetId($data);
            // $user_login = ['email' => $request->email, 'password' => $request->password];
            $registerdata['registeruser_firstname'] = $request->first_name;
            $registerdata['registeruser_lastname'] = $request->last_name;
            $registerdata['registeruser_email'] = $request->regi_email;
            //  $registerdata['registeruser_dob'] = $request->registeruser_dob;
            $registerdata['registeruser_password'] = $request->regi_password;
            $registerdata['registeruser_login_id'] = $user_id;
            $registeruser_id = DB::table('registeruser')->insertGetId($registerdata);
            Auth::loginUsingId($user_id);
//            $new_arrival = DB::table('product')
//                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
//                    ->select('product.product_id', 'product.product_name', 'product.product_price', 'product.product_pricediscounted', 'product.discount_product_price', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
//                    //  ->whereRaw('DATEDIFF(product_insertion_date,current_date) < 120')
//                    ->skip(5)->take(2)
//                    ->where('product_active_deactive', 0)
//                    ->orderBy('product.product_order', 'ASC')
//                    ->orderBy('product.product_id', 'desc')
//                    ->groupBy('product.product_id')
//                    ->get();
//            $email_data['new_arrival'] = $new_arrival;
//            $email_data['first_name'] = $request->first_name;
//            $email_data['last_name'] = $request->last_name;
//            $email_data['email'] = $request->regi_email;
//            $email_data['name'] = $request->first_name;
//            Mail::send('emails.welcome', $email_data, function($message) use($email_data) {
//                $message->to($email_data['email']);
//                $message->subject('Registration Comfirmation');
//            });
            //  return redirect('/user-details')->with('user_id', $user_id);
            return redirect()->route('user-details', ['id' => $user_id]);
        }
    }

    public function UserDetails($user_id) {
        $check_user_id = Auth::user()->id;
        if ($check_user_id == $user_id) {
            $data['login_user_id'] = $user_id;
            $data['user_info'] = DB::table('registeruser')->where('registeruser_login_id', $user_id)->first();
            return view('user.user_details_form', $data);
        } else {
            
        }
    }

    public function SaveUserDetails(Request $request) {
        $vaildation = Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'Shipping_ddlcountry' => 'required',
                    'registeruser_address' => 'required',
                    'registeruser_city' => 'required|string',
                    'registeruser_zipcode' => 'numeric|min:4',
                    'registeruser_phone' => 'required|numeric|min:11'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $userdetails['registeruser_login_id'] = $request->login_user_id;
            $userdetails['registeruser_id'] = $request->registeruser_id;
            $userdetails['registeruser_address'] = $request->registeruser_address;
            $userdetails['registeruser_country'] = $request->Shipping_ddlcountry;
            $userdetails['registeruser_city'] = $request->registeruser_city;
            $userdetails['registeruser_zipcode'] = $request->registeruser_zipcode;
            $userdetails['registeruser_phone'] = $request->registeruser_phone;
            DB::table('registeruserdetails')->insertGetId($userdetails);
            $registeruser_ip = request()->ip();
            $check_user_id = Auth::user()->id;
            $offer = DB::table('registeruser')
                    ->where('registeruser_ip', $registeruser_ip)
                    ->where('registeruser_login_id', '!=', $request->login_user_id)
                    ->first();
            if ($offer == null) {
                $duplicate = DB::table('registeruserdetails')
                        ->where('registeruser_phone', $request->registeruser_phone)
                        ->where('registeruser_login_id', '!=', $request->login_user_id)
                        ->first();
                if ($duplicate == null) {
                    $u['offer'] = 0;
                } else {
                    $u['offer'] = 1;
                    DB::table('registeruser')->where('registeruser_login_id', $check_user_id)->update($u);
                }
            } else {
                $u['offer'] = 1;
                DB::table('registeruser')->where('registeruser_login_id', $check_user_id)->update($u);
            }
            return redirect()->intended('/checkout');
            /*
              $userdetails['registeruser_login_id'] = $request->login_user_id;
              $userdetails['registeruser_id'] = $request->registeruser_id;
              $userdetails['registeruser_address'] = $request->registeruser_address;
              $userdetails['registeruser_country'] = $request->Shipping_ddlcountry;
              $userdetails['registeruser_city'] = $request->registeruser_city;
              $userdetails['registeruser_zipcode'] = $request->registeruser_zipcode;
              $userdetails['registeruser_phone'] = $request->registeruser_phone;
              DB::table('registeruserdetails')->insertGetId($userdetails);
              return redirect()->intended('/checkout'); */
        }
        //  dd($request);
    }

    public function edit($id) {
        $data['user'] = DB::table('users')->where('id', $id)->first();
        $data['role'] = DB::table('roles')->get();
        return view('user.user_edit_form')->with($data);
    }

    public function update(Request $request) {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'user_name' => 'required|max:10|unique:users,user_name,' . $request->id,
            'email' => 'required|email|unique:users,email,' . $request->id,
            'role_id' => 'required'
        ];
        $vaildation = Validator::make($request->all(), $rules);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $data = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'user_name' => $request->user_name,
                'email' => $request->email,
                //'password'=>Hash::make($request->password),
                'role_id' => $request->role_id
            ];

            $result = DB::table('users')->where('id', $request->id)->update($data);
            if ($result) {
                return redirect()->back()->with('save', 'User update successfully');
            } else {
                return redirect()->back()->with('error', 'User not update');
            }
        }
    }

    public function UpdateShippingInfo(Request $request) {
        //    dd($request);
        //    exit();
        $userdata = [
            'registeruser_firstname' => $request->firstname,
            'registeruser_lastname' => $request->lastname
        ];
        DB::table('registeruser')->where('registeruser_login_id', $request->user_login_id)->update($userdata);
        $userdetailsdata = [
            'registeruser_address' => $request->address,
            'registeruser_country' => $request->country,
            'registeruser_city' => $request->City,
            'registeruser_zipcode' => $request->zipcode,
            'registeruser_phone' => $request->mobile
        ];
        $result = DB::table('registeruserdetails')->where('registeruser_login_id', $request->user_login_id)->update($userdetailsdata);
        return redirect()->back()->with('update', 'Your shipping info update successfully!.');
    }

    public function ForgotPassword($user_enid) {
        $decrypted = Crypt::decryptString($user_enid);
        $data['user_id'] = $decrypted;
        return view('auth.reset_password', $data);
    }

    public function ResetPassword(Request $request) {
        $email = $request->email;
        $check_email = DB::table('users')->where('email', $email)->first();
        if ($check_email) {
            $user_id = $check_email->id;
            $encrypted = Crypt::encryptString($user_id);

            $folder_path = "https://www.pride-limited.com/forgot-password/$encrypted";
            $message = 'Please clicked on this link to reset your password.<a href="' . $folder_path . '">Click Here</a>';
            $to = $email;
            $sub = "Reset Password";
            $from = "Pride Limited<admin@pride-limited.com>";
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            //$headers .= 'To: '.$to."\r\n";		
            $headers .= 'From: ' . $from . "\r\n";
            $headers .= 'Bcc: salam.pustcse@gmail.com';
            mail($to, $sub, $message, $headers);
            return redirect()->back()->with('success', 'Please check your email.');
        } else {
            return redirect()->back()->with('error', 'Your email is not correct.Please enter your correct email.');
        }
    }

    public function UpdatePassword(Request $request) {
        $user_id = $request->user_id;
        $hashedPassword = Hash::make($request->password);
        $data['password'] = $hashedPassword;
        $result = DB::table('users')->where('id', $user_id)->update($data);
        Auth::loginUsingId($user_id);
        return redirect()->route('checkout');
    }

    public function delete($id) {
        $result = DB::table('users')->where('id', $id)->delete();
        if ($result) {
            return redirect()->back()->with('save', 'Delete successfully');
        } else {
            return redirect()->back()->with('error', 'User not delete');
        }
    }

}
