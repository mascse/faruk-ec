<?php

namespace App\Http\Controllers\user;

use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use Illuminate\Support\Facades\Hash;
use Session;

class TrackOrderController extends Controller {

    //
    public function __construct() {
        $this->middleware('auth');
    }

    public function MyAccount() {
       $login_id = Auth::user()->id;
        $data['login_user_info'] = DB::table('users')->where('id', $login_id)->first();
        $get_user_id = DB::table('registeruser')
                ->select('registeruser.registeruser_id')
                ->where('registeruser.registeruser_login_id', $login_id)
                ->first();
        $register_user_id = $get_user_id->registeruser_id;
        $data['user_info'] = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_firstname','registeruserdetails.registeruserdetails_id', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_id', $register_user_id)
                ->orderBy('registeruserdetails.registeruserdetails_id', 'DESC')
                ->first();
        if ($data['user_info']  == null) {
            session()->flash('save', 'Please enter your address first.');
            return redirect()->route('user-details', ['id' => $login_id]);
        } else {
        $user_id = $data['user_info']->registeruser_id;
        $data['user_details'] = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruserdetails.registeruserdetails_id', 'registeruser.registeruser_email', 'registeruser.registeruser_firstname', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_id', $register_user_id)
                ->whereNotNull('registeruserdetails.registeruser_address')
                ->orderBy('registeruserdetails.registeruserdetails_id', 'DESC')
                ->first();
         return view('user.user_account', $data);
        }
    }
    
    public function MyAccountInfo() {
        $login_id = Auth::user()->id;
        $data['login_user_info'] = DB::table('users')->where('id', $login_id)->first();
        $get_user_id = DB::table('registeruser')
                ->select('registeruser.registeruser_id')
                ->where('registeruser.registeruser_login_id', $login_id)
                ->first();
        $register_user_id = $get_user_id->registeruser_id;
        $data['user_info'] = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_firstname', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_id', $register_user_id)
                ->whereNotNull('registeruser_address')
                ->orderBy('registeruserdetails.registeruserdetails_id', 'DESC')
                ->first();
        $user_id = $data['user_info']->registeruser_id;
        $data['user_details'] = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_email', 'registeruser.user_gender', 'registeruser.registeruser_year', 'registeruser.registeruser_month', 'registeruser.registeruser_day', 'registeruser.registeruser_firstname', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_id', $register_user_id)
                ->orderBy('registeruserdetails.registeruserdetails_id', 'DESC')
                ->first();
        return view('user.account_info', $data);
    }

    public function AccountEmailChange() {
        $login_id = Auth::user()->id;
        $data['login_user_info'] = DB::table('users')->where('id', $login_id)->first();
        $get_user_id = DB::table('registeruser')
                ->select('registeruser.registeruser_id')
                ->where('registeruser.registeruser_login_id', $login_id)
                ->first();
        return view('user.change_email', $data);
    }

    public function UpdateAccountEmail(Request $request) {
        $messages = [
            'email.required' => 'please insert your vaild email address.',
        ];
        $vaildation = Validator::make($request->all(), [
                    'email' => 'required|unique:users,email',
                        ], $messages);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $data['email'] = $request->email;
            $update = DB::table('users')->where('id', $request->registeruser_login_id)->update($data);
            if ($update) {
                $data_register_tbl['registeruser_email'] = $request->email;
                DB::table('registeruser')->where('registeruser_login_id', $request->registeruser_login_id)->update($data_register_tbl);
                return redirect()->back()->with('save', 'Email updated successfully');
            } else {
                return redirect()->back()->with('save', 'Email not updated.');
            }

            //  dd($request);
        }
    }

    public function AccountChangePassword() {
        $login_id = Auth::user()->id;
        $data['login_user_info'] = DB::table('users')->where('id', $login_id)->first();
        $get_user_id = DB::table('registeruser')
                ->select('registeruser.registeruser_id')
                ->where('registeruser.registeruser_login_id', $login_id)
                ->first();
        return view('user.change_password', $data);
    }

    public function UpdatePassword(Request $request) {
		
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        if (Hash::check($request->password, $user->password)) {
            $hashedPassword = Hash::make($request->newPassword);
            $data['password'] = $hashedPassword;
            $result = DB::table('users')->where('id', $id)->update($data);
            $request->session()->flash('success', 'Password changed');
            return redirect('/account/change-password');
        } else {
            $request->session()->flash('error', 'Old password does not match');
            return redirect('/account/change-password');
        }
    }

    public function EditAccount(Request $request) {
        $messages = [
            'gender.required' => 'please select your gender.',
            'day.required' => 'required',
            'month.required' => 'required',
            'Year.required' => 'required',
        ];
        $vaildation = Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'gender' => 'required',
                    'email' => 'unique:users,email',
                    'day' => 'required|integer|between:1,31',
                    'month' => 'required|integer|between:1,12',
                    'Year' => 'required|integer|between:1910,2014'
                        ], $messages);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $editdata['registeruser_firstname'] = $request->first_name;
            $editdata['registeruser_lastname'] = $request->last_name;
            $editdata['user_gender'] = $request->gender;
            $editdata['registeruser_day'] = $request->day;
            $editdata['registeruser_month'] = $request->month;
            $editdata['registeruser_year'] = $request->Year;
            DB::table('registeruser')->where('registeruser_id', $request->registeruser_id)->update($editdata);
            return redirect()->back()->with('save', 'Account updated successfully');
            //  dd($editdata);
        }
    }

    public function AddressBook() {
        $login_id = Auth::user()->id;
        $data['login_user_info'] = DB::table('users')->where('id', $login_id)->first();
        $get_user_id = DB::table('registeruser')
                ->select('registeruser.registeruser_id')
                ->where('registeruser.registeruser_login_id', $login_id)
                ->first();
        $register_user_id = $get_user_id->registeruser_id;
        $data['user_details'] = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_email', 'registeruser.registeruser_firstname', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruserdetails_id', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_id', $register_user_id)
                ->whereNotNull('registeruserdetails.registeruser_address')
                ->orderBy('registeruserdetails.registeruserdetails_id', 'DESC')
                ->first();
        $data['address_book'] = DB::table('address_book')->where('registeruser_id', $register_user_id)->get();
        return view('user.address_book', $data);
    }

    public function AddressCreate() {
        $login_id = Auth::user()->id;
        $data['login_user_info'] = DB::table('users')->where('id', $login_id)->first();
        $get_user_id = DB::table('registeruser')
                ->select('registeruser.registeruser_id')
                ->where('registeruser.registeruser_login_id', $login_id)
                ->first();
        $register_user_id = $get_user_id->registeruser_id;
        $data['user_details'] = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_email', 'registeruser.registeruser_firstname', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruserdetails_id', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_id', $register_user_id)
                ->whereNotNull('registeruserdetails.registeruser_address')
                ->orderBy('registeruserdetails.registeruserdetails_id', 'DESC')
                ->first();
        return view('user.address_create', $data);
    }

    public function SaveAddress(Request $request) {
        $messages = [
            'address1.required' => 'The Address field is required.',
        ];
        $vaildation = Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'phone' => 'required|numeric',
                    'address1' => 'required',
                    'RegionList' => 'required',
                    'CityList' => 'required'
                        ], $messages);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $data['registeruser_id'] = $request->registeruser_id;
            $data['first_name'] = $request->first_name;
            $data['last_name'] = $request->last_name;
            $data['phone'] = $request->phone;
            $data['additional_phone'] = $request->additional_phone;
            $data['address_1'] = $request->address1;
            $data['address_2'] = $request->address2;
            $data['region'] = $request->RegionList;
            $data['city'] = $request->CityList;
            $data['zipcode'] = $request->zipcode;
            DB::table('address_book')->insert($data);
            $request->session()->flash('save', 'New address saved successfully!.');
            return redirect('/address-book');
        }
    }

    public function EditAddress($address_id) {
        $data['user_details'] = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_email', 'registeruser.registeruser_firstname', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruserdetails_id', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruserdetails.registeruserdetails_id', $address_id)
                ->first();
        return view('user.edit_address', $data);
    }

    public function UpdateAddress(Request $request) {
        $messages = [
            'address1.required' => 'The Address field is required.',
        ];
        $vaildation = Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'phone' => 'required|numeric',
                    'address1' => 'required',
                    'city' => 'required',
                    'zipcode' => 'numeric'
                        ], $messages);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $data['first_name'] = $request->first_name;
            $data['last_name'] = $request->last_name;
            $data['phone'] = $request->phone;
            $data['additional_phone'] = $request->additional_phone;
            $data['address1'] = $request->address1;
            $data['address2'] = $request->address2;
            $data['city'] = $request->city;
            $data['zipcode'] = $request->zipcode;
            $registeruser_data['registeruser_firstname'] = $request->first_name;
            $registeruser_data['registeruser_lastname'] = $request->last_name;
            DB::table('registeruser')->where('registeruser_id', $request->registeruser_id)->update($registeruser_data);
            $registerdetail_data['registeruser_address'] = $request->address1;
            $registerdetail_data['registeruser_phone'] = $request->phone;
            $registerdetail_data['registeruser_city'] = $request->city;
            $registerdetail_data['registeruser_zipcode'] = $request->zipcode;
            DB::table('registeruserdetails')->where('registeruser_id', $request->registeruser_id)->update($registerdetail_data);
            return redirect()->back()->with('save', 'Default address set successfully!');
        }
    }

    public function EditAdditionalAddress($address_id) {
        $data['address'] = DB::table('address_book')->where('address_id', $address_id)->first();
        return view('user.edit_additional_address', $data);
    }

    public function UpdateAdditionalAddress(Request $request) {
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['phone'] = $request->phone;
        $data['additional_phone'] = $request->additional_phone;
        $data['address_1'] = $request->address_1;
        $data['address_2'] = $request->address_2;
        $data['city'] = $request->city;
        $data['zipcode'] = $request->zipcode;
        DB::table('address_book')->where('address_id', $request->address_id)->update($data);
        return redirect()->back()->with('save', 'Address updated successfully!');
    }

    public function SetDefaultAddress($address_id) {
        $login_id = Auth::user()->id;
        $get_user_id = DB::table('registeruser')
                ->select('registeruser.registeruser_id')
                ->where('registeruser.registeruser_login_id', $login_id)
                ->first();
        $register_user_id = $get_user_id->registeruser_id;
        $user_details = DB::table('registeruser')
                ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                ->select('registeruser.registeruser_id', 'registeruser.registeruser_email', 'registeruser.registeruser_firstname', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruserdetails_id', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                ->where('registeruser.registeruser_id', $register_user_id)
                ->whereNotNull('registeruserdetails.registeruser_address')
                ->orderBy('registeruserdetails.registeruserdetails_id', 'DESC')
                ->first();
        Session::put('first_name', $user_details->registeruser_firstname);
        Session::put('last_name', $user_details->registeruser_lastname);
        Session::put('phone', $user_details->registeruser_phone);
        Session::put('address_1', $user_details->registeruser_address);
        Session::put('region', $user_details->registeruser_city);
        Session::put('zipcode', $user_details->registeruser_zipcode);
        Session::save();
        $first_name = session('first_name');
        $last_name = session('last_name');
        $phone = session('phone');
        $address_1 = session('address_1');
        $region = session('region');
        $zipcode = session('zipcode');
        //$request->session()->forget('admin_id');
        $address = DB::table('address_book')->where('address_id', $address_id)->first();
        $registeruser_data['registeruser_firstname'] = $address->first_name;
        $registeruser_data['registeruser_lastname'] = $address->last_name;
        DB::table('registeruser')->where('registeruser_id', $register_user_id)->update($registeruser_data);
        $registerdetail_data['registeruser_address'] = $address->address_1;
        $registerdetail_data['registeruser_phone'] = $address->phone;
        $registerdetail_data['registeruser_city'] = $address->region;
        $registerdetail_data['registeruser_zipcode'] = $address->zipcode;
        DB::table('registeruserdetails')->where('registeruser_id', $register_user_id)->update($registerdetail_data);
        $address_data['first_name'] = $first_name;
        $address_data['last_name'] = $last_name;
        $address_data['phone'] = $phone;
        $address_data['address_1'] = $address_1;
        $address_data['region'] = $region;
        $address_data['zipcode'] = $zipcode;
        DB::table('address_book')->where('address_id', $address_id)->update($address_data);
        session()->forget('first_name');
        session()->forget('last_name');
        session()->forget('phone');
        session()->forget('address_1');
        session()->forget('region');
        session()->forget('zipcode');
        return redirect()->back()->with('save', 'Default address set successfully!');
    }

    public function AddressDelete($address_id) {
        DB::table('address_book')->where('address_id', $address_id)->delete();
        return redirect()->back()->with('error', 'Address deleted successfully!');
    }

    public function UserTrackOrder() {
        $user_id = Auth::user()->id;
        $data['user_details'] = DB::table('registeruser')
                        ->join('registeruserdetails', 'registeruser.registeruser_login_id', '=', 'registeruserdetails.registeruser_login_id')
                        ->select('registeruser.registeruser_id', 'registeruser.registeruser_email', 'registeruser.registeruser_firstname', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                        ->where('registeruser.registeruser_login_id', $user_id)->first();
        if ($data['user_details']  == null) {
            session()->flash('save', 'Please enter your address first.');
            return redirect()->route('user-details', ['id' => $user_id]);
        } else {
        $register_id = $data['user_details']->registeruser_id;
        $data['orderlist'] = DB::table('conforder')
                ->join('shoppingcart', 'conforder.shoppingcart_id', '=', 'shoppingcart.shoppingcart_id')
                ->select('conforder.conforder_id', 'conforder.shoppingcart_id', 'conforder.conforder_tracknumber', 'conforder.conforder_placed_date', 'conforder.conforder_status', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.shoppingcart_total')
                ->where('conforder.registeruser_id', $register_id)
                ->orderBy('conforder.conforder_id', 'desc')
                ->get();
          return view('user.track_order', $data);
        }
    }

    public function orderPreview($confo_id, $shopping_cart_id) {
        $data['confo_info'] = DB::table('conforder')
                ->where('conforder_id', $confo_id)
                ->first();
        $data['order_info'] = DB::table('shoppingcart')
                ->where('shoppingcart_id', $shopping_cart_id)
                ->first();
        $data['order_product'] = DB::table('shoppinproduct')
                ->join('product', '.shoppinproduct.product_id', '=', 'product.product_id')
                ->select('shoppinproduct.cart_image', 'shoppinproduct.shoppinproduct_quantity', 'shoppinproduct.product_price', 'product.product_name', 'product.product_code')
                ->where('shoppingcart_id', $shopping_cart_id)
                ->get();
        return view('user.order_view', $data);
    }
	
	public function OrderTrackByECR($ecr){
		$curl = curl_init();
		$ecr = [
                'parcel' => 'track ',
                'ecr' => $ecr
            ];
			
			curl_setopt_array($curl, array(
                CURLOPT_URL => "http://ecourier.com.bd/apiv2",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 3000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $ecr,
                CURLOPT_HTTPHEADER => array(
                    "API_SECRET:zReJG",
                    "API_KEY: 66eY",
                    "USER_ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			$delivery_response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
				 dd($delivery_response);
			}
	}

}
