<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //    $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $pride_girls = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img_medium')
                ->whereIn('product.procat_id', [5])
                ->where('product.product_active_deactive', 0)
                ->groupBy('product.product_id')
                ->orderBy('product.product_id', 'DESC')
                ->orderBy('productimg.productimg_id', 'ASC')
                ->limit(8)
                ->get();
        $signature = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img_medium')
//                ->where('product.procat_id', 5)
                ->whereIn('product.procat_id', [9])
                ->groupBy('product.product_id')
                ->orderBy('product.product_id', 'DESC')
                ->limit(8)
                ->get();
        $procat_id = "5,6,7,9,10,11,12,13,14,15,16,17,18,19";
        $productlist = DB::select(DB::raw("SELECT product.product_id,product_price,product_code,product.product_img_thm,product_name,productimg.productimg_img,productimg.productimg_img_medium,productalbum.productalbum_name FROM product 
                    INNER JOIN productalbum ON product.product_id=productalbum.product_id 
                    inner join subprocat on product.subprocat_id = subprocat.subprocat_id 
                    INNER JOIN productimg on productalbum.productalbum_id=productimg.productalbum_id
                    where product.product_active_deactive=0 AND product.procat_id in($procat_id) AND DATEDIFF(CURRENT_DATE,STR_TO_DATE(product.product_insertion_date, '%d-%m-%Y'))< 120 GROUP BY product.product_id  order by product.product_id desc"));
        //  dd($productlist);
        $data['pride_girls'] = $pride_girls;
        $data['signature_collection'] = $signature;
        $data['product_list'] = $productlist;
        return view('home', $data);
    }

    public function FlagunCollection2019($title = null, $pro_id = null) {
        if ($pro_id == null) {
            $productlist = DB::table('product')
                    ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                    ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                    ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                    ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                    ->where('product_active_deactive', 0)
                    ->where('isSpecial', 4)
                    ->orderBy('product.sold', 'DESC')
                    ->orderBy('product.product_id', 'desc')
                    ->groupBy('product.product_id')
                    ->paginate(21);
        } else {
            if ($pro_id == 17) {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->whereIn('product.procat_id', [17, 18])
                        ->where('isSpecial', 4)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.sold', 'DESC')
                        ->groupBy('product.product_id')
                        ->paginate(21);
            } elseif ($pro_id == 9) {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->whereIn('product.procat_id', [5, 9, 10, 11, 12, 13, 20, 7])
                        ->where('isSpecial', 4)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.sold', 'DESC')
                        // ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->paginate(12);
            } else {
                $productlist = DB::table('product')
                        ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                        ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                        ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                        ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                        ->where('product.procat_id', $pro_id)
                        ->where('isSpecial', 4)
                        ->where('product_active_deactive', 0)
                        ->orderBy('product.product_order', 'ASC')
                        ->orderBy('product.product_id', 'desc')
                        ->groupBy('product.product_id')
                        ->get();
            }
        }
        $data['title'] = $title;
        $data['product_list'] = $productlist;
        return view('falgun_collection_2018', $data);
    }

    public function LimitedSari() {
        $productlist = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productimg.productimg_img_medium', 'productalbum.productalbum_name', 'product.product_name', 'product.product_img_thm', 'productalbum.productalbum_name', 'productalbum.productalbum_img')
                ->where('product.procat_id', 9)
                ->where('spcollection', 1)
                ->where('product_active_deactive', 0)
                ->orderBy('product.product_order', 'ASC')
                ->orderBy('product.product_id', 'desc')
                ->groupBy('product.product_id')
                ->paginate(16);
        $data['product_list'] = $productlist;
        return view('limited_edition_sari', $data);
    }

    public function ComingSoonPage() {
        return view('coming_soon');
    }

    public function AboutUs() {
        return view('footer_page.about_us_new');
    }

    public function WorkWithUs() {
        return view('footer_page.work_with_us');
    }

    public function ContactUs() {
        return view('footer_page.contact_us');
    }

    public function ExchangePolicy() {
        return view('footer_page.exchange_policy');
    }

    public function Faq() {
        return view('footer_page.faq');
    }

    public function StoreLocator() {
        return view('footer_page.store_locator');
    }

    public function PrivacyCookies() {
        return view('footer_page.privacy_cookies');
    }

    public function HowToOrder() {
        return view('footer_page.how_to_order');
    }
	
	public function SiteMap(){
		return view('footer_page.sitemap');
	}

    public function search($key = null) {
        $search = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id', 'product.product_price', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img_medium')
                ->where('product.product_name', 'like', "%{$key}%")
                ->groupBy('product.product_id')
                ->orderBy('product.product_id', 'DESC')
                ->get();
        echo "<main class='main shop-page'>
                    <div class='container'>
                     <div class='row align-items-start'>
                      <div class='col-12 col-lg-12'>
                      <div class='row'>";
        if ($search->isEmpty()) {
            echo "
                       <div class='shop-st-header text-center'>
                           <span class='text-second'>Sorry, no matches found for" . " '$key'</span>
                       </div>";
        }
        $i = 0;
        foreach ($search as $get_result) {
            $i++;
            $product_name = str_replace(' ', '-', $get_result->product_name);
            $product_url = strtolower($product_name);
            if (!$search->isEmpty()) {
                echo "<div class='product-grid col-6 col-md-3'>
                        <div class='product-item row js-shop-item-product'>
                            <div class='product-item__grid col-5 col-sm-5 col-lg-4 mb-4'>
                                <div class='product-item__img'>
                                    <a class='product-item__img-link js-shop-img-flip d-block' href='https://www.pride-limited.com/shop/$product_url/color-$get_result->productalbum_name/$get_result->product_id'>
                                        <span class='front'>
                                          <img  src='https://www.pride-limited.com//storage/app/pgallery/$get_result->productimg_img_medium' width='255px' height='383px' alt='#'/>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class='product-item__list collapse js-list-show col-7 col-sm-7 col-lg-8'>
                                <div class='product-item__title h4 text-uppercase mb-4'>$get_result->product_name</div>
                                <div class='product-item__price'><span class='currency'>Tk </span><span>  $get_result->product_price</span></div>
                            </div>
                            <div class='product-item__bottom js-grid-show'>
                                <div class='product-item__title h4 text-uppercase mb-2'><a href='https://www.pride-limited.com/shop/$product_url/color-$get_result->productalbum_name/$get_result->product_id'>$get_result->product_name</a></div>
                                <div class='product-item__price mb-0'><span class='currency'>Tk </span><span>$get_result->product_price</span></div>
                            </div>
                        </div>
                    </div>";
            }
        }
        echo "</div>
                    </div>
                    </div>
                     </div>
                </main>";
    }

    public function SearchNew($key) {
        $data['search'] = DB::table('product')
                ->join('productalbum', 'product.product_id', '=', 'productalbum.product_id')
                ->join('productimg', 'productalbum.productalbum_id', '=', 'productimg.productalbum_id')
                ->select('product.product_id','product.product_code','product.product_price', 'product.product_code', 'product.product_name', 'productalbum.productalbum_name', 'productimg.productimg_img', 'productimg.productimg_img_medium')
                ->where('product.product_active_deactive', 0)
                ->where('product.product_name', 'like', "%{$key}%")
                ->orWhere('product.product_code', 'like', "%{$key}%")
                ->groupBy('product.product_id')
                ->orderBy('product.product_id', 'DESC')
                ->get();
        return view('search', $data);
    }
	
	

}
