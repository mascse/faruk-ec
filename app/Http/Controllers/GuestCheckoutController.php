<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Validator;
use Mail;
use Cart;
use Carbon\Carbon;
use App\Product;
use App\Productalbum;
use App\Shoppinproduct;
use App\Shoppingcart;
use App\Conforder;
use App\Registeruser;
use App\Registeruserdetails;
use App\Ordershipping;
use App\PromoCode;
// payment gateway
use App\Services\IPay;
use App\Services\SSLCommerz;

class GuestCheckoutController extends Controller {

    public function __construct() {
		$this->base_url = url('/');
        $this->ipay = new IPay();
        $this->sslc = new SSLCommerz();
    }
	
    public function index() {
        return view('guest_checkout');
    }

    public function SaveData(Request $request) {
        $vaildation = Validator::make($request->all(), [
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'address' => 'required',
                    'mobile_no' => 'required',
                    'RegionList' => 'required',
                    'region' => 'required',
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $user_id = 0;
            $get_offer = 0;
            //shopping cart save data
            $shoppingcart_info = new Shoppingcart();
            $shoppingcart_id = $shoppingcart_info->SaveData($request, $user_id, $get_offer);

            //shoppinproduct save data
            $shoppinproduct = new Shoppinproduct();
            $shoppinproduct->SaveData($request, $shoppingcart_id);
			
            //conforder save data
            $conforder_placed_date = Carbon::now();
            $conforder = new Conforder();
            $conforder_id = $conforder->SaveData($shoppingcart_id, $user_id, $request, $conforder_placed_date);
            $tracknumber = "PLORDER#100-" . $conforder_id;
            PromoCode::deactivate($request->promo_code);
			
            //Ordershipping data save
            $rdershipping = new Ordershipping();
            $rdershipping->SaveData($conforder_id, $shoppingcart_id, $user_id, $request);
			
            //update register offer
            //  $update_offer['offer'] = 1;
            //   DB::table('registeruser')->where('registeruser_login_id', $login_id)->update($update_offer);
            //$conforder_id = time().'abcd';
			//dd($conforder_id);
            $url = $this->paymentMethodHandle($request, $conforder_id);
            if ($url != false && $request->dmselect == 'iPay') {
                 $this->paymentMethodHandle($request, $conforder_id);
            }
            
            //order confirmation mail send
            $email_data = $this->SendInvoice($conforder_id);

            //redirect order confirm page
            //$this->OrderConfirmed()->with('email_data', $email_data);
            return redirect('/guest-order-confirmed')->with('email_data', $email_data);
        }
    }

    public function SendInvoice($tran_id) {
        //conforder details
        $order_detials = Conforder::find($tran_id);
        //get guest customer details
        $user_details = Ordershipping::where('conforder_id', $tran_id)->first();
		//dd($user_details);
        //order confirmation mail send
        $email_data['email'] = $user_details->email;
        $email_data['track_number'] = $order_detials->conforder_tracknumber;
        $email_data['first_name'] = $user_details->Shipping_txtfirstname;
        $email_data['last_name'] = $user_details->Shipping_txtlastname;
        $email_data['address'] = $user_details->Shipping_txtaddress1;
        $email_data['city'] = $user_details->Shipping_txtcity;
        $email_data['zip'] = $user_details->Shipping_txtzipcode;
        $email_data['country'] = $user_details->Shipping_ddlcountry;
        $email_data['phone'] = $user_details->Shipping_txtphone;
        $email_data['order_date'] = date('Y-m-d');

        //shopping cart info
        $shoppingcart_info = Shoppingcart::find($order_detials->shoppingcart_id);
        $email_data['shipping_Charge'] = $shoppingcart_info->shipping_charge;
        $email_data['shoppingcart_subtotal'] = $shoppingcart_info->shoppingcart_subtotal;
        $email_data['shoppingcart_total'] = $shoppingcart_info->shoppingcart_total;
        $email_data['get_offer'] = $shoppingcart_info->get_offer;

        //shopping product info
        $email_data['order_product'] = DB::table('shoppinproduct')
                ->join('product', '.shoppinproduct.product_id', '=', 'product.product_id')
                ->select('shoppinproduct.cart_image', 'shoppinproduct.productalbum_name', 'shoppinproduct.prosize_name', 'shoppinproduct.shoppinproduct_quantity', 'shoppinproduct.product_price', 'product.product_name', 'product.product_styleref')
                ->where('shoppingcart_id', $order_detials->shoppingcart_id)
                ->get();

        // send email to the customer
        /* Mail::send('emails.order_email', $email_data, function($message) use($email_data) {
          $message->from('order@pride-limited.com', 'Pride Limited');
          $message->to($email_data['email']);
          $message->bcc('pride.orderlog@gmail.com', 'Pride Group');
          $message->subject('Order Comfirmation');
          }); */

        $email_data['conforder_tracknumber'] = $order_detials->conforder_tracknumber;
        $email_data['shoppingcart_id'] = $order_detials->shoppingcart_id;

        //cart item delete
        Cart::instance('products')->destroy();

        return $email_data;
    }

    public function OrderConfirmed() {
        $data['shoppingcart_total'] = session("email_data")['shoppingcart_total'];
        $data['shipping_Charge'] = session("email_data")['shipping_Charge'];
        $data['track_number'] = session("email_data")['track_number'];
        $data['conforder_tracknumber'] = session("email_data")['conforder_tracknumber'];
        $shoppingcart_id = session("email_data")['shoppingcart_id'];
        $data['order_product'] = DB::table('shoppinproduct')
                ->join('product', '.shoppinproduct.product_id', '=', 'product.product_id')
                ->join('subprocat', 'product.subprocat_id', '=', 'subprocat.subprocat_id')
                ->select('product.product_code as sku', 'product.product_name as name', 'subprocat.subprocat_name as category', 'shoppinproduct.product_price as price', 'shoppinproduct.shoppinproduct_quantity as quantity')
                ->where('shoppingcart_id', $shoppingcart_id)
                ->get();
        return view('orderconfirmed', $data);
    }

    public function paymentMethodHandle($request, $referencedId) {
        if ($request->dmselect == 'iPay') {
            $url = $this->submitpayment($request, $referencedId);
            return $url;
        } elseif ($request->dmselect == 'ssl') {
			//dd($referencedId);
            $this->sslc->submitpayment($request, $referencedId);
        }
        return false;
    }
	
	public function submitpayment($request, $referencedId) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://app.ipay.com.bd/api/pg/order',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => '{"amount" : '.$request->shoppingcart_total.', "referenceId": "'.$referencedId.'", "description" : "Buy '.implode(', ', $request->product_name).' from '.$this->base_url.'", "successCallbackUrl" : "'.$this->base_url.'/iPay/success", "failureCallbackUrl" : "'.$this->base_url.'/iPay/failure", "cancelCallbackUrl" : "'.$this->base_url.'/iPay/cancel"}',
          CURLOPT_HTTPHEADER => array(
            "Authorization: ".IPAY_AUTHORIZATION,
            "Content-Type: application/json",
            "Postman-Token: c83eb032-1e5f-4950-94a7-7f1b13c148ee",
            "cache-control: no-cache"
          ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($response);
		$json->{'orderId'};
        $orderId = $json->{'orderId'};
        
        Session::put('orderId', $orderId);
        
        
        //Redirect user to iPay website for payment.
        return $json->paymentUrl;
        //return redirect($json->paymentUrl);
    }

    public function success(Request $request, $payment_method) {
        if ($payment_method == 'iPay') {
            $response = $this->ipay->ipay_success();
            if ($response != false) {
                //dd($response->referenceId);
                $update_product = DB::table('conforder')->where('conforder_id', $response->referenceId)->update(['conforder_status' => 'Processing']);
                $email_data = $this->SendInvoice($response->referenceId);
                return redirect('/guest-order-confirmed')->with('email_data', $email_data);
            }
        } elseif ($payment_method == 'ssl') {
            $conforder_id = $this->sslc->ssl_success($request);
            if ($conforder_id != false) {
                $update_product = DB::table('conforder')->where('conforder_id', $conforder_id)->update(['conforder_status' => 'Processing']);
                $email_data = $this->SendInvoice($conforder_id);
                return redirect('/guest-order-confirmed')->with('email_data', $email_data);
            }
        } else {
            return 'Unknown payment method';
        }
        return false;
    }

}
