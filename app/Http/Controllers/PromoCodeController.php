<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PromoCode;
use Auth;
use DB;

class PromoCodeController extends Controller {

    public function promoCodeCheck($code = null) {
        $promo_code = null;
        if ($code !== null)
            $promo_code = PromoCode::isExists($code);
        if ($promo_code) {
            $row = DB::table('registeruser')
                    ->select('registeruser.offer')
                    ->where('registeruser.registeruser_login_id', Auth::user()->id)
                    ->first();
            $first_customer_check = $row->offer;
            if ($first_customer_check == 0) {
                //first customer
                return 0;
            } else {
                //not first customer
                return 1;
            }
        } else {
            //invalid promocode
            return 2;
        }
    }

}
