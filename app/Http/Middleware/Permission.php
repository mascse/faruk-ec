<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\RolePermission;

class Permission {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        // $role_id = Auth::user()->role_id;
        $role_id = session('role_id');
      //  $url = $request->segment(1) . '-' . $request->segment(2);
        $url = $request->segment(1);
        $access = RolePermission::where('role_id', $role_id)->where('permission', $url)->count();
        if ($access > 0) {
            return $next($request);
        } else {
            return response()->view('errors.503');
        }
    }

}
