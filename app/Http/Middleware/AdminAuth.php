<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class AdminAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $admin_login_id = session('pride_admin_id');
        if ($admin_login_id != null) {
            return $next($request);
        } else {
            return response()->view('admin.Auth.login');
        }
        //  return $next($request);
    }

}
