<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class Conforder extends Model {

    //conforder
    protected $primaryKey = 'conforder_id';
    protected $table = 'conforder';

    public function SaveData($shoppingcart_id, $user_id, $request, $conforder_placed_date) {
        $conforder['shoppingcart_id'] = $shoppingcart_id;
        $conforder['registeruser_id'] = $user_id;
        $conforder['conforder_status'] = 'Order_Verification_Pending';
        $conforder['conforder_deliverynotes'] = $request->conforder_deliverynotes;
        $conforder['conforder_statusdetails'] = 'Order Verification Pending';
        $conforder['conforder_placed_date'] = $conforder_placed_date;
        $conforder['created_at'] = $conforder_placed_date;
        $conforder['conforder_lastupdte'] = Carbon::now();
        $conforder['conforder_placed'] = 'NIA';
        $conforder['month'] = date('m');
        $conforder['year'] = date('Y');
        $conforder['promo_code'] = $request->promo_code;
        $conforder_id = DB::table('conforder')->insertGetId($conforder);
        $tracknumber = "PLORDER#100-" . $conforder_id;
        //set conforder track number
        $update_data['conforder_tracknumber'] = $tracknumber;
        DB::table('conforder')->where('conforder_id', $conforder_id)->update($update_data);
        return $conforder_id;
    }
	
	public function getTotalPrice($conforder_id) {
		//dd($conforder_id);
	    $row = DB::table('conforder')
	        ->join('shoppingcart', 'shoppingcart.shoppingcart_id', '=',  'conforder.shoppingcart_id')
	        ->select('shoppingcart.shoppingcart_total')
	        ->where('conforder_id', $conforder_id)
	        ->first();
	    return $row->shoppingcart_total;
	}

}
