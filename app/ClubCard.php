<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClubCard extends Model
{
    
		protected $primaryKey = 'card_id';
        protected $table = 'membershipcard';
	
}
