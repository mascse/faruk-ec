<?php

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/boishak-1425', 'HomeController@Boishak')->name('Boishak');
//Promotion Code
Route::get('/promotion-code/{id}', 'ServiceController@PromotionCode')->name('PromotionCode');
Route::get('/promotion-amount-update/{id}/{id2}', 'ServiceController@UpdatePromotionAmount')->name('PromotionCode');
//boishak
Route::get('/boishakh-1425', 'HomeController@Boishak')->name('Boishak');
Route::get('/boishakh-1425/pride-girls/{id}', 'HomeController@BoishakProduct')->name('BoishakProductList');
Route::get('/boishakh-1425/pride-signature/{id}', 'HomeController@BoishakProduct')->name('BoishakProductList');
Route::get('/boishakh-1425/ethnic-menswear/{id}', 'HomeController@BoishakProduct')->name('BoishakProductList');
Route::get('/boishakh-1425/pride-kids/{id}', 'HomeController@BoishakProduct')->name('BoishakProductList');
Route::get('/woman/limited-edition-sari', 'HomeController@LimitedSari')->name('BoishakProductList');
Route::get('/coming-soon', 'HomeController@ComingSoonPage')->name('comingsoon');
Route::get('/new-in/{id}', 'product\ProductController@NewIn')->name('product.list');
//falgun Collection 2019
Route::get('/falgun-collection-2019/{id1?}/{id2?}', 'HomeController@FlagunCollection2019')->name('FlagunCollection2019');
//independence day
Route::get('/independence-day/{id1?}/{id2?}', 'CampaignController@IndependenceDay')->name('IndependenceDay');

//eid collection
Route::get('/eid-collection/{id1?}/{id2?}', 'CampaignController@EidCollection2019')->name('eid.collection');

//search 
Route::get('/search/{id}', 'HomeController@search')->name('search');
Route::get('/search-new/{id}', 'HomeController@SearchNew')->name('search.new');
//product list
// Signature Sari
Route::get('/signature/signature-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/cotton-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/taat-silk-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/half-silk-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/andi-silk-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/muslin-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature/kameez/digital_print/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/signature-sari/dupatta/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
//Classic 
Route::get('/classic/classic-sari/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/classic/classic-dupatta/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/classic/unstitched-three-piece/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
//Pride Girls 
Route::get('/pride-girls/all/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/formal/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/semi-formal/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/casual/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/bottoms/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/dupatta/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/pride-girls/jewelry/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
//Ethnic men
Route::get('/athenic-men/panjabi/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/athenic-men/long-panjabi/regular-fit/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/athenic-men/long-panjabi/slim-fit/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/athenic-men/short-panjabi/regular-fit/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/athenic-men/short-panjabi/slim-fit/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
//kids 
Route::get('/kids/girls/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
Route::get('/kids/boys/{id1}/{id2}', 'product\ProductController@ProductList')->name('product.list');
//product details
//Route::get('/shop/{id1}/color-{id2}/{id3}', 'product\ProductController@ProductDetails')->name('product.details');
Route::get('/shop/{id1}/color-{id2}/{id3}', 'product\ProductController@ProductDetails')->name('product.details');
Route::get('/GetTotalForAlert/{id1}', 'product\ProductController@ProductTotalQty')->name('ProductTotalQty');
Route::get('/ajaxcall-getBarcode/{id1}/{id2}/{id3}', 'AjaxCallController@GetProductBarcodeBySize')->name('GetBarcode');
//get fin in store city list
Route::get('/find-get-city/{id1}', 'product\ProductController@FindInStoreCity')->name('get.city');
//cart
Route::get('/shop-cart', 'cart\CartController@index');
Route::post('/cart/add-to-cart', 'cart\CartController@addProduct');
Route::get('/cart/cart-destroy', 'cart\CartController@cartDestroy');
Route::get('/cart/delete-product/{id1}', 'cart\CartController@cartProductDelete');
Route::post('/cart/update-product', 'cart\CartController@cartProductUpdate');
Route::post('/cart/update-cart', 'cart\CartController@orderpreviewUpdate');
//User Sign up
Route::post('/create/user', 'user\UserController@create');
Route::get('/user-details/{id}', 'user\UserController@UserDetails')->name('user-details');
Route::post('/user-details-save', 'user\UserController@SaveUserDetails')->name('UserDetailsSave');
Route::post('/update-shipping-info', 'user\UserController@UpdateShippingInfo')->name('ShippingInfo');
Route::post('/reset-password', 'user\UserController@ResetPassword')->name('ResetPassword');
Route::post('/update-password', 'user\UserController@UpdatePassword')->name('UpdatePassword');
Route::get('/forgot-password/{id}', 'user\UserController@ForgotPassword')->name('ForgetPassword');

//user account
Route::get('/track-order', 'user\TrackOrderController@UserTrackOrder');
Route::get('/order-preview/{id1}/{id2}', 'user\TrackOrderController@orderPreview')->name('Preview');
Route::get('/my-account', 'user\TrackOrderController@MyAccount')->name('my.account');
Route::get('/my-account-info', 'user\TrackOrderController@MyAccountInfo')->name('account.info');
Route::post('/account-edit', 'user\TrackOrderController@EditAccount')->name('account.edit');
Route::get('/account/change-email', 'user\TrackOrderController@AccountEmailChange');
Route::post('/account/update-email', 'user\TrackOrderController@UpdateAccountEmail');
Route::get('/account/change-password', 'user\TrackOrderController@AccountChangePassword');
Route::post('/account/update-password', 'user\TrackOrderController@UpdatePassword');
Route::get('/set-default-address/{id}', 'user\TrackOrderController@SetDefaultAddress')->name('address.create');
Route::get('/address-delete/{id}', 'user\TrackOrderController@AddressDelete')->name('address.delete');
Route::get('/eidt-additional-address/{id}', 'user\TrackOrderController@EditAdditionalAddress')->name('address.additional.edit');
Route::post('/additional-address-update', 'user\TrackOrderController@UpdateAdditionalAddress')->name('update.additional.address');

//track order 
Route::get('/track-order-byecr/{id}', 'user\TrackOrderController@OrderTrackByECR')->name('OrderTrackByECR');

//checkout
Route::get('/checkout', 'OrderPreviewController@index')->name('checkout');
Route::get('/orderpreview/delete-product/{id1}', 'cart\CartController@orderpreviewproductDelete');
Route::post('/orderpreview-submit/{nid?}', 'OrderPreviewController@OrderpreviewSubmit');

//guest checkout
Route::get('/guest-checkout', 'GuestCheckoutController@index')->name('guest.checkout');
Route::post('/guest-order', 'GuestCheckoutController@SaveData')->name('guest.order.save');
Route::get('/guest-order-confirmed', 'GuestCheckoutController@OrderConfirmed')->name('guest.order.confirm');

//payment gatway
Route::get('/{ipay}/success', 'OrderPreviewController@success');
Route::get('/{ipay}/failure', 'OrderPreviewController@failure');
Route::get('/{ipay}/cancel', 'OrderPreviewController@cancel');
Route::post('/{ssl}/success', 'OrderPreviewController@success');
Route::post('/{ssl}/failure', 'OrderPreviewController@failure');
Route::post('/{ssl}/cancel', 'OrderPreviewController@cancel');
Route::post('/ssl/ipn', 'OrderPreviewController@ipn');
//end payment gateway
Route::post('/saveshippinginfo', 'OrderPreviewController@SaveShippingProductInfo')->name('saveshippinginfo');

//promo code
Route::get('/promo-code-check/{code?}', 'PromoCodeController@promoCodeCheck')->name('promo.code.check');
Route::get('/order-confirmed', 'OrderPreviewController@OrderConfirmed');

//ajax call
Route::get('/getSizeByColor/{id1}/{id2}', 'AjaxCallController@getSizeByColor')->name('GetSize');
Route::get('/ajaxcall-getQuantityByColor/{id1}/{id2}/{id3}', 'AjaxCallController@getQuantityByColor')->name('GetQty');
Route::get('/getImageByColor/{id1}/{id2}', 'AjaxCallController@getImageByColor')->name('GetQty');
Route::get('citylist/{id}', 'OrderPreviewController@GetCityList');
Route::get('/ProductFiltering/{id1}/{id2}', 'AjaxCallController@ProductFiltering')->name('GetSize');
Route::get('/get-product-by-subpro/{id1}', 'AjaxCallController@GetProductListByCategory')->name('get.product.by.category');
//admin related product
Route::get('/add-related-product/{id}', 'admin\product\ProductController@AddRelatedProduct')->name('add.related.product');
Route::post('/related-product-save', 'admin\product\ProductController@AddRelatedProductSave')->name('add.related.product.store');


Route::get('/add-number/{id}/{id1}/{id2?}', 'AjaxCallController@AddNumber')->name('AddNumber');
//footer page
Route::get('/work-with-us', 'HomeController@WorkWithUs')->name('WorkWithUs');
Route::get('/contact-us', 'HomeController@ContactUs')->name('ContactUs');
Route::get('/exchange-policy', 'HomeController@ExchangePolicy')->name('ExchangePolicy');
Route::get('/faq', 'HomeController@Faq')->name('Faq');
Route::get('/store-locator', 'HomeController@StoreLocator')->name('StoreLocator');
Route::get('/privacy-cookies', 'HomeController@PrivacyCookies')->name('PrivacyCookies');
Route::get('/about-us', 'HomeController@AboutUs')->name('AboutUs');
Route::get('/how-to-order', 'HomeController@HowToOrder')->name('AboutUs');
Route::get('/site-map', 'HomeController@SiteMap')->name('SiteMap');
//Admin Login
Route::get('/pride-login', 'admin\Auth\LoginController@Login')->name('Login');
Route::post('/pride-login-check', 'admin\Auth\LoginController@LoginCheck')->name('LoginCheck');
Route::get('/admin-logout', 'admin\Auth\LoginController@AdminLogout')->name('LoginCheck');
//Admin Order
Route::get('/pride-admin/manage-incomplete-order', 'admin\ManageOrderController@manageIncompleteOrder')->name('manageincompleteorder');
Route::get('/pride-admin/manage-all-order', 'admin\ManageOrderController@manageAllOrder')->name('manageallorder');
Route::get('/pride-admin/order-details/{id}', 'admin\ManageOrderController@OrderDetails')->name('order-details');

Route::get('/order-details/{id}', 'admin\ManageOrderController@OrderDetails')->name('order-details');
Route::post('/order-details/update-address', 'admin\ManageOrderController@updateAddress')->name('update-address');
Route::post('/update-city-in-product-details', 'admin\ManageOrderController@updateCityInProductDetails');

//cashbook
Route::get('/cash-book-histroy', 'admin\CashBookController@CashbookHistrory')->name('cash.book');
Route::get('/cash-out', 'admin\CashBookController@cashOut')->name('cash.out');
Route::post('/cash-out', 'admin\CashBookController@storeCashout')->name('store.cashout');

//Admin reports
Route::get('/utadmin/sale-reports', 'admin\ReportController@saleReport')->name('pride_admin.sale_reports');

//order status
Route::post('/update_order_status', 'admin\ManageOrderController@UpdateOrderStatus')->name('UpdateOrderStatus');
//Route::get('/update_order_status/{id}/{id2}', 'admin\ManageOrderController@UpdateOrderStatus')->name('UpdateOrderStatus');
Route::get('/update_threepl_status/{id1}/{id2}', 'admin\ManageOrderController@UpdateTPLStatus')->name('UpdateTPLStatus');
Route::get('/cancel_indivisual_product_qty/{id1}/{id2}/{id3}/{id4}/{id5}', 'admin\ManageOrderController@CanceIndivisuallQty')->name('CancelQty');
Route::post('/exchange', 'admin\ManageOrderController@ExchangeUpdateSave')->name('ExchangeUpdateSave');

Route::get('/pride-admin/gebrowserdata', 'AjaxCallController@geBrowserData')->name('GetBrowser');
Route::get('/pride-admin/getmonthlysalesdata/{id1}', 'AjaxCallController@getmonthlysalesdata')->name('getmonthlysalesdata');


Route::get('/pride-admin', 'admin\DashboardController@index')->name('hadmin');
//Invoice Print
Route::get('/invoice-print/{id1}', 'admin\ManageOrderController@InvoicePrint')->name('UpdateTPLStatus');
// admin category
Route::get('/manage-category', 'admin\category\CategoryController@ManageCategory');
Route::get('/edit-procate/{id1}', 'admin\category\CategoryController@EditProcat');
Route::post('update-procat', 'admin\category\CategoryController@UpdateProcat');
Route::get('/add-subcat', 'admin\category\CategoryController@AddSubCat');
Route::post('/save-subpro', 'admin\category\CategoryController@SaveSubPro');
Route::get('/manage-subpro', 'admin\category\CategoryController@ManageSubPro');

//admin product
Route::get('/add-product', 'admin\product\ProductController@ProductAddForm');
Route::get('/subpro-list/{id}', 'admin\category\CategoryController@GetProWiseSubpro');
Route::post('/product-save', 'admin\product\ProductController@ProductSave')->name('ProductForm');
Route::get('/product-manage/{id?}', 'admin\product\ProductController@ViewAllProduct')->name('ViewAllProduct');
Route::get('/edit-product/{id}', 'admin\product\ProductController@EditProduct')->name('ViewAllProduct');
Route::post('/product-edit', 'admin\product\ProductController@UpdateProduct')->name('edit.product');
Route::get('/product-deactive-active/{id1}/{id2}/{id3}/{id4}', 'admin\product\ProductController@ProductActiveDeactive');
//Route::get('/product-delete/{id1}/{id2}', 'admin\product\ProductController@ProductDelete')->name('ViewAllProduct');
Route::get('/product-in-trash/{id1}/{id2}', 'admin\product\ProductController@ProductDeleteTrash')->name('ProductDeleteTrash');
Route::get('/product-delete/{id1}/{id2}', 'admin\product\ProductController@ProductDelete')->name('ProductDelete');
Route::get('/show-trash-product', 'admin\product\ProductController@ProductInTrash')->name('ProductInTrash');
Route::get('/product-restore/{id}/{id2}', 'admin\product\ProductController@ProductRestore')->name('ProductRestore');

//admin product album
Route::get('/product-album-manage/{id}/{id2}', 'admin\product\ProductController@ManageProductAlbum')->name('ManageProductAlbum');
Route::get('/manage-product-gallery/{id}/{id2}', 'admin\product\ProductController@ManageProductGallery')->name('ManageProductAlbum');
Route::get('/edit-album-image/{id}/{id2}/{id3}/{id4}', 'admin\product\ProductController@EditAlbumImage')->name('ManageProductAlbum');
Route::post('/update-album-image', 'admin\product\ProductController@UpdateAlbumImage')->name('ManageProductAlbum');
Route::get('/delete_album_image/{id}/{id2}/{id3}', 'admin\product\ProductController@DeleteAlbumImage')->name('DeleteProductAlbum');
Route::get('/add-album/{id}/{id2}', 'admin\product\AlbumController@AddAlbumForm')->name('AddAlbum');
Route::post('/save-album', 'admin\product\AlbumController@SaveAlbum')->name('SaveAlbum');
Route::get('/add-product-style/{id}', 'admin\product\AlbumController@AddProductStyle')->name('AddProductStyle');
Route::post('/save-product-style', 'admin\product\AlbumController@SaveProductStyle')->name('AddProductStyle');
//New swatch edit
Route::get('/edit-swatch/{id}/{id2}/{id3}', 'admin\product\AlbumController@EditSwatch')->name('edit.swatch');
Route::post('/save-edit-swatch', 'admin\product\AlbumController@UpdateSwatch')->name('update.swatch');
//product quantity 
Route::get('/update-quantity-search', 'admin\product\QuantityController@UpdateQuantitySearchForm');
Route::post('/update-quantity', 'admin\product\QuantityController@UpdateQuantitySearch');
Route::post('/update-quantity-save', 'admin\product\QuantityController@UpdateQuantitySave');
Route::get('/update-quantity-save-by-ajax/{id}/{id2}', 'admin\product\QuantityController@UpdateQuantitySaveByAjax');
Route::post('/save-new-size-qty', 'admin\product\QuantityController@SaveNewSizeQty');


//site user manage
Route::get('/site-user', 'admin\DashboardController@SiteUserList')->name('site.user');
Route::get('/site-user/delete-user/{id}', 'admin\DashboardController@DeleteUser')->name('site.user.delete');
Route::get('/edit-site-user/{id}', 'admin\DashboardController@EditSiteUser')->name('site.user.delete');
Route::post('/update-site-user-info', 'admin\DashboardController@UpdateSiteUser')->name('site.user.update');

//role management
Route::get('/add-user', 'admin\user\usercontroller@index');
Route::post('/create-admin-user', 'admin\user\usercontroller@create');
Route::get('/view-user', 'admin\user\usercontroller@view');
Route::get('/user-edit/{id}', 'admin\user\usercontroller@edit');
Route::post('/update-user', 'admin\user\usercontroller@update');
Route::get('/delete-user/{id}/{id2}', 'admin\user\usercontroller@delete');
Route::get('/add-role', 'admin\user\RoleController@index');
Route::post('/create-role', 'admin\user\RoleController@create');
Route::get('/view-role', 'admin\user\RoleController@view');
Route::get('/edit-role/{id}', 'admin\user\RoleController@edit');
Route::post('/role-update/{id}', 'admin\user\RoleController@update');



//address book
Route::get('/address-book', 'user\TrackOrderController@AddressBook');
Route::get('/address-create', 'user\TrackOrderController@AddressCreate')->name('address.create');
Route::post('/address-save', 'user\TrackOrderController@SaveAddress')->name('SaveAddress');
Route::get('/address-edit/{id}', 'user\TrackOrderController@EditAddress')->name('address.create');
Route::post('/address-update', 'user\TrackOrderController@UpdateAddress')->name('update.address');
//Manage home page
//slider
Route::get('/add-slider', 'admin\ContentHandleController@AddSlider')->name('add.slider');
Route::post('/save-slider', 'admin\ContentHandleController@SaveSlider')->name('save.slider');
Route::get('/manage-slider', 'admin\ContentHandleController@ManageSlider')->name('manage.slider');
Route::get('/delete-slider/{id}', 'admin\ContentHandleController@DeleteSlider')->name('delete.slider');
Route::get('/edit-slider/{id}', 'admin\ContentHandleController@EditSlider')->name('edit.slider');
Route::post('/update-slider', 'admin\ContentHandleController@UpdateSlider')->name('update.slider');
Route::get('/active-slider/{id}', 'admin\ContentHandleController@ActiveSlider')->name('active.slider');
Route::get('/deactive-slider/{id}', 'admin\ContentHandleController@DeactiveSlider')->name('deactive.slider');
//banner
Route::get('/add-banner', 'admin\ContentHandleController@AddBanner')->name('add.banner');
Route::post('/save-banner', 'admin\ContentHandleController@SaveBanner')->name('save.banner');
Route::get('/manage-banner', 'admin\ContentHandleController@ManageBanner')->name('manage.banner');
Route::get('/active-banner/{id}', 'admin\ContentHandleController@ActiveBanner')->name('active.banner');
Route::get('/deactive-banner/{id}', 'admin\ContentHandleController@DeactiveBanner')->name('deactive.banner');
Route::get('/edit-banner/{id}', 'admin\ContentHandleController@EditBanner')->name('edit.banner');
Route::post('/update-banner', 'admin\ContentHandleController@UpdateBanner')->name('update.banner');
Route::get('/delete-banner', 'admin\ContentHandleController@DeleteBanner')->name('delete.banner');
//report
Route::post('/search-order', 'admin\report\ReportController@SearchOrder')->name('SearchOrder');
Route::post('/pdf-report', 'admin\report\ReportController@PdfReport')->name('PdfReport');
Route::get('/order-report', 'admin\report\ReportController@TodayReport')->name('TodayReport');
//Find in store
Route::get('/showroom-wise-stock', 'FindStoreController@ShowroomStock')->name('ShowroomStock');
Route::get('/showroom-city-list', 'FindStoreController@ShowroomCityList')->name('ShowroomStock');
Route::get('/showroom-wise-stock-by-designref/{id}', 'FindStoreController@ShowroomStockByDesignRef')->name('ShowroomStockByDesignRef');

//coupon generate
Route::get('/coupon-generate', 'CuponController@GenerateCoupon')->name('GenerateCoupon');
Route::post('/save-promcodes', 'CuponController@StorePromo')->name('StorePromo');

//Find in store
Route::get('/showroom-city-list','FindStoreController@ShowroomCityList')->name('ShowroomStock');
Route::get('/store-list/{district}/{barcode}', 'FindStoreController@ShowroomStock')->name('store.list');
Route::get('/showroom-wise-stock-by-designref/{id}/{id1}','FindStoreController@ShowroomStockByDesignRef')->name('ShowroomStockByDesignRef');

