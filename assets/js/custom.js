jQuery(document).ready(function ($) {
    $('.open-close').click(function () {
        $(this).toggleClass('plus');
        $(this).toggleClass('minus');
    });
});
var base_url = "https://pride-limited.com/";
/*Facebook Pixel Code
!function (f, b, e, v, n, t, s)
{
    if (f.fbq)
        return;
    n = f.fbq = function () {
        n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
    };
    if (!f._fbq)
        f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = '2.0';
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s)
}(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '567318743638505');
fbq('track', 'PageView');
/* End Facebook Pixel Code */

try {
    if (!window.localStorage || !window.sessionStorage) {
        throw new Error();
    }
    localStorage.setItem('storage_test', 1);
    localStorage.removeItem('storage_test');
} catch (e) {
    (function () {
        var Storage = function (type) {
            var data;

            function createCookie(name, value, days) {
                var date, expires;

                if (days) {
                    date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = '; expires=' + date.toGMTString();
                } else {
                    expires = '';
                }
                document.cookie = name + '=' + value + expires + '; path=/';
            }

            function readCookie(name) {
                var nameEQ = name + '=',
                        ca = document.cookie.split(';'),
                        i = 0,
                        c;

                for (i = 0; i < ca.length; i++) {
                    c = ca[i];

                    while (c.charAt(0) === ' ') {
                        c = c.substring(1, c.length);
                    }

                    if (c.indexOf(nameEQ) === 0) {
                        return c.substring(nameEQ.length, c.length);
                    }
                }

                return null;
            }

            function setData(data) {
                data = encodeURIComponent(JSON.stringify(data));
                createCookie(type === 'session' ? getSessionName() : 'localStorage', data, 365);
            }

            function clearData() {
                createCookie(type === 'session' ? getSessionName() : 'localStorage', '', 365);
            }

            function getData() {
                var data = type === 'session' ? readCookie(getSessionName()) : readCookie('localStorage');

                return data ? JSON.parse(decodeURIComponent(data)) : {};
            }

            function getSessionName() {
                if (!window.name) {
                    window.name = new Date().getTime();
                }

                return 'sessionStorage' + window.name;
            }

            data = getData();

            return {
                length: 0,
                clear: function () {
                    data = {};
                    this.length = 0;
                    clearData();
                },

                getItem: function (key) {
                    return data[key] === undefined ? null : data[key];
                },

                key: function (i) {
                    var ctr = 0,
                            k;

                    for (k in data) {
                        if (ctr.toString() === i.toString()) {
                            return k;
                        } else {
                            ctr++
                        }
                    }

                    return null;
                },

                removeItem: function (key) {
                    delete data[key];
                    this.length--;
                    setData(data);
                },

                setItem: function (key, value) {
                    data[key] = value.toString();
                    this.length++;
                    setData(data);
                }
            };
        };

        window.localStorage.__proto__ = window.localStorage = new Storage('local');
        window.sessionStorage.__proto__ = window.sessionStorage = new Storage('session');
    })();
}


//jQuery(window).load(function(){
//   jQuery('#top-opener-hover').hide();
//});

jQuery('a.nav-opener').click(function () {
    //  alert('ok');
    //  jQuery(this).parents('body').toggleClass('nav-active');
    jQuery(this).parents('body').toggleClass('nav-close');
});

function showHideDiv(ele) {
    var srcElement = document.getElementById(ele);
    if (srcElement != null) {
        if (srcElement.style.display == "block") {
            srcElement.style.display = 'none';
        } else {
            srcElement.style.display = 'block';
        }
        return false;
    }
}

// page init
jQuery(function () {
    initMobileNav();
});

// mobile menu init
function initMobileNav() {
    jQuery('body').mobileNav({
        hideOnClickOutside: true,
        menuActiveClass: 'nav-active',
        menuOpener: '.nav-opener',
        menuDrop: '.nav-holder'
    });
}

/*
 * Simple Mobile Navigation
 */
;
(function ($) {
    function MobileNav(options) {
        this.options = $.extend({
            container: null,
            hideOnClickOutside: false,
            menuActiveClass: 'nav-active',
            menuOpener: '.nav-opener',
            menuDrop: '.nav-drop',
            toggleEvent: 'click',
            outsideClickEvent: 'click touchstart pointerdown MSPointerDown'
        }, options);
        this.initStructure();
        this.attachEvents();
    }
    MobileNav.prototype = {
        initStructure: function () {
            this.page = $('html');
            this.container = $(this.options.container);
            this.opener = this.container.find(this.options.menuOpener);
            this.drop = this.container.find(this.options.menuDrop);
        },
        attachEvents: function () {
            var self = this;

            if (activateResizeHandler) {
                activateResizeHandler();
                activateResizeHandler = null;
            }

            this.outsideClickHandler = function (e) {
                if (self.isOpened()) {
                    var target = $(e.target);
                    if (!target.closest(self.opener).length && !target.closest(self.drop).length) {
                        self.hide();
                    }
                }
            };

            this.openerClickHandler = function (e) {
                e.preventDefault();
                self.toggle();
            };

            this.opener.on(this.options.toggleEvent, this.openerClickHandler);
        },
        isOpened: function () {
            return this.container.hasClass(this.options.menuActiveClass);
        },
        show: function () {
            this.container.addClass(this.options.menuActiveClass);
            if (this.options.hideOnClickOutside) {
                this.page.on(this.options.outsideClickEvent, this.outsideClickHandler);
            }
        },
        hide: function () {
            this.container.removeClass(this.options.menuActiveClass);
            if (this.options.hideOnClickOutside) {
                this.page.off(this.options.outsideClickEvent, this.outsideClickHandler);
            }
        },
        toggle: function () {
            if (this.isOpened()) {
                this.hide();
            } else {
                this.show();
            }
        },
        destroy: function () {
            this.container.removeClass(this.options.menuActiveClass);
            this.opener.off(this.options.toggleEvent, this.clickHandler);
            this.page.off(this.options.outsideClickEvent, this.outsideClickHandler);
        }
    };

    var activateResizeHandler = function () {
        var win = $(window),
                doc = $('html'),
                resizeClass = 'resize-active',
                flag, timer;
        var removeClassHandler = function () {
            flag = false;
            doc.removeClass(resizeClass);
        };
        var resizeHandler = function () {
            if (!flag) {
                flag = true;
                doc.addClass(resizeClass);
            }
            clearTimeout(timer);
            timer = setTimeout(removeClassHandler, 500);
        };
        win.on('resize orientationchange', resizeHandler);
    };

    $.fn.mobileNav = function (options) {
        return this.each(function () {
            var params = $.extend({}, options, {container: this}),
                    instance = new MobileNav(params);
            $.data(this, 'MobileNav', instance);
        });
    };
}(jQuery));


jQuery(document).mouseup(function (e)
{
    var container = jQuery("#searchsuite_autocomplete");
    if (!container.is(e.target)
            && container.has(e.target).length === 0)
    {
        container.hide();
    }
});
jQuery("#question").click(function ()
{
    jQuery("#searchsuite_autocomplete").css("display", "block");
});
jQuery("#question").keyup(function () {
    var question = jQuery("#question").val();
    //  alert(question);
    if (question == '') {
        jQuery("#searchsuite_autocomplete").css("display", "none");
    } else {
        question = jQuery("#question").val();
        var url = base_url + "/search-new/" + question;
        jQuery.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                //  alert(data);
                jQuery("#searchsuite_autocomplete").css("display", "block");
                jQuery('.searchsuite-autocomplete').html(data);
            }
        });
    }
});


function submitContactForm() {
    var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var name = jQuery('#inputName').val();
    var number = jQuery('#inputNumber').val();
    var message = jQuery('#inputmessage').val();
    if (number.trim() == '') {
    alert('Please enter your contact number.');
    jQuery('#inputNumber').focus();
    return false;
    } else if (message.trim() == '') {
    alert('Please enter message.');
    jQuery('#inputmessage').focus();
    return false;
    } else {
    var url = base_url + "/add-number/" + number + "/" + message + "/" + name;
    jQuery.ajax({
    url: url,
    		type: 'GET',
    		beforeSend: function () {
    		jQuery('.submitBtn').attr("disabled", "disabled");
    		jQuery('.modal-body').css('opacity', '.5');
    		},
    		success: function (msg) {
    		if (msg == 'ok') {
    		jQuery('#inputName').val('');
    		jQuery('#inputNumber').val('');
    		jQuery('#inputmessage').val('');
    		jQuery('.statusMsg').html('<span style="color:green;">Your contact number is stored, we\'ll get back to you soon.</p>');
    		} else {
    		jQuery('.statusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
    		}
    		jQuery('.submitBtn').removeAttr("disabled");
    		jQuery('.modal-body').css('opacity', '');
    		}
    });
    }
}
function updateSearchLink() {
    var question = document.getElementById("question");
    var search_form = document.getElementById("search_mini_form");
    search_form.action = 'http://pride-limited.com/search-view/'+question.value;
}