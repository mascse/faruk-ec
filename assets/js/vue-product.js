var product=new Vue({
	el:"#vue-product",
	data:{
		product_id: product_id,
		color_name: color_name,
		product_price: product_price,
		product_code: product_code,
		//size_input: '',
		//firstname: "",
		text:'',
		city_list:[],
		cartItems:cart_items,
	},
	created:function(){
		var size=jQuery('#product-size-input').val();
		var url_barcode = base_url + "/ajaxcall-getBarcode/" + this.product_id + '/' + size + '/' + this.color_name;
            jQuery.ajax({
                url: url_barcode,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                if(html !=''){
                       jQuery(".product_barcode").show();
                       jQuery("#barcode").html(html);
                    }
                }
            });
		 var url_op = base_url + "/ajaxcall-getQuantityByColor/" + this.product_id + '/' + size + '/' + this.color_name;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                   //  alert(html);
                    var qty = html[0].SizeWiseQty;
                  //  alert(qty);
                    if (qty > 0) {
                        jQuery('#qty').attr({"max": qty-amountToCart()});
						jQuery("#sold_out_msg").html(" ");
						jQuery('#qty').val(1);
                        document.getElementById("add-cart").disabled = false;
                    } else {
						jQuery('#qty').val(0);
						jQuery('#qty').attr({"max": 0});
						jQuery("#sold_out_msg").html("<span style='color:red;font-weight:700;padding-bottom:10px;'>" + size + " size has sold out. Please select another size.</span>");
					//	document.getElementById("add-cart").disabled = true;
					}
                }
            });
			//alert(size);
		},
	methods:{
		swatchoption:function(event){
			size = event.currentTarget.id;
			var clickedElement = event.target;
			//console.log(clickedElement);
			jQuery(clickedElement).siblings().removeClass('active');
            jQuery(clickedElement).addClass('active');
			jQuery('#product-size-input').val(size);
			console.log(size);
			var url_barcode = base_url + "/ajaxcall-getBarcode/" + this.product_id + '/' + size + '/' + this.color_name;
            jQuery.ajax({
                url: url_barcode,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                if(html !=''){
                       jQuery(".product_barcode").show();
                       jQuery("#barcode").html(html);
                    }
                }
            });
			var url_op = base_url + "ajaxcall-getQuantityByColor/" + this.product_id + '/' + size + '/' + this.color_name;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                   //  alert(html);
                    var qty = html[0].SizeWiseQty;
                  //  alert(qty);
                    if (qty > 0) {
                        jQuery('#qty').attr({"max": qty-amountToCart()});
						jQuery("#sold_out_msg").html(" ");
						jQuery('#qty').val(1);
                        document.getElementById("add-cart").disabled = false;
                    } else {
						jQuery('#qty').val(0);
						jQuery('#qty').attr({"max": 0});
						jQuery("#sold_out_msg").html("<span style='color:red;font-weight:700;padding-bottom:10px;'>" + size + " size has sold out. Please select another size.</span>");
					//	document.getElementById("add-cart").disabled = true;
					}
                }
            });
			//alert(size);
			
		},
		addQty:function(event){
			var currentval = jQuery('#qty').val();
			var newValue = parseInt(currentval) + 1;
			jQuery('#qty').val(newValue);
			event.preventDefault();
		},
		minusQty:function(event){
			var currentval1 = jQuery('#qty').val();
			var newValue1 = currentval1 - 1;
			if (newValue1 > 0) {
				jQuery('#qty').val(newValue1);
			}
			event.preventDefault();
		},
		getStore:function(event){
			//console.log('clicked');
		text = event.target.innerText;
		console.log(text);
		var barcode = document.getElementById('barcode').textContent;
		var get_style_name = "{{$singleproduct->product_code}}";
        var stylecode = get_style_name.replace("/", "_");
        
		document.getElementById('store-list-loading').style.display = "block";
		var no_data = document.getElementById('no-data');
		if(no_data) {
		    no_data.style.display = "none";
		}
		
		var address = '';
		
		if(barcode) {
		    address = base_url+"/store-list/"+text+"/"+barcode;
		} else {
		    address = base_url+"/showroom-wise-stock-by-designref/"+text+"/"+stylecode;
		}
		
		jQuery.ajax({url: address, success: function(html){
			if(text==null || text=="") {
				alert('Error');
			} else {
			    document.getElementById('store-list-loading').style.display = "none";
				document.getElementById('store-list-container').innerHTML = html;
				
				jQuery('.simple').on('click', function (e) {
            
            jQuery('.searchstore').css('display', 'inline-block');
            jQuery('.find_store_close').show();
            e.preventDefault();
            });
            jQuery(".store-details").mouseenter(function () {
                    jQuery(this).children(".stockmessage").removeClass('hide');
                });
                jQuery(".store-details").mouseleave(function () {
                   jQuery(this).children(".stockmessage").addClass('hide');
            });
            jQuery(".find_store_close").on('click',function(){
                jQuery('.searchstore').css('display', 'none');
                jQuery('.find_store_close').hide();
            });
			}
		}});
		document.getElementById('select-district').innerHTML = text + ' <span class="caret"></span>';
		},
		findinstore:function(event){
		 var  _this = this;
          var url_op = base_url + "/find-get-city/" + this.product_code;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data)
                {
					_this.city_list=data;
                }
            });
			jQuery('.searchstore').css('display', 'inline-block');
            jQuery('.find_store_close').show();
			event.preventDefault();
			//return false;
		},
		findinstoreclose:function(event){
			jQuery('.searchstore').css('display', 'none');
            jQuery('.find_store_close').hide();
			event.preventDefault();
		},
		amountToCart:function(){
		var size = jQuery(".swatch-option.text.active").text();
	   },
	},
	
	computed:{}
});