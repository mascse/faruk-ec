var vuejs = new Vue({
    el: '#order-preview-page',
    data: {
        cart_items: cart_items,
        offer: offer,
        hour: hour_now,
        item_number: item_number,
        city_list: 0,
        city_id_outside_dhaka: [7, 24, 26, 28, 32, 42, 45, 50, 51, 58, 59, 60, 65, 69, 73, 78, 82, 85, 3, 4, 5, 6, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 156, 157, 158],
        delivery_method: "cDelivery",
        outside_dhaka: false,
        delivery_type: 0,
        shipping_charges: [70, 130, 150, 85],
        tax_percent: 0,
        error_city: '',
        bKash_selected: false,
        iPay_selected: false,
        ssl_selected: false,
        promo_code: null,
        promo_discount: 0
    },
    methods: {
        changeCity: function (event) {
            this.city_list = 0;
            var RegionId = event.target.value;
            this.outside_dhaka = event.target.value == 1 ? false : true;
            this.delivery_type = event.target.value == 1 ? 0 : 2;
            if (this.outside_dhaka && this.delivery_method == "cDelivery") {
                document.getElementById('bKash').click();
            }
            jQuery('#regionId').val(RegionId);
            var url_op = base_url + "/citylist/" + RegionId;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    jQuery('#CityList').empty();
                    jQuery('#CityList').append('<option value="">--- Select Area ---</option>');
                    jQuery.each(data, function (index, cityobj) {
                        jQuery('#CityList').append('<option value="' + cityobj.CityId + '">' + cityobj.CityName + '</option>');
                    });
                }
            });
        },
        changeArea: function (event) {
            this.outside_dhaka = this.city_id_outside_dhaka.includes(parseInt(event.target.value));
            this.delivery_type = this.city_id_outside_dhaka.includes(parseInt(event.target.value)) ? 2 : 0;
            if (this.outside_dhaka && this.delivery_method == "cDelivery") {
                document.getElementById('bKash').click();
            }
            // var AreaId = event.target.value;
            //this.outside_dhaka = event.target.value == 24 ? true : false;
            //this.delivery_type = event.target.value == 24 ? 0 : 2;
            // alert(AreaId);
        },
        setPopupIndex: {
            set: function (index) {
                this.popup_index = index;
            }
        },
        setSelectedIndex: {
            set: function (index) {
                this.selected_index = index;
            }
        },
        storeToTemp: function () {
            this.temp = Object.assign({}, this.addresses[this.popup_index]);
            /*Object.assign(this.temp, this.addresses[this.popup_index]);*/
        },
        getFromTemp: function () {
            Object.assign(this.addresses[this.popup_index], this.temp);

        },
        checkForm: function (e) {
            if (!document.getElementById("CityList").value) {
                document.getElementById("CityList").focus();
                this.error_city = "Please select City and Region";
                e.preventDefault();
            }
        },
        promoCodeCheck: function () {
            if (!this.promo_code) {
                document.getElementById("promo-code").focus();
                e.preventDefault();
            }
            var url_op = base_url + "/promo-code-check/" + this.promo_code;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                success: function (data = false) {
                    if (data == 0) {
                        percentage = 10;
                        vuejs.promo_discount = vuejs.subTotal * percentage / 100;
                         jQuery(".messages").html("vaild: " + vuejs.promo_code);
                    } else if (data == 1) {
                        alert("This promocode only for first purchase cutomer: " + vuejs.promo_code);
                        jQuery("#promo_check").html("This promocode only for first purchase cutomer: " + vuejs.promo_code);
                    } else if (data == 2) {
                        alert("Invalid Promo Code: " + vuejs.promo_code);
                        jQuery("#promo_check").html("Invalid Promo Code: " + vuejs.promo_code);
                }
                }
            });
        }
    },
    computed: {
        getShippingCharge: function () {
            if (this.subTotal >= 3000){
				var charge=this.shipping_charges[this.delivery_type];
				if(charge == 70 || charge == 150){
					return 0;
				}else{
					return charge;
				}
			}else{
             var charge=this.shipping_charges[this.delivery_type];
			 return charge;
			}

        },
        subTotal: {
            get: function () {
                var subTotal = 0;
                for (var key in this.cart_items) {
                    if (!this.cart_items.hasOwnProperty(key))
                        continue;
                    subTotal += cart_items[key].price * cart_items[key].qty;
                }
                return subTotal;
            }
        },
        amountOfOffer: {
            get: function () {
                var total = 0;
                total = this.subTotal * 10 / 100;
                return total;
            }
        },
        amountWithOffer: {
            get: function () {
                if (this.offer == 0) {
                    var total = 0;
                    total = this.subTotal - this.subTotal * 10 / 100;
                } else {
                    total = this.subTotal;
                }
                return total;
            }
        },
        totalBeforeTax: {
            get: function () {
                var total = 0;
                total = this.amountWithOffer;
                return total;
            }
        },
        amountOfTax: {
            get: function () {
                var total = 0;
                total = this.totalBeforeTax * this.tax_percent / 100;
                return total;
            }
        },
        total: {
            get: function () {
                var total = 0;
                total = this.totalBeforeTax + this.amountOfTax + this.getShippingCharge;
                return total;
            }
        },
    },
    beforeMount() {
        this.selectedAddress;
    }
});