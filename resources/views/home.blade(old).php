<?php

use App\Http\Controllers\product\ProductController;
?>
@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/owl.carousel.min.css')}}" />
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/owl.theme.default.min.css')}}" />
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/quickview-style.css')}}" />
<style>
    .owl-carousel {
        display: block;
        position: relative;
        width: 100%;
        overflow: hidden;
        -ms-touch-action: pan-y;
    }
    .owl-theme .owl-nav [class*=owl-] {
        color: #FFF;
        font-size: 14px;
        margin: 5px;
        padding: 4px 7px;
        background: #ffffff00;
        display: inline-block;
        cursor: pointer;
        border-radius: 3px;
    }
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page messages">
                        <div data-placeholder="messages"></div>
                        <div data-bind="scope: 'messages'">
                            <div data-bind="foreach: { data: cookieMessages, as: 'message' }" class="messages">
                                <div data-bind="attr: {
            class: 'message-' + message.type + ' ' + message.type + ' message',
            'data-ui-id': 'message-' + message.type
        }">
                                    <div data-bind="html: message.text"></div>
                                </div>
                            </div>
                            <div data-bind="foreach: { data: messages().messages, as: 'message' }" class="messages">
                                <div data-bind="attr: {
            class: 'message-' + message.type + ' ' + message.type + ' message',
            'data-ui-id': 'message-' + message.type
        }">
                                    <div data-bind="html: message.text"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style xml="space">
        @media (max-width: 999px) {
            .slideshow .desktop-img {
                display: none;
            }
            .slideshow .mobile-img {
                display: block;
            }
        </style>
        <section class="slideshow win-height">
            <div class="slideset">
                <div class="slide">
                    <div class="desktop-img">
                        <img src="{{url('/')}}/storage/app/public/img/51_banner_Banner_2_crop.jpg" alt="" />
					</div>
                    <div class="mobile-img">
                        <img src="{{url('/')}}/storage/app/public/img/51_banner_Banner_2_crop_mobile.jpg" alt="" />
					</div>
                        <a class="btn" href="#">Shop Now</a>
				</div>
            </div>
        </section>
        <section class="category-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="category-holder">
                            <div class="category-gallery">
                                <div class="mask">
                                    <div class="slideset">
                                        <div class="product">
                                            <div class="holder">
                                                <div class="img"><a href="#"><img src="{{url('/')}}/storage/app/public/img/ethinicmes.jpg" alt="" /></a></div>
                                                <div class="info"><strong>ETHNIC MENSWEAR</strong> 
                                                    <a class="btn btn-default" href="#">shop now</a></div>
                                            </div>
                                        </div>
                                        <div class="product">
                                            <div class="holder">
                                                <div class="img"><a href="#"><img src="{{url('/')}}/storage/app/public/img/homes.jpg" alt="" /></a></div>
                                                <div class="info"><strong>Pride Homes</strong> <a class="btn btn-default" href="#">shop now</a></div>
                                            </div>
                                        </div>
                                        <div class="product">
                                            <div class="holder">
                                                <div class="img"><a href="#">
                                                        <img src="{{url('/')}}/storage/app/public/img/test.jpg" alt="" /></a></div>
                                                <div class="info"><strong>PRIDE GIRLS</strong> <a class="btn btn-default" href="#">shop now</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product add">
                                    <div class="holder">
                                        <div class="img"><a href="#"><img src="{{url('/')}}/storage/app/public/img/banner/banner4.jpg" alt="" /></a></div>
                                        <div class="info"><strong>Kids</strong> <a class="btn btn-default" href="#">shop now</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <style xml="space">
                .best-seller .col2 .holder .caption {
                    top: 0;
                    margin: 0 0 0 -200px;
                    display:none;
                }
                @media (max-width: 766px) {
                    .best-seller .col2 .holder .caption {
                        margin: 0;
                    } 
                }
            </style>
            <div class="container">
                <div class="row">
                    <div class="col11">
                        <center>
                            <div class="h3" style="padding-bottom:8px;color:#291e88;">NEW ARRIVAL ! </div>
                            <div class="owl-carousel owl-theme">
                                @foreach($product_list as $product)
                                <?php
                                $product_name = str_replace(' ', '-', $product->product_name);
                                $product_url = strtolower($product_name);
                                $data = ProductController::GetProductColorAlbum($product->product_id);
                                $album_name = ProductController::GetUpdatedProductColorAlbumName($product->product_id);
                                $sold_out = ProductController::ProductWiseQty($product->product_id);
                                foreach ($data as $pro_album) {
                                    $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                                }
                                ?>
                                <div class="item product product-item">                                
                                    <div class="product-item-info holder">                  
                                        <a href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}" class="product photo product-item-photo">
                                            <span class="product-image-container">
                                                <span class="product-image-wrapper img">
                                                    <img class="product-image-photo" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img; ?>" alt="<?php echo $product->productimg_img; ?>"/></span>
                                            </span>
                                        </a>
                                        <div class="product details product-item-details">
                                            <strong class="product name product-item-name"><a class="product-item-link" title="{{$product->product_name}}" href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}">
                                                    {{$product->product_name}}</a>
                                            </strong>
                                            <div class="price-box price-final_price" data-role="priceBox" data-product-id="75564" data-price-box="{{$product->product_id}}">
                                                <span class="price-container price-final_price tax weee">
                                                    <span  id="product-price-75564" data-price-amount="{{$product->product_price}}" data-price-type="finalPrice" class="price-wrapper">
                                                        <span class="price">Tk {{$product->product_price}} </span>    
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </center>
                    </div>
                    <div class="block upsell" data-mage-init='{"upsellProducts":{}}' data-limit="4" data-shuffle="1">
                        <div class="block-title title">
                            <strong id="block-upsell-heading" role="heading" aria-level="2">NEW ARRIVAL</strong>
                        </div>
                        <div class="block-content content" aria-labelledby="block-upsell-heading">
                            <!--        <div class="block-actions">
                                            <button type="button" class="action select" role="select-all"><span></span></button>
                            </div>-->
                            <div class="products wrapper grid products-grid products-upsell">
                                <ol class="products list items product-items">
                                    <?php
                                    foreach ($product_list as $product) {
                                        ?>
                                        <li class="item product product-item">                                
                                            <div class="product-item-info ">
                                                <!-- upsell_products_list-->                    
                                                <a href="#" class="product photo product-item-photo">
                                                    <span class="product-image-container">
                                                        <span class="product-image-wrapper">
                                                            <img class="product-image-photo"
                                                                 src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img; ?>"
                                                                 alt="<?php echo $product->productimg_img; ?>"/></span>
                                                    </span>
                                                </a>
                                                <div class="product details product-item-details">
                                                    <strong class="product name product-item-name"><a class="product-item-link" title="N18301" href="#">
                                                            {{$product->product_name}}</a>
                                                    </strong>

                                                    <div class="price-box price-final_price" data-role="priceBox" data-product-id="75564" data-price-box="product-id-75564">


                                                        <span class="price-container price-final_price tax weee"
                                                              >
                                                            <span  id="product-price-75564"                data-price-amount="1680"
                                                                   data-price-type="finalPrice"
                                                                   class="price-wrapper "
                                                                   >
                                                                <span class="price">Tk {{$product->product_price}} </span>    
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ol>
                            </div>
                            <a class="btn-next" href="javascript:void(0);">next</a>
                            <a class="btn-prev" href="javascript:void(0);">prev</a>
                        </div>
                    </div>
                </div>
                <div class="holder">
                    <div class="img">
                        <a href="#"><img src="#" alt="" /></a>
                    </div>
                    <div class="caption"></div>
                </div>
            </div>
        </section>
       <!-- <section class="newsletter-sec">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="news-holder">
                            <div class="news-block">
                                <div class="news-frame">
                                    <h1>Sign up for our newsletter to receive <br> special offers, news &amp; events.</h1>
                                    <div class="block newsletter">
                                        <form class="form subscribe"
                                              novalidate
                                              action="#"
                                              method="post"
                                              data-mage-init='{"validation": {"errorClass": "mage-error"}}'
                                              id="newsletter-validate-detail-home">
                                            <div class="field newsletter">
                                                <label class="label" for="newsletter"><span>Sign Up for Our Newsletter:</span></label>
                                                <div class="control">
                                                    <input name="email" type="email" id="newsletter-home"
                                                           placeholder="Enter your email address"
                                                           data-validate="{required:true, 'validate-email':true}"/>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <button class="action subscribe primary" title="Subscribe" type="submit">
                                                    <span>Subscribe</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="authenticationPopup"  style="display: block;">
                        <!--Google Tag Manager: dataLayer - Added by Mageplaza-->
                        <div class="checkout-popup" style="display: none;">
                            <div class="popup-holder">
                                <div class="popup-frame">
                                    <div class="popup-block"><a class="popup-close" href="#">
                                            <img src="{{url('/')}}/storage/app/public/popup-close.png" alt="" />
                                        </a>
                                        <div class="img-area"><a href="">
                                                <img src="{{url('/')}}/storage/app/public/popup_reupload.jpg" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <style>
                                .beadcumarea{display:none !important;}
                                .checkout-popup {
                                    position: fixed;
                                    left: 0;
                                    top: 0;
                                    bottom: 0;
                                    right: 0;
                                    z-index: 999;
                                    overflow: hidden;
                                    background: rgba(40, 40, 40, 0.7); }
                                .popup-open .checkout-popup {
                                    display: block; 
                                }
                                .checkout-popup .popup-holder {
                                    display: table;
                                    width: 100%;
                                    height: 100%; }
                                .checkout-popup .popup-frame {
                                    display: table-cell;
                                    vertical-align: middle; }
                                @media (max-width: 500px) {
                                    .checkout-popup .popup-frame {
                                        vertical-align: top;
                                        padding: 50px 20px 0; } }
                                .checkout-popup .popup-block {
                                    max-width: 450px;
                                    margin: 0 auto;
                                    background: #fff;
                                    position: relative;
                                    text-align: center;
                                    color: #000;
                                    font-size: 15px;
                                    line-height: 18px;
                                    text-transform: uppercase;
                                    padding: 10px; }
                                .checkout-popup .popup-close {
                                    position: absolute;
                                    right: 0;
                                    top: 0;
                                    width: 30px;
                                    opacity: 0.7;
                                    margin: 2px 2px 0 0; }
                                @media (max-width: 500px) {
                                    .checkout-popup .popup-close {
                                        width: 25px; } 
                                }
                                .checkout-popup .popup-close:hover {
                                    opacity: 1; 
                                }
                                .checkout-popup img {
                                    display: block;
                                    width: 100%; 
                                }
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script  type="text/javascript"  src="{{asset('assets/js/owl.js')}}"></script>
    <script type="text/javascript" xml="space">
jQuery('.popup-close').click(function () {
    jQuery(this).parents('body').toggleClass('popup-open');
    jQuery('.checkout-popup').css("display", "none");
});
    </script>
    @endsection
