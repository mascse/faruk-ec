<?php

use App\Http\Controllers\product\ProductController;
?>
@extends('layouts.app')
@section('title','Ecommerce Limited | A Ecommerce Group venture')
@section('content')
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/owl.carousel.min.css')}}" />
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/owl.theme.default.min.css')}}" />
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/quickview-style.css')}}" />
<style>
    .owl-carousel {
        display: block;
        position: relative;
        width: 100%;
        overflow: hidden;
        -ms-touch-action: pan-y;
    }
    .owl-theme .owl-nav [class*=owl-] {
        color: #FFF;
        font-size: 14px;
        margin: 5px;
        padding: 4px 7px;
        background: #ffffff00;
        display: inline-block;
        cursor: pointer;
        border-radius: 3px;
    }
    .slider-section {
        position:relative;
    }
    .slider-section a.btn {
        position:absolute;
        bottom:70px;
        left:50%;
        transform:translate(-50%, 0);
        margin:0;
        padding:0;
    }
    .cms-index-index #header {
        margin-bottom:63px;
    }
    #desktop-carousel {
        display:block;
    }
    #mobile-carousel {
        display:none;
    }
    .page-header {
        padding-bottom:0;
    }
    .btn.shop-now {
        width: 140px;
        height: 30px;
        text-align: center;
        line-height: 27px;
        margin: 0 0 0 -70px;
        background: black;
        color: #fff;
        border-radius: 0;
        font-size: 14px;
        border-color: black;

    }
    /* Next & previous buttons */
    .carousel-control.right  { 
        cursor: pointer;
        position: absolute;
        top: 46%;
        width: auto;
        margin-top: -22px;
        padding: 16px;
        color:white;
        font-weight: bold;
        font-size: 25px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
    }
    .carousel-control.left{
        cursor: pointer;
        position: absolute;
        top: 46%;
        width: auto;
        margin-top: -22px;
        padding: 16px;
        color:white;
        font-weight: bold;
        font-size: 25px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
    }

    /* Position the "next button" to the right */
    .next {
        right: 0;
        border-radius: 3px 0 0 3px;
    }

    .prev {
        left: 0;
        border-radius: 3px 0 0 3px;
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover, .next:hover {
        background-color: rgba(0,0,0,0.8);
    }
    .module-small {
        padding: 33px 0;
    }
    .checklist ul li .active{
        font-weight: 700;
    }
    .carousel-control.left, .carousel-control.right{ 
        background: none !important;
        filter: progid:none !important;>
    }
    @media only screen and (max-width:992px) {
        .cms-index-index #header {
            margin-bottom:0;
        }
        .category-area.mobile-margin {
            margin-top:40px;
        }

        #desktop-carousel {
            display:none;
        }
        #mobile-carousel {
            display:block;
        }
    }
    .block.upsell .btn-prev, .block.upsell .btn-next {
        position:absolute;
        top:50%;
        background: rgba(255, 255, 255, 0);
    }
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page messages">
                        <div data-placeholder="messages"></div>
                        <div data-bind="scope: 'messages'">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style xml="space">
        @media (max-width: 999px) {
            .slideshow .desktop-img {
                display: none;
            }
            .slideshow .mobile-img {
                display: block;
            }
        </style>
        <section class="slider-section">
            <div id="desktop-carousel" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="{{url('/')}}/storage/app/public/img/slider/1_rez.jpg" alt="Los angeles" style="width:100%;">
                    </div>

                    <div class="item">
                        <img src="{{url('/')}}/storage/app/public/img/slider/2_rez.jpg" alt="Chicago" style="width:100%;">
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#desktop-carousel" data-slide="prev">
                   <!-- <span class="glyphicon glyphicon-chevron-left"></span>
                    <img src="{{url('/')}}/storage/app/public/left-arrow.svg"/> --->
                    <img style="width:34px" src="{{url('/')}}/storage/app/public/icon_prev.svg"/>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#desktop-carousel" data-slide="next">
                   <!-- <span class="glyphicon glyphicon-chevron-right"></span> 
                    <img  src="{{url('/')}}/storage/app/public/right-arrow.svg"/>--->
                    <img style="width:34px" src="{{url('/')}}/storage/app/public/icon_next.svg"/>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <a class="btn shop-now" href="#">Shop Now</a>
            <div id="mobile-carousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="{{url('/')}}/storage/app/public/img/slider/mobile_banner.jpg" alt="Los angeles" style="width:100%;">
                    </div>

                    <div class="item">
                        <img src="{{url('/')}}/storage/app/public/img/slider/m_b_1.jpg" alt="Chicago" style="width:100%;">
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#mobile-carousel" data-slide="prev">
                    <img style="width:34px" src="{{url('/')}}/storage/app/public/icon_prev.svg"/>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#mobile-carousel" data-slide="next">
<!--                    <span class="glyphicon glyphicon-chevron-right"></span>-->
                    <img style="width:34px" src="{{url('/')}}/storage/app/public/icon_next.svg"/>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <a class="btn shop-now" href="#">Shop Now</a>
        </section>
        <!--/Carousel slider -->
        <section class="category-area mobile-margin">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="category-holder">
                            <div class="category-gallery">
                                <div class="mask">
                                    <div class="slideset">
                                        <div class="product">
                                            <div class="holder">
                                                <div class="img"><a href="#"><img src="https://pride-limited.com/storage/app/public/banner/small_banner_1.jpg" alt="" /></a></div>
                                                <div class="info"><strong>ETHNIC MENSWEAR</strong> 
                                                    <a class="btn btn-default" href="#">shop now</a></div>
                                            </div>
                                        </div>
                                        <div class="product">
                                            <div class="holder">
                                                <div class="img"><a href="#"><img src="https://pride-limited.com/storage/app/public/img/banner/small_banner_home_resize.jpg" alt="" /></a></div>
                                                <div class="info"><strong>Ecommerce Homes</strong> <a class="btn btn-default" href="#">shop now</a></div>
                                            </div>
                                        </div>
                                        <div class="product">
                                            <div class="holder">
                                                <div class="img"><a href="#">
                                                        <img src="https://pride-limited.com/storage/app/public/banner/small_banner_2.jpg" alt="" /></a></div>
                                                <div class="info"><strong>Ecommerce GIRLS</strong> <a class="btn btn-default" href="#">shop now</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product add">
                                    <div class="holder">
                                        <div class="img"><a href="#"><img src="{{url('/')}}/storage/app/public/img/banner/banner4.jpg" alt="" /></a></div>
                                        <div class="info"><strong>Kids</strong> <a class="btn btn-default" href="#">shop now</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
            <div class="container">
                <div class="row">
                    <div class="block upsell" data-mage-init='{"upsellProducts":{}}' data-limit="4" data-shuffle="1">
                        <div class="block-title title">
                            <strong id="block-upsell-heading" role="heading" aria-level="2">NEW ARRIVAL</strong>
                        </div>
                        <div class="block-content content" aria-labelledby="block-upsell-heading">
                            <div class="products wrapper grid products-grid products-upsell">
                                 <ol class="products list items product-items">
                                    @foreach($product_list as $product)
                                    <?php
                                    $product_name = str_replace(' ', '-', $product->product_name);
                                    $product_url = strtolower($product_name);
                                    $data = ProductController::GetProductColorAlbum($product->product_id);
                                    $color_album=str_replace('/','-',$product->productalbum_name);
                                    // dd($data);
                                    $sold_out = ProductController::ProductWiseQty($product->product_id);
                                    $medium_image = ProductController::GetProductImage($product->product_id);
                                   // foreach ($data as $pro_album) {
                                    //    $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                                   // }
                                    ?>
                                    <li class="item product product-item">    
                                       <?php  if($sold_out <= 0){ ?>
                                         <!--<span class="sprice-tag">Sold Out</span> --->
                                         <span class="sold-out">Sold Out</span>
                                       <?php } ?>
                                        <div class="product-item-info" data-container="product-grid">
                                            <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}">
                                                <span class="product-image-container">
                                                        <span class="product-image-wrapper">
                                                            <img style="border:1px solid #eee;" class="product-image-photo" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $medium_image->productimg_img_medium; ?>" alt="<?php echo $product->product_name; ?>"/></span>
                                                    </span>
                                            </a>
                                            <div class="product details product-item-details">
                                                <div class="info-holder">
                                                    <strong class="product name product-item-name">
                                                        <a class="product-item-link"
                                                           href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}">
                                                            {{$product->product_name}}
                                                        </a>
                                                    </strong>
                
                                                </div>
                                                <div class="info-holder">
                                                    <div class="price-box price-final_price" data-role="priceBox" data-product-id="79644" data-price-box="product-id-79644">
                                                        <span class="normal-price">
                                                            <span class="price-container price-final_price tax weee"
                                                                  >
                                                                <!--        <span class="price-label">As low as</span>-->
                                                                <span  id="product-price-79644"                data-price-amount="{{$product->product_price}}"
                                                                       data-price-type="finalPrice"
                                                                       class="price-wrapper "
                                                                       >
                                                                    <span>Tk,&nbsp;{{$product->product_price}}</span>    </span>
                                                            </span>
                                                        </span>
                                                    </div>                                                        
                                                    <div class="product-item-inner">
                                                        <div class="product actions product-item-actions">
                                                            <div class="actions-primary">
                                                                <a href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}" class="action tocart primary"><span>Shop Now</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ol>
                            </div>
                            <a class="btn-next" href="javascript:void(0);"><img style="width:34px" src="{{url('/')}}/storage/app/public/icon_next.svg"/>Next</a>
                            <a class="btn-prev" href="javascript:void(0);"> <img style="width:34px" src="{{url('/')}}/storage/app/public/icon_prev.svg"/>prev</a>
                        </div>
                    </div>
                </div>
                <!--<div class="holder">
                    <div class="img">
                        <a href="#"><img src="#" alt="" /></a>
                    </div>
                    <div class="caption"></div>
                </div> -->
            </div>
        </section>
       <!-- <section class="newsletter-sec">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="news-holder">
                            <div class="news-block">
                                <div class="news-frame">
                                    <h1>Sign up for our newsletter to receive <br> special offers, news &amp; events.</h1>
                                    <div class="block newsletter">
                                        <form class="form subscribe"
                                              novalidate
                                              action="#"
                                              method="post"
                                              data-mage-init='{"validation": {"errorClass": "mage-error"}}'
                                              id="newsletter-validate-detail-home">
                                            <div class="field newsletter">
                                                <label class="label" for="newsletter"><span>Sign Up for Our Newsletter:</span></label>
                                                <div class="control">
                                                    <input name="email" type="email" id="newsletter-home"
                                                           placeholder="Enter your email address"
                                                           data-validate="{required:true, 'validate-email':true}"/>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <button class="action subscribe primary" title="Subscribe" type="submit">
                                                    <span>Subscribe</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="authenticationPopup"  style="display: block;">
                        <!--Google Tag Manager: dataLayer - Added by Mageplaza-->
                        <div class="checkout-popup" style="display: none;">
                            <div class="popup-holder">
                                <div class="popup-frame">
                                    <div class="popup-block"><a class="popup-close" href="#">
                                            <img src="{{url('/')}}/storage/app/public/popup-close.png" alt="" />
                                        </a>
                                        <div class="img-area"><a href="">
                                                <img src="{{url('/')}}/storage/app/public/popup_reupload.jpg" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <style>
                                .beadcumarea{display:none !important;}
                                .checkout-popup {
                                    position: fixed;
                                    left: 0;
                                    top: 0;
                                    bottom: 0;
                                    right: 0;
                                    z-index: 999;
                                    overflow: hidden;
                                    background: rgba(40, 40, 40, 0.7); }
                                .popup-open .checkout-popup {
                                    display: block; 
                                }
                                .checkout-popup .popup-holder {
                                    display: table;
                                    width: 100%;
                                    height: 100%; }
                                .checkout-popup .popup-frame {
                                    display: table-cell;
                                    vertical-align: middle; }
                                @media (max-width: 500px) {
                                    .checkout-popup .popup-frame {
                                        vertical-align: top;
                                        padding: 50px 20px 0; } }
                                .checkout-popup .popup-block {
                                    max-width: 450px;
                                    margin: 0 auto;
                                    background: #fff;
                                    position: relative;
                                    text-align: center;
                                    color: #000;
                                    font-size: 15px;
                                    line-height: 18px;
                                    text-transform: uppercase;
                                    padding: 10px; }
                                .checkout-popup .popup-close {
                                    position: absolute;
                                    right: 0;
                                    top: 0;
                                    width: 30px;
                                    opacity: 0.7;
                                    margin: 2px 2px 0 0; }
                                @media (max-width: 500px) {
                                    .checkout-popup .popup-close {
                                        width: 25px; } 
                                }
                                .checkout-popup .popup-close:hover {
                                    opacity: 1; 
                                }
                                .checkout-popup img {
                                    display: block;
                                    width: 100%; 
                                }
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script  type="text/javascript"  src="{{asset('assets/js/owl.js')}}"></script>
    <script type="text/javascript" xml="space">
jQuery('.popup-close').click(function () {
    jQuery(this).parents('body').toggleClass('popup-open');
    jQuery('.checkout-popup').css("display", "none");
});
    </script>
    <script>
        (function () {
         //   sliderHeight();
        })();
        function sliderHeight() {

            var sliders = document.querySelectorAll("#desktop-carousel .carousel-inner .item img");
            setHeight(sliders, 160) // Call listener function at run time
            var sliders = document.querySelectorAll("#mobile-carousel .carousel-inner .item img");
            setHeight(sliders, 120) // Call listener function at run time


            function setHeight(sliders, offset) {
                var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
                for (let i = 0; i < sliders.length; i++) {
                    sliders[i].style.height = h - offset + 'px';
                }
            }
        }
    </script>
    @endsection
