<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head >
        <meta charset="utf-8"/>
        <meta name="description" content="Shop from Pride Limited Online Store" />
        <meta name="keywords" content=" pride limited, pride limited Bangladesh, pride limited bd, pride limited company, pride limited dhaka Bangladesh ,pride limited jfp dhaka Bangladesh,  
              Pride Classic Collection, Pride Signature Collection, Pride Actinic  Menswear,  Pride Kids Collection, Pride Girls Collection, Pride Crafted, Pride Homes, Pride Classic sari, Pride Signature Sari, 
              Pride classic share, pride signature share, pride classic dupatta, pride classic unstitched three piece,  pride signature unstitched three piece, pride signature stitched three piece,urbantruthbd, 
              pride signature unstitched two piece, pride signature stitched two piece, pride signature unstitched one piece, pride signature stitched one piece,   
              pridegirls, pride girls formal, pride girls semi formal, pride girls casual, pride girls bottom, pride girls dupatta, pride kids boys, pride kids girls, new arrival, 
              pride women collection, pride group, pride online shopping, online ladies wear, online pride, online order, pride Panjabi, pride mans wear, pride long Panjabi, pride short Panjabi,
              pride shari, pride showroom, pride digital print collection, Pride banani 11, pride newmarket, pride Dhanmondi, pride Chittagong,pride bd,pride, bd pride" />
        <meta name="robots" content="INDEX,FOLLOW"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>@yield('title')</title>
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/calendar.css')}}" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/bootstrap.css')}}" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/main.css')}}" />
        <!--<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/font-awesome-4.7.0/css/font-awesome.min.css')}}" /> -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/font.css')}}" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/searchsuiteautocomplete.css')}}" />

<!--        <script  type="text/javascript"  src="{{asset('assets/js/require.js')}}"></script>-->
<!--        <script  type="text/javascript"  src="{{asset('assets/js/mixins.js')}}"></script>-->
<!--        <script  type="text/javascript"  src="{{asset('assets/js/requirejs-config.js')}}"></script>-->
        <script  type="text/javascript"  src="{{asset('assets/js/jquery.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/bootstrap.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/jquery.main.js')}}"></script>

        <link  rel="icon" type="image/x-icon" href="{{ URL::to('') }}/storage/app/public/img/logo.png" />
        <link  rel="shortcut icon" type="image/x-icon" href="{{ URL::to('') }}/storage/app/public/img/logo.png" />
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">
        <meta name="google-site-verification" content="" />
        <style>
            #om button {display:none;}
            .icon-instagram:before {
                content: "\f030";
                font-family: FontAwesome;
            }
            .icon-social-twitter:before {
                content: "\f099";
                font-family: FontAwesome;
            }
            .icon-facebook:before {
                content: "\f09a";
                font-family: FontAwesome;
            }
        </style>
        <script type="text/javascript">
var base_url = "{{ URL::to('') }}";
var csrf_token = "{{ csrf_token() }}";
        </script>
        <script>
            jQuery('button.open-close').click(function () {
                jQuery(this).parent(this).toggleClass('open-close-icon');
            });
        </script>
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/font-awesome-4.7.0/css/font-awesome.min.css')}}"/>    
    </head>
    <body data-container="body" data-mage-init='{"loaderAjax": {}, "loader": { "icon": "#"}}' class="cms-home cms-index-index page-layout-1column">
        <div class="wrapper">
            <script>
                try {
                    if (!window.localStorage || !window.sessionStorage) {
                        throw new Error();
                    }
                    localStorage.setItem('storage_test', 1);
                    localStorage.removeItem('storage_test');
                } catch (e) {
                    (function () {
                        var Storage = function (type) {
                            var data;

                            function createCookie(name, value, days) {
                                var date, expires;

                                if (days) {
                                    date = new Date();
                                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                                    expires = '; expires=' + date.toGMTString();
                                } else {
                                    expires = '';
                                }
                                document.cookie = name + '=' + value + expires + '; path=/';
                            }

                            function readCookie(name) {
                                var nameEQ = name + '=',
                                        ca = document.cookie.split(';'),
                                        i = 0,
                                        c;

                                for (i = 0; i < ca.length; i++) {
                                    c = ca[i];

                                    while (c.charAt(0) === ' ') {
                                        c = c.substring(1, c.length);
                                    }

                                    if (c.indexOf(nameEQ) === 0) {
                                        return c.substring(nameEQ.length, c.length);
                                    }
                                }

                                return null;
                            }

                            function setData(data) {
                                data = encodeURIComponent(JSON.stringify(data));
                                createCookie(type === 'session' ? getSessionName() : 'localStorage', data, 365);
                            }

                            function clearData() {
                                createCookie(type === 'session' ? getSessionName() : 'localStorage', '', 365);
                            }

                            function getData() {
                                var data = type === 'session' ? readCookie(getSessionName()) : readCookie('localStorage');

                                return data ? JSON.parse(decodeURIComponent(data)) : {};
                            }

                            function getSessionName() {
                                if (!window.name) {
                                    window.name = new Date().getTime();
                                }

                                return 'sessionStorage' + window.name;
                            }

                            data = getData();

                            return {
                                length: 0,
                                clear: function () {
                                    data = {};
                                    this.length = 0;
                                    clearData();
                                },

                                getItem: function (key) {
                                    return data[key] === undefined ? null : data[key];
                                },

                                key: function (i) {
                                    var ctr = 0,
                                            k;

                                    for (k in data) {
                                        if (ctr.toString() === i.toString()) {
                                            return k;
                                        } else {
                                            ctr++
                                        }
                                    }

                                    return null;
                                },

                                removeItem: function (key) {
                                    delete data[key];
                                    this.length--;
                                    setData(data);
                                },

                                setItem: function (key, value) {
                                    data[key] = value.toString();
                                    this.length++;
                                    setData(data);
                                }
                            };
                        };

                        window.localStorage.__proto__ = window.localStorage = new Storage('local');
                        window.sessionStorage.__proto__ = window.sessionStorage = new Storage('session');
                    })();
                }
            </script>
            <noscript>
            <div class="message global noscript">
                <div class="content">
                    <p>
                        <strong>JavaScript seems to be disabled in your browser.</strong>
                        <span>For the best experience on our site, be sure to turn on Javascript in your browser.</span>
                    </p>
                </div>
            </div>
            </noscript>
            <div class="page-wrapper"><header id="header" class="page-header">
                    <div class="topnote">
                        <p style="margin: 0 auto; max-width: 1250px; padding: 0 15px; text-align: left;">Free shipping on all orders above tk.3000/-</p>
                    </div>
                    <div class="header-block"><div class="container">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-4"><div class="help-area">
                                        <strong class="need-help"><a href="#">Need help?</a></strong>
                                        <div class="select-lang">
                                            <span><a href="tel:0966-910-0216">0966-910-0216</a></span> 
                                            <!--        <select>
                                            <option>Bangla</option>
                                            <option>English</option>
                                        </select>-->
                                        </div>
                                    </div>
                                </div><div class="col-xs-6 col-sm-6 col-md-4 text-center hidden-sm hidden-xs">
                                    <strong class="logo">
                                        <a  href="{{url('/')}}"><img src="{{url('/')}}/storage/app/public/logo.png"/><span></span></a>
                                    </strong>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-4">
                                    <ul class="header links">
                                        <li><a href="#" >My Account</a></li>  	
                                        @guest
                                        <li class="dropdown-submenu"><a class="dropdown-menu__link" href="{{ route('login') }}">Login / Register</a></li>
                                        @else
                                        <li class="dropdown-submenu"><a class="dropdown-menu__link" href="{{url('/shop-cart')}}">View Bag</a></li>
                                        <!--  <li><a href="{{url('/my-account')}}">My Account</a></li>
                                          <li><a href="{{url('/track-order')}}">Track My Order</a></li> --->
                                        <li class="dropdown-submenu">
                                            <a class="dropdown-menu__link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
										<li class="greet welcome" data-bind="scope: 'customer'">
											<!-- ko if: customer().fullname  -->
											<span data-bind="text: new String('Welcome, %1!').replace('%1', customer().firstname)">Welcome, {{ Auth::user()->name }}!</span>
											<!-- /ko -->
											<!-- ko ifnot: customer().fullname  --><!-- /ko -->
										</li>
                                        @endguest
                                        
                                    </ul>
                                </div>
                                <a class="action skip contentarea" href="#contentarea"><span>Skip to Content</span></a>
                            </div>
                        </div>
                    </div><div class="nav-block">
                        <div class="nav-wrap">
                            <div class="nav-holder"> 
                                <a href="#" class="nav-opener"><span>menu</span></a>	
                                <div class="block block-search">
                                    <div class="block block-title"><strong>Search</strong></div>
                                    <div class="block block-content">
                                        <form class="form minisearch" id="search_mini_form" action="#" method="get">
                                            <div class="field search">
                                                <label class="label" for="search" data-role="minisearch-label">
                                                    <span>Search </span>
                                                </label>
                                                <div class="control">
                                                    <input id="search"
                                                           data-mage-init='{"quickSearch":{
                                                           "formSelector":"#search_mini_form",
                                                           "url":"#",
                                                           "destinationSelector":"#search_autocomplete"}
                                                           }'
                                                           type="text"
                                                           name="q"
                                                           value=""
                                                           placeholder="Search"
                                                           class="input-text"
                                                           maxlength="128"
                                                           role="combobox"
                                                           aria-haspopup="false"
                                                           aria-autocomplete="both"
                                                           autocomplete="off"/>
                                                    <div id="search_autocomplete" class="search-autocomplete"></div>
                                                    <div class="nested">
                                                        <a class="action advanced" href="#" data-action="advanced-search">
                                                            Advanced Search    
                                                        </a>
                                                    </div>

                                                    <div data-bind="scope: 'searchsuiteautocomplete_form'">
                                                        <!-- ko template: getTemplate() --><!-- /ko -->
                                                    </div>

                                                    <script type="text/x-magento-init">
                                                        {
                                                        "*": {
                                                        "Magento_Ui/js/core/app": {
                                                        "components": {
                                                        "searchsuiteautocomplete_form": {
                                                        "component": "MageWorx_SearchSuiteAutocomplete/js/autocomplete"
                                                        },
                                                        "searchsuiteautocompleteBindEvents": {
                                                        "component": "MageWorx_SearchSuiteAutocomplete/js/bindEvents",
                                                        "config": {
                                                        "searchDelay": "500"
                                                        }
                                                        },
                                                        "searchsuiteautocompleteDataProvider": {
                                                        "component": "MageWorx_SearchSuiteAutocomplete/js/dataProvider",
                                                        "config": {
                                                        "url": "#"
                                                        }
                                                        }
                                                        }
                                                        }
                                                        }
                                                        }
                                                    </script>                
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <button type="submit"
                                                        title="Search"
                                                        class="action search">
                                                    <span>Search </span>
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <nav id="nav" role="navigation" >
                                    <div class="menu" id="om">
                                        <ul>
                                            <li><a href="#"  class="level-top" ><span>New In</span></a>
                                                <ul>
                                                    <li><a href="{{url('new-in/woman')}}" ><span>Woman</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-1-1-1 first"><a href="#" ><span>Signature</span></a></li>
                                                            <li  class="level2 nav-1-1-2"><a href="#" ><span>Classic</span></a></li>
                                                            <li  class="level2 nav-1-1-3"><a href="#" ><span>Pride Girls</span></a></li>
                                                            <li  class="level2 nav-1-1-4 last"><a href="#" ><span>Bottoms</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <li><a href="{{url('new-in/man')}}" ><span>Mens</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-1-4-1 first"><a href="" ><span>Ethnic Menswear</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <li>
                                                        <a href="{{url('new-in/kids')}}" ><span>Kids</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-1-2-1 first"><a href="" ><span>Boys</span></a></li>
                                                            <li  class="level2 nav-1-2-2 last"><a href="" ><span>Girls</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <li><a href="{{url('new-in/pride-homes')}}" ><span>Accessories</span></a>
                                                        <ul><li  class="level2 nav-1-3-1 first"><a href="" ><span>Woman</span></a></li>
                                                            <li  class="level2 nav-1-3-2 last"><a href="" ><span>Kids</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <div class="bottomstatic"></div>
                                                </ul>
                                            </li>
                                            <li><a href="#"  class="level-top" ><span>Woman</span></a>
                                                <ul>
                                                    <li><a href="{{url("/signature/signature-sari/9/signature")}}" ><span>Pride Signature</span></a><ul>
                                                            <li  class="level2 nav-2-1-1 first"><a href="{{url("/signature/kameez/digital_print/16/72")}}" ><span>Kameez Set</span></a></li>
                                                            <li  class="level2 nav-2-1-2"><a href="{{url("/signature/unstitched_three_piece/12/66")}}" ><span>Unstitched Three Piece</span></a></li>
                                                            <li  class="level2 nav-2-1-3"><a href="{{url("/signature-sari/dupatta/5/13")}}" ><span>Dupatta</span></a></li>
                                                            <li  class="level2 nav-2-1-4"><a href="{{url("/signature/cotton-sari/9/55")}}" ><span>Cotton Sari</span></a></li>
                                                            <li  class="level2 nav-2-1-5"><a href="{{url("/signature/taat-silk-sari/9/56")}}" ><span>Taat/Silk Sari</span></a></li>
                                                            <li  class="level2 nav-2-1-6 last"><a href="{{url("/signature/half-silk-sari/9/58")}}" ><span>Half Silk Sari</span></a></li>
                                                            <li  class="level2 nav-2-1-6 last"><a href="{{url("/signature/muslin-sari/9/60")}}" ><span>Muslin Silk Sari</span></a></li>
                                                        </ul><div class="bottomstatic" ></div>
                                                    </li>
                                                    <li><a href="" ><span>Pride Classic</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-2-2-1 first"><a href="{{url("/classic/classic-sari/7/40")}}" ><span>Classic Sari</span></a></li>
                                                            <li  class="level2 nav-2-2-2"><a href="{{url("/classic/unstitched-three-piece/7/77")}}" ><span>Unstitched Three Piece</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <li><a href="{{url("/pride-girls/all/5/pride-girls")}}" ><span>Pride Girls</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/formal/5/14")}}" ><span>Formal</span></a></li>
                                                            <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/semi-formal/5/15")}}" ><span>Semi Formal</span></a></li>
                                                            <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/casual/5/16")}}" ><span>Casual</span></a></li>
                                                            <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/bottoms/5/50")}}" ><span>Bottoms</span></a></li>
                                                            <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/dupatta/5/13")}}" ><span>Dupatta</span></a></li>
                                                            <li  class="level2 nav-2-3-2 last"><a href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewelry</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div></li>
                                                    <li>
                                                        <a href="#" ><span>Collection</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-2-4-1 first">
                                                                <a href="#" ><span>Eid Collection' 18</span></a>
                                                            </li>
                                                            <li  class="level2 nav-2-4-2">
                                                                <a href="#" ><span>Boishakh 1425</span></a>
                                                            </li>
                                                            <li  class="level2 nav-2-4-3">
                                                                <a href="#" ><span>Puja' 18</span></a>
                                                            </li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <div class="bottomstatic" ><h2>New In</h2>
                                                        <div style="width: 200px; height: 200px; background: url('https://pride-limited.com/storage/app/public/pgallery/241_product_image_thm_2.5.jpg') no-repeat;"></div>
                                                        <p><a class="btn" href="#">Shop Now</a></p>
                                                    </div>
                                                </ul>
                                            </li>
                                            <li><a href="#"  class="level-top" ><span>Mens</span></a>
                                                <ul>
                                                    <li><a href="#" ><span>Panjabi</span></a><ul>
                                                            <li  class="level2 nav-3-1-1 first"><a href="#" ><span>All</span></a></li>
                                                            <li  class="level2 nav-3-1-2"><a href="#" ><span>Regular Fit Panjabi</span></a></li>
                                                            <li  class="level2 nav-3-1-3"><a href="{{url("/athenic-men/short-panjabi/slim-fit/18/76")}}" ><span>Slim Fit Panjabi</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <li><a href="#" ><span>Collection</span></a><ul>
                                                            <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                            <li  class="level2 nav-3-4-2"><a href="#" ><span>Eid Collection '18</span></a></li>
                                                            <li  class="level2 nav-3-4-3"><a href="#" ><span>Puja Collection</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <div class="bottomstatic">
                                                        <h2>New In</h2>
                                                        <div style="width: 200px; height: 200px; background: url('{{url('/')}}/storage/app/public/pgallery/244_product_image_theme.jpg') no-repeat;"></div>
                                                        <p><a class="btn" href="#">Shop Now</a></p>
                                                    </div>
                                                </ul>
                                            </li>
                                            <li><a href="#"  class="level-top" ><span>Kids</span></a>
                                                <ul>
                                                    <li><a href="{{url("/kids/girls/6/all-girls")}}" ><span>Girls</span></a><ul>
                                                            <li  class="level2 nav-3-1-1 first"><a href="#" ><span>Dresses</span></a></li>
                                                            <li  class="level2 nav-3-1-2"><a href="#" ><span>Tunics</span></a></li>
                                                            <li  class="level2 nav-3-1-3"><a href="" ><span>Tops</span></a></li>
                                                            <li  class="level2 nav-3-1-4 last"><a href="#" ><span>Bottoms</span></a></li>
                                                            <li  class="level2 nav-3-1-4 last"><a href="#" ><span>Clothing Sets</span></a></li>
                                                            <li  class="level2 nav-3-1-4 last"><a href="#" ><span>Rompers</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <li><a href="#" ><span>Boys</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-3-2-1 first"><a href="#" ><span>Panjabi</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <li><a href="#" ><span>Collection</span></a><ul>
                                                            <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                            <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Eid Collection</span></a></li>
                                                            <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <div class="bottomstatic">
                                                        <h2>New In</h2>
                                                        <div style="width: 200px; height: 200px; background: url('{{url('/')}}/storage/app/public/pgallery/308_product_image_thm_6.4.jpg') no-repeat;"></div>
                                                        <p><a class="btn" href="#">Shop Now</a></p>
                                                    </div>
                                                </ul>
                                            </li>
                                            <li><a href="#"  class="level-top" ><span>Accessories</span></a>
                                                <ul>
                                                    <li><a href="#" ><span>Woman</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-4-1-5"><a href="#" ><span>Jewellery</span></a></li>
                                                        </ul><div class="bottomstatic" ></div>
                                                    </li>
                                                    <li><a href="#" ><span>Kids</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-4-2-3"><a href="#" ><span>Jewellery</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <div class="bottomstatic" ><h2>New In</h2>
                                                        <div style="width: 200px; height: 200px; background: url('{{url('/')}}/storage/app/public/pgallery/459_product_image_1_thm_4.1.jpg') no-repeat;"></div>
                                                        <p><a class="btn" href="">Shop Now</a></p>
                                                    </div>
                                                </ul>
                                            </li>
                                            <li><a href="#"  class="level-top" ><span>Home</span></a>
                                                <ul>
                                                    <li><a href="#" ><span>Cushion Covers</span></a>
                                                        <ul>
                                                            <li  class="level2 nav-5-2-1 first"><a href="#" ><span>Cushion Covers</span></a></li>
                                                        </ul>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <div class="bottomstatic" ><h2>Home</h2>
                                                        <div style="width: 200px; height: 200px; background: url('{{url('/')}}/storage/app/public/pgallery/396_product_image_1_thm_2.15.jpg') no-repeat;"></div>
                                                        <p><a class="btn" href="#">Shop Now</a></p>
                                                    </div>
                                                </ul>
                                            </li>
                                            <li><a href="#"  class="level-top" ><span>Crafted</span></a></li>
                                            <li><a href=""  class="level-top" ><span>Gift</span></a>
                                                <ul>
                                                    <li><a href="#" ><span>woman</span></a><div class="bottomstatic" ></div>
                                                    </li><li><a href="#" ><span>Gift Card and Boxes</span></a>
                                                        <div class="bottomstatic" ></div>
                                                    </li>
                                                    <div class="bottomstatic" ><p>
                                                            <img src="" alt="" /></p>
                                                        <p><a href="" class="btn">Check My Balance</a></p>
                                                    </div>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="menu-mobile">
                                        <div class="mobile-holder">
                                            <ul>
                                                <li  class="level0 nav-1 first level-top parent"><a href="#"  class="level-top" ><span>New In</span></a>
                                                    <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-346')"></button>
                                                    <div class="dropdown" id="drop-category-node-346"><ul class="level0 "><li  class="level1 nav-1-1 first parent">
                                                                <a href="#" ><span>Woman</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-347')"></button>
                                                                <div class="dropdown" id="drop-category-node-347"><ul class="level1 ">
                                                                        <li  class="level2 nav-1-1-1 first"><a href="{{url('signature/kameez/digital_print/16/72')}}" ><span>Signature</span></a></li>
                                                                        <li  class="level2 nav-1-1-2"><a href="#" ><span>Classic</span></a></li>
                                                                        <li  class="level2 nav-1-1-3"><a href="#" ><span>Pride Girls</span></a></li>
                                                                        <li  class="level2 nav-1-1-4 last"><a href="#" ><span>Bottoms</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-1-2 parent"><a href="#" ><span>Kids</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-349')"></button>
                                                                <div class="dropdown" id="drop-category-node-349">
                                                                    <ul class="level1 ">
                                                                        <li  class="level2 nav-1-2-1 first"><a href="#" ><span>Boys</span></a></li>
                                                                        <li  class="level2 nav-1-2-2 last"><a href="#" ><span>Girls</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-1-3 parent"><a href="#" ><span>Accessories</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-350')"></button>
                                                                <div class="dropdown" id="drop-category-node-350"><ul class="level1 "><li  class="level2 nav-1-3-1 first">
                                                                            <a href="#" ><span>Woman</span></a></li>
                                                                        <li  class="level2 nav-1-3-2 last"><a href="#" ><span>Kids</span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li  class="level0 nav-2 level-top parent"><a href="#"  class="level-top" ><span>Woman</span></a>
                                                    <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-20')"></button>
                                                    <div class="dropdown" id="drop-category-node-20"><ul class="level0 ">
                                                            <li  class="level1 nav-2-1 first parent">
                                                                <a href="#" ><span>Pride Signature</span></a><button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-185')"></button>
                                                                <div class="dropdown" id="drop-category-node-185"><ul class="level1 ">
                                                                        <li  class="level2 nav-2-1-1 first"><a href="#" ><span>Kameez Set</span></a></li>
                                                                        <li  class="level2 nav-2-1-2"><a href="#" ><span>Unstitched Three Piece</span></a></li>
                                                                        <li  class="level2 nav-2-1-3"><a href="#" ><span>Dupatta</span></a></li>
                                                                        <li  class="level2 nav-2-1-4"><a href="" ><span>Cotton Sari</span></a></li>
                                                                        <li  class="level2 nav-2-1-5"><a href="#" ><span>Taat/Silk Sari</span></a></li>
                                                                        <li  class="level2 nav-2-1-6 last"><a href="" ><span>Half Silk Sari</span></a></li>
                                                                        <li  class="level2 nav-2-1-6 last"><a href="" ><span>Muslin Silk Sari</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-2-2 parent">
                                                                <a href="#" ><span>Pride Classic</span></a><button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-139')"></button>
                                                                <div class="dropdown" id="drop-category-node-139">
                                                                    <ul class="level1 ">
                                                                        <li  class="level2 nav-2-2-1 first"><a href="#" ><span>Classic Sari</span></a></li>
                                                                        <li  class="level2 nav-2-2-2"><a href="#" ><span>Unstitched Three Piece</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-2-3 parent"><a href="" ><span>Pride Girls</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-21')"></button>
                                                                <div class="dropdown" id="drop-category-node-21"><ul class="level1 ">
                                                                        <li  class="level2 nav-2-3-1 first"><a href="#" ><span>Formal</span></a></li>
                                                                        <li  class="level2 nav-2-3-1 first"><a href="#" ><span>Semi Formal</span></a></li>
                                                                        <li  class="level2 nav-2-3-1 first"><a href="#" ><span>Casual</span></a></li>
                                                                        <li  class="level2 nav-2-3-1 first"><a href="#" ><span>Bottoms</span></a></li>
                                                                        <li  class="level2 nav-2-3-1 first"><a href="#" ><span>Dupatta</span></a></li>
                                                                        <li  class="level2 nav-2-3-2 last"><a href="#" ><span>Jewelry</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-2-4 last parent"><a href="#" ><span>Collection</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-42')"></button>
                                                                <div class="dropdown" id="drop-category-node-42"><ul class="level1 ">
                                                                        <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                                        <li  class="level2 nav-3-4-2"><a href="#" ><span>Eid Collection '18</span></a></li>
                                                                        <li  class="level2 nav-3-4-3"><a href="#" ><span>Puja Collection</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li  class="level0 nav-8 last level-top parent"><a href="#"  class="level-top" ><span>Mens</span></a>
                                                    <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-456')"></button>
                                                    <div class="dropdown" id="drop-category-node-456"><ul class="level0 "><li  class="level1 nav-8-1 first parent">
                                                                <a href="#" ><span>Panjabi</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-459')"></button>
                                                                <div class="dropdown" id="drop-category-node-459"><ul class="level1 ">
                                                                        <li  class="level2 nav-3-1-1 first"><a href="#" ><span>All</span></a></li>
                                                                        <li  class="level2 nav-3-1-2"><a href="#" ><span>Regular Fit Panjabi</span></a></li>
                                                                        <li  class="level2 nav-3-1-3"><a href="" ><span>Slim Fit Panjabi</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-8-3 last parent"><a href="#" ><span>Collection</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-461')"></button>
                                                                <div class="dropdown" id="drop-category-node-461">
                                                                    <ul class="level1 ">
                                                                        <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                                        <li  class="level2 nav-3-4-2"><a href="#" ><span>Eid Collection '18</span></a></li>
                                                                        <li  class="level2 nav-3-4-3"><a href="#" ><span>Puja Collection</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li  class="level0 nav-3 level-top parent"><a href="#"  class="level-top" ><span>Kids</span></a>
                                                    <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-3')"></button>
                                                    <div class="dropdown" id="drop-category-node-3"><ul class="level0 ">
                                                            <li  class="level1 nav-3-1 first parent"><a href="#" ><span>Girls</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-5')"></button>
                                                                <div class="dropdown" id="drop-category-node-5"><ul class="level1 ">
                                                                        <li  class="level2 nav-3-1-1 first"><a href="#" ><span>Dresses</span></a></li>
                                                                        <li  class="level2 nav-3-1-2"><a href="#" ><span>Tunics</span></a></li>
                                                                        <li  class="level2 nav-3-1-3"><a href="" ><span>Tops</span></a></li>
                                                                        <li  class="level2 nav-3-1-4 last"><a href="#" ><span>Bottoms</span></a></li>
                                                                        <li  class="level2 nav-3-1-4 last"><a href="#" ><span>Clothing Sets</span></a></li>
                                                                        <li  class="level2 nav-3-1-4 last"><a href="#" ><span>Rompers</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-3-2 parent"><a href="#" ><span>Boys</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-171')"></button>
                                                                <div class="dropdown" id="drop-category-node-171">
                                                                    <ul class="level1 ">

                                                                        <li  class="level2 nav-3-2-1 first"><a href="#" ><span>Panjabi</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-3-3 parent"><a href="#" ><span>Collection</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-6')"></button>
                                                                <div class="dropdown" id="drop-category-node-6"><ul class="level1 ">
                                                                        <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                                        <li  class="level2 nav-3-4-2"><a href="#" ><span>Eid Collection '18</span></a></li>
                                                                        <li  class="level2 nav-3-4-3"><a href="#" ><span>Puja Collection</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li  class="level0 nav-4 level-top parent"><a href="#"  class="level-top" ><span>Accessories</span></a>
                                                    <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-54')"></button>
                                                    <div class="dropdown" id="drop-category-node-54">
                                                        <ul class="level0 ">
                                                            <li  class="level1 nav-4-1 first parent"><a href="#" ><span>Woman</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-165')"></button>
                                                                <div class="dropdown" id="drop-category-node-165">
                                                                    <ul class="level1 ">
                                                                        <li  class="level2 nav-4-1-5"><a href="#" ><span>Jewellery</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-4-2 parent"><a href="#" ><span>Kids</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-1787')"></button>
                                                                <div class="dropdown" id="drop-category-node-1787">
                                                                    <ul class="level1 ">
                                                                        <li  class="level2 nav-4-2-3"><a href="#" ><span>Jewellery</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-4-3 last parent"><a href="#" ><span>Collection</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-301')"></button>
                                                                <div class="dropdown" id="drop-category-node-301"><ul class="level1 ">
                                                                        <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                                        <li  class="level2 nav-3-4-2"><a href="#" ><span>Eid Collection '18</span></a></li>
                                                                        <li  class="level2 nav-3-4-3"><a href="#" ><span>Puja Collection</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li  class="level0 nav-5 level-top parent"><a href="#"  class="level-top" ><span>Home</span></a>
                                                    <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-1024')"></button>
                                                    <div class="dropdown" id="drop-category-node-1024"><ul class="level0 ">
                                                            <li  class="level1 nav-5-1 first parent"><a href="#" ><span>Cushion Covers</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-1103')"></button>
                                                                <div class="dropdown" id="drop-category-node-1103">
                                                                    <ul class="level1 ">
                                                                        <li  class="level2 nav-5-1-3"><a href="#" ><span>Cushion Covers</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li  class="level1 nav-5-3 parent"><a href="#" ><span>Collection</span></a>
                                                                <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-1104')"></button>
                                                                <div class="dropdown" id="drop-category-node-1104">
                                                                    <ul class="level1 ">
                                                                        <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                                        <li  class="level2 nav-3-4-2"><a href="#" ><span>Eid Collection '18</span></a></li>
                                                                        <li  class="level2 nav-3-4-3"><a href="#" ><span>Puja Collection</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li  class="level0 nav-6 level-top"><a href="#"  class="level-top" ><span>Crafted</span></a></li>
                                                <li  class="level0 nav-7 level-top parent"><a href=""  class="level-top" ><span>Gift</span></a>
                                                    <button class="open-close" value="Show/Hide" onclick="showHideDiv('drop-category-node-196')"></button>
                                                    <div class="dropdown" id="drop-category-node-196">
                                                        <ul class="level0 ">
                                                            <li  class="level1 nav-7-1 first"><a href="#" ><span>woman</span></a></li>
                                                            <li  class="level1 nav-7-2 last"><a href="#" ><span>Gift Card and Boxes</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                            </div><script type="text/javascript">
                                //jQuery(window).load(function(){
                                //   jQuery('#top-opener-hover').hide();
                                //});

                                jQuery('button.open-close').click(function () {
                                    $(this).parent(this).toggleClass('open-close-icon');
                                });

                                jQuery('a.nav-opener').click(function () {
                                    //  alert('ok');
                                    //  jQuery(this).parents('body').toggleClass('nav-active');
                                    jQuery(this).parents('body').toggleClass('nav-close');
                                });

                                function showHideDiv(ele) {
                                    var srcElement = document.getElementById(ele);
                                    if (srcElement != null) {
                                        if (srcElement.style.display == "block") {
                                            srcElement.style.display = 'none';
                                        } else {
                                            srcElement.style.display = 'block';
                                        }
                                        return false;
                                    }
                                }

                            </script>
                            <style>
                                /*    #top-opener-hover{
                                        position: absolute;
                                        left: 0;
                                        top: 0;
                                        margin: 24px 0 0 12px;
                                        width: 50px;
                                        z-index: 20;
                                    }
                                    @media (min-width: 1000px) {
                                     #top-opener-hover {
                                        display: none; } 
                                    }*/
                            </style>
                            <!--<div id="top-opener-hover"><img src="#" /></div>-->
                            <a href="#" class="nav-opener top-opener"><span>menu</span></a>	
                            <strong class="logo"><a  href="#"><span>PRIDE</span></a></strong>
                            <div class="nav-frame">
                                <div data-block="minicart" class="minicart-wrapper">
                                    <a class="action showcart" href="{{url('/shop-cart')}}"
                                       data-bind="scope: 'minicart_content'">
                                        <span class="text">My Cart <?php
                                            $i = 0;
                                            foreach (Cart::instance('products')->content() as $row) : $i++;
                                                ?>
                                            <?php endforeach; ?>
                                            {{ $i }}&nbsp;</span>
                                        <span class="counter qty empty"
                                              data-bind="css: { empty: !!getCartParam('summary_count') == false }, blockLoader: isLoading">
                                            <span class="counter-number"></span>
                                            <span class="counter-label">
                                                <?php
                                                $i = 0;
                                                foreach (Cart::instance('products')->content() as $row) : $i++;
                                                    ?>
                                                <?php endforeach; ?>
                                                {{ $i }}
                                            </span>
                                        </span>
                                    </a>
                                    <div class="block block-minicart empty"
                                         data-role="dropdownDialog"
                                         data-mage-init='{"dropdownDialog":{
                                         "appendTo":"[data-block=minicart]",
                                         "triggerTarget":".showcart",
                                         "timeout": "2000",
                                         "closeOnMouseLeave": false,
                                         "closeOnEscape": true,
                                         "triggerClass":"active",
                                         "parentClass":"active",
                                         "buttons":[]}}'>
                                        <div id="minicart-content-wrapper" data-bind="scope: 'minicart_content'">
                                            <!-- ko template: getTemplate() --><!-- /ko -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <!-- main content --->
                @yield('content')
                <!-- end main content ---->
                <footer class="page-footer">
                    <div class="footer-aside"><div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <strong class="logo">
                                        <a  href="#"><img src="{{url('/')}}/storage/app/public/logo.png"></a>
                                    </strong>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer content">
                        <div class="container">
                            <div class="row"><div class="col-xs-12">
                                    <div class="footer-links">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <strong class="title">Company</strong>
                                                <ul>
                                                    <li><a href="{{url('/about-us')}}">About Us</a></li>
                                                    <li><a href="{{url('/work-with-us')}}">Work with us</a></li>
                                                    <li><a href="{{url('/contact-us')}}">Contact us</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3"><strong class="title">Customer Care</strong>
                                                <ul>
                                                    <li><a href="{{url('/how-to-order')}}">How To Order</a></li>
                                                    <li><a href="{{url('/exchange-policy')}}">Exchange Policy</a></li>
                                                    <li><a href="#" data-toggle="modal" data-target="#myModal">Size Guide</a></li>
                                                    <li><a href="#">Gift Card</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3"><strong class="title">QUICK LINK</strong>
                                                <ul>
                                                    <li><a href="#" target="_blank">Track Your Order</a></li>
                                                    <li><a href="{{url('/store-locator')}}">Store Locator</a></li>
                                                    <li><a href="{{url('/privacy-cookies')}}">Privacy & Cookies</a></li>
                                                    <li><a href="{{url('/faq')}}">FAQ's</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="footer-newsletter">
                                                    <?php if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) { ?>
                                                        <img src="{{url('/')}}/storage/app/public/superbrand-logo.png" class="img-responsive" style="margin-top: -16px;">
                                                    <?php } else { ?>
                                                        <img src="{{url('/')}}/storage/app/public/superbrand-logo.png" class="img-responsive" style="margin-top: -48px;">
                                                    <?php } ?>
                                                    <!--                                                    <div class="block newsletter">
                                                                                                            <form class="form subscribe"
                                                                                                                  novalidate
                                                                                                                  action="#"
                                                                                                                  method="post"
                                                                                                                  data-mage-init='{"validation": {"errorClass": "mage-error"}}'
                                                                                                                  id="newsletter-validate-detail">
                                                                                                                <div class="field newsletter">
                                                                                                                    <label class="label" for="newsletter"><span>Sign Up for Our Newsletter:</span></label>
                                                                                                                    <div class="control">
                                                                                                                        <input name="email" type="email" id="newsletter"
                                                                                                                               placeholder="Enter your email address"
                                                                                                                               data-validate="{required:true, 'validate-email':true}"/>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="actions">
                                                                                                                    <button class="action subscribe primary" title="Subscribe" type="submit">
                                                                                                                        <span>Sign Up</span>
                                                                                                                    </button>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                        </div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="social-networks">
                        <ul class="social-icons">
                            <li><a class="icon-instagram" href="https://www.instagram.com/instaPride/"><span>instagram</span></a></li>
                            <li><a class="icon-social-twitter" href="https://twitter.com/_Pride"><span>twitter</span></a></li>
                            <li><a class="icon-facebook" href="https://www.facebook.com/Pride/"><span>facebook</span></a></li>
                            <!-- li><a class="icon-pinterest-p" href="https://www.pinterest.com/Pride/"><span>pinterest</span></a></li -->
                        </ul>
                    </div>
                </footer>

                <script type="text/javascript" id="lightning_bolt" src="{{asset('assets/js/LightningBolt.js')}}"></script>
                <script src="{{asset('assets/js/ajax_modal.js')}}"></script>
                <!-- Lightning Bolt Ends -->  
                <script type="text/javascript">
                                // page init
                                jQuery(function () {
                                    initMobileNav();
                                });

                                // mobile menu init
                                function initMobileNav() {
                                    jQuery('body').mobileNav({
                                        hideOnClickOutside: true,
                                        menuActiveClass: 'nav-active',
                                        menuOpener: '.nav-opener',
                                        menuDrop: '.nav-holder'
                                    });
                                }

                                /*
                                 * Simple Mobile Navigation
                                 */
                                ;
                                (function ($) {
                                    function MobileNav(options) {
                                        this.options = $.extend({
                                            container: null,
                                            hideOnClickOutside: false,
                                            menuActiveClass: 'nav-active',
                                            menuOpener: '.nav-opener',
                                            menuDrop: '.nav-drop',
                                            toggleEvent: 'click',
                                            outsideClickEvent: 'click touchstart pointerdown MSPointerDown'
                                        }, options);
                                        this.initStructure();
                                        this.attachEvents();
                                    }
                                    MobileNav.prototype = {
                                        initStructure: function () {
                                            this.page = $('html');
                                            this.container = $(this.options.container);
                                            this.opener = this.container.find(this.options.menuOpener);
                                            this.drop = this.container.find(this.options.menuDrop);
                                        },
                                        attachEvents: function () {
                                            var self = this;

                                            if (activateResizeHandler) {
                                                activateResizeHandler();
                                                activateResizeHandler = null;
                                            }

                                            this.outsideClickHandler = function (e) {
                                                if (self.isOpened()) {
                                                    var target = $(e.target);
                                                    if (!target.closest(self.opener).length && !target.closest(self.drop).length) {
                                                        self.hide();
                                                    }
                                                }
                                            };

                                            this.openerClickHandler = function (e) {
                                                e.preventDefault();
                                                self.toggle();
                                            };

                                            this.opener.on(this.options.toggleEvent, this.openerClickHandler);
                                        },
                                        isOpened: function () {
                                            return this.container.hasClass(this.options.menuActiveClass);
                                        },
                                        show: function () {
                                            this.container.addClass(this.options.menuActiveClass);
                                            if (this.options.hideOnClickOutside) {
                                                this.page.on(this.options.outsideClickEvent, this.outsideClickHandler);
                                            }
                                        },
                                        hide: function () {
                                            this.container.removeClass(this.options.menuActiveClass);
                                            if (this.options.hideOnClickOutside) {
                                                this.page.off(this.options.outsideClickEvent, this.outsideClickHandler);
                                            }
                                        },
                                        toggle: function () {
                                            if (this.isOpened()) {
                                                this.hide();
                                            } else {
                                                this.show();
                                            }
                                        },
                                        destroy: function () {
                                            this.container.removeClass(this.options.menuActiveClass);
                                            this.opener.off(this.options.toggleEvent, this.clickHandler);
                                            this.page.off(this.options.outsideClickEvent, this.outsideClickHandler);
                                        }
                                    };

                                    var activateResizeHandler = function () {
                                        var win = $(window),
                                                doc = $('html'),
                                                resizeClass = 'resize-active',
                                                flag, timer;
                                        var removeClassHandler = function () {
                                            flag = false;
                                            doc.removeClass(resizeClass);
                                        };
                                        var resizeHandler = function () {
                                            if (!flag) {
                                                flag = true;
                                                doc.addClass(resizeClass);
                                            }
                                            clearTimeout(timer);
                                            timer = setTimeout(removeClassHandler, 500);
                                        };
                                        win.on('resize orientationchange', resizeHandler);
                                    };

                                    $.fn.mobileNav = function (options) {
                                        return this.each(function () {
                                            var params = $.extend({}, options, {container: this}),
                                                    instance = new MobileNav(params);
                                            $.data(this, 'MobileNav', instance);
                                        });
                                    };
                                }(jQuery));
                </script>


                <!-- Facebook Pixel Code -->

                <!-- End Facebook Pixel Code -->
            </div>
        </div>
        <div class="container">
            <div class="row"><div class="col-md-12">
                    <small class="copyright">
                        <span>Copyright © 2018 Pride Limited . All Rights reserved. </span>
                    </small>
                </div>
            </div>
        </div>    
    </body>
</html>
