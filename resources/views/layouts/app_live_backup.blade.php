<?php

use App\Http\Controllers\product\ProductController;
?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head >
        <meta charset="utf-8"/>
        <meta name="description" content="Shop from Pride Limited Online Store" />
        <meta name="keywords" content=" pride limited, pride limited Bangladesh, pride limited bd, pride limited company, pride limited dhaka Bangladesh ,pride limited jfp dhaka Bangladesh,  
              Pride Classic Collection, Pride Signature Collection, Pride Actinic  Menswear,  Pride Kids Collection, Pride Girls Collection, Pride Crafted, Pride Homes, Pride Classic sari, Pride Signature Sari, 
              Pride classic share, pride signature share, pride classic dupatta, pride classic unstitched three piece,  pride signature unstitched three piece, pride signature stitched three piece,urbantruthbd, 
              pride signature unstitched two piece, pride signature stitched two piece, pride signature unstitched one piece, pride signature stitched one piece,   
              pridegirls, pride girls formal, pride girls semi formal, pride girls casual, pride girls bottom, pride girls dupatta, pride kids boys, pride kids girls, new arrival, 
              pride women collection, pride group, pride online shopping, online ladies wear, online pride, online order, pride Panjabi, pride mans wear, pride long Panjabi, pride short Panjabi,
              pride shari, pride showroom, pride digital print collection, Pride banani 11, pride newmarket, pride Dhanmondi, pride Chittagong,pride bd,pride, bd pride,pride" />
        <meta name="robots" content="INDEX,FOLLOW"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>@yield('title')</title>
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/calendar.css')}}" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/bootstrap.css')}}" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/main.css')}}?<?php echo time(); ?>" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/font-awesome-4.7.0/css/font-awesome.min.css')}}" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/font.css')}}" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/searchsuiteautocomplete.css')}}" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/bootstrap-social.css')}}" />

<!--        <script  type="text/javascript"  src="{{asset('assets/js/require.js')}}"></script>-->
<!--        <script  type="text/javascript"  src="{{asset('assets/js/mixins.js')}}"></script>-->
<!--        <script  type="text/javascript"  src="{{asset('assets/js/requirejs-config.js')}}"></script>-->
        <script  type="text/javascript"  src="{{asset('assets/js/jquery.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/bootstrap.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/jquery.main.js')}}"></script>
        <?php if (isset($show_fb)) { ?>
            <meta property="og:image" content="{{ url('/')}}/storage/app/public/pgallery/{{ $image }}" />
            <meta property="og:image:secure_url" content="{{ url('/')}}/storage/app/public/pgallery/{{ $image }}" />
            <meta property="og:image:type" content="image/jpeg" />
            <meta property="og:image:width" content="400" />
            <meta property="og:image:height" content="300" />
            <meta property="og:image:alt" content="{{ $product_name}}" />
            <meta property="og:type" content="product" />
            <meta property="og:url" content="{{url()->current()}}" />
            <meta property="og:title" content="{{ $alt }}" />
            <meta property="og:description" content="{{ $description}}" />
        <?php } else { ?>
            <meta property="og:image" content="https://pride-limited.com/storage/app/public/slider/1549195395_1_rez.jpg" />
            <meta property="og:image:secure_url" content="https://pride-limited.com/storage/app/public/slider/1549195395_1_rez.jpg" />
            <meta property="og:image:type" content="image/jpeg" />
            <meta property="og:image:width" content="400" />
            <meta property="og:image:height" content="300" />
            <meta property="og:image:alt" content="Spring Collection 2018" />
            <meta property="og:type" content="product" />
            <meta property="og:url" content="http://pride-limited.com/" />
            <meta property="og:title" content="Pride Limited | A Pride Group Venture" />
            <meta property="og:description" content="Shop from Pride Limited Online Store" />
        <?php }
        ?>
        <link  rel="icon" type="image/x-icon" href="{{ URL::to('') }}/storage/app/public/img/logo.png" />
        <link  rel="shortcut icon" type="image/x-icon" href="{{ URL::to('') }}/storage/app/public/img/logo.png" />
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">
        <meta name="google-site-verification" content="soQvn3sDBt1wfXIsdtZGdXQygBJtZ-JwgnknduoanMI" />
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/custom.css?2')}}" />
        @yield('appended.style')
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100593661-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-100593661-1');
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=567318743638505&ev=PageView &noscript=1"/>
        </noscript>
        <style>
            body {
                font-family:myriad-pro !important;
            }
            #om button {display:none;}
            .icon-instagram:before {
                content: "\f030";
                font-family: FontAwesome;
            }
            .icon-social-twitter:before {
                content: "\f099";
                font-family: FontAwesome;
            }
            .icon-facebook:before {
                content: "\f09a";
                font-family: FontAwesome;
            }
        </style>
        <!-- End Facebook Pixel Code -->
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/font-awesome-4.7.0/css/font-awesome.min.css')}}"/>
        <style>
            <?php
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) { ?>
              .nav-block .logo {
                width: 16%;
                left: 50%;
                margin: -13px 0 0 -65px;
            }
            <?php } ?>
        </style>
        <script type="text/javascript">
          var base_url = "{{ URL::to('') }}";
          var csrf_token = "{{ csrf_token() }}";
        </script>
    </head>
<body data-container="body" data-mage-init='{"loaderAjax": {}, "loader": { "icon": "#"}}' class="cms-home cms-index-index page-layout-1column">
    <div class="wrapper">
        <noscript>
        <div class="message global noscript">
            <div class="content">
                <p>
                    <strong>JavaScript seems to be disabled in your browser.</strong>
                    <span>For the best experience on our site, be sure to turn on Javascript in your browser.</span>
                </p>
            </div>
        </div>
        </noscript>
        <div class="page-wrapper"><header id="header" class="page-header">
                <div class="topnote">
                    <p style="margin: 0 auto; max-width: 1250px; padding: 0 15px; text-align: left; font-size:14px">Free shipping on all orders above tk.3000/-</p>
                </div>
                <div class="header-block">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-4"><div class="help-area">
                                    <strong class="need-help"><a href="#">Need help?</a></strong>
                                    <div class="select-lang">
                                        <span>
                                            <a style="text-decoration:underline" href="#" data-toggle="modal" data-target="#modalForm">Contact-Us Here!</a>
                                        </span> 
                                        <!--        <select>
                                        <option>Bangla</option>
                                        <option>English</option>
                                    </select>-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-4 text-center hidden-sm hidden-xs" style="margin-top:-30px;">
                                <strong class="logo">
                                    <a  href="{{url('/')}}"><img src="{{url('/')}}/storage/app/public/logo.png"/><span></span></a>
                                </strong>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-4">
                                <ul class="header links">
                                    <li><a href="{{url('/my-account')}}" >My Account</a></li>  	
                                    @guest
                                    <!--<li class="dropdown-submenu"><a class="dropdown-menu__link" href="{{ route('login') }}">Login / Register</a></li>-->
                                    @else
                                    <!---<li class="dropdown-submenu"><a class="dropdown-menu__link" href="{{url('/shop-cart')}}">View Bag</a></li>  -->
                                    <!--  <li><a href="{{url('/my-account')}}">My Account</a></li>
                                      <li><a href="{{url('/track-order')}}">Track My Order</a></li> --->
                                    <!--<li class="dropdown-submenu">
                                        <a class="dropdown-menu__link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li> -->
                                    <li class="greet welcome" data-bind="scope: 'customer'">
                                        <span data-bind="text: new String('Welcome, %1!').replace('%1', customer().firstname)">Welcome, {{ Auth::user()->name }}!</span>
                                    </li>
                                    @endguest
                                </ul>
                            </div>
                            <a class="action skip contentarea" href="#contentarea"><span>Skip to Content</span></a>
                        </div>
                    </div>
                </div><div class="nav-block">
                    <div class="nav-wrap">
                        <div class="nav-holder"> 
                            <a href="#" class="nav-opener"><span>menu</span></a>	
                            <div class="block block-search">
                                <div class="block block-title"><strong>Search</strong></div>
                                <div class="block block-content">
                                    <form class="form minisearch" onsubmit="updateSearchLink()" id="search_mini_form" action="{{ url('search-view') }}" method="get">
                                        <div class="field search">
                                            <label class="label" for="search" data-role="minisearch-label">
                                                <span>Search </span>
                                            </label>
                                            <div class="control">
                                                <input id="question" type="text" name="question" value="{{ $key or '' }}" placeholder="Search" class="input-text" maxlength="128" role="combobox"  aria-haspopup="false" aria-autocomplete="both" autocomplete="off"/>
                                                <div id="search_autocomplete" class="search-autocomplete"></div>
                                                <div class="nested">
                                                    <a class="action advanced" href="#" data-action="advanced-search">
                                                        Advanced Search    
                                                    </a>
                                                </div>
                                                <div data-bind="scope: 'searchsuiteautocomplete_form'">
                                                    <div id="searchsuite_autocomplete" class="searchsuite-autocomplete" data-bind="visible: showPopup()" style="">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="actions">
                                            <button type="submit"
                                                    title="Search"
                                                    class="action search">
                                                <span>Search </span>
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <nav id="nav" role="navigation" >
                                <div class="menu" id="om">
                                    <ul>
                                        <li><a href="{{url('/new-arrival')}}"  class="level-top" ><span>New In</span></a>
                                            <ul>
                                                <li><a href="{{url('new-in/woman')}}" ><span>Woman</span></a>
                                                    <ul>
                                                        <!--<li  class="level2 nav-1-1-1 first"><a href="{{url('/amar-ekushay-collection-2019/woman/9')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                        <li  class="level2 nav-1-1-1 first"><a href="{{url('/falgun-collection-2019/woman/9')}}" ><span>Spring 2019</span></a></li>
                                                        <li  class="level2 nav-1-1-1 first"><a href="{{url('/new-arrival/woman/signature')}}" ><span>Signature</span></a></li>
                                                        <li  class="level2 nav-1-1-2"><a href="{{url('/new-arrival/woman/classic')}}" ><span>Classic</span></a></li>
                                                        <li  class="level2 nav-1-1-3"><a href="{{url('/new-arrival/woman/pride-girls')}}" ><span> Girls</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li><a href="{{url('new-in/man')}}" ><span>Mens</span></a>
                                                    <ul>
                                                      <!-- <li  class="level2 nav-1-4-1 first"><a  href="{{url('/amar-ekushay-collection-2019/man/17')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                       <li  class="level2 nav-1-4-1 first"><a href="{{url('/falgun-collection-2019/man/17')}}" ><span>Spring 2019</span></a></li>
                                                        <li  class="level2 nav-1-4-2"><a href="{{url('/new-arrival/man/ethnic')}}" ><span>Ethnic Menswear</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li>
                                                    <a href="{{url('new-in/kids')}}" ><span>Kids</span></a>
                                                    <ul>
                                                      <!--  <li  class="level2 nav-1-2-1 first"><a  href="{{url('/amar-ekushay-collection-2019/kids/6')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                        <li  class="level2 nav-1-2-1 first"><a href="{{url('/falgun-collection-2019/kids/6')}}" ><span>Spring 2019</span></a></li>
                                                        <li  class="level2 nav-1-2-2"><a href="{{url('/new-arrival/kids/boys')}}" ><span>Boys</span></a></li>
                                                        <li  class="level2 nav-1-2-3 last"><a href="{{url('/new-arrival/kids/girls')}}" ><span>Girls</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li><a href="{{url('new-in/pride-homes')}}" ><span>Accessories</span></a>
                                                    <ul><li  class="level2 nav-1-3-1 first"><a href="{{url('/new-arrival/woman/accessories')}}" ><span>Woman</span></a></li>
                                                        <li  class="level2 nav-1-3-2 last"><a href="{{url('/new-arrival/kids/accessories')}}" ><span>Kids</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <div class="bottomstatic" ><h2>New In</h2>
                                                    <?php
                                                    $woman = ProductController::GetLatestProduct();
                                                    $product_name = str_replace(' ', '-', $woman->product_name);
                                                    $product_url = strtolower($product_name);
                                                    ?>
                                                    <div>
                                                        <center>
                                                            <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $woman->productimg_img; ?>" style="width: 150px; height: 200px;border:1px solid #eee;"/>
                                                        </center>
                                                    </div>
                                                    <p>
                                                        <a class="btn" href="{{url("shop/{$product_url}/color-{$woman->productalbum_name}/{$woman->product_id}")}}">Shop Now</a>
                                                    </p>
                                                </div>
                                            </ul>
                                        </li>
                                        <li><a href="#"  class="level-top" ><span>Woman</span></a>
                                            <ul>
                                                <li>
                                                    <a href="{{url("/signature/signature-sari/9/signature")}}" >
                                                        <img class="logo-img" src="{{ URL::to('') }}/storage/app/public/img/menu/s1.png"/>
<!--                                                        <span> Signature</span>-->
                                                    </a>
                                                    <ul>
                                                        <li  class="level2 nav-2-1-1 first"><a href="{{url("/signature/kameez/digital_print/16/72")}}" ><span>Kameez Set</span></a></li>
                                                        <li  class="level2 nav-2-1-2"><a href="{{url("/signature/unstitched_three_piece/12/66")}}" ><span>Unstitched Three Piece</span></a></li>
                                                        <li  class="level2 nav-2-1-3"><a href="{{url("/signature-sari/dupatta/5/13")}}" ><span>Orna</span></a></li>
                                                        <li  class="level2 nav-2-1-4"><a href="{{url("/signature/cotton-sari/9/55")}}" ><span>Cotton Sari</span></a></li>
                                                        <li  class="level2 nav-2-1-5"><a href="{{url("/signature/taat-silk-sari/9/56")}}" ><span>Taat/Silk Sari</span></a></li>
                                                        <li  class="level2 nav-2-1-6 last"><a href="{{url("/signature/half-silk-sari/9/58")}}" ><span>Half Silk Sari</span></a></li>
                                                        <li  class="level2 nav-2-1-6 last"><a href="{{url("/signature/muslin-sari/9/60")}}" ><span>Muslin Silk Sari</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li>
                                                    <a href="#" >
                                                        <img class="logo-img" src="{{ URL::to('') }}/storage/app/public/img/menu/classic.png"/>
<!--                                                        <span> Classic</span>-->
                                                    </a>
                                                    <ul>
                                                        <li  class="level2 nav-2-2-1 first"><a href="{{url("/classic/classic-sari/7/40")}}" ><span>Classic Sari</span></a></li>
                                                        <li  class="level2 nav-2-2-2"><a href="{{url("/classic/unstitched-three-piece/7/77")}}" ><span>Unstitched Three Piece</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li><a href="{{url("/pride-girls/all/5/pride-girls")}}" >
                                                        <img class="logo-img" src="{{ URL::to('') }}/storage/app/public/img/menu/girls.png"/>
<!--                                                        <span> Girls</span>-->
                                                    </a>
                                                    <ul>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/formal/5/14")}}" ><span>Formal</span></a></li>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/semi-formal/5/15")}}" ><span>Semi Formal</span></a></li>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/casual/5/16")}}" ><span>Casual</span></a></li>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/bottoms/5/50")}}" ><span>Bottoms</span></a></li>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/dupatta/5/13")}}" ><span>Orna</span></a></li>
                                                        <li  class="level2 nav-2-3-2 last"><a href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewelry</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div></li>
                                                <li>
                                                    <a href="#" ><span>Collection</span></a>
                                                    <ul>
                                                       <!-- <li  class="level2 nav-2-4-1 first">
                                                          <a href="{{url('/amar-ekushay-collection-2019/woman/9')}}" ><span>Amar Ekushay 2019</span></a>
                                                        </li> -->
                                                        <li  class="level2 nav-2-4-1 first">
                                                          <a href="{{url('/falgun-collection-2019/woman/9')}}" ><span>Spring 2019</span></a>
                                                        </li>
                                                         <!--<li  class="level2 nav-2-4-1 first">
                                                            <a href="{{url('/amar-ekushay-collection-2019')}}" ><span>Amar Ekushay Collection</span></a>
                                                        </li> -->
                                                        
                                                        <!--<li  class="level2 nav-2-4-1 first">
                                                            <a href="#" ><span>Eid Collection' 18</span></a>
                                                        </li>
                                                        <li  class="level2 nav-2-4-2">
                                                            <a href="#" ><span>Boishakh 1425</span></a>
                                                        </li>
                                                        <li  class="level2 nav-2-4-3">
                                                            <a href="#" ><span>Puja' 18</span></a>
                                                        </li> --->
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <div class="bottomstatic" ><h2>New In</h2>
                                                    <?php
                                                    $woman = ProductController::GetLatestWomanProduct();
                                                    $product_name = str_replace(' ', '-', $woman->product_name);
                                                    $product_url = strtolower($product_name);
                                                    ?>
                                                    <div>
                                                        <center>
                                                            <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $woman->productimg_img; ?>" style="width: 150px; height: 200px;border:1px solid #eee;"/>
                                                        </center>
                                                    </div>
                                                    <p>
                                                        <a class="btn" href="{{url("shop/{$product_url}/color-{$woman->productalbum_name}/{$woman->product_id}")}}">Shop Now</a>
                                                    </p>
                                                </div>
                                            </ul>
                                        </li>
                                        <li><a href="#"  class="level-top" ><span>Mens</span></a>
                                            <ul>
                                                <li>
                                                    <a href="{{url("/athenic-men/panjabi/17/all-panjabi")}}" >
                                                        <img class="logo-img" src="{{ URL::to('') }}/storage/app/public/img/menu/athenic.png"/>
<!--                                                        <span>Panjabi</span>-->
                                                    </a>
                                                    <ul>
                                                        <li  class="level2 nav-3-1-2"><a href="{{url("/athenic-men/long-panjabi/regular-fit/17/73")}}" ><span>Regular Fit Panjabi</span></a></li>
                                                        <li  class="level2 nav-3-1-3"><a href="{{url("/athenic-men/short-panjabi/slim-fit/18/76")}}" ><span>Slim Fit Panjabi</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li><a href="#" ><span>Collection</span></a>
                                                <ul>
                                                       <!-- <li  class="level2 nav-2-4-1 first">
                                                           <a  href="{{url('/amar-ekushay-collection-2019/man/17')}}" ><span>Amar Ekushay 2019</span></a>
                                                        </li> --->
                                                        <li  class="level2 nav-2-4-1 first">
                                                            <a href="{{url('/falgun-collection-2019/man/17')}}" ><span>Spring 2019</span></a>
                                                        </li>
                                                       <!-- <li  class="level2 nav-3-4-1 first"><a href="{{url('/boishakh-1425/ethnic-menswear/17')}}" ><span>Boishakh 1425</span></a></li>
                                                        <li  class="level2 nav-3-4-2"><a href="{{url('/eid-collection-201/ethnic-menswear/17')}}" ><span>Eid Collection '18</span></a></li>
                                                        <li  class="level2 nav-3-4-3"><a href="{{url('/puja-collection-2018/ethnic-menswear/17')}}" ><span>Puja Collection</span></a></li> -->
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <div class="bottomstatic">
                                                    <h2>New In</h2>
                                                    <?php
                                                    $woman = ProductController::GetLatestManProduct();
                                                    $product_name = str_replace(' ', '-', $woman->product_name);
                                                    $product_url = strtolower($product_name);
                                                    ?>
                                                    <div>
                                                        <center>
                                                            <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $woman->productimg_img; ?>" style="width: 150px; height: 200px;border:1px solid #eee;"/>
                                                        </center>
                                                    </div>
                                                    <p><a class="btn" href="{{url("shop/{$product_url}/color-{$woman->productalbum_name}/{$woman->product_id}")}}">Shop Now</a></p>
                                                </div>
                                            </ul>
                                        </li>
                                        <li><a href="#"  class="level-top" ><span>Kids</span></a>
                                            <ul>
                                                <li><a href="{{url("/kids/girls/6/all-girls")}}" ><span>Girls</span></a><ul>
                                                        <li  class="level2 nav-3-1-1 first"><a href="{{url('/kids/girls/6/52')}}" ><span>Dresses</span></a></li>
                                                        <li  class="level2 nav-3-1-2"><a href="{{url('/kids/girls/6/81')}}" ><span>Tunics</span></a></li>
                                                        <li  class="level2 nav-3-1-3"><a href="{{url('/kids/girls/6/82')}}" ><span>Tops</span></a></li>
                                                        <li  class="level2 nav-3-1-4 last"><a href="{{url('/kids/girls/6/83')}}" ><span>Bottoms</span></a></li>
                                                        <li  class="level2 nav-3-1-4 last"><a href="{{url('/kids/girls/6/84')}}" ><span>Clothing Sets</span></a></li>
                                                        <li  class="level2 nav-3-1-4 last"><a href="{{url('/kids/girls/6/85')}}" ><span>Rompers</span></a></li>
                                                        <li  class="level2 nav-3-1-4 last"><a href="{{url('/kids/girls/6/89')}}" ><span>Jewelry</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li><a href="{{url('/kids/boys/6/39')}}" ><span>Boys</span></a>
                                                    <ul>
                                                        <li  class="level2 nav-3-2-1 first"><a href="{{url('/kids/boys/6/39')}}" ><span>Panjabi</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li><a href="#" ><span>Collection</span></a>
                                                <ul>
                                                       <!-- <li  class="level2 nav-2-4-1 first">
                                                            <a  href="{{url('/amar-ekushay-collection-2019/kids/6')}}" ><span>Amar Ekushay 2019</span></a>
                                                        </li>  --->
                                                        <li  class="level2 nav-2-4-1 first">
                                                            <a href="{{url('/falgun-collection-2019/kids/6')}}" ><span>Spring 2019</span></a>
                                                        </li> 
                                                       <!-- <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                        <li  class="level2 nav-3-4-1 first"><a href="{{url('/eid-collection-201/pride-kids/6')}}" ><span>Eid Collection</span></a></li>
                                                        <li  class="level2 nav-3-4-1 first"><a href="{{url('/boishakh-1425/pride-kids/6')}}" ><span>Boishakh 1425</span></a></li> -->
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <div class="bottomstatic">
                                                    <h2>New In</h2>
                                                    <?php
                                                    $woman = ProductController::GetLatestKidsProduct();
                                                    $product_name = str_replace(' ', '-', $woman->product_name);
                                                    $product_url = strtolower($product_name);
                                                    ?>
                                                    <div>
                                                        <center>
                                                            <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $woman->productimg_img; ?>" style="width: 150px; height: 200px;border:1px solid #eee;"/>
                                                        </center>
                                                    </div>
                                                    <p><a class="btn" href="{{url("shop/{$product_url}/color-{$woman->productalbum_name}/{$woman->product_id}")}}">Shop Now</a></p>
                                                </div>
                                            </ul>
                                        </li>
                                        <li><a href="#"  class="level-top" ><span>Accessories</span></a>
                                            <ul>
                                                <li><a href="#" ><span>Woman</span></a>
                                                    <ul>
                                                        <li  class="level2 nav-4-1-5"><a href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewellery</span></a></li>
                                                    </ul><div class="bottomstatic" ></div>
                                                </li>
                                                <li><a href="#" ><span>Kids</span></a>
                                                    <ul>
                                                        <li  class="level2 nav-4-2-3"><a href="{{url("/pride-girls/jewelry/6/89")}}" ><span>Jewellery</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <div class="bottomstatic" ><h2>New In</h2>
                                                    <?php
                                                    $woman = ProductController::GetLatestAccessoriesProduct();
                                                    $product_name = str_replace(' ', '-', $woman->product_name);
                                                    $product_url = strtolower($product_name);
                                                    ?>
                                                    <div>
                                                        <center>
                                                            <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $woman->productimg_img; ?>" style="width: 150px; height: 200px;border:1px solid #eee;"/>
                                                        </center>
                                                    </div>
                                                    <p><a class="btn" href="{{url("shop/{$product_url}/color-{$woman->productalbum_name}/{$woman->product_id}")}}">Shop Now</a></p>
                                                </div>
                                            </ul>
                                        </li>
                                        <li><a href="#"  class="level-top" ><span>Homes</span></a>
                                            <ul>
                                                <li><a href="{{url('pride-homes/19/86')}}" ><span>Cushion Covers</span></a>
                                                    <ul>
                                                        <li  class="level2 nav-5-2-1 first"><a href="{{url('pride-homes/19/86')}}" ><span>Cushion Covers</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <div class="bottomstatic" ><h2>New In</h2>
                                                    <?php
                                                    $woman = ProductController::GetLatestHomesProduct();
                                                    $product_name = str_replace(' ', '-', $woman->product_name);
                                                    $product_url = strtolower($product_name);
                                                    ?>
                                                    <div>
                                                        <center>
                                                            <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $woman->productimg_img; ?>" style="width: 150px; height: 200px;border:1px solid #eee;"/>
                                                        </center>
                                                    </div>
                                                    <p><a class="btn" href="{{url("shop/{$product_url}/color-{$woman->productalbum_name}/{$woman->product_id}")}}">Shop Now</a></p>
                                                </div>
                                            </ul>
                                        </li>
                                        <li><a href="#"  class="level-top" ><span>Crafted</span></a></li>
                                        <!---<li><a href="#"  class="level-top" ><span>Gift</span></a>
                                            <ul>
                                                <li><a href="#" ><span>woman</span></a><div class="bottomstatic" ></div>
                                                </li><li><a href="#" ><span>Gift Card and Boxes</span></a>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <div class="bottomstatic" ><p>
                                                        <img src="" alt="" /></p>
                                                    <p><a href="" class="btn">Check My Balance</a></p>
                                                </div>
                                            </ul>
                                        </li> --->
                                    </ul>
                                </div>
                                <div class="menu-mobile">
                                    <div class="mobile-holder">
                                        <ul>
                                            <li  class="level0 nav-1 first level-top parent"><a href="{{url('/new-arrival')}}"  class="level-top" ><span>New In</span></a>
                                                <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-346')"></button>
                                                <div class="dropdown" id="drop-category-node-346"><ul class="level0 ">
                                                        <li  class="level1 nav-1-1 first parent">
                                                            <a href="#" ><span>Woman</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-347')"></button>
                                                            <div class="dropdown" id="drop-category-node-347"><ul class="level1 ">
                                                                  <!--  <li  class="level2 nav-1-1-1 first"><a href="{{url('/amar-ekushay-collection-2019/woman/9')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                                    <li  class="level2 nav-1-1-2"><a href="{{url('/falgun-collection-2019/woman/9')}}" ><span>Spring 2019</span></a></li>
                                                                    <li  class="level2 nav-1-1-3"><a href="{{url('/new-arrival/woman/signature')}}" ><span>Signature</span></a></li>
                                                                    <li  class="level2 nav-1-1-4"><a href="{{url('/new-arrival/woman/classic')}}" ><span>Classic</span></a></li>
                                                                    <li  class="level2 nav-1-1-5"><a href="{{url('/new-arrival/woman/pride-girls')}}" ><span> Girls</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-1-4 parent"><a href="{{url('new-in/man')}}" ><span>Mens</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-351')"></button>
                                                            <div class="dropdown" id="drop-category-node-351">
                                                                <ul class="level1 ">
                                                                   <!-- <li  class="level2 nav-1-4-1 first"><a  href="{{url('/amar-ekushay-collection-2019/man/17')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                                    <li  class="level2 nav-1-4-2"><a href="{{url('/falgun-collection-2019/man/17')}}" ><span>Spring 2019</span></a></li>
                                                                    <li  class="level2 nav-1-4-3"><a href="{{url('/new-arrival/man/ethnic')}}" ><span>Ethnic Menswear</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-1-2 parent"><a href="{{url('new-in/kids')}}" ><span>Kids</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-349')"></button>
                                                            <div class="dropdown" id="drop-category-node-349">
                                                                <ul class="level1 ">
                                                                  <!---  <li  class="level2 nav-1-2-1 first"><a  href="{{url('/amar-ekushay-collection-2019/kids/6')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                                    <li  class="level2 nav-1-2-2"><a href="{{url('/falgun-collection-2019/kids/6')}}" ><span>Spring 2019</span></a></li>
                                                                    <li  class="level2 nav-1-2-3"><a href="{{url('/new-arrival/kids/boys')}}" ><span>Boys</span></a></li>
                                                                    <li  class="level2 nav-1-2-4 last"><a href="{{url('/new-arrival/kids/girls')}}" ><span>Girls</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-1-3 parent"><a href="#" ><span>Accessories</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-350')"></button>
                                                              <div class="dropdown" id="drop-category-node-350"><ul class="level1 ">
                                                                    <li  class="level2 nav-1-3-1 first"><a href="{{url('/new-arrival/woman/accessories')}}" ><span>Woman</span></a></li>
                                                                    <li  class="level2 nav-1-3-2 last"><a href="{{url('/new-arrival/kids/accessories')}}" ><span>Kids</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li  class="level0 nav-2 level-top parent"><a href="#"  class="level-top" ><span>Woman</span></a>
                                                <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-20')"></button>
                                                <div class="dropdown" id="drop-category-node-20"><ul class="level0 ">
                                                        <li  class="level1 nav-2-1 first parent">
                                                            <a href="{{url("/signature/signature-sari/9/signature")}}" ><span> Signature</span></a><button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-185')"></button>
                                                            <div class="dropdown" id="drop-category-node-185"><ul class="level1 ">
                                                                    <li  class="level2 nav-2-1-1 first"><a href="{{url("/signature/kameez/digital_print/16/72")}}" ><span>Kameez Set</span></a></li>
                                                                    <li  class="level2 nav-2-1-2"><a href="{{url("/signature/unstitched_three_piece/12/66")}}" ><span>Unstitched Three Piece</span></a></li>
                                                                    <li  class="level2 nav-2-1-3"><a href="{{url("/signature-sari/dupatta/5/13")}}" ><span>Orna</span></a></li>
                                                                    <li  class="level2 nav-2-1-4"><a href="{{url("/signature/cotton-sari/9/55")}}" ><span>Cotton Sari</span></a></li>
                                                                    <li  class="level2 nav-2-1-5"><a href="{{url("/signature/taat-silk-sari/9/56")}}" ><span>Taat/Silk Sari</span></a></li>
                                                                    <li  class="level2 nav-2-1-6 last"><a href="{{url("/signature/half-silk-sari/9/58")}}" ><span>Half Silk Sari</span></a></li>
                                                                    <li  class="level2 nav-2-1-6 last"><a href="{{url("/signature/muslin-sari/9/60")}}" ><span>Muslin Silk Sari</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-2-2 parent">
                                                            <a href="{{url("/classic/classic-sari/7/40")}}" ><span> Classic</span></a><button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-139')"></button>
                                                            <div class="dropdown" id="drop-category-node-139">
                                                                <ul class="level1 ">
                                                                    <li  class="level2 nav-2-2-1 first"><a href="{{url("/classic/classic-sari/7/40")}}" ><span>Classic Sari</span></a></li>
                                                                    <li  class="level2 nav-2-2-2"><a href="{{url("/classic/unstitched-three-piece/7/77")}}" ><span>Unstitched Three Piece</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-2-3 parent"><a href="{{url("/pride-girls/all/5/pride-girls")}}" ><span>Girls</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-21')"></button>
                                                            <div class="dropdown" id="drop-category-node-21"><ul class="level1 ">
                                                                    <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/formal/5/14")}}" ><span>Formal</span></a></li>
                                                                    <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/semi-formal/5/15")}}" ><span>Semi Formal</span></a></li>
                                                                    <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/casual/5/16")}}" ><span>Casual</span></a></li>
                                                                    <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/bottoms/5/50")}}" ><span>Bottoms</span></a></li>
                                                                    <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/dupatta/5/13")}}" ><span>Orna</span></a></li>
                                                                    <li  class="level2 nav-2-3-2 last"><a href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewelry</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-2-4 last parent"><a href="#" ><span>Collection</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-42')"></button>
                                                            <div class="dropdown" id="drop-category-node-42"><ul class="level1 ">
                                                                  <!--   <li  class="level2 nav-3-4-1 first"><a href="{{url('/amar-ekushay-collection-2019/woman/9')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                                     <li  class="level2 nav-3-4-2"><a href="{{url('/falgun-collection-2019/woman/9')}}" ><span>Spring 2019</span></a></li>
                                                                <!--<li  class="level2 nav-3-4-2"><a href="{{url('/eid-collection-2018')}}" ><span>Eid Collection '18</span></a></li>
                                                                    <li  class="level2 nav-3-4-3"><a href="{{url('/puja-collection-2018/pride-signature/9')}}" ><span>Puja Collection</span></a></li>
                                                                    <li  class="level2 nav-3-4-3"><a href="{{url('/boishakh-1425/pride-girls/5')}}" ><span>Boishakh 1425</span></a></li> -->
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li  class="level0 nav-8 last level-top parent"><a href="{{url("/athenic-men/panjabi/17/all-panjabi")}}"  class="level-top" ><span>Mens</span></a>
                                                <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-456')"></button>
                                                <div class="dropdown" id="drop-category-node-456"><ul class="level0 "><li  class="level1 nav-8-1 first parent">
                                                            <a href="{{url("/athenic-men/panjabi/17/all-panjabi")}}" ><span>Panjabi</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-459')"></button>
                                                            <div class="dropdown" id="drop-category-node-459"><ul class="level1 ">
                                                                    <li  class="level2 nav-3-1-2"><a href="{{url("/athenic-men/long-panjabi/regular-fit/17/73")}}" ><span>Regular Fit Panjabi</span></a></li>
                                                                    <li  class="level2 nav-3-1-3"><a href="{{url("/athenic-men/short-panjabi/slim-fit/18/76")}}" ><span>Slim Fit Panjabi</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-8-3 last parent"><a href="#" ><span>Collection</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-461')"></button>
                                                            <div class="dropdown" id="drop-category-node-461">
                                                                <ul class="level1 ">
                                                                  <!-- <li  class="level2 nav-3-4-1 first"><a  href="{{url('/amar-ekushay-collection-2019/man/17')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                                   <li  class="level2 nav-3-4-2"><a href="{{url('/falgun-collection-2019/man/17')}}" ><span>Spring 2019</span></a></li>
                                                                   <!-- <li  class="level2 nav-3-4-2"><a href="{{url('/eid-collection-201/ethnic-menswear/17')}}" ><span>Eid Collection '18</span></a></li>
                                                                    <li  class="level2 nav-3-4-3"><a href="{{url('/puja-collection-2018/ethnic-menswear/17')}}" ><span>Puja Collection</span></a></li>
                                                                    <li  class="level2 nav-3-4-3"><a href="{{url('/boishakh-1425/ethnic-menswear/17')}}" ><span>Boishakh 1425</span></a></li> -->
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li  class="level0 nav-3 level-top parent"><a href="#"  class="level-top" ><span>Kids</span></a>
                                                <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-3')"></button>
                                                <div class="dropdown" id="drop-category-node-3"><ul class="level0 ">
                                                        <li  class="level1 nav-3-1 first parent"><a href="{{url("/kids/girls/6/all-girls")}}" ><span>Girls</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-5')"></button>
                                                            <div class="dropdown" id="drop-category-node-5"><ul class="level1 ">
                                                                    <li  class="level2 nav-3-1-1 first"><a href="{{url('/kids/girls/6/52')}}" ><span>Dresses</span></a></li>
                                                                    <li  class="level2 nav-3-1-2"><a href="{{url('/kids/girls/6/81')}}" ><span>Tunics</span></a></li>
                                                                    <li  class="level2 nav-3-1-3"><a href="{{url('/kids/girls/6/82')}}" ><span>Tops</span></a></li>
                                                                    <li  class="level2 nav-3-1-4"><a href="{{url('/kids/girls/6/83')}}" ><span>Bottoms</span></a></li>
                                                                    <li  class="level2 nav-3-1-5"><a href="{{url('/kids/girls/6/84')}}" ><span>Clothing Sets</span></a></li>
                                                                    <li  class="level2 nav-3-1-6"><a href="{{url('/kids/girls/6/85')}}" ><span>Rompers</span></a></li>
                                                                    <li  class="level2 nav-3-1-7 last"><a href="{{url('/kids/girls/6/89')}}" ><span>Jewelry</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-3-2 parent"><a href="{{url('/kids/boys/6/39')}}" ><span>Boys</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-171')"></button>
                                                            <div class="dropdown" id="drop-category-node-171">
                                                                <ul class="level1 ">

                                                                    <li  class="level2 nav-3-2-1 first"><a href="{{url('/kids/boys/6/39')}}" ><span>Panjabi</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-3-3 parent"><a href="#" ><span>Collection</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-6')"></button>
                                                            <div class="dropdown" id="drop-category-node-6"><ul class="level1 ">
                                                                  <!--- <li  class="level2 nav-3-4-1 first"><a  href="{{url('/amar-ekushay-collection-2019/kids/6')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                                   <li  class="level2 nav-3-4-2"><a href="{{url('/falgun-collection-2019/kids/6')}}" ><span>Spring 2019</span></a></li>
                                                                    <!--<li  class="level2 nav-3-4-2"><a href="{{url('/eid-collection-201/pride-kids/6')}}" ><span>Eid Collection '18</span></a></li>
                                                                    <li  class="level2 nav-3-4-3"><a href="{{url('/boishakh-1425/pride-kids/6')}}" ><span>Boishakh 1425</span></a></li> -->
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li  class="level0 nav-4 level-top parent"><a href="#"  class="level-top" ><span>Accessories</span></a>
                                                <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-54')"></button>
                                                <div class="dropdown" id="drop-category-node-54">
                                                    <ul class="level0 ">
                                                        <li  class="level1 nav-4-1 first parent"><a href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Woman</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-165')"></button>
                                                            <div class="dropdown" id="drop-category-node-165">
                                                                <ul class="level1 ">
                                                                    <li  class="level2 nav-4-1-5"><a href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewellery</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-4-2 parent"><a href="{{url("/pride-girls/jewelry/6/89")}}" ><span>Kids</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-1787')"></button>
                                                            <div class="dropdown" id="drop-category-node-1787">
                                                                <ul class="level1 ">
                                                                    <li  class="level2 nav-4-2-3"><a href="{{url("/pride-girls/jewelry/6/89")}}" ><span>Jewellery</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-4-3 last parent"><a href="#" ><span>Collection</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-301')"></button>
                                                            <div class="dropdown" id="drop-category-node-301"><ul class="level1 ">
                                                                    <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Spring 2019</span></a></li>
                                                                   <!-- <li  class="level2 nav-3-4-2"><a href="#" ><span>Eid Collection '18</span></a></li>
                                                                    <li  class="level2 nav-3-4-3"><a href="#" ><span>Puja Collection</span></a></li> -->
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li  class="level0 nav-5 level-top parent"><a href="#"  class="level-top" ><span>Ecommerce Site Homes</span></a>
                                                <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-1024')"></button>
                                                <div class="dropdown" id="drop-category-node-1024"><ul class="level0 ">
                                                        <li  class="level1 nav-5-1 first parent"><a href="{{url('pride-homes/19/86')}}" ><span>Cushion Covers</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-1103')"></button>
                                                            <div class="dropdown" id="drop-category-node-1103">
                                                                <ul class="level1 ">
                                                                    <li  class="level2 nav-5-1-3"><a href="{{url('pride-homes/19/86')}}" ><span>Cushion Covers</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li  class="level1 nav-5-3 parent"><a href="#" ><span>Collection</span></a>
                                                            <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-1104')"></button>
                                                            <div class="dropdown" id="drop-category-node-1104">
                                                                <ul class="level1 ">
                                                                   <!-- <li  class="level2 nav-3-4-1 first"><a href="#" ><span>Boishakh 1425</span></a></li>
                                                                    <li  class="level2 nav-3-4-2"><a href="#" ><span>Eid Collection '18</span></a></li>
                                                                    <li  class="level2 nav-3-4-3"><a href="#" ><span>Puja Collection</span></a></li> -->
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li  class="level0 nav-6 level-top"><a href="#"  class="level-top" ><span>Crafted</span></a></li>
                                           <!-- <li  class="level0 nav-7 level-top parent"><a href="#"  class="level-top" ><span>Gift</span></a>
                                                <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-196')"></button>
                                                <div class="dropdown" id="drop-category-node-196">
                                                    <ul class="level0 ">
                                                        <li  class="level1 nav-7-1 first"><a href="#" ><span>woman</span></a></li>
                                                        <li  class="level1 nav-7-2 last"><a href="#" ><span>Gift Card and Boxes</span></a></li>
                                                    </ul>
                                                </div>
                                            </li> -->

                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        <!--<div id="top-opener-hover"><img src="#" /></div>-->
                        <a href="#" class="nav-opener top-opener"><span>menu</span></a>	
                        <strong class="logo"><a href="{{ url('/') }}"><img class="logo-tab" src="{{url('/')}}/storage/app/public/logo_new.png"/></a></strong>
                        <div class="nav-frame">
                            <div data-block="minicart" class="minicart-wrapper">
                                <a class="action showcart" href="{{url('/shop-cart')}}"
                                   data-bind="scope: 'minicart_content'">
                                    <span class="text"><span class="hidden-xs">My Cart</span> <?php
                                        $i = 0;
                                        foreach (Cart::instance('products')->content() as $row) : $i++;
                                            ?>
                                        <?php endforeach; ?>
                                        {{ $i }}&nbsp;</span>
                                    <span class="counter qty empty"
                                          data-bind="css: { empty: !!getCartParam('summary_count') == false }, blockLoader: isLoading">
                                        <span class="counter-number"></span>
                                        <span class="counter-label">
                                            <?php
                                            $i = 0;
                                            foreach (Cart::instance('products')->content() as $row) : $i++;
                                                ?>
                                            <?php endforeach; ?>
                                            {{ $i }}
                                        </span>
                                    </span>
                                </a>
                                <div class="block block-minicart empty"
                                     data-role="dropdownDialog"
                                     data-mage-init='{"dropdownDialog":{
                                     "appendTo":"[data-block=minicart]",
                                     "triggerTarget":".showcart",
                                     "timeout": "2000",
                                     "closeOnMouseLeave": false,
                                     "closeOnEscape": true,
                                     "triggerClass":"active",
                                     "parentClass":"active",
                                     "buttons":[]}}'>
                                    <div id="minicart-content-wrapper" data-bind="scope: 'minicart_content'">
                                        <!-- ko template: getTemplate() --><!-- /ko -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- main content --->
            @yield('content')
            <!-- end main content ---->
            <footer class="page-footer">
                <div class="footer-aside"><div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong class="logo">
                                    <a  href="{{url('/')}}"><img src="{{url('/')}}/storage/app/public/logo.png"></a>
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer content">
                    <div class="container">
                        <div class="row"><div class="col-xs-12">
                                <div class="footer-links">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="title">Company</strong>
                                            <ul>
                                                <li><a href="{{url('/about-us')}}">About Us</a></li>
                                                <li><a href="{{url('/work-with-us')}}">Work with us</a></li>
                                                <li><a href="{{url('/contact-us')}}">Contact us</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><strong class="title">Customer Care</strong>
                                            <ul>
                                                <li><a href="{{url('/how-to-order')}}">How To Order</a></li>
                                                <li><a href="{{url('/exchange-policy')}}">Exchange Policy</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#myModal">Size Guide</a></li>
                                                <li><a href="#">Gift Card</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3" style="z-index:1"><strong class="title">QUICK LINK</strong>
                                            <ul>
                                                <li><a href="{{url('/track-order')}}" target="_blank">Track Your Order</a></li>
                                                <li><a href="{{url('/store-locator')}}">Store Locator</a></li>
                                                <li><a href="{{url('/privacy-cookies')}}">Privacy & Cookies</a></li>
                                                <li><a href="{{url('/faq')}}">FAQ's</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="footer-newsletter">
                                                <?php if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) { ?>
                                                    <img src="{{url('/')}}/storage/app/public/superbrand-logo.png" class="img-responsive" style="margin-top: -16px;">
                                                <?php } else { ?>
                                                    <img src="{{url('/')}}/storage/app/public/superbrand-logo.png" class="img-responsive" style="margin-top: -48px;">
                                                <?php } ?>
                                                <!--                                                    <div class="block newsletter">
                                                                                                        <form class="form subscribe"
                                                                                                              novalidate
                                                                                                              action="#"
                                                                                                              method="post"
                                                                                                              data-mage-init='{"validation": {"errorClass": "mage-error"}}'
                                                                                                              id="newsletter-validate-detail">
                                                                                                            <div class="field newsletter">
                                                                                                                <label class="label" for="newsletter"><span>Sign Up for Our Newsletter:</span></label>
                                                                                                                <div class="control">
                                                                                                                    <input name="email" type="email" id="newsletter"
                                                                                                                           placeholder="Enter your email address"
                                                                                                                           data-validate="{required:true, 'validate-email':true}"/>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="actions">
                                                                                                                <button class="action subscribe primary" title="Subscribe" type="submit">
                                                                                                                    <span>Sign Up</span>
                                                                                                                </button>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="social-networks">
                    <ul class="social-icons">
                        <li><a class="icon-instagram" target="_blank" href="https://www.instagram.com/pridelimitedpr/"><span>instagram</span></a></li>
                        <li><a class="icon-social-twitter" target="_blank" href="#"><span>twitter</span></a></li>
                        <li><a class="icon-facebook" target="_blank" href="https://www.facebook.com/PrideLimitedBangladesh/"><span>facebook</span></a></li>
                        <!-- li><a class="icon-pinterest-p" href="https://www.pinterest.com/Pride/"><span>pinterest</span></a></li -->
                    </ul>
                </div>
            </footer>
            <!--- phone modal   ----> 
			<div class="modal fade" id="modalForm" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Modal Header -->
						<!--<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button> -->

						<!-- Modal Body -->
						<div class="modal-body">
							<!--<img src="{{url('/')}}/assets/image/callimage.jpg" class="img-responsive"> -->
							 <h2 style="color:#2516a7;"><i class="fa fa-phone" aria-hidden="true" style="color:#2516a7;font-size:20px;"></i> Need Assistance? Let Us Call You!</h2>
							 <br>
							<div class="form-group">
								<label for= "inputName">Name</label>
								<input type="text" class="form-control" id="inputName" placeholder="Write your name"/>
							</div>
							<div class="form-group">
								<label for= "inputName">Contact Number</label>
								<input type="text" class="form-control" id="inputNumber" placeholder="Enter your contact number"/>
							</div>
							<div class="form-group">
								<label for= "inputName">Leave Message</label>
								<textarea type="text" class="form-control" id="inputmessage" placeholder="Leave message" rows="3"></textarea>
							</div>
							<p class="statusMsg"></p>

						</div>
						<div class="modal-footer">
							<button type="button" style="width:48%;background-color:#2516a7;" class="pull-left" data-dismiss="modal">Cancel</button>
							<button type="button" style="width:48%;background-color:#2516a7;" class="pull-right submitBtn" onClick="submitContactForm()">Send</button>
						</div>
					</div>
				</div>
			</div>
			<!--- End phone modal --->
           <!-- <script type="text/javascript" id="lightning_bolt" src="{{asset('assets/js/LightningBolt.js')}}"></script> -->
            <script src="{{asset('assets/js/ajax_modal.js')}}"></script>
            <!-- Lightning Bolt Ends -->
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <small class="copyright">
                    <span>Copyright © 2019 Ecommerce Site . All Rights reserved. </span>
                </small>
            </div>
        </div>
    </div>
<script  type="text/javascript"  src="{{asset('assets/js/custom.js?17')}}"></script>
@yield('appended.script')
</body>
</html>
