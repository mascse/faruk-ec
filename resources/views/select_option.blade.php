<style>
/* HIDE RADIO */
[type=checkbox] { 
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
}

/* IMAGE STYLES */
[type=checkbox] + img {
  cursor: pointer;
}

/* CHECKED STYLES */
[type=checkbox]:checked + img {
  outline: 2px solid #f00;
}
</style>
@foreach($productlist as $procat)
     <div class="col-md-2 col-xs-4 col-lg-2">
		<label>
		  <input type="checkbox" name="related_product_id[]" value="{{$procat->product_id}}">
		     <img src="http://localhost/pride/storage/app/public/pgallery/{{$procat->productimg_img}}" style="width: 100px;height: auto;border: 1px solid #eee;">
		</label>
	  </div>
@endforeach