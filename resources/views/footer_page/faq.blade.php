@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/sephora.css')}}" />
<main data-v-7cf67cfb="" id="page" class="page-container container">
    <div data-v-72bbedf8="" data-v-7cf67cfb="" class="bp-container">
        <div data-v-72bbedf8="" class="bp-section">
            <div data-v-72bbedf8="" class="header-section" style="background-image: url(&quot;http://192.168.1.182:8080/pride_new_v/storage/app/public/loyalty/banner.jpg&quot;);">
                <div data-v-72bbedf8="" class="logo">
                </div>
                <!--                <div data-v-3b9ae1da="" data-v-72bbedf8="" class="inner-header">
                                    <div data-v-3b9ae1da="" class="text">Beauty Pass is an all-access pass to a bounty of beauty rewards. It's free to join, and every $1 spent earns a point that gets you closer to your next beauty fix.</div>
                                    <div data-v-3b9ae1da="" class="sessions">
                                        <a data-v-3b9ae1da="" href="/users/sign_up" class="btn register">join Loyalty</a>
                                        <div data-v-3b9ae1da="" class="or">or</div
                                        ><a data-v-3b9ae1da="" href="/users/sign_in" class="btn sign-in">sign in</a>
                                    </div>
                                </div>-->
            </div>
            <div data-v-72bbedf8="" class="content-section">

                <!--                <div data-v-506590fb="" data-v-72bbedf8="" class="rewards-container">
                                    <div data-v-61fb2e0f="" data-v-506590fb="" class="heading-container">
                                        <div data-v-61fb2e0f="" class="title">Beauty Pass Rewards</div>
                                        <div data-v-61fb2e0f="" class="description">Browse an exciting collection of must-have deluxe samples and curated sample sets from top brands.</div>
                                    </div>
                
                                    <a data-v-506590fb="" href="/rewards-boutique" class="view-rewards">
                                        <div data-v-506590fb="" class="text">View All Rewards</div>
                                        <div data-v-506590fb="" class="arrow">
                
                                        </div>
                                    </a>
                
                                </div>-->

                <hr data-v-72bbedf8="" class="hr">
                <div data-v-20494ef0="" data-v-72bbedf8="" class="gifts-container">

                    <div data-v-20494ef0="" class=" row">
                        <div data-v-20494ef0="" class="gift col-md-4">
                            <div data-v-20494ef0="" class="img img-bday" style="background-image: url(http://192.168.1.182:8080/pride_new_v/storage/app/public/loyalty/loy.jpg);"></div>
                            <div data-v-20494ef0="" class="title">birthday</div>
                            <div data-v-20494ef0="" class="description">Enjoy 2x bonus points on one purchase during your birthday month</div>
                        </div>
                        <div data-v-20494ef0="" class="gift col-md-4">
                            <div data-v-20494ef0="" class="img img-upgrade" style="background-image: url(&quot;https://static-reg.lximg.com/images/beauty_pass_page/mobile/BP_Revamp_Tier_Upgrade_Web_b1b035fe79145ee8f2c3e6e32ed9627c16e1f5d1_1534824425.jpg&quot;);"></div>
                            <div data-v-20494ef0="" class="title">tier upgrade</div>
                            <div data-v-20494ef0="" class="description">Receive an exclusive Beauty Pass gift with each tier upgrade</div>

                        </div>
                        <div data-v-61fb2e0f="" data-v-20494ef0="" class="heading-container col-md-4">
                            <div data-v-61fb2e0f="" class="title">Complimentary Gifts &amp; Services</div>
                            <div data-v-61fb2e0f="" class="description">Enjoy more amazing rewards and perks when you upgrade to the next tier.</div>

                        </div>
                    </div>

                </div>
                <hr data-v-72bbedf8="" class="hr">
                <div data-v-04702ec3="" data-v-72bbedf8="" class="points-container">
                    <div data-v-61fb2e0f="" data-v-04702ec3="" class="heading-container">
                        <div data-v-61fb2e0f="" class="title">Accumulate Points</div>
                        <div data-v-61fb2e0f="" class="description">Earn points every time you shop, connect and share with us.</div>

                    </div>
                    <div data-v-04702ec3="" class="points">
                        <div data-v-04702ec3="" class="point">
                            <div data-v-04702ec3="" class="img icon-1"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Shop with Sephora</div>
                                <div data-v-04702ec3="" class="description">1 Point for every $1 spent</div>

                            </div>

                        </div>
                        <div data-v-04702ec3="" class="point">
                            <div data-v-04702ec3="" class="img icon-3"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Register your account</div>
                                <div data-v-04702ec3="" class="description">+50 points</div>

                            </div>

                        </div>
                        <div data-v-04702ec3="" class="point">
                            <div data-v-04702ec3="" class="img icon-5"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Complete your Beauty Profile</div>
                                <div data-v-04702ec3="" class="description">+50 points</div>

                            </div>

                        </div>
                        <div data-v-04702ec3="" class="point">
                            <div data-v-04702ec3="" class="img icon-2"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Celebrate your Birthday</div>
                                <div data-v-04702ec3="" class="description">2x points in your birthday month</div>

                            </div>

                        </div>
                        <div data-v-04702ec3="" class="point">
                            <div data-v-04702ec3="" class="img icon-4"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Write a product review</div>
                                <div data-v-04702ec3="" class="description">+5 points</div>

                            </div>

                        </div>
                        <div data-v-04702ec3="" class="point">
                            <div data-v-04702ec3="" class="img icon-6"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Download the mobile app</div>
                                <div data-v-04702ec3="" class="description">+40 points</div>

                            </div>

                        </div>

                    </div>

                </div>
                <hr data-v-72bbedf8="" class="hr">
                <div data-v-be2d245e="" data-v-72bbedf8="" class="membership-container">
                    <div data-v-be2d245e="" class="title">membership levels</div>
                    <table data-v-be2d245e="" class="levels-table">
                        <tr data-v-be2d245e="" class="tr">
                            <td data-v-be2d245e="" class="td"></td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="svg-icon white-card card-icon"></div>
                            </td><td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="svg-icon black-card card-icon"></div>
                            </td><td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="svg-icon gold-card card-icon"></div>
                            </td>
                        </tr>
                        <tr data-v-be2d245e="" class="tr">
                            <td data-v-be2d245e="" class="td">Spend per calendar year</td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="span">FREE</div>
                            </td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="span">BDT 50,000</div>
                            </td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="span">BDT 100,000</div>
                            </td>
                        </tr><tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Beauty Pass Rewards</td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="dot red-dot"></div>
                            </td><td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="dot black-dot"></div>
                            </td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="dot gold-dot"></div>
                            </td></tr><tr data-v-be2d245e="" class="tr">
                            <td data-v-be2d245e="" class="td">Birthday 2x points</td>
                            <td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot red-dot"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot black-dot"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td></tr><tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Members-only promotions</td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot red-dot"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot black-dot"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td></tr><tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Welcome gift</td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot black-dot"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td></tr><tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Birthday gift</td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot black-dot"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td></tr><tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Private sales</td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot black-dot"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td></tr><tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Private sale early access</td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td></tr><tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Exclusive events &amp; launches</td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td></tr><tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Custom makeovers</td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td><td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td></tr></table></div><hr data-v-72bbedf8="" class="hr"><div data-v-0834b57d="" data-v-72bbedf8="" class="how-container"><div data-v-0834b57d="" class="title">how it works</div><div data-v-0834b57d="" class="hows"><div data-v-0834b57d="" class="how"><div data-v-0834b57d="" class="img-section"><div data-v-0834b57d="" class="img img-create"></div></div><div data-v-0834b57d="" class="description"><div data-v-0834b57d="" class="title">create an account</div><div data-v-0834b57d="" class="detail">Sign up with your email address or your social media accounts.</div></div></div><div data-v-0834b57d="" class="how"><div data-v-0834b57d="" class="img-section"><div data-v-0834b57d="" class="img img-earn"></div></div><div data-v-0834b57d="" class="description"><div data-v-0834b57d="" class="title">earn points</div><div data-v-0834b57d="" class="detail">Every $1 spent earns you one point.</div></div></div><div data-v-0834b57d="" class="how"><div data-v-0834b57d="" class="img-section"><div data-v-0834b57d="" class="img img-rewarded"></div></div><div data-v-0834b57d="" class="description"><div data-v-0834b57d="" class="title">get rewarded</div><div data-v-0834b57d="" class="detail">Use points to redeem exclusive rewards.</div></div></div><div data-v-0834b57d="" class="how"><div data-v-0834b57d="" class="img-section"><div data-v-0834b57d="" class="img img-upgrade"></div></div><div data-v-0834b57d="" class="description"><div data-v-0834b57d="" class="title">upgrade</div><div data-v-0834b57d="" class="detail">Advance to next tier for even more benefits.</div></div></div></div></div><hr data-v-72bbedf8="" class="hr"><div data-v-6524658e="" data-v-72bbedf8="" class="join-container">
							<div data-v-6524658e="" class="title">join beauty pass</div>
							<div data-v-6524658e="" class="description">When you sign up for Beauty Pass, you'll instantly receive 50 bonus points that you can use to redeem rewards from your reward boutique. Get started and sign up now.</div><div data-v-6524658e="" class="sessions">
							
							<a data-v-6524658e="" href="/users/sign_in" class="btn sign-in">Sign In</a>
							
							<a data-v-6524658e="" href="/users/sign_up" class="btn register">Register</a>
							<br>
							<h2 style="padding-top: 20px;">Sign Up in Stores</h2>
							<div class="row">
							   <div class="col-md-3">1. Pride- Banani, Dhaka.</div>
							   <div class="col-md-3">2. Pride – Dhanmondi, Dhaka</div>
							   <div class="col-md-3">3. Pride – Baily Road, Dhaka.</div>
							   <div class="col-md-3">4. PRIDE - Rapa Plaza</div>
							   <div class="col-md-3">5. PRIDE - Dhaka New Market</div>
							   <div class="col-md-3">6. PRIDE - Mirpur 10</div>
							</div>
							</div></div>
							<hr data-v-72bbedf8="" class="hr"><div data-v-0f0c09c8="" data-v-72bbedf8="" class="faqs-container"><div data-v-0f0c09c8="" class="h3 title">FAQs</div><div data-v-0f0c09c8="" class="faqs"><div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion"><div data-v-0f0c09c8="" data-target="#faq-0" data-parent="#faq-0-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i><div data-v-0f0c09c8="" class="question">How do I redeem Rewards?</div></div><div data-v-0f0c09c8="" id="faq-0" role="tabpanel" class="collapse"><div data-v-0f0c09c8="" class="answer">You will need to  log into your Beauty Pass account online, in order to add a Reward to your Shopping Bag from the Rewards Boutique. You can also select from the ‘Beauty Pass Rewards’ Tab available during the checkout process. All Rewards can be redeemed only upon purchase of an item. Rewards can only be redeemed in the country of your Beauty Pass registration.</div></div></div><div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion"><div data-v-0f0c09c8="" data-target="#faq-1" data-parent="#faq-1-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i><div data-v-0f0c09c8="" class="question">How can I redeem my Birthday Gift?</div></div><div data-v-0f0c09c8="" id="faq-1" role="tabpanel" class="collapse"><div data-v-0f0c09c8="" class="answer">Black and Gold Members are eligible to redeem one birthday gift per year during their birthday month. To redeem in-store, no purchase is necessary when you are redeeming your birthday gift. To redeem online, a merchandise purchase is required. Please click on your loyalty redemption tab at checkout to receive your birthday gift. Due to limited quantities, substitution of gifts may occur. Gifts will be available while stocks last. This offer is subject to change, alteration, or termination by Sephora at its sole discretion at any time.</div></div></div><div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion"><div data-v-0f0c09c8="" data-target="#faq-2" data-parent="#faq-2-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i><div data-v-0f0c09c8="" class="question">Can I combine points from multiple accounts into a single account?</div></div><div data-v-0f0c09c8="" id="faq-2" role="tabpanel" class="collapse"><div data-v-0f0c09c8="" class="answer">Yes! If you have more than one Sephora Beauty Pass account under your name, please call customer service at +65 3163 1032 or email us at contact@sephora.sg and we can assist you by combining your accounts. Please provide us with the full account details of the multiple accounts.</div></div></div><div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion"><div data-v-0f0c09c8="" data-target="#faq-3" data-parent="#faq-3-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i><div data-v-0f0c09c8="" class="question">How do I become a Gold or Black member and what is the validity of my tier status?</div></div><div data-v-0f0c09c8="" id="faq-3" role="tabpanel" class="collapse"><div data-v-0f0c09c8="" class="answer">To become a Black Member, you will need to spend an accumulated value of $300 within 12 months of signing up for Sephora Beauty Pass. Your status is valid for a 12 month period. To become a Gold Member, you will need to spend an accumulated value of $1,500 within 12 months of getting qualified for Black Member Status. Your status is valid for a 12 month period.</div></div></div><div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion"><div data-v-0f0c09c8="" data-target="#faq-4" data-parent="#faq-4-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i><div data-v-0f0c09c8="" class="question">How do I maintain my Gold or Black member tier status?</div></div><div data-v-0f0c09c8="" id="faq-4" role="tabpanel" class="collapse"><div data-v-0f0c09c8="" class="answer">To maintain your Gold Member status, you must spend a minimum of S$1,500 online or in Singapore Sephora stores within 365 days from the date you qualified as a GOLD Member. If you spend less than S$1,500 but more than S$300, you will be placed in the Black  Member status. If you spend less than S$300, you will be placed in the White Member status.</div></div></div></div><a data-v-0f0c09c8="" href="/faqs/115000833506-Sephora-Beauty-Pass" class="view-faqs"><div data-v-0f0c09c8="" class="text">VIEW ALL FAQS</div><div data-v-0f0c09c8="" class="arrow"></div></a></div></div></div></div></main>
@endsection