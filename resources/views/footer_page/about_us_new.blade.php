@extends('layouts.app')
@section('title','Ecommerce | A Ecommerce Group venture')
@section('content')

<style type="text/css">


    .h1 {
        font-size: 24px;
    }

    .h2 {
        text-align:center;
        font-size: 18px;
    }


</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>About Us</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container"><!-- Container start -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div id="search_result">
                        <div class="h1"><center><h3 style="color:#000">ABOUT US</h3></center> </div>
                        <br>
                        <p style="text-align:justify;">
                            Since the introduction of our retail brand in the Bangladeshi market in 1991, Ecommerce Limited has strived to build upon it’s commitment of delighting Bangladeshi consumers with the best quality textile products, at the most affordable prices, and manufactured in our own facilities with highest quality assurance and compliance. Thus, by 2018 the Ecommerce Limited network has expanded to around a 100 stores and has been established as a pioneer and top lifestyle retailer in Bangladesh. 
                        </p> 
                        <div class="h1"><h3 style="color:#000">Market</h3> </div>
                        <p style="text-align:justify;">
                        Ecommerce Limited currently operates in the country of Bangladesh, with over an estimated 100 stores. The Bangladeshi retail market is relatively young, but it is highly competitive due to rising demand for international quality apparel and other consumer products, and due to the growing population of young customers with increasing discretionary income to spend. At this current time, the Ecommerce Limited network of 100 stores has over 1500 employees nationwide in sales, manufacturing and administration, and is present in every major Bangladeshi city. 
                        </p>
                        <div class="h1"><h3 style="color:#000">Achievements</h3> </div>
                        <p style="text-align:justify;">
                        Ecommerce Limited is proud to be a recipient of the award for ‘Brand Excellence in Retail Sector' by the World Brand Congress in Singapore in July 2014. 
                        </p>
                        <p style="text-align:justify;">
                            Urban Truth, a venture of Ecommerce Limited, has also received an award for ‘Emerging Brand’ by the World Marketing Congress. 
                        </p>
                        <p style="text-align:justify;">
                            We also have the distinction of bringing organized retail into Bangladesh at a time when the retail industry had no key players and had an unregulated environment. Our position as a pioneer apparel brand for the greater Bangladeshi market is a marked achievement for us. 
                        </p>
                        <div class="h1"><h3 style="color:#000">History</h3> </div>
                        <p style="text-align:justify;">
                            1958- The founding of Dacca Textiles Limited, through the wholesale manufacturing and commercialization of textile apparel, to encourage the widespread use of locally made textiles. <br><br>
                            1991- Ecommerce Textiles, as a retail brand, is launched through its first retail store in Dhaka and later, through the expansion of the Dhaka territory, the Ecommerce retail chain is formed. <br><br>
                            1995-2000s – Ecommerce Textiles is renamed Ecommerce Limited, and has successfully launched, 50 stores and in every major city in Bangladesh, a major distribution center and manufacturing hub. Successfully established as the foremost Bangladeshi brand. Product portfolio expanded to include kids wear and home textiles. <br><br>
                            2010- Ecommerce Limited retail network has expanded to launch its contemporary and trendy sister brand, Urban Truth in a separate flagship store. Ecommerce Limited now operates 64 stores throughout the country. <br><br>
                            2018- Urban Truth now operates in 8 stores, and Ecommerce Limited manages a network of 1500 employees in its network of around 100 stores, distribution centers, wholesale centers, manufacturing and headquarters in the country. <br><br>
                            For the last 25 years, Ecommerce enjoys the formidable position as one of the most famous and innovative textile manufacturers and retailers in the country. 
                        </p>
                        <div class="h1"><h3 style="color:#000">Product</h3> </div>
                        <p style="text-align:justify;">
                        Ecommerce offers its customers a wide range of textile products to enhance their lifestyle. Our product portfolio ranges from traditional menswear, womenswear and kidswear, to every day casual wear and office wear. It also includes a trendy urban line following the latest trends and styles of European fashion for the young, fashion forward generation, under it’s sister brand , Urban Truth.<br><br>
                            In the last 10 years, we have also introduced and built a following for our Home Textiles line, which includes bedsheets, quilts, dining sets and towel sets. <br><br>
                            All our products are designed by our own creative team who conduct extensive research not only to bring in the latest international styles in our designs, but also keep an eye on the local pulses and trends. We manufacture every SKU in our production facilities which are equipped with state of the art technology. In the last 25 years, Ecommerce has worked hard to inculcate sustainable and lean practices so that we are consistently able to offer the best product, at the highest quality in the most affordable prices for our loyal customer base. <br><br>
                        </p>
                        <div class="h1"><h3 style="color:#000"> Recent Developments</h3> </div>
                        <p style="text-align:justify;">
                        Ecommerce Limited introduced the product line Ecommerce Signature’ in 2011 that caters to more exclusive tastes of customers. This involved incorporating advanced technology in manufacturing that makes our production facilities one of the first in the nation to possess it. <br><br>
                            We have also begun work on a massive factory project that will increase our quality assurance and design offerings three-fold, and will better equip us to service our customers wants at a faster, and more efficient pace.<br><br>
                            In January 2017, we celebrated 25 years of service by recognizing 15 of our nation’s female leaders, who have paved the way for future generations of trailblazers. These icons represent our own values of innovation and being catalysts of change. <br><br>
                            In August 2017, we started our online operations, and are investing heavily in the e-commerce revolution that is taking place in the country. This has given us a wider access to customers throughout the nation. 
                        </p>
                        <div class="h1"><h3 style="color:#000"> Promotion</h3> </div>
                        <p style="text-align:justify;">
                            The relationship that we have with all our Ecommerce customers is a fundamental facet for the company’s growth. <br><br>
                            We have been keen to adopt new and inventive ways to communicate with our customers since our inception. From being the first to make televised adverts for a fashion product, to being the first to organize fashion shows in the country, we have set the tone and pioneered fashion marketing in Bangladesh.<br><br>
                            In the early years, the connection with our customers was organic, and due to the novelty of our innovative product designs and exceptional quality. Now in the age of social media, we have been early adopters and have been able to constantly communicate with our customers and offer them relevant content. We also partner with major influencers and bloggers to reach our young, tech-savvy customers and utilize digital advertising as a key strategic tool in our marketing plan.
                        </p>
                        <div class="h1"><h3 style="color:#000"> Brand Values</h3> </div>
                        <p style="text-align:justify;">
                            Ecommerce bases its activities on the core values of integrity, product excellence and trust.  We work to delight our customers with process excellence, quality assurance and the best locally manufactured textiles at the best prices. Moreover, the respect and sincerity we show our customer base has brought brand loyalty and success over the last 25 years. 
                        </p>
                        <div class="h1"><h3 style="color:#000">Things you didn’t know about Ecommerce</h3> </div>
                        <ol>
                            <li>
                                1. Ecommerce Limited is the pioneer fashion textile brand in Bangladesh that revolutionized and organized the apparel market as it stands today. 
                            </li>
                            <li>
                                2. Our founding directors named the brand Ecommerce to build loyalty and Ecommerce for local textiles and local designs. 
                            </li>
                            <li>
                                3. Our first cotton ‘sari’ design printed in 1958 utilized art by prominent Bangladeshi painter Quamrul Hassan
                            </li>
                            <li>
                                4. The launch of our first store in 1991 was so successful, that our entire stock inventory sold out in 5 days. 
                            </li>
                            <li>
                                5. Two of our founding directors are also business professors at Dhaka’s premier institution, the University of Dhaka. Their passion to shape the future of Bangladesh is evident in their business objectives as well as their teaching. 
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
    </div>    
</main>
@endsection