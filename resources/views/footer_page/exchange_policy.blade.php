@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style type="text/css">
    .h1 {
        font-size: 24px;
    }
    .h2 {
        font-size: 18px;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Exchange Policy</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container"><!-- Container start -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="h1"><center><h3 style="color:#000">Exchange Policy</h3></center> </div>
                    <br>
                    <p style="text-align:justify;">Please keep in mind that products may show minor difference than photographs in real life.Pride Limited does not refund any products.However, we do exchange products if , We have sent you the wrong product, size or color Faulty product
                        The exchange must be made within 15 day's of purchase and the product, product package and the hangtag must be intact.The original invoice must be presented during exchange. Call our customer service to exchange the product.
                        You can return your items through courier service to our e-commerce warehouse at the following address and then we will ship the right sized/colored product to you.E-Commerce Warehouse Pride Group Level 3, Mirandel, House 3, Road 5, Block J, Baridhara,Dhaka, Bangladesh.
                        We also encourage you to check your product instantly when the delivery is made.</p> 
                    <p>Our delivery man will wait a few minutes for you to check the product and if there are any issues, we urge you to take it up with the customer service and resolve immediately.This exchange policy is at the management's disposal and may be subject to change.</p>
                </div>
            </div>
    </div><!-- end container -->
</main>
@endsection