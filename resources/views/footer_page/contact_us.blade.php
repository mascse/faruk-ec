@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style type="text/css">
    .h7 {
        font-size: 28px;
        margin-bottom:20px;

    }
    .h2 {
        margin-top:20px;
        float:left;
        font-size: 18px;

        text-align:center;
        overflow:hidden;
    }
    .h1 {
        font-size: 26px;
    }
    .bnew2 {
        width: 250px;
        height: auto;
        padding: 3px 9px 3px 9px;
        text-align: center;
        color: #00000;
        font-size: 14px;
        cursor: pointer;
        float: left;
        border-right: 1px solid #999;
        margin-bottom: 10px;
        margin-left: 200px;
        margin-right: 10px;
    }
    .bnew {
        width: 250px;
        height: auto;

        padding:3px 9px 3px 9px;
        text-align:center;
        color:#00000;
        font-size:14px;
        cursor:pointer;
        float:left;
        border-right: 1px solid #999;
        margin-bottom:10px;
    }
    .imb{
        width:60px;
        height:60px;
        float:left;
        overflow:hidden;
        background:#fff;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Contact Us</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container"><!-- Container start -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <center> 
                        <div class="h7">Get in Touch!
                        </div>
                        <div class="bnew2"><div class="h2"><img src="{{ URL::to('') }}/storage/app/public/img/callnew.jpg"><p><div class="h1">Call</p></div>0966-910-0216 </p>
                                Timings 10am - 7pm </p>(Sat - Thu)</div></div>
                        <div class="bnew"><div class="h2"><img src="{{ URL::to('') }}/storage/app/public/img/mailnew.jpg"><p><div class="h1"> E-Mail</p></div><a href="mailto:care@pride-limited.com?Subject=Hello%20again" target="_top">care@pride-limited.com</p></a>
                            </div></div>

                        <div class="bnew"><div class="h2"><img src="{{ URL::to('') }}/storage/app/public/img/ad.jpg"><p><div class="h1">Store</p></div>Click here to find our stors</p> <a href="{{url('/store-locator')}}">Store Location.</a>
                            </div></div>
                    </center>
                </div>
            </div>
    </div><!-- end container -->
</main>
@endsection