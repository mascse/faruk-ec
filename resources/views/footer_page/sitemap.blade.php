@extends('layouts.app')
@section('title','Site Map | Pride Limited')
@section('content')
<style>
ul ul, ul ol, ol ul, ol ol {
    margin-top: 10px;
}
li{
    padding: 5px;
}
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Site Map</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container start -->
    <div class="container">
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                                <div>
								   <center><h2>Site Map</h2></center>
                                    <ul>
                                        <li class="col-md-12" style="border: 1px solid #eee;padding: 20px;">
										<a href="#"  class="level-top" ><span style="font-size:20px;padding-bottom:10px;">Woman</span></a>
                                            <ul>
                                                <li class="col-md-4" style="padding-top: 16px;">
                                                    <a href="{{url("/signature/signature-sari/9/signature")}}" >
                                                        <img class="logo-img" src="{{ URL::to('') }}/storage/app/public/img/menu/s1.png"/>
<!--                                                        <span>Pride Signature</span>-->
                                                    </a>
                                                    <ul>
                                                        <li  class="level2 nav-2-1-1 first"><a href="{{url("/signature/kameez/digital_print/16/72")}}" ><span>Digital Print Collection</span></a></li>
                                                        <li  class="level2 nav-2-1-1 first"><a href="{{url("/signature/kameez/digital_print/16/91")}}" ><span>Silk Tunics</span></a></li>
                                                        <!--<li  class="level2 nav-2-1-1 first"><a href="{{url("/signature/kameez/digital_print/16/72")}}" ><span>Kameez Set</span></a></li> -->
                                                        <li  class="level2 nav-2-1-1"><a href="{{url("/signature/single-tunic/digital_print/16/90")}}" ><span>Ready-To-Wear Single Tunics</span></a></li>
                                                        <li  class="level2 nav-2-1-1 first"><a href="{{url("/signature/blouse/9/92")}}" ><span>Ethnic Sari Blouse</span></a></li>
                                                        <li  class="level2 nav-2-1-2"><a href="{{url("/signature/unstitched_three_piece/12/66")}}" ><span>Unstitched Three Piece</span></a></li>
                                                        <!--<li  class="level2 nav-2-1-3"><a href="{{url("/signature-sari/dupatta/5/13")}}" ><span>Orna</span></a></li> -->
                                                        <li  class="level2 nav-2-1-4"><a href="{{url("/signature/cotton-sari/9/55")}}" ><span>Cotton Sari</span></a></li>
                                                        <li  class="level2 nav-2-1-5"><a href="{{url("/signature/taat-silk-sari/9/56")}}" ><span>Taat/Silk Sari</span></a></li>
                                                        <li  class="level2 nav-2-1-6 last"><a href="{{url("/signature/half-silk-sari/9/58")}}" ><span>Half Silk Sari</span></a></li>
                                                        <li  class="level2 nav-2-1-6 last"><a href="{{url("/signature/muslin-sari/9/60")}}" ><span>Muslin Silk Sari</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li class="col-md-4" style="padding-top: 16px;">
                                                    <a href="#" >
                                                        <img class="logo-img" src="{{ URL::to('') }}/storage/app/public/img/menu/classic.png"/>
<!--                                                        <span>Pride Classic</span>-->
                                                    </a>
                                                    <ul>
                                                        <li  class="level2 nav-2-2-1 first"><a href="{{url("/classic/classic-sari/7/40")}}" ><span>Classic Sari</span></a></li>
                                                        <li  class="level2 nav-2-2-2"><a href="{{url("/classic/unstitched-three-piece/7/77")}}" ><span>Unstitched Three Piece</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li class="col-md-4" style="padding-top: 16px;"><a href="{{url("/pride-girls/all/5/pride-girls")}}" >
                                                        <img class="logo-img" src="{{ URL::to('') }}/storage/app/public/img/menu/girls.png"/>
<!--                                                        <span>Pride Girls</span>-->
                                                    </a>
                                                    <ul>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/formal/5/14")}}" ><span>Formal</span></a></li>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/semi-formal/5/15")}}" ><span>Semi Formal</span></a></li>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/casual/5/16")}}" ><span>Casual</span></a></li>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/bottoms/5/50")}}" ><span>Bottoms</span></a></li>
                                                        <li  class="level2 nav-2-3-1 first"><a href="{{url("/pride-girls/dupatta/5/13")}}" ><span>Orna</span></a></li>
                                                        <li  class="level2 nav-2-3-2 last"><a href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewelry</span></a></li>
                                                    </ul>
                                                   
                                            </ul>
                                        </li>
										
										
                                        <li class="col-md-12" style="border: 1px solid #eee;padding: 20px;">
										<a href="#"  class="level-top" ><span style="font-size:20px;padding-bottom:10px;">Mens</span></a>
                                            <ul>
                                                <li class="col-md-4" style="padding-top: 16px;">
                                                    <a href="{{url("/athenic-men/panjabi/17/all-panjabi")}}" >
                                                        <img class="logo-img" src="{{ URL::to('') }}/storage/app/public/img/menu/athenic.png"/>
<!--                                                        <span>Panjabi</span>-->
                                                    </a>
                                                    <ul>
                                                        <li  class="level2 nav-3-1-2"><a href="{{url("/athenic-men/long-panjabi/regular-fit/17/73")}}" ><span>Regular Fit Panjabi</span></a></li>
                                                        <li  class="level2 nav-3-1-3"><a href="{{url("/athenic-men/short-panjabi/slim-fit/18/76")}}" ><span>Slim Fit Panjabi</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                
                                            </ul>
                                        </li>
										
                                        <li class="col-md-12" style="border: 1px solid #eee;padding: 20px;"><a href="#"  class="level-top" ><span style="font-size:20px;padding-bottom:10px;">Kids</span></a>
                                            <ul>
                                                <li class="col-md-4" style="padding-top: 16px;">
												<a href='{{url("/kids/girls/6/all-girls")}}' ><span style="font-size:16px;font-weight:700;">Girls</span></a><ul>
                                                        <li  class="level2 nav-3-1-1 first"><a href="{{url('/kids/girls/6/52')}}" ><span>Dresses</span></a></li>
                                                        <li  class="level2 nav-3-1-2"><a href="{{url('/kids/girls/6/81')}}" ><span>Tunics</span></a></li>
                                                        <li  class="level2 nav-3-1-3"><a href="{{url('/kids/girls/6/82')}}" ><span>Tops</span></a></li>
                                                        <li  class="level2 nav-3-1-4 last"><a href="{{url('/kids/girls/6/83')}}" ><span>Bottoms</span></a></li>
                                                        <li  class="level2 nav-3-1-4 last"><a href="{{url('/kids/girls/6/84')}}" ><span>Clothing Sets</span></a></li>
                                                        <li  class="level2 nav-3-1-4 last"><a href="{{url('/kids/girls/6/85')}}" ><span>Rompers</span></a></li>
                                                        <li  class="level2 nav-3-1-4 last"><a href="{{url('/kids/girls/6/89')}}" ><span>Jewelry</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                <li class="col-md-4" style="padding-top: 16px;">
												  <a href="{{url('/kids/boys/6/39')}}" ><span style="font-size:16px;font-weight:700;">Boys</span></a>
                                                    <ul>
                                                        <li  class="level2 nav-3-2-1 first"><a href="{{url('/kids/boys/6/39')}}" ><span>Panjabi</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        <li class="col-md-12" style="border: 1px solid #eee;padding: 20px;">
										<a href="#"  class="level-top" ><span style="font-size:20px;padding-bottom:10px;">Accessories</span></a>
                                            <ul>
                                                <li class="col-md-4" style="padding-top: 16px;"><a href="#" ><span style="font-size:16px;font-weight:700;">Woman</span></a>
                                                    <ul>
                                                        <li  class="level2 nav-4-1-5"><a href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewellery</span></a></li>
                                                    </ul><div class="bottomstatic" ></div>
                                                </li>
                                                <li class="col-md-4" style="padding-top: 16px;"><a href="#" ><span style="font-size:16px;font-weight:700;">Kids</span></a>
                                                    <ul>
                                                        <li  class="level2 nav-4-2-3"><a href="{{url("/pride-girls/jewelry/6/89")}}" ><span>Jewellery</span></a></li>
                                                    </ul>
                                                    <div class="bottomstatic" ></div>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                    
                                    </ul>
                                </div>
                                
                                </div>
                         
				  
                </div>
            </div>
		</section>
    </div>
</main>
<!-- end container -->
<script>
    jQuery(document).ready(function () {
        jQuery('.showSingle').click(function () {
            jQuery('.panel-collapse').removeClass("show");
            jQuery('.showSingle').removeClass("active");
            jQuery('#collapse' + jQuery(this).attr('target')).addClass("show");
            jQuery('#div' + jQuery(this).attr('target')).addClass("active");
        });
    });
</script>
@endsection