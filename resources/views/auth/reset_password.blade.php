@extends('layouts.app')
@section('title','Login | Register')
@section('content')
  <main class="main shop-page">
    <div class="container">
        <div class="shop-page-title">
            <h1 class="h1  text-uppercase"><span class="text-second">Reset  | Password</span></h1>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-6 col-md-6 mx-auto pb-5">
                <form action="{{ url('/update-password') }}" method="POST" data-toggle="validator">
                    {{ csrf_field() }}
                        <h4 class="h4 text-uppercase mt-0 mb-3">New Password</h4>
                        <div class="form-group d-flex">
                             <input class="form-control" type="hidden" name="user_id" value="{{$user_id}}" placeholder="New Password" required>
                            <input class="form-control" type="password" name="password" placeholder="New Password" data-minlength="4" data-error="Minimum of 4 characters" required autofocus>
                            <span class="help-block">
                                <strong></strong>
                            </span>
                        </div>
                        <div class="modal-footer flex-column px-0 pb-0 border-top-0">
                            <input class="btn btn-shop" type="submit" value="Update Password ">
                            <!--<a class="d-inline-block" href="#" data-dismiss="modal">I have my password</a> -->
                        </div>
                    </form>
            </div>
        </div>
    </div>
</main>
@endsection