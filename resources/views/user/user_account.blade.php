@extends('layouts.app')
@section('title', 'My Account')
@section('content')
<style>
    .sidebar {
        padding: 0 20px 9999px 0;
        font-size: 12px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
    .block-dashboard-info .block-content .box-actions a {
        padding: 0 0 1px;
        margin: 0 5px 0 0;
        border-bottom: 1px solid black;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">My Account</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Account Dashboard</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-collapsible-nav">
                        <div class="title block-collapsible-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#block-collapsible-nav", "selectorsToggleClass": "active"}}'>
                            <strong>Account Dashboard</strong>
                        </div>
                        <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                            <ul class="nav items">
                                <li class="nav item current"><a href="{{url('/my-account')}}"><strong>Account Dashboard</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/address-book')}}">Address Book</a></li>
                                <li class="nav item"><a href="{{url('/my-account-info')}}">Account Information</a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/track-order')}}">My Orders</a></li>
                                <!--<li class="nav item"><a href="">Gift Card</a></li>  -->
                                <li class="nav item">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form></li>
                            </ul>    
                        </div>
                    </div>
                    <div class="block account-nav">
                        <div class="title account-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#account-nav", "selectorsToggleClass": "active"}}'>
                            <strong></strong>
                        </div>
                        <div class="content account-nav-content" id="account-nav">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">

                </div>
                <div class="block block-dashboard-info">
                    <div class="block-title"><strong>Account Information</strong></div>
                    <div class="block-content">
                        <div class="box box-information">
                            <strong class="box-title">
                                <span>Contact Information</span>
                            </strong>
                            <div class="box-content">
                                <p>
                                    <?php echo $user_details->registeruser_firstname . ' ' . $user_details->registeruser_lastname; ?><br>
                                    <?php echo $user_details->registeruser_email; ?> - <a href="{{url('/account/change-email')}}" class="action edit"><span>Change E-mail</span></a><br>
                                </p>
                            </div>
                            <div class="box-actions">
                                <a class="action edit" href="{{url('/my-account-info')}}">
                                    <span>Edit</span>
                                </a> | 
                                <a href="{{url('/account/change-password')}}" class="action change-password"> Change Password </a>
                            </div>
                        </div>
                        <div class="box box-newsletter">
                            <strong class="box-title">
                                <span>Newsletters</span>
                            </strong>
                            <div class="box-content">
                                <p>
                                    You don't subscribe to our newsletter.                                            
                                </p>
                            </div>
                            <div class="box-actions">
                                <a class="action edit" href="#"><span>Edit</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block block-dashboard-addresses">
                    <div class="block-title">
                        <strong>Address Book</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a class="action edit" href="{{url('/address-book')}}"><span style="border-bottom:1px solid black;">Manage Addresses</span></a>
                    </div>
                    <div class="block-content">
                        <div class="box box-shipping-address">
                            <div class="box box-bdr mtm pam myaccountBox2" style="margin-top: 10px!important;">
                                <h4 class="ui-borderBottom pbs fsml">Default delivery address</h4>
                                <div class="mtm">
                                    <div class="osh-address -widget">
                                        <div class="osh-row -name ft-name"><?php echo $user_details->registeruser_firstname . ' ' . $user_details->registeruser_lastname; ?></div>
                                        <div class="osh-row -with-icon-left"><i class="osh-font-address"></i> 
                                            <span class="osh-field -address">
                                                <span class="osh-field -address1 ft-address1 word-wrap"><i class="fa fa-fw">&#xF041;</i><?php echo $user_details->registeruser_address; ?>,<?php echo $user_details->registeruser_city; ?> ,<?php echo $user_details->registeruser_country; ?></span> 
                                                <span class="osh-field -address2 ft-address2 word-wrap"></span> 
                                                <span class="osh-field -city ft-city">    </span>
                                                <span class="osh-field -region ft-region"></span>
                                            </span>
                                        </div>
                                        <div class="osh-row -with-icon-left">
                                            <i class="osh-font-simple-phone"></i> <span class="osh-field -phones">
                                                <span class="osh-field -phone ft-phone1"><i class="fa fa-fw">&#xF095;</i> <?php echo $user_details->registeruser_phone; ?></span> 
                                                <span class="osh-field -additional-phone ft-phone2"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="osh-address -widget -pickupStation hidden"><div class="osh-row -name"></div>
                                        <div class="osh-row -with-icon-left"><i class="osh-font-address"></i> <span class="osh-field -address1"></span></div>
                                    </div>                        </div>
                                <a class="mts rfloat myaccountBox2-edit-shipping-address sel-edit-my-shipping-addresse" href="{{url("/address-edit/$user_details->registeruserdetails_id")}}"><i class="fa fa-fw">&#xF040;</i> Edit address</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

