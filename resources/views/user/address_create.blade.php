@extends('layouts.app')
@section('title', 'Create Address')
@section('content')
<style>
    .sidebar {
        padding: 0 20px 9999px 0;
        font-size: 12px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
    .osh-msg-box.-success {
        border-color: #B0DA7C;
        color: #B0DA7C;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Address Create</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-2" id="search_result">
                <div class="sidebar">
                    <div class="block block-collapsible-nav">
                        <div class="title block-collapsible-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#block-collapsible-nav", "selectorsToggleClass": "active"}}'>
                            <strong>Account Dashboard</strong>
                        </div>
                        <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                            <ul class="nav items">
                                <li class="nav"><a href="{{url('/my-account')}}">Account Dashboard</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/address-book')}}">Address Book</a></li>
                                <li class="nav item current"><a href="{{url('/my-account-info')}}"><strong>Account Information</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/track-order')}}">My Orders</a></li>
                                <!--<li class="nav item"><a href="">Gift Card</a></li>  -->
                                <li class="nav item"><a href="{{url('/logout')}}">Logout</a></li>
                            </ul>    
                        </div>
                    </div>
                    <div class="block account-nav">
                        <div class="title account-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#account-nav", "selectorsToggleClass": "active"}}'>
                            <strong></strong>
                        </div>
                        <div class="content account-nav-content" id="account-nav">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="my-account">
                    @if (session('save'))
                    <center>
                        <div class=" osh-msg-box -success">{{session('save')}}
                        </div>
                    </center>
                    @endif    
                    <div class="page-title">
                        <h2 class="pbs ui-borderBottom" style="text-transform:uppercase;font-size: 18px;">Create Account</h2>
                    </div>
                    <div class="dashboard">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box mtl">
                                    <div class="box box-bdr mtm pam myaccountBox2">
                                        <h4 class="ui-borderBottom pbs fsml">Contact Information</h4>
                                        <div class="mts txtRight mrm">
                                            <p class="fss pull-right">* Required fields</p>
                                        </div>
                                        <form id="address-form" action="{{url('/address-save')}}" method="post">
                                            {{ csrf_field() }}
                                            <input id="registeruser_id" type="hidden" class="form-control" name="registeruser_id" value="<?php echo $user_details->registeruser_id; ?>" />
                                            <fieldset class="ui-fieldset">
                                                <div class="unit size3of4">
                                                    <div class="col-md-6">
                                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                            <label for="AddressForm_first_name" class="required">First name <span class="required">*</span></label>
                                                            <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}"  maxlength="50"/>
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('first_name') }}</strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                            <label class="mts mts1 required" for="AddressForm_last_name">Last name <span class="required">*</span></label>
                                                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}"  maxlength="50"/>
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('last_name') }}</strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                            <label class="mts required" for="AddressForm_phone">Telephone <span class="required">*</span></label> 
                                                            <input  type="text" class="form-control" name="phone" value="{{ old('phone') }}"  placeholder="+8801" />
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('phone') }}</strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group{{ $errors->has('additional_phone') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                            <label class="mts" for="AddressForm_additional_phone">Additional phone number (optional)</label>
                                                            <input id="additional_phone" type="text" class="form-control" name="additional_phone" value="{{ old('additional_phone') }}"  placeholder="+8801" />
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('additional_phone') }}</strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                            <label class="mts required" for="AddressForm_address1">Address <span class="required">*</span></label>
                                                            <textarea id="address1" type="text" class="form-control" name="address1"  rows="3">{{ old('address1') }}</textarea>
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('address1') }}</strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                            <label class="mts required" for="AddressForm_address1">Address_2</label>
                                                            <textarea id="address2" type="text" class="form-control" name="address2" rows="3">{{ old('address2') }}</textarea>
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('address2') }}</strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group{{ $errors->has('RegionList') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                    <label for="AddressForm_fk_customer_address_region" class="required">Region <span class="required">*</span></label> 
                                                                    <input id="zipcode" type="text" class="form-control" name="RegionList" value="{{ old('RegionList') }}" />																	
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('RegionList') }}</strong>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="form-group{{ $errors->has('CityList') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                    <label for="AddressForm_fk_customer_address_city" class="required">City / Nearest Zone <span class="required">*</span></label>   
                                                                    <input id="CityList" type="text" class="form-control" name="CityList" value="{{ old('CityList') }}" />																		
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('CityList') }}</strong>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-12">
                                                                <div class="form-group{{ $errors->has('zipcode') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                    <label class="mts mts1 required" for="AddressForm_last_name">Zip Code</label>
                                                                    <input id="zipcode" type="text" class="form-control" name="zipcode" value="{{ old('zipcode') }}"  maxlength="50"/>
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('zipcode') }}</strong>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </fieldset>

                                    </div>
                                </div>
                                <div class="unit size1of2 mtl">
                                    <p><a class="btn btn-round btn-b" href="{{ URL::previous() }}">« Back</a></p>
                                </div>
                                <div class="unit size1of2 rfloat mtl txtRight mbl">
                                    <button class="btn btn-default " type="submit" id="send"><span>Save this address</span></button>
                                </div> 
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    jQuery(document).ready(function () {
        jQuery('select#RegionList').on('change', function () {
            var RegionId = this.value;
            jQuery('#regionId').val(RegionId);
            var url_op = base_url + "/citylist/" + RegionId;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    jQuery('#CityList').empty();
                    jQuery('#CityList').append('<option value="">Select City</option>');
                    jQuery.each(data, function (index, cityobj) {
                        jQuery('#CityList').append('<option value="' + cityobj.CityId + '">' + cityobj.CityName + '</option>');
                    });
                }
            });
        });
    });
</script>
@endsection

