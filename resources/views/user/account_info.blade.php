@extends('layouts.app')
@section('title', 'My Account')
@section('content')
<style>
    .sidebar {
        padding: 0 20px 9999px 0;
        font-size: 12px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
    .osh-msg-box.-success {
        border-color: #B0DA7C;
        color: #B0DA7C;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }

.block-dashboard-info .block-content .box-actions a {
    padding: 0 0 1px;
    margin: 0 5px 0 0;
    border-bottom: 1px solid black;
}
.sidebar {
        padding: 0 20px 9999px 0;
        font-size: 12px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
	.block-dashboard-info .block-content .box-actions a {
    padding: 0 0 1px;
    margin: 0 5px 0 0;
    border-bottom: 1px solid black;
   }
    .ui-borderBottom {
        border-bottom: 1px solid #4b4b4b99!important;
    }
    .pas, .pbs, .pvs {
        padding-bottom: 5px!important;
    }
    .box-bdr, .ui-blueBtn {
        border: 1px solid;
    }
    .box {
        position: relative;
        display: block;
        box-sizing: border-box;
    }
    .box-bdr {
        border-color: #D3D3D3;
        border-radius: 0;
    }
    .mas, .mhs, .mrs {
        margin-right: 5px!important;
    }
    .pam, .phm, .plm {
        padding-left: 10px!important;
    }
    .pam, .pbm, .pvm {
        padding-bottom: 10px!important;
    }
    .myaccountBox1 {
        min-height: 140px;
    }
    .fsml {
        font-size: 14px;
    }
    .pam, .phm, .plm {
        padding-left: 10px!important;
    }
    .mas, .mhs, .mrs {
        margin-right: 5px!important;
    }
    .pam, .phm, .prm {
        padding-right: 10px!important;
    }
    .myaccountBox1 {
        min-height: 140px;
    }
    .myaccountBox1 {
        min-height: 209px;
    }
    .myaccountBox1>a {
        position: absolute;
        bottom: 10px;
        right: 20px;
    }
    .i-edit {
        background: url(storage/app/public/edit.png) no-repeat;
    }
    .i-attention, .i-checked, .i-edit, .i-favorite, .i-information, .i-remove {
        padding-left: 20px;
        cursor: pointer;
    }
    .myaccountBox1 p:first-of-type, .myaccountBox2 .address p, .sel-address-additional .address p, .sel-address-billing .address p, .sel-address-shipping .address p {
        white-space: nowrap;
        overflow: hidden!important;
        text-overflow: ellipsis;
    }
    .rfloat, .thm-spinbasket .l-hasSidebar .l-main, .thm-spinbasket.l-hasSidebar .l-main {
        float: right;
    }
    .l-row {
        display: table;
    }
    .myaccountBox2 {
        min-height: 180px;
    }
    .mam, .mtm, .mvm {
        margin-top: 10px!important;
    }
    .strong {
        font-weight: 700;
    }
    p{
        margin: 0 0 10px;
    }


    #form-account-edit .size1of2 {
        width: 100%;
    }
    .ui-fieldset, .ui-formRow {
        position: relative;
    }
    .ui-formRow {
        margin-bottom: .8em;
    }
    .mam, .mtm, .mvm {
        margin-top: 10px!important;
    }
    .ui-formRow, .ui-formRow .col2 {
        overflow: hidden;
    }
    #form-account-edit .size1of2 .ui-formRow .col1 {
        width: 22%;
    }
    .ui-formRow .col1 {
        float: left;
    }
    .txtRight {
        text-align: right;
    }
    #form-account-edit .size1of2 .ui-formRow .col2 {
        float: left;
        width: 58%;
        margin-left: 5%;
    }
    .mas, .mts, .mvs {
        margin-top: 5px!important;
    }
    .card-desktop img, label {
        max-width: 100%;
    }
    input {
        padding: 4px;
        font-size: 13px;
        font-size: 1.3rem;
        background-color: #FFF;
        border: 1px solid #ccc;
    }
    input, select, textarea {
        box-sizing: border-box;
        outline: 0;
    }
    .lfloat {
        float: left;
    }
    .fsxs {
        font-size: 10px;
    }
    .accountEditErrorBirthMonth, .accountEditLabelBirthDay, .accountEditLabelBirthMonth, .wishlistEditErrorBirthDay, .wishlistEditErrorBirthMonth, .wishlistEditLabelBirthDay, .wishlistEditLabelBirthMonth {
        width: 82px;
    }
    .accountEditErrorBirthMonth, .accountEditLabelBirthDay, .accountEditLabelBirthMonth, .wishlistEditErrorBirthDay, .wishlistEditErrorBirthMonth, .wishlistEditLabelBirthDay, .wishlistEditLabelBirthMonth {
        width: 82px;
    }
    .accountEditBirthDay, .accountEditBirthMonth, .accountEditBirthYear, .wishlistEditBirthDay, .wishlistEditBirthMonth {
        width: 78px;
    }
    .accountEditErrorBirthYear, .accountEditLabelBirthYear {
        width: 76px;
    }
    .accountEditErrorBirthYear, .accountEditLabelBirthYear {
        width: 76px;
    }
    .accountEditErrorBirthYear, .accountEditLabelBirthYear {
        width: 76px;
    }
    *, .my-orders-list ul li.order-detail-container, .my-orders-list ul li.order-detail-container *, .my-orders-list ul li.order-detail-header, :after, :before {
        box-sizing: border-box;
    }
    main.osh-container label {
        display: block;
    }
    .box-bd, .box-ft, .box-hd {
        padding: 10px 10px 10px 0;
        box-sizing: border-box;
    }
    #content, .box, .box-bd, .box-ft, .box-hd, .clearfix, .l-pageWrapper {
        zoom: 1;
    }
    .has-error input{
        border-color: #883333;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    }
    .help-block{
        color:#833;
    }
    .osh-msg-box.-success {
        border-color: #28a745;
        color: #28a745;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
    .block-account .block-content li:before {
        content: "";
        font-family: FontAwesome;
        font-size: 10px;
        display: inline-block;
        position: absolute;
        cursor: pointer;
        line-height: 23px;
        color: #333;
    }
    .left-aside{
        background-color: #f3f3f385;
        border: 1px solid #00000017;
        padding: 26px;
    }
    .h4, .h5, .h6, h4, h5, h6 {
        margin-top: 11px;
        margin-bottom: 10px;
    }
    .page-title h1, .page-title h2 {
        color: #000;
        font-size: 18px;
        margin: 0;
        padding: 0;
        font-weight: bold;
    }
    .form-control {
        border-radius: 0;
        border: none;
        border: 1px solid #00000042;
        color: black;
        background-color: white;
        outline: none!important;
        box-shadow: none!important;
        /* font-family: Oswald, sans-serif; */
    }
    .btn-default {
        font-weight: 400;
        font-size: 13px;
        padding: .3125em 3.375em;
        color: #111;
        border-width: 1px;
        border-color: currentColor;
        text-transform: uppercase;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">My Account</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Account Information</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-2" id="search_result">
                <div class="sidebar">
                    <div class="block block-collapsible-nav">
                        <div class="title block-collapsible-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#block-collapsible-nav", "selectorsToggleClass": "active"}}'>
                            <strong>Account Dashboard</strong>
                        </div>
                        <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                            <ul class="nav items">
                                <li class="nav"><a href="{{url('/my-account')}}">Account Dashboard</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/address-book')}}">Address Book</a></li>
                                <li class="nav item current"><a href="{{url('/my-account-info')}}"><strong>Account Information</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/track-order')}}">My Orders</a></li>
                                <!--<li class="nav item"><a href="">Gift Card</a></li>  -->
                                <li class="nav item">
								<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Logout
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
								</li>
                            </ul>    
                        </div>
                    </div>
                    <div class="block account-nav">
                        <div class="title account-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#account-nav", "selectorsToggleClass": "active"}}'>
                            <strong></strong>
                        </div>
                        <div class="content account-nav-content" id="account-nav">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="my-account">
                    @if (session('save'))
                    <center>
                        <div class=" osh-msg-box -success">{{session('save')}}
                        </div>
                    </center>
                    @endif    
                    <div class="page-title">
                        <h2 class="pbs ui-borderBottom" style="text-transform:uppercase;font-size: 18px;">Edit Account</h2>
                    </div>
                    <div class="dashboard">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box mtl">
                                    <div class="box box-bdr mtm pam myaccountBox2">
                                        <div class="box-bd">
										<p class="mbs fsm pull-right">* Required fields</p>
                                            <form id="form-account-edit" action="{{url('/account-edit')}}" method="post">
                                                {{ csrf_field() }}
                                                <input name="registeruser_login_id" id="first_name" type="hidden"  value="<?php echo $login_user_info->id; ?>">
                                                <input name="registeruser_id" id="first_name" type="hidden"  value="<?php echo $user_details->registeruser_id; ?>"> 
                                                <fieldset class="ui-fieldset">
                                                    <div class="unit size1of2">
                                                        <div class="ui-formRow mtm">
                                                            <div class="col1 txtRight">
                                                                <label class="mts required" for="EditForm_first_name">First name <span class="required">*</span></label>                            </div>
                                                            <div class="col2">
                                                                <div class="collection {{ $errors->has('first_name') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                    <input class="form-control" name="first_name" id="first_name" type="text" maxlength="50"  value="<?php echo $user_details->registeruser_firstname; ?>">   
                                                                </div>
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="ui-formRow">
                                                            <div class="col1 txtRight">
                                                                <label class="mts required" for="last_name">Last name <span class="required">*</span></label>                            </div>
                                                            <div class="col2">
                                                                <div class="collection {{ $errors->has('last_name') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                    <input class="form-control" name="last_name" id="last_name" type="text" maxlength="50" value="<?php echo $user_details->registeruser_lastname; ?>">  
                                                                </div>
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="ui-formRow">
                                                            <div class="col1 txtRight">
                                                                <label class="mts required" for="EditForm_gender">Gender <span class="required">*</span></label>                            
                                                            </div>
                                                            <div class="col2">
                                                                <div class="collection {{ $errors->has('gender') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                    <select class="form-control input-text" name="gender" id="Form_gender">
                                                                        <option value="">Select</option>
                                                                        <option value="male">Male</option>
                                                                        <option value="female">Female</option>
                                                                    </select>                                                                    
                                                                </div>
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('gender') }}</strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="ui-formRow">
                                                            <div class="col1 txtRight">
                                                                <label for="EditForm_currentEmail">Current E-mail</label>                            
                                                            </div>
                                                            <div class="col2">
                                                                <div class="collection">
                                                                    <strong><?php echo $user_details->registeruser_email; ?></strong>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="ui-formRow">
                                                            <div class="col1 txtRight">
                                                                <label class="mts" for="EditForm_email">New E-mail</label>                            
                                                            </div>
                                                            <div class="col2">
                                                                <div class="collection {{ $errors->has('email') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                    <input class="form-control" name="email" id="EditForm_email" type="email" value="{{old('email')}}" />                                                                    
                                                                </div>
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="ui-formRow">
                                                            <div class="col1 txtRight">
                                                                <label class="mts error" for="EditForm_birthday">Birthday</label>                            
                                                            </div>
                                                            <div class="col2 1-row">
                                                                <div class="collection">
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <div class="form-group {{ $errors->has('day') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                                <label for="AddressForm_fk_customer_address_region" class="required"> Day (DD) <span class="required">*</span></label>            
                                                                                <input id="new_email" type="number" class="form-control" name="day" maxlength="2" min="1" max="31" value="<?php echo $user_details->registeruser_day; ?>"/>
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('day') }}</strong>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                                <label for="AddressForm_fk_month" class="required"> Month(MM) <span class="required">*</span></label>          
                                                                                <input type="number" class="form-control" name="month" maxlength="2" min="1" max="12" value="<?php echo $user_details->registeruser_month; ?>"/>
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('month') }}</strong>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group{{ $errors->has('Year') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                                <label for="AddressForm_fk_customer_address_city" class="required"> Year (YYYY) <span class="required">*</span></label>          
                                                                                <input type="text" class="form-control" name="Year" maxlength="4" value="<?php echo $user_details->registeruser_year; ?>"/>
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('Year') }}</strong>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>                              
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="ui-formRow">
                                                            <div class="col1">
                                                                &nbsp;
                                                            </div>
                                                            <div class="col2">
                                                                
                                                                <button style="width:48%;" class="action primary checkout pull-right" type="submit" id="send"><span>Update</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    document.getElementById('Form_gender').value = "<?php echo $user_details->user_gender; ?>"
</script>
@endsection

