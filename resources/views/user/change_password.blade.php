@extends('layouts.app')
@section('title', 'My Account')
@section('content')
<style>
    .ui-borderBottom {
        border-bottom: 1px solid #4b4b4b99!important;
    }
    .pas, .pbs, .pvs {
        padding-bottom: 5px!important;
    }
    .box-bdr, .ui-blueBtn {
        border: 1px solid;
    }
    .box {
        position: relative;
        display: block;
        box-sizing: border-box;
    }
    .box-bdr {
        border-color: #D3D3D3;
        border-radius: 0;
    }
    .mas, .mhs, .mrs {
        margin-right: 5px!important;
    }
    .pam, .phm, .plm {
        padding-left: 10px!important;
    }
    .pam, .pbm, .pvm {
        padding-bottom: 10px!important;
    }
    .myaccountBox1 {
        min-height: 140px;
    }
    .fsml {
        font-size: 14px;
    }
    .pam, .phm, .plm {
        padding-left: 10px!important;
    }
    .mas, .mhs, .mrs {
        margin-right: 5px!important;
    }
    .pam, .phm, .prm {
        padding-right: 10px!important;
    }
    .myaccountBox1 {
        min-height: 140px;
    }
    .myaccountBox1 {
        min-height: 209px;
    }
    .myaccountBox1>a {
        position: absolute;
        bottom: 10px;
        right: 20px;
    }
    .i-edit {
        background: url(storage/app/public/edit.png) no-repeat;
    }
    .i-attention, .i-checked, .i-edit, .i-favorite, .i-information, .i-remove {
        padding-left: 20px;
        cursor: pointer;
    }
    .myaccountBox1 p:first-of-type, .myaccountBox2 .address p, .sel-address-additional .address p, .sel-address-billing .address p, .sel-address-shipping .address p {
        white-space: nowrap;
        overflow: hidden!important;
        text-overflow: ellipsis;
    }
    .rfloat, .thm-spinbasket .l-hasSidebar .l-main, .thm-spinbasket.l-hasSidebar .l-main {
        float: right;
    }
    .l-row {
        display: table;
    }
    .myaccountBox2 {
        min-height: 180px;
    }
    .mam, .mtm, .mvm {
        margin-top: 10px!important;
    }
    .strong {
        font-weight: 700;
    }
    p{
        margin: 0 0 10px;
    }


    #form-account-edit .size1of2 {
        width: 100%;
    }
    .ui-fieldset, .ui-formRow {
        position: relative;
    }
    .ui-formRow {
        margin-bottom: .8em;
    }
    .mam, .mtm, .mvm {
        margin-top: 10px!important;
    }
    .ui-formRow, .ui-formRow .col2 {
        overflow: hidden;
    }
    #form-account-edit .size1of2 .ui-formRow .col1 {
        width: 22%;
    }
    .ui-formRow .col1 {
        float: left;
    }
    .txtRight {
        text-align: right;
    }
    #form-account-edit .size1of2 .ui-formRow .col2 {
        float: left;
        width: 58%;
        margin-left: 5%;
    }
    .mas, .mts, .mvs {
        margin-top: 5px!important;
    }
    .card-desktop img, label {
        max-width: 100%;
    }
    input {
        padding: 4px;
        font-size: 13px;
        font-size: 1.3rem;
        background-color: #FFF;
        border: 1px solid #ccc;
    }
    input, select, textarea {
        box-sizing: border-box;
        outline: 0;
    }
    .lfloat {
        float: left;
    }
    .fsxs {
        font-size: 10px;
    }
    .accountEditErrorBirthMonth, .accountEditLabelBirthDay, .accountEditLabelBirthMonth, .wishlistEditErrorBirthDay, .wishlistEditErrorBirthMonth, .wishlistEditLabelBirthDay, .wishlistEditLabelBirthMonth {
        width: 82px;
    }
    .accountEditErrorBirthMonth, .accountEditLabelBirthDay, .accountEditLabelBirthMonth, .wishlistEditErrorBirthDay, .wishlistEditErrorBirthMonth, .wishlistEditLabelBirthDay, .wishlistEditLabelBirthMonth {
        width: 82px;
    }
    .accountEditBirthDay, .accountEditBirthMonth, .accountEditBirthYear, .wishlistEditBirthDay, .wishlistEditBirthMonth {
        width: 78px;
    }
    .accountEditErrorBirthYear, .accountEditLabelBirthYear {
        width: 76px;
    }
    .accountEditErrorBirthYear, .accountEditLabelBirthYear {
        width: 76px;
    }
    .accountEditErrorBirthYear, .accountEditLabelBirthYear {
        width: 76px;
    }
    *, .my-orders-list ul li.order-detail-container, .my-orders-list ul li.order-detail-container *, .my-orders-list ul li.order-detail-header, :after, :before {
        box-sizing: border-box;
    }
    main.osh-container label {
        display: block;
    }
    .box-bd, .box-ft, .box-hd {
        padding: 10px 10px 10px 0;
        box-sizing: border-box;
    }
    #content, .box, .box-bd, .box-ft, .box-hd, .clearfix, .l-pageWrapper {
        zoom: 1;
    }
    .has-error input{
        border-color: #883333;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    }
    .help-block{
        color:#833;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
    .osh-msg-box.-error {
        border-color: #D10B23;
        color: #D10B23;
    }
    .osh-msg-box.-success {
        border-color: #28a745;
        color: #28a745;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
    .block-account .block-content li:before {
        content: "";
        font-family: FontAwesome;
        font-size: 10px;
        display: inline-block;
        position: absolute;
        cursor: pointer;
        line-height: 23px;
        color: #333;
    }
    .left-aside{
        background-color: #f3f3f385;
        border: 1px solid #00000017;
        padding: 26px;
    }
    .h4, .h5, .h6, h4, h5, h6 {
        margin-top: 11px;
        margin-bottom: 10px;
    }
    .page-title h1, .page-title h2 {
        color: #000;
        font-size: 18px;
        margin: 0;
        padding: 0;
        font-weight: bold;
    }
    .form-control {
        border-radius: 0;
        border: none;
        border: 1px solid #00000042;
        color: black;
        background-color: white;
        outline: none!important;
        box-shadow: none!important;
        /* font-family: Oswald, sans-serif; */
    }
    .btn-default {
        font-weight: 400;
        font-size: 13px;
        padding: .3125em 3.375em;
        color: #111;
        border-width: 1px;
        border-color: currentColor;
        text-transform: uppercase;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
        <div class="beadcumarea">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12"><div class="breadcrumbs">
                            <ul class="items">
                                <li class="item home">
                                    <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                                </li>
                                <li class="item cms_page">
                                    <strong>Change Password</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-collapsible-nav">
                        <div class="title block-collapsible-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#block-collapsible-nav", "selectorsToggleClass": "active"}}'>
                            <strong>Account Dashboard</strong>
                        </div>
                        <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                            <ul class="nav items">
                                <li class="nav item current"><a href="{{url('/my-account')}}"><strong>Account Dashboard</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/address-book')}}">Address Book</a></li>
                                <li class="nav item"><a href="{{url('/my-account-info')}}">Account Information</a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/track-order')}}">My Orders</a></li>
                                <!--<li class="nav item"><a href="">Gift Card</a></li>  -->
                                <li class="nav item">
								<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Logout
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form></li>
                            </ul>    
                        </div>
                    </div>
                    <div class="block account-nav">
                        <div class="title account-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#account-nav", "selectorsToggleClass": "active"}}'>
                            <strong></strong>
                        </div>
                        <div class="content account-nav-content" id="account-nav">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">

                </div>
                <div class="block block-dashboard-info">
                    <div class="block-title"><strong>Change Password</strong></div>
					    @if (session('success'))
						<center>
							<div class=" osh-msg-box -success">{{session('success')}}
							</div>
						</center>
						@endif    
						@if (session('error'))
						<center>
							<div class=" osh-msg-box -error">{{session('error')}}
							</div>
						</center>
						@endif    
                    <div class="box mtl">
						<div class="box box-bdr mtm pam myaccountBox2">
							<div class="box-bd">
								<form id="form-account-edit" action="{{url('/account/update-password')}}" method="post">
									{{ csrf_field() }}
									<input name="registeruser_login_id" id="first_name" type="hidden"  value="<?php echo $login_user_info->id; ?>">
									<p class="pbs strong">Authorization information</p>
									<fieldset class="ui-fieldset">
										<div class="unit size1of2">
											<div class="ui-formRow">
												<div class="col1 txtRight">
													<label for="EditForm_currentEmail">Current Password <span class="required">*</span></label>                            
												</div>
												<div class="col2">
													<div class="collection {{ $errors->has('password') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
														<input name="password" id="EditForm_password" type="password" />                                                                    
													</div>
													<span class="help-block">
														<strong>{{ $errors->first('password') }}</strong>
													</span>
												</div>
											</div>
											<div class="ui-formRow">
												<div class="col1 txtRight">
													<label class="mts" for="EditForm_email">New Password <span class="required">*</span></label>                            
												</div>
												<div class="col2">
													<div class="collection {{ $errors->has('newPassword') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
														<input name="newPassword" id="EditForm_password" type="password" />                                                                    
													</div>
													<span class="help-block">
														<strong>{{ $errors->first('newPassword') }}</strong>
													</span>
												</div>
											</div>
											<div class="ui-formRow">
												<div class="col1">
													&nbsp;
												</div>
												<div class="col2">
													<button class="btn btn-default ui-button ui-buttonAccount sel-account-edit-button" type="submit" id="send"><span>Update</span></button>
												</div>
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

