@extends('layouts.app')
@section('title','User Details')
@section('content')
<style>
    button {
        width: 100%;
        height: 30px;
        font-size: 10px;
        line-height: 28px;
        color: #fff;
        text-transform: uppercase;
        letter-spacing: 2px;
        position: relative;
        border: none;
        background: rgb(41, 30, 136);
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .omniauth-container {
        box-shadow: 0 0 0 1px #e5e5e5;
        border-bottom-right-radius: 2px;
        border-bottom-left-radius: 2px;
        padding: 15px;
    }
    .prepend-top-15 {
        margin-top: 25px;
    }
    .d-block {
        display: block !important;
    }
    label.label-bold {
        font-weight: 600;
    }
    .omniauth-btn {
        margin-bottom: 16px;
        width: 48%;
        padding: 8px;
    }
    .omniauth-container .omniauth-btn img {
        width: 18px;
        height: 18px;
        margin-right: 16px;
    }
    .btn, .project-buttons .stat-text {
        border-radius: 3px;
        font-size: 14px;
        height: 38px;
        font-weight: 400;
        padding: 6px 10px;
        background-color: #fff;
        border-color: #e5e5e5;
        color: #2e2e2e;
        color: #2e2e2e;
    }
    .text-left {
        text-align: left !important;
    }
    .align-items-center, .page-title-holder {
        align-items: center !important;
    }
    .btn, .project-buttons .stat-text {
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        user-select: none;

        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 20px;
        border-radius: 0.25rem;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }
    img {
        vertical-align: middle;
        border-style: none;
    }
    .justify-content-between {
        justify-content: space-between !important;
    }
    .flex-wrap {
        flex-wrap: wrap !important;
    }
    .d-flex, .page-title-holder, .right-sidebar.build-sidebar .trigger-variables-btn-container {
        display: flex !important;
    }
	.login-container:after {
    position: absolute;
    left: 50%;
    top: 0;
    bottom: 0;
    background: none;
    width: 1px;
    content: '';
}
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>User Details</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
                </div>
                <div class="login-container">
                    <div class="block">
                        <div class="block-title">
                            <strong id="block-new-customer-heading" role="heading" aria-level="2">Please complete your address infomatrion</strong>
                        </div>
                        <form class="form create account form-create-account" action="{{route('UserDetailsSave')}}" method="post" id="form-validate" enctype="multipart/form-data" autocomplete="off">
                            {{ csrf_field() }}
							<input type="hidden" name="login_user_id" value="{{$login_user_id}}"/>
                           <input type="hidden" name="registeruser_id" value="{{$user_info->registeruser_id}}"/>
                            <fieldset class="fieldset create info">
                                <div class="field">
                                    <label class="label" for="firstname"><span>First Name</span></label>
                                    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                       <input class="form-control" type="text" name="first_name" placeholder="First name*" value="{{ $user_info->registeruser_firstname }}" required />
                                        <div class="help-block with-errors">{{ $errors->first('first_name') }}</div>
                                    </div>
                                </div>
                                <div class="field field-name-lastname required">
                                    <label class="label" for="lastname"><span>Last Name</span></label>
                                    <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <input type="text" id="lastname" class="form-control"
                                               name="last_name"
                                               value="{{ $user_info->registeruser_lastname }}"
                                               title="Last&#x20;Name"
                                               class="input-text required-entry"  data-validate="{required:true}">
                                        <div class="help-block with-errors">{{ $errors->first('last_name') }}</div>
                                    </div>
                                </div>
                                <div class="field gender">
                                    <label class="label" for="gender"><span>Address</span></label>
                                    <div class="form-group {{ $errors->has('registeruser_address') ? ' has-error' : '' }}">
                                        <textarea type="text" id="lastname" name="registeruser_address"  title="Last&#x20;Name" class="form-control input-text required-entry"  data-validate="{required:true}">{{ old('registeruser_address') }}</textarea>
										<div class="help-block with-errors">{{ $errors->first('registeruser_address') }}</div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset create account" data-hasrequired="* Required Fields">
                                <div class="password-area">
                                    <div class="field password required" data-mage-init='{"passwordStrengthIndicator": {}}'>
                                        <label for="password" class="label"><span>Country</span></label>
                                        <div class="control {{ $errors->has('Shipping_ddlcountry') ? ' has-error' : '' }}">
										<select class="form-control js-select js-order-country" name="Shipping_ddlcountry" required>
											<option value="Bangladesh">Bangladesh</option>
										</select>
                                        </div>
                                    </div>
                                    <div class="field confirmation required">
                                        <label for="password-confirmation" class="label"><span>City</span></label>
                                        <div class="form-group {{ $errors->has('registeruser_city') ? ' has-error' : '' }}">
                                            <input class="form-control" id="registeruser_city" type="text" name="registeruser_city" value="{{ old('registeruser_city') }}" required/>
											<div class="help-block with-errors">{{ $errors->first('registeruser_city') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
							<fieldset class="fieldset create account" data-hasrequired="* Required Fields">
                                <div class="password-area">
                                    <div class="field password required" data-mage-init='{"passwordStrengthIndicator": {}}'>
                                        <label for="password" class="label"><span>Post Code / Zipe</span></label>
                                        <div class="form-group {{ $errors->has('registeruser_zipcode') ? ' has-error' : '' }}">
										<input class="form-control" id="" type="text" name="registeruser_zipcode" data-minlength="4" data-error="Minimum of 4 characters" value="{{ old('registeruser_zipcode') }}" />
										  <div class="help-block with-errors"> {{ $errors->first('registeruser_zipcode') }}</div>
                                        </div>
                                    </div>
                                    <div class="field confirmation required">
                                        <label for="password-confirmation" class="label"><span>Mobile Number</span></label>
                                        <div class="form-group {{ $errors->has('registeruser_phone') ? ' has-error' : '' }}">
                                             <input class="form-control" id="" type="text" name="registeruser_phone"  data-minlength="11" data-error="Minimum of 11 digits" value="{{ old('registeruser_phone') }}" required />
											 <div class="help-block with-errors"> {{ $errors->first('registeruser_phone') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="actions-toolbar">
                                <div class="primary">
                                    <button type="submit" class="action submit primary" title="Create an Account"><span>Complete Account</span></button>
                                </div>
                            </div>
                        </form>
<!--                        <script>
                            jQuery(function ($) {
                                var dataForm = $('#form-validate');
                                var ignore = 'input[id$="full"]';
                                dataForm.mage('validation', {
                                    errorPlacement: function (error, element) {
                                        if (element.prop('id').search('full') !== -1) {
                                            var dobElement = $(element).parents('.customer-dob'),
                                                    errorClass = error.prop('class');
                                            error.insertAfter(element.parent());
                                            dobElement.find('.validate-custom').addClass(errorClass)
                                                    .after('<div class="' + errorClass + '"></div>');
                                        } else {
                                            error.insertAfter(element);
                                        }
                                    },
                                    ignore: ':hidden:not(' + ignore + ')'
                                }).find('input:text').attr('autocomplete', 'off');
                            });
                        </script>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection