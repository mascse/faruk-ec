@extends('layouts.app')
@section('title', 'Edit Address')
@section('content')
<style>
    .ui-borderBottom {
        border-bottom: 1px solid #4b4b4b99!important;
    }
    .pas, .pbs, .pvs {
        padding-bottom: 5px!important;
    }
    .box-bdr, .ui-blueBtn {
        border: 1px solid;
    }
    .box {
        position: relative;
        display: block;
        box-sizing: border-box;
    }
    .box-bdr {
        border-color: #D3D3D3;
        border-radius: 0;
    }
    .mas, .mhs, .mrs {
        margin-right: 5px!important;
    }
    .pam, .phm, .plm {
        padding-left: 10px!important;
    }
    .pam, .pbm, .pvm {
        padding-bottom: 10px!important;
    }
    .myaccountBox1 {
        min-height: 140px;
    }
    .fsml {
        font-size: 15px;
        color:black;
    }
    .pam, .phm, .plm {
        padding-left: 10px!important;
    }
    .mas, .mhs, .mrs {
        margin-right: 5px!important;
    }
    .pam, .phm, .prm {
        padding-right: 10px!important;
    }
    .myaccountBox1 {
        min-height: 140px;
    }
    .myaccountBox1 {
        min-height: 209px;
    }
    .myaccountBox1>a {
        position: absolute;
        bottom: 10px;
        right: 20px;
    }
    .i-edit {
        background: url(storage/app/public/edit.png) no-repeat;
    }
    .i-attention, .i-checked, .i-edit, .i-favorite, .i-information, .i-remove {
        padding-left: 20px;
        cursor: pointer;
    }
    .myaccountBox1 p:first-of-type, .myaccountBox2 .address p, .sel-address-additional .address p, .sel-address-billing .address p, .sel-address-shipping .address p {
        white-space: nowrap;
        overflow: hidden!important;
        text-overflow: ellipsis;
    }
    .rfloat, .thm-spinbasket .l-hasSidebar .l-main, .thm-spinbasket.l-hasSidebar .l-main {
        float: right;
    }
    .l-row {
        display: table;
    }
    .myaccountBox2 {
        min-height: 180px;
    }
    .mam, .mtm, .mvm {
        margin-top: 10px!important;
    }
    .strong {
        font-weight: 700;
    }
    p{
        margin: 0 0 10px;
    }
    .box-bdr {
        border-color: #D3D3D3;
        border-radius: 0;
    }
    .box-bgcolor {
        background: #fff;
    }
    #newAddress .box-bd {
        padding-left: 10px;
    }
    .box-bd, .box-ft, .box-hd {
        padding: 10px 10px 10px 0;
        box-sizing: border-box;
    }
    #content, .box, .box-bd, .box-ft, .box-hd, .clearfix, .l-pageWrapper {
        zoom: 1;
    }
    .txtRight {
        text-align: right;
    }
    .mam, .mhm, .mrm {
        margin-right: 10px!important;
    }
    .mas, .mts, .mvs {
        margin-top: 5px!important;
    }
    main.osh-container fieldset {
        border: 0;
        margin: 0;
        padding: 0;
    }
    .size3of4 {
        width: 75%;
    }
    .unit {
        float: left;
    }
    .ui-formRow, .ui-formRow .col2 {
        overflow: hidden;
    }
    .ui-formRow {
        margin-bottom: .8em;
    }
    .ui-fieldset, .ui-formRow {
        position: relative;
    }
    .ui-formRow .two-col1 {
        width: 50%;
        float: left;
    }
    .ui-inputPassword, .ui-inputText, select, textarea {
        padding: 4px;
        font-size: 13px;
        font-size: 1.3rem;
        background-color: #FFF;
        border: 1px solid #ccc;
    }
    .ui-formRow .ui-inputPassword, .ui-formRow .ui-inputRemember, .ui-formRow .ui-inputText, .ui-formRow select, .ui-inputText {
        width: 100%;
    }
    input, select, textarea {
        box-sizing: border-box;
        outline: 0;
    }
    .ui-formRow .two-col2 {
        width: 45%;
        float: right;
    }
    .mts1 {
        margin-top: 0!important;
    }
    .hidden-message {
        display: none;
    }
    .ui-formRow .three-col1 {
        width: 30%;
        float: left;
    }
    .ui-formRow .three-col2 {
        width: 30%;
        float: left;
        padding-left: 5%;
    }
    .mal, .mbl, .mvl {
        margin-bottom: 20px!important;
    }
    .mal, .mtl, .mvl {
        margin-top: 20px!important;
    }

    .txtRight {
        text-align: right;
    }
    .sel-addresses-button {
        right: 0;
    }
    .size1of2 {
        width: 50%;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
    .osh-msg-box.-error {
        border-color: #D10B23;
        color: #D10B23;
    }
    .osh-msg-box.-success {
        border-color: #28a745;
        color: #28a745;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
    .block-account .block-content li:before {
        content: "";
        font-family: FontAwesome;
        font-size: 10px;
        display: inline-block;
        position: absolute;
        cursor: pointer;
        line-height: 23px;
        color: #333;
    }
    .left-aside{
        background-color: #f3f3f385;
        border: 1px solid #00000017;
        padding: 26px;
    }
    .h4, .h5, .h6, h4, h5, h6 {
        margin-top: 11px;
        margin-bottom: 10px;
    }
    .page-title h1, .page-title h2 {
        color: #000;
        font-size: 18px;
        margin: 0;
        padding: 0;
        font-weight: bold;
    }
    .form-control {
        border-radius: 0;
        border: none;
        border: 1px solid #00000042;
        color: black;
        background-color: white;
        outline: none!important;
        box-shadow: none!important;
        /* font-family: Oswald, sans-serif; */
    }
    .btn-default {
        font-weight: 400;
        font-size: 13px;
        padding: .3125em 3.375em;
        color: #111;
        border-width: 1px;
        border-color: currentColor;
        text-transform: uppercase;
    }
	 .sidebar {
        padding: 0 20px 9999px 0;
        font-size: 12px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
    .osh-msg-box.-success {
        border-color: #79d00d;
        color: #75c511;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
	button {
    width: 100%;
    height: 30px;
    font-size: 10px;
    line-height: 28px;
    color: #fff;
    text-transform: uppercase;
    letter-spacing: 2px;
    position: relative;
    border: none;
    background: rgb(41, 30, 136);
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -ms-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Edit Addinational Address</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="container">
        <div class="row">
            <div class="col-md-2" id="search_result">
                <div class="sidebar">
                    <div class="block block-collapsible-nav">
                        <div class="title block-collapsible-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#block-collapsible-nav", "selectorsToggleClass": "active"}}'>
                            <strong>Account Dashboard</strong>
                        </div>
                        <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                            <ul class="nav items">
                                <li class="nav"><a href="{{url('/my-account')}}">Account Dashboard</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/address-book')}}">Address Book</a></li>
                                <li class="nav item current"><a href="{{url('/my-account-info')}}"><strong>Account Information</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/track-order')}}">My Orders</a></li>
                                <!--<li class="nav item"><a href="">Gift Card</a></li>  -->
                                <li class="nav item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Logout
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form></li>
                            </ul>    
                        </div>
                    </div>
                    <div class="block account-nav">
                        <div class="title account-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#account-nav", "selectorsToggleClass": "active"}}'>
                            <strong></strong>
                        </div>
                        <div class="content account-nav-content" id="account-nav">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="my-account">
                    @if (session('save'))
                    <center>
                        <div class=" osh-msg-box -success">{{session('save')}}
                        </div>
                    </center>
                    @endif    
                    <div class="page-title">
                        <h2 class="pbs ui-borderBottom" style="text-transform:uppercase;font-size: 18px;">Edit Address</h2>
                    </div>
                    <div class="dashboard">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box mtl">
                                                    <div class="box box-bdr mtm pam myaccountBox2">
                                                        <h4 class="ui-borderBottom pbs fsml">Contact Information</h4>
                                                        <div class="mts txtRight mrm">
                                                            <p class="fss">* Required fields</p>
                                                        </div>
                                                        <form id="address-form" action="{{url('/additional-address-update')}}" method="post">
                                                            {{ csrf_field() }}
                                                            <input name="address_id" type="hidden" value="{{$address->address_id}}"/>
                                                            <fieldset class="ui-fieldset">
                                                                <div class="unit size3of4">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                            <label for="AddressForm_first_name" class="required">First name <span class="required">*</span></label>
                                                                            <input id="first_name" type="text" class="form-control" name="first_name" value="<?php echo $address->first_name; ?>"  maxlength="50"/>
                                                                            <span class="help-block">
                                                                                <strong>{{ $errors->first('first_name') }}</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                            <label class="mts mts1 required" for="AddressForm_last_name">Last name <span class="required">*</span></label>
                                                                            <input id="last_name" type="text" class="form-control" name="last_name" value="<?php echo $address->last_name; ?>"  maxlength="50"/>
                                                                            <span class="help-block">
                                                                                <strong>{{ $errors->first('last_name') }}</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                            <label class="mts required" for="AddressForm_phone">Telephone <span class="required">*</span></label> 
                                                                            <input  type="text" class="form-control" name="phone" value="<?php echo $address->phone; ?>"  placeholder="+8801" />
                                                                            <span class="help-block">
                                                                                <strong>{{ $errors->first('phone') }}</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group{{ $errors->has('additional_phone') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                            <label class="mts" for="AddressForm_additional_phone">Additional phone number (optional)</label>
                                                                            <input id="additional_phone" type="text" class="form-control" name="additional_phone" value="<?php echo $address->additional_phone; ?>"  placeholder="+8801" />
                                                                            <span class="help-block">
                                                                                <strong>{{ $errors->first('additional_phone') }}</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                            <label class="mts required" for="AddressForm_address1">Address <span class="required">*</span></label>
                                                                            <textarea id="address1" type="text" class="form-control" name="address_1"  rows="3"><?php echo $address->address_1; ?></textarea>
                                                                            <span class="help-block">
                                                                                <strong>{{ $errors->first('address1') }}</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                            <label class="mts required" for="AddressForm_address1">Address_2 </label>
                                                                            <textarea id="address2" type="text" class="form-control" name="address2" rows="3"><?php echo $address->address_2; ?></textarea>
                                                                            <span class="help-block">
                                                                                <strong>{{ $errors->first('address2') }}</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="row">
                                                                            <div class="col-md-5">
                                                                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                                    <label for="AddressForm_fk_customer_address_region" class="required">City <span class="required">*</span></label>            
                                                                                    <input id="city" type="text" class="form-control" name="city" value="<?php echo $address->city; ?>"  placeholder="+8801" />
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('city') }}</strong>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <div class="form-group{{ $errors->has('zipcode') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                                                    <label for="AddressForm_fk_customer_address_city">Zip Code </label>          
                                                                                    <input id="zipcode" type="text" class="form-control" name="zipcode" value="<?php echo $address->zipcode; ?>" />
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('zipcode') }}</strong>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2"></div>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                            </fieldset>

                                                    </div>
                                                </div>
                                <div class="col-md-6 unit size1of2 mtl">
                                    <p><a class="btn action primary checkout pull-left" href="{{ URL::previous() }}">« Back</a></p>
                                </div>
                                <div class="col-md-6 unit size1of2 rfloat mtl txtRight mbl">
                                    <button style="width:48%;" class="action primary checkout pull-right" type="submit" id="send"><span>Update this address</span></button>
                                </div> 
                                </form>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</main>
<script>
    jQuery(document).ready(function () {
        jQuery('select#RegionList').on('change', function () {
            var RegionId = this.value;
            jQuery('#regionId').val(RegionId);
            var url_op = base_url + "/citylist/" + RegionId;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    jQuery('#CityList').empty();
                    jQuery('#CityList').append('<option value="">Select City</option>');
                    jQuery.each(data, function (index, cityobj) {
                        jQuery('#CityList').append('<option value="' + cityobj.CityId + '">' + cityobj.CityName + '</option>');
                    });
                }
            });
        });
    });
</script>
@endsection

