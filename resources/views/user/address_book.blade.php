@extends('layouts.app')
@section('title', 'Address Book')
@section('content')
<style>
    .sidebar {
        padding: 0 20px 9999px 0;
        font-size: 12px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
    .osh-msg-box.-success {
        border-color: #B0DA7C;
        color: #B0DA7C;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Address Book</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id="search_result">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-collapsible-nav">
                        <div class="title block-collapsible-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#block-collapsible-nav", "selectorsToggleClass": "active"}}'>
                            <strong>Account Dashboard</strong>
                        </div>
                        <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                            <ul class="nav items">
                                <li class="nav item current"><a href="{{url('/my-account')}}"><strong>Account Dashboard</strong></a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/address-book')}}">Address Book</a></li>
                                <li class="nav item"><a href="{{url('/my-account-info')}}">Account Information</a></li>
                                <li class="nav item">
                                    <span class="delimiter"></span>
                                </li>
                                <li class="nav item"><a href="{{url('/track-order')}}">My Orders</a></li>
                                <!--<li class="nav item"><a href="">Gift Card</a></li>  -->
                                <li class="nav item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Logout
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form></li>
                            </ul>    
                        </div>
                    </div>
                    <div class="block account-nav">
                        <div class="title account-nav-title" data-mage-init='{"toggleAdvanced": {"toggleContainers": "#account-nav", "selectorsToggleClass": "active"}}'>
                            <strong></strong>
                        </div>
                        <div class="content account-nav-content" id="account-nav">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">

                <div class="block block-dashboard-info">
                    <div class="block-title"><strong>Address Book</strong>
                        <a href="{{url('/address-create')}}" class="btn btn-round btn-b rfloat myaccountBox1-select-customer-address sel-link-my-addresses pull-right">Add A New Address</a>
                    </div>
                    <div class="col-md-6" style="padding-left: 0px;">
                        <div class="box mtl size1of2">
                            <br>
                            <div class="box box-bdr mtm pam myaccountBox2">
                                <h4 class="ui-borderBottom pbs fsml">Your address</h4>
								<hr>
                                <div class="mtm">
                                    <div class="osh-address -widget">
                                        <div class="osh-row -name ft-name"><?php echo $user_details->registeruser_firstname . ' ' . $user_details->registeruser_lastname; ?></div>
                                        <div class="osh-row -with-icon-left"><i class="osh-font-address"></i> 
                                            <span class="osh-field -address">
                                                <span class="osh-field -address1 ft-address1 word-wrap"><i class="fa fa-fw">&#xF041;</i><?php echo $user_details->registeruser_address; ?>,<?php echo $user_details->registeruser_city; ?> ,<?php echo $user_details->registeruser_country; ?></span> 
                                                <span class="osh-field -address2 ft-address2 word-wrap"></span> 
                                                <span class="osh-field -city ft-city">    </span>
                                                <span class="osh-field -region ft-region"></span>
                                            </span>
                                        </div>
                                        <div class="osh-row -with-icon-left">
                                            <i class="osh-font-simple-phone"></i> <span class="osh-field -phones">
                                                <span class="osh-field -phone ft-phone1"><i class="fa fa-fw">&#xF095;</i><?php echo $user_details->registeruser_phone; ?></span> 
                                                <span class="osh-field -additional-phone ft-phone2"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="osh-address -widget -pickupStation hidden"><div class="osh-row -name"></div><div class="osh-row -with-icon-left"><i class="osh-font-address"></i> <span class="osh-field -address1"></span></div></div>                        </div>
                                <a class="mts  myaccountBox2-edit-shipping-address sel-edit-my-shipping-addresse" href="{{url("/address-edit/$user_details->registeruserdetails_id")}}"><i class="fa fa-fw">&#xF129;</i> Edit address</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="unit size1of2 mtm lastUnit">
                            <br>
                            <div class="box box-bdr pam mls mbl">
                                <h4 class="ui-borderBottom fsml pbs mbm">Additional addresses</h4>
								<hr>
                                <?php foreach ($address_book as $address) { ?>
                                    <div class="ui-borderBottom mbm ft-address sel-address-additional">
                                        <!-- There are no additional addresses in your address book --->
                                        <div class="osh-address -widget"><div class="osh-row -name ft-name"><?php echo $address->first_name; ?> <?php echo $address->last_name; ?></div>
                                            <div class="osh-row -with-icon-left"><i class="osh-font-address"></i> 
                                                <span class="osh-field -address"><span class="osh-field -address1 ft-address1 word-wrap">
                                                        <i class="fa fa-fw">&#xF041;</i> <?php echo $address->address_1; ?></span> 
                                                    <span class="osh-field -address2 ft-address2 word-wrap"></span> 
                                                    <span class="osh-field -city ft-city"><?php echo $address->city; ?></span>
                                                    <span class="osh-field -region ft-region"> - <?php echo $address->zipcode; ?></span></span>
                                            </div>
                                            <div class="osh-row -with-icon-left">
                                                <i class="osh-font-simple-phone"></i> <span class="osh-field -phones">
                                                    <span class="osh-field -phone ft-phone1"> <i class="fa fa-fw">&#xF095;</i><?php echo $address->phone; ?></span> 
                                                    <span class="osh-field -additional-phone ft-phone2"><?php echo $address->additional_phone; ?></span></span>
                                            </div>
                                        </div>
                                        <div class="osh-address -widget -pickupStation hidden">
                                            <div class="osh-row -name"></div>
                                            <div class="osh-row -with-icon-left"><i class="osh-font-address"></i> <span class="osh-field -address1"></span></div>
                                        </div>                                
                                        <a class="ui-link icon i-information mts mbm ft-edit-address-button sel-link-edit-additional-871607" href="{{url("/eidt-additional-address/{$address->address_id}")}}"><i class="fa fa-fw">&#xF040;</i> Edit Address</a>
                                        <a role="button" tabindex="0" class="ui-link icon i-remove mts mbm ft-delete-address-button sel-link-delete-additional-871607" onclick="confirm_delete('{{url('/address-delete/')}}/<?php echo $address->address_id; ?>')">
                                            <i  class="fa fa-fw">&#xf00d;</i>Delete Address</a>
                                        <a class="ui-link icon i-favorite mts mbm ft-set-as-default-button sel-link-make-additional-default-shipping-871607" href="{{url("/set-default-address/{$address->address_id}")}}">
                                            <i class="fa fa-fw">&#xF006;</i> Set as default delivery</a>
                                    </div>
									<hr>
                                <?php } ?>
                                <?php
                                if (is_null($address_book)) {
                                    echo "There are no additional addresses in your address book";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--- Delete modal ----->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">                                                                    
            <div class="modal-header">
                <h6 class="font-alt" style="text-align:center;">Are you sure to remove this item ?</h6>
                <a class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#"  id="delete_link" style="color:red;">Remove</a> &nbsp;&nbsp; | &nbsp;&nbsp;
                <a href="#" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>
@endsection

