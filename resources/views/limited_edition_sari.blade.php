<?php

use App\Http\Controllers\product\ProductController;
?>
@extends('layouts.app')
@section('title','Woman Limited Edition Sari')
@section('content')
<!--- slider here ---->
<style>
    .img-fluid{
        height:383px !important;
    }
</style>
<div id="search_result">
    <div class="container">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img  src="{{ URL::to('') }}/storage/app/img/shop_banner/home page banner 3.jpg" class="img-responsive" alt="First slide">
                </div>
            </div>
        </div>
    </div>
<!--- slider end here ---->
<main class="main shop-page">
    <div class="container">
        <div class="row align-items-start">
            <div class="col-12 d-flex justify-content-end d-lg-none">
            </div>
            <!--            <div class="col-12 col-lg-3 collapse d-lg-block" id="shopSidebar">
                            <aside class="shop-sidebar">
                                <section class="shop-sidebar__category">
                                    <h4 class="h4 text-uppercase">category</h4>
                                    <ul class="shop-sidebar__category-list js-collapse-menu">
                                        <li><a class="item-link" href="#">Pants</a>
                                        </li>
                                        <li><a class="item-link active" href="#" data-toggle="collapse-next">Shirts<i class="caret ml-1 fa fa-angle-down"></i></a>
                                            <div class="collapse show">
                                                <ul class="w-100">
                                                    <li><a class="item-link" href="#">Sneakers</a></li>
                                                    <li><a class="item-link" href="#">Shoes</a></li>
                                                    <li><a class="item-link" href="#">Boots</a></li>
                                                    <li><a class="item-link" href="#">Sandals</a></li>
                                                    <li><a class="item-link" href="#">Slippers</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a class="item-link" href="#">Shorts</a>
                                        </li>
                                        <li><a class="item-link" href="#" data-toggle="collapse-next">Footwear<i class="caret ml-1 fa fa-angle-down"></i></a>
                                            <div class="collapse">
                                                <ul class="w-100">
                                                    <li><a class="item-link" href="#">Sneakers</a></li>
                                                    <li><a class="item-link" href="#">Shoes</a></li>
                                                    <li><a class="item-link" href="#">Boots</a></li>
                                                    <li><a class="item-link" href="#">Sandals</a></li>
                                                    <li><a class="item-link" href="#">Slippers</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a class="item-link" href="#">Jeans</a>
                                        </li>
                                        <li><a class="item-link" href="#" data-toggle="collapse-next">T-shirts<i class="caret ml-1 fa fa-angle-down"></i></a>
                                            <div class="collapse">
                                                <ul class="w-100">
                                                    <li><a class="item-link" href="#">Sneakers</a></li>
                                                    <li><a class="item-link" href="#">Shoes</a></li>
                                                    <li><a class="item-link" href="#">Boots</a></li>
                                                    <li><a class="item-link" href="#">Sandals</a></li>
                                                    <li><a class="item-link" href="#">Slippers</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a class="item-link" href="#">Hoodies</a>
                                        </li>
                                        <li><a class="item-link" href="#">Glasses</a>
                                        </li>
                                        <li><a class="item-link" href="#">Bags</a>
                                        </li>
                                        <li><a class="item-link" href="#">Underwear</a>
                                        </li>
                                    </ul>
                                </section>
                                <section class="shop-sidebar__price mb-3">
                                    <h4 class="h4 text-uppercase">price</h4>
                                    <div class="price-slider">
                                        <div class="price-slider__ranger mb-4" id="priceSliderChange"></div>
                                        <div class="d-flex justify-content-between">
                                            <label class="price-slider__label d-inline-flex"><span class="label mr-1 d-none">From</span>
                                                <input class="form-control" id="priceSliderMin" type="text">
                                            </label>
                                            <label class="price-slider__label d-inline-flex"><span class="label mr-1 d-none">To</span>
                                                <input class="form-control text-right" id="priceSliderMax" type="text">
                                            </label>
                                        </div>
                                    </div>
                                </section>
                                <section class="shop-sidebar__colors mb-4">
                                    <h4 class="h4 mb-4 text-uppercase">color</h4>
                                    <div class="row js-product-filter">
                                        <div class="col-12 col-md-6">
                                            <ul class="product-filter">
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-all-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">All</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Black</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">White</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Blue</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Green</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Red</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Beige</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <ul class="product-filter">
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Terracotta</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Ivory</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Beige</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Pink</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Khaki</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox pointer">
                                                        <input class="custom-control-input js-item-check" type="checkbox" name="filter_color"><span class="d-inline-block custom-control-indicator desc">Purple</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <section class="shop-sidebar__size mb-4">
                                    <h4 class="h4 mb-4 text-uppercase">color</h4>
                                    <div class="product-filter product-filter--size d-flex flex-wrap">
                                        <label class="custom-control custom-radio m-0">
                                            <input class="custom-control-input" type="checkbox" name="filter_size" required><span class="d-inline-block custom-control-indicator desc">XS</span>
                                        </label>
                                        <label class="custom-control custom-radio m-0">
                                            <input class="custom-control-input" type="checkbox" name="filter_size" required><span class="d-inline-block custom-control-indicator desc">S</span>
                                        </label>
                                        <label class="custom-control custom-radio m-0 active">
                                            <input class="custom-control-input" type="checkbox" name="filter_size" required><span class="d-inline-block custom-control-indicator desc">M</span>
                                        </label>
                                        <label class="custom-control custom-radio m-0">
                                            <input class="custom-control-input" type="checkbox" name="filter_size" required><span class="d-inline-block custom-control-indicator desc">L</span>
                                        </label>
                                        <label class="custom-control custom-radio m-0">
                                            <input class="custom-control-input" type="checkbox" name="filter_size" required><span class="d-inline-block custom-control-indicator desc">XL</span>
                                        </label>
                                        <label class="custom-control custom-radio m-0">
                                            <input class="custom-control-input" type="checkbox" name="filter_size" required><span class="d-inline-block custom-control-indicator desc">XXL</span>
                                        </label>
                                    </div>
                                </section>
                                <section class="shop-sidebar__brands mb-4">
                                    <h4 class="h4 mb-4 text-uppercase">Brands</h4>
                                    <ul class="product-filter js-product-filter">
                                        <li>
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input js-all-check" type="checkbox" name="filter_brand"><span class="d-inline-block custom-control-indicator desc">All</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input js-item-check" type="checkbox" name="filter_brand"><span class="d-inline-block custom-control-indicator desc">Kaden Anderson</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input js-item-check" type="checkbox" name="filter_brand"><span class="d-inline-block custom-control-indicator desc">Rachel Lewis</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input js-item-check" type="checkbox" name="filter_brand"><span class="d-inline-block custom-control-indicator desc">David Crossman</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input js-item-check" type="checkbox" name="filter_brand"><span class="d-inline-block custom-control-indicator desc">Darcy Farmer</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input js-item-check" type="checkbox" name="filter_brand"><span class="d-inline-block custom-control-indicator desc">Benjamin Flatcher</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input js-item-check" type="checkbox" name="filter_brand"><span class="d-inline-block custom-control-indicator desc">Deborah Rodriguez</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input js-item-check" type="checkbox" name="filter_brand"><span class="d-inline-block custom-control-indicator desc">Bryden Dutton</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input js-item-check" type="checkbox" name="filter_brand"><span class="d-inline-block custom-control-indicator desc">Cheryl Chesterton</span>
                                            </label>
                                        </li>
                                    </ul>
                                </section>
                                <section class="shop-sidebar__tags">
                                    <h4 class="h4 mb-4 text-uppercase">Tags</h4>
                                    <ul class="product-filter d-block d-md-flex flex-wrap js-product-filter">
                                        <li class="mr-2">
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input" type="checkbox" name="filter_tag"><span class="d-inline-block custom-control-indicator desc">Design</span>
                                            </label>
                                        </li>
                                        <li class="mr-2">
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input" type="checkbox" name="filter_tag"><span class="d-inline-block custom-control-indicator desc">Tourism</span>
                                            </label>
                                        </li>
                                        <li class="mr-2">
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input" type="checkbox" name="filter_tag"><span class="d-inline-block custom-control-indicator desc">Work</span>
                                            </label>
                                        </li>
                                        <li class="mr-2">
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input" type="checkbox" name="filter_tag"><span class="d-inline-block custom-control-indicator desc">Food</span>
                                            </label>
                                        </li>
                                        <li class="mr-2">
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input" type="checkbox" name="filter_tag"><span class="d-inline-block custom-control-indicator desc">People</span>
                                            </label>
                                        </li>
                                        <li class="mr-2">
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input" type="checkbox" name="filter_tag"><span class="d-inline-block custom-control-indicator desc">Web Design</span>
                                            </label>
                                        </li>
                                        <li class="mr-2">
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input" type="checkbox" name="filter_tag"><span class="d-inline-block custom-control-indicator desc">Adventure</span>
                                            </label>
                                        </li>
                                        <li class="mr-2">
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input" type="checkbox" name="filter_tag"><span class="d-inline-block custom-control-indicator desc">Travel</span>
                                            </label>
                                        </li>
                                        <li class="mr-2">
                                            <label class="custom-control custom-checkbox pointer">
                                                <input class="custom-control-input" type="checkbox" name="filter_tag"><span class="d-inline-block custom-control-indicator desc">Landscape</span>
                                            </label>
                                        </li>
                                    </ul>
                                </section>
                            </aside>
                        </div>-->
            <div class="col-12 col-lg-12">
                <!-- <div class="sortbar">
                     <div class="sortbar__row row align-items-center">
                         <div class="col-12 d-flex justify-content-between col-lg-4 col-xl-4">
                             <div class="sortbar__search">showing<span> 9</span> of<span> 100</span> results</div>
                         </div>
                         <div class="col-lg-3 col-xl-3 d-flex align-items-center">
                             <div class="sortbar__label">show</div>
                             <div class="select-dropdown sortbar__select sortbar__select--show js-sortbar-show">
                                 <select class="form-control js-select js-sort-show" name="sort_show">
                                     <option value="9">9</option>
                                     <option value="8">8</option>
                                     <option value="7">7</option>
                                     <option value="6">6</option>
                                     <option value="5">5</option>
                                     <option value="4">4</option>
                                     <option value="3">3</option>
                                 </select>
                             </div>
                         </div>
                         <div class="col-lg-3 col-xl-3 d-flex align-items-center">
                             <div class="sortbar__label">price</div>
                             <div class="select-dropdown sortbar__select sortbar__select--price js-sortbar-price">
                                 <select class="form-control select-dropdown js-select js-sort-price" name="sort_price">
                                     <option value="asc">asc</option>
                                     <option value="descending">descending</option>
                                 </select>
                             </div>
                         </div>
                         <div class="sortbar__col-grid col-lg-2 col-xl-2 text-right">
                             <ul class="sortbar__gridlist">
                                 <li><a class="btn-toggle-grid js-toggle-grid active" href="#" data-grid="grid" data-cols="col-6 col-lg-4" data-toggle="tooltip" data-placement="top" title="Grid"><span></span><span></span><span></span></a></li>
                                 <li><a class="btn-toggle-list js-toggle-grid" href="#" data-grid="list" data-cols="list col-12" data-toggle="tooltip" data-placement="top" title="List"><span></span><span></span></a>
                                 </li>
                             </ul>
                         </div>
                     </div>
                 </div> --->
                <div class="row">
                    @foreach($product_list as $product)
                    <?php
                    $product_name = str_replace(' ', '-', $product->product_name);
                    $product_url = strtolower($product_name);
                    $data = ProductController::GetProductColorAlbum($product->product_id);
                    ?>
                    <div class="product-grid col-6 col-md-3">
                        <div class="product-item row js-shop-item-product">
                            <div class="product-item__grid col-5 col-sm-5 col-lg-4 mb-4">
                                <div class="product-item__img">
                                    <a class="product-item__img-link js-shop-img-flip d-block" href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}">
                                        <?php
                                        foreach ($data as $pro_album) {
                                            $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                                        }
                                        ?>
                                        <span class="front">
                                            <img class="img-fluid"  
                                                 src="{{ URL::to('') }}/storage/app/pgallery/<?php echo $product->productimg_img_medium; ?>" 
                                                 onmouseover="this.src = base_url + '/storage/app/pgallery/<?php echo $colorwiseimg->productimg_img_medium; ?>'" 
                                                 onmouseout="this.src = base_url + '/storage/app/pgallery/<?php echo $product->productimg_img_medium; ?>'" border="0" alt=""/>
<!--                                            <img class="img-fluid" src="{{ URL::to('') }}/storage/app/pgallery/{{$product->productimg_img_medium}}" alt="#" height="383px"/>-->
                                        </span>
                                        <span class="back"><img class="img-fluid" src="{{ URL::to('') }}/storage/app/img/product/294_detailimg_2.13.jpg" alt="#"/></span>
                                    </a>
                                </div>
                            </div>
                            <div class="product-item__list collapse js-list-show col-7 col-sm-7 col-lg-8">
                                <div class="product-item__title h4 text-uppercase mb-4">{{$product->product_name}}</div>
                                <div class="product-item__price"><span class="currency">Tk</span><span> {{$product->product_price}}</span></div>
                                <div class="product-rating mb-3">
                                    <div class="product-rating__stars mr-2 mb-2">
                                        <select class="js-product-rating">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4" selected="selected">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                    <div class="product-rating__stat mb-2"><span class="text-uppercase">number of ratings</span><span class="d-inline-block align-top px-2">|</span><span class="value">340</span></div>
                                </div>
                                <div class="product-item__list-desc">
                                    <p></p>
                                </div>
                                <div class="product-share">
                                    <button class="product-share__btn-cart btn btn-shop" type="button">add to cart</button>
<!--                                    <button class="product-share__btn pointer" type="button" data-toggle="tooltip" data-placement="top" title="View"><i class="ion-ios-eye-outline"></i></button>
                                    <button class="product-share__btn pointer" type="button" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="ion-android-favorite-outline"></i></button>-->
                                </div>
                            </div>
                            <div class="product-item__bottom js-grid-show">
                                <div class="product-item__title h4 text-uppercase mb-2"><a href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}">{{$product->product_name}}</a></div>
                                <div class="product-item__price mb-0"><span class="currency">Tk</span><span> {{$product->product_price}}</span></div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <nav aria-label="Page navigation example">
                    <center>
                        {{ $product_list->links() }}
                    </center>
                </nav>
            </div>
        </div>

    </div>
 </main>
</div>
@endsection