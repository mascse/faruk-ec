@extends('layouts.app')
@section('title','Order Confirm')
@section('content')
<main id="maincontent" class="page-main" style="padding-top: 64px;">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Order Confirm</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="static-page cms-content">
                    <header class="head">
                        <center><h1>Your Order Has Been Confirmed!</h1></center>
                    </header>
                    <div class="holder">
                        <center>
                            <div class="module-subtitle font-serif">
                                <div class="trackinginfo">
                                    <center>Here is your tracking information:</center>
                                    <center>
                                        Order Tracking Number#&nbsp;<strong>{{$conforder_tracknumber}}</strong></center>
                                    <center> Please note it may take 24 hours for your tracking number to return any information.</center>
                                    <center>  To track your order click <a href="{{url('/track-order')}}" target="_self" title="Track Your Order" style="font-size: 14px; color: #000000; text-decoration: underline;">here</a> 
                                    </center>
                                    <center><p class="bodytxt" style="margin-top: 5px;color:#000000;">
                                            <a class="tracking" style="font-size: 14px; color:#000000; text-decoration: underline;" href="#">Contact Us for More Information</a>
                                        </p>  </center>                
                                </div>   
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
{!! $order_product !!}
<!--<script>
window.dataLayer = window.dataLayer || [];
dataLayer.push({
 'event': 'purchase',
 'transactionId': "{{$track_number}}",
 'transactionAffiliation': 'Pride Limited',
 'transactionTotal': {{$shoppingcart_total}},
 'transactionTax': 0,
 'transactionShipping': {{$shipping_Charge}},
 'transactionProducts': {!! $order_product !!}
});
console.log(dataLayer);
</script> --->
@endsection

