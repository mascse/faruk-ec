@extends('admin.layouts.app')
@section('title', 'Manage Category')
@section('content')
<section class="content-header">
    <h1>
        Category
        <small>Manage Category</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Category</a></li>
        <li class="active">Manage Category</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Browse All Product Category</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Product Category</th>
                                <th>Last Update</th>
                                <th>Update By</th>
                                <th style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($main_category as $category)
                            <tr>
                                <td style="width:2%;"></td>
                                <td>{{$category->procat_name}}</td>
                                <td><?php
                                    $up_date = strtotime($category->procat_lastupdate);
                                    echo date('M d, Y h:m a', $up_date);
                                    ?></td>
                                <td>{{$category->employe_name}}</td>
                                <td><a class="btn btn-info btn-flat btn-sm margin tdata" href="{{url("/edit-procate/{$category->procat_id}")}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
@endsection