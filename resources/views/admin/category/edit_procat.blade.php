@extends('admin.layouts.app')
@section('title', 'Edit Category')
@section('content')
<section class="content-header">
    <h1>
        Category
        <small>Edit </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <form name="edit_procat" action="{{url('update-procat')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Edit CATEGORY FORM</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <center>
                            @if (session('update'))
                            <div class="alert alert-success">
                                {{ session('update') }}
                            </div>
                            @endif
                        </center>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Category Name</label>
                            <input type="hidden" class="form-control" name="procat_id" value="<?php echo $procat->procat_id; ?>" required/>
                            <input type="text" class="form-control" name="procat_name" value="<?php echo $procat->procat_name; ?>" required/>
                        </div>
                        <div class="form-group">
                            <label>Current Banner</label>
                            <div class="mb10">
                                <span class="file-input">
                                    <div                                                                                                                                                                                                        class="file-preview">
                                        <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                        <div class="file-preview-thumbnails">
                                            <div class="file-preview-frame" id="preview">
                                                <img class="img-responsive" src="{{ URL::to('') }}/storage/app/productbanner/{{$procat->procat_banner}}" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>   
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>

                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>New Banner</label>
                            <input id="input-upload-img1" type="file" class="file" name="filebanner" data-preview-file-type="text">
                            <span class="help-block" style="color:#f39c12;">only .jpg image is allowed Size (Width: 980px X Height: 300px)</span>  
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Update Category"/>
                </div>
            </div>
        </div>
    </form>  
</section>
@endsection