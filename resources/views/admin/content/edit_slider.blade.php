@extends('admin.layouts.app')
@section('title','Edit Slider')
@section('content')
<section class="content-header">
    <h1>
        Slider
        <small>Edit </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Manage Home Page</li>
        <li>Slider</li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">SLIDER EDIT FORM</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <form name="add_subpro" action="{{url('/update-slider')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" class="form-control" name="slider_id" value="{{$slider->slider_id}}"/>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <center>
                            @if (session('save'))
                            <div class="alert alert-success">
                                {{ session('save') }}
                            </div>
                            @endif
                        </center>
                        <center>
                            @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                            @endif
                        </center>
                        <div class="form-group {{ $errors->has('slider_caption') ? ' has-error' : '' }}">
                            <label>Slider Caption</label>
                            <input type="text" class="form-control" name="slider_caption" value="{{$slider->slider_caption}}"/>
                            <div class="help-block with-errors">{{ $errors->first('slider_caption') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('slider_occasion') ? ' has-error' : '' }}">
                            <label>Occasion</label>
                            <input type="text" class="form-control" name="slider_occasion" value="{{$slider->slider_occasion}}"/>
                            <div class="help-block with-errors">{{ $errors->first('slider_occasion') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('slider_order') ? ' has-error' : '' }}">
                            <label>Slider Position</label>
                            <input type="number" class="form-control" name="slider_order" min="1" max="10" value="{{$slider->slider_order}}"/>
                            <span class="help-block" style="color:#06be1c;">Only Numbers</span>
                            <div class="help-block with-errors">{{ $errors->first('slider_order') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('url_link') ? ' has-error' : '' }}">
                            <label>Slider Url</label>
                            <input type="url" class="form-control" name="url_link" value="{{$slider->url_link}}"/>
                            <span class="help-block" style="color:#06be1c;">Only Url</span>
                            <div class="help-block with-errors">{{ $errors->first('url_link') }}</div>
                        </div>
                        <div class="form-group">
                            <label>Current Slider</label>
                            <div class="mb10">
                                <span class="file-input">
                                    <div class="file-preview">
                                        <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                        <div class="file-preview-thumbnails">
                                            <div class="file-preview-frame" id="preview">
                                                <img class="img-responsive" src="{{ URL::to('') }}/storage/app/public/slider/{{$slider->slider_image}}" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>   
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>

                                </span>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('slider_image') ? ' has-error' : '' }}">
                            <label>New Slider Image</label>
                            <input id="input-upload-img1" type="file" class="file" name="slider_image" data-preview-file-type="text">
                            <span class="help-block" style="color:#06be1c">only .jpg image is allowed Size (Width: 1110px X Height: 550px)</span>  
                            <div class="help-block with-errors">{{ $errors->first('slider_image') }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Update Slider"/>
                </div>
            </div>
        </form>    
    </div>
</section>
@endsection