<?php

use App\Http\Controllers\admin\ManageOrderController;
?>

@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<?php
//date_default_timezone_set("America/New_York");
//date_default_timezone_set("Asia/Dhaka");
//echo "The time is " . date("h:i:sa");
$subtotal = 0;
$update_subtotal = 0;
$totalprice = 0;
$shipping_area = '';
$totalamount = 0;
$shoppingcartid = 0;
$shipping_charge = 0;
foreach ($total_incomplete_order_info as $orderinfo) {
    $selectdestination_id = $orderinfo->shipping_area;
}
$shippingdestnation = ManageOrderController::GetShippingDestinationName($selectdestination_id);
?>
<style>
    .lead {
        font-size: 14px;
        font-weight: 700;
    }
    .table > tbody > tr > td {
        vertical-align: middle;
    }
</style>
<link rel="stylesheet" href="{{asset('assets_admin/lightbox/dist/css/lightbox.min.css')}}">
<script src="{{asset('assets_admin/lightbox/dist/js/lightbox-plus-jquery.min.js')}}"></script>
<div class="content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>ORDER ID <small>{{ $shipping_address_details->conforder_tracknumber }}</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">Pride Limited</a>
            </li>
            <li>
                <a href="#">Manage Incomplete Order</a>
            </li>
            <li class="active">Order Details</li>
        </ol>
    </section>
    @if (session('update'))
    <div class="pad margin no-print">
        <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4>{{ session('update') }}</h4> </div>
    </div>
    @endif
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">PRIDE LIMITED
                    <small class="pull-right">Date: <?php
                        date_default_timezone_set("Asia/Dhaka");
                        echo date("M  d, Y h:i:sa");
                        ?></small></h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                Shipping Address
                <address id='address-text' class="show">
                    Customer Name: <strong id='text-name'>{{ $shipping_address_details->Shipping_txtfirstname }} {{$shipping_address_details->Shipping_txtlastname }}</strong>
                    <br />Address: <strong id='text-address'><?php if($shipping_address_details->Shipping_txtaddressname == ''){ echo $shipping_address_details->Shipping_txtaddress1;}else{ echo $shipping_address_details->Shipping_txtaddressname;} ?></strong>
                    <br />City: <strong><span id='text-city'>{{ $shipping_address_details->Shipping_txtcity }}</span>-<span id='text-zip'>{{ $shipping_address_details->Shipping_txtzipcode }}</span></strong>
                    <br />Phone: <strong id='text-phone'>{{ $shipping_address_details->Shipping_txtphone }}</strong></address>
                <address id = 'address-field' class="hidden">
                    <input type="hidden" value="{{ $shipping_address_details->ordershipping_id }}" id="address-ordershipping-id">
                    <input type="hidden" value="{{ $shipping_address_details->CityId }}" id="address-destination-id">
                    <div class="form-group">
                        <label for="address-fname">First name: </label>
                        <input type="text" value="{{ $shipping_address_details->Shipping_txtfirstname }}" class="form-control" id="address-fname">
                    </div>
                    <div class="form-group">
                        <label for="address-lname">Last name: </label>
                        <input type="text" value="{{$shipping_address_details->Shipping_txtlastname }}" class="form-control" id="address-lname">
                    </div>
                    <div class="form-group">
                        <label for="address-address">Address: </label>
                        <textarea rows="4" class="form-control" id="address-address">{{{ $shipping_address_details->Shipping_txtaddressname }}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="address-city">City: </label>
                        <input type="text" value="{{ $shipping_address_details->Shipping_txtcity }}" class="form-control" id="address-city">
                    </div>
                    <div class="form-group">
                        <label for="address-zip">ZIP code: </label>
                        <input type="number" value="{{ $shipping_address_details->Shipping_txtzipcode }}" class="form-control" id="address-zip">
                    </div>
                    <div class="form-group">
                        <label for="address-phone">Phone: </label>
                        <input type="tel" value="{{ $shipping_address_details->Shipping_txtphone }}" class="form-control" id="address-phone">
                    </div>
                </address>
                <button id="edit-address" type="button" class="btn btn-default btn-xs pull-left show">
                    <span class="glyphicon glyphicon-edit"></span> Edit Address
                </button>
                <button id="save-address" type="button" class="btn btn-default btn-xs pull-left hidden">
                    <i class="fas fa-angle-double-right"></i> Save Address
                </button>
            </div>
            <div class="col-sm-4 invoice-col">
                <strong>Shipping Destination</strong> 
                <address id="route-text">
                    <p id="route-text-default"><?php echo $shippingdestnation; ?></p>
                    <button id="show-route-form" type="button" class="btn btn-default btn-xs pull-left">
                        <span class="glyphicon glyphicon-edit"></span> Edit Route
                    </button>
                </address>
                
                <div class="hidden" id="route-form">
                    <div id="route-form">
                        <div class="input-group">
                            <label>City Name</label>
                            <select name="route-region" class="form-control" id="route-region">
                                <option value="1" selected>Dhaka</option> 
                                <option value="2" selected>Chittagong</option>
                                <option value="3" selected>Barisal</option>
                                <option value="4" selected>Khulna</option> 
                                <option value="5" selected>Mymensingh</option>
                                <option value="6" selected>Rajshahi</option>
                                <option value="7" selected>Rangpur</option> 
                                <option value="8" selected>Sylhet</option>
                                <option value="" selected>&nbsp;--- Select City ---&nbsp;&nbsp;</option>
                            </select>
                        </div>
                        <div class="input-group">
                            <label>Area (Route)</label>
                            <select name="route-city" class="form-control" id="route-city">
                                <option value="">--Select--</option>
                            </select>
                        </div>
                    </div>
                    <br/>
                    <div class="input-group">
                        <button id="save-route-form" type="button" class="btn btn-default btn-xs pull-left">
                            <span class="glyphicon glyphicon-edit"></span> Save Route
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 invoice-col">
                <b>{{ $shipping_address_details->conforder_tracknumber }}</b><br>
                <br>
                3pl Status: <b>{{ $shipping_address_details->order_threepldlv }}</b><br>
                Order Status:
                <b> <?php
                    if ($shipping_address_details->conforder_completed == 1) {
                        echo "Order Completed";
                    } else if ($shipping_address_details->conforder_completed == 0) {
                        echo $shipping_address_details->conforder_status;
                    } else {
                        
                    }
                    ?></b>
                <br>
                Order Placed Date:<b>  <?php $place_date=strtotime($shipping_address_details->created_at); echo date('d M, Y  h:i A',$place_date); ?></b><br>
                Delivery Date: <b> <?php echo $shipping_address_details->conforder_deliverydate; ?></b>
            </div>
        </div>
        <!-- Table row -->
        <br>
        <div class="row">
            <div class="col-xs-12 table-responsive" id="ViewTable">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Style</th>
                            <th>Color</th>
                            <th>Size</th>
                            <th>Unit Price</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $k = 1; ?>
                        @foreach($total_incomplete_order_info as $orderinfo)   						
                        <tr>
                            <?php
                            $subtotal = $subtotal + ($orderinfo->shoppinproduct_quantity * $orderinfo->product_price);
                            $shipping_charge = $orderinfo->Shipping_Charge;
                            $selectdestination = $orderinfo->shipping_area;
                            $dmselect = $orderinfo->deliveryMethod;
                            ?>
                          <td>
                            <a class="example-image-link" href="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" data-lightbox="example-1"><img class="example-image" src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" height="150" width="120" alt="image-1" /></a>
                            </td>
                            <td style="color:black">{{$orderinfo->product_name}}</td>
                            <td style="color:black">{{$orderinfo->product_styleref}}</td>
                            <td style="color:black">{{$orderinfo->productalbum_name}}</td>
                            <td>{{$orderinfo->prosize_name}}</td>
                            <td>{{$orderinfo->product_price}}</td>
                            <td>{{$orderinfo->shoppinproduct_quantity}}</td>
                            <td>{{$orderinfo->product_price*$orderinfo->shoppinproduct_quantity}}</td>
                            <td>
                                <input type="hidden" name="product_id"  id="product_id_<?php echo $k; ?>" value="{{$orderinfo->product_id}}">
                                <input type="hidden" name="shoppinproduct_id"  id="shoppinproduct_id_<?php echo $k; ?>" value="{{$orderinfo->shoppinproduct_id}}">
                                <input type="hidden" name="shoppinproduct_id"  id="Color_<?php echo $k; ?>" value="{{$orderinfo->productalbum_name}}">
                                <input type="hidden" name="shoppinproduct_id"  id="ProSize_<?php echo $k; ?>" value="{{$orderinfo->prosize_name}}">
                                <input type="hidden" name="shoppinproduct_id"  id="ProQty_<?php echo $k; ?>" value="{{$orderinfo->shoppinproduct_quantity}}">
                                <a href="#" id="btnexchange_<?php echo $k; ?>" class="btn btn-sm btn-primary exchange">Exchanged</a> |  
                                <input type="submit" name="cancle" id="cancel_<?php echo $k; ?>" class="btn btn-sm btn-danger" value="Cancel"/>
                            </td>
                        </tr>
                        <?php $k++; ?>
                        @endforeach
                    </tbody>
                </table>
                <input type="hidden" name="total_loop" id="total_loop" value="<?php echo $k - 1; ?>">	
            </div>
            <!-- /.col -->
            <div class="col-xs-12 table-responsive" id="EditTable" style="display:none;">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Style</th>
                            <th>Color</th>
                            <th>Size</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $m = 1;
                        ?>
                        @foreach($total_incomplete_order_info as $orderinfo)   
                    <form action="{{url('/exchange')}}" method="post">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <tr style="display:none;" id="rowId_<?php echo $m; ?>">
                            <?php
                            $update_subtotal = $update_subtotal + ($orderinfo->shoppinproduct_quantity * $orderinfo->product_price);
                            $shipping_charge = $orderinfo->Shipping_Charge;
                            $selectdestination = $orderinfo->shipping_area;
                            $dmselect = $orderinfo->deliveryMethod;
                            ?>

                            <td style="color:black"  width="5%"><img src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" width="100"/></td>
                            <td style="color:black">{{$orderinfo->product_name}}</td>
                            <td style="color:black">{{$orderinfo->product_styleref}}</td>
                            <td style="color:black">
                                <?php $Colorlist = ManageOrderController::GetColorListByProductId($orderinfo->product_id); ?>
                                <select class="form-control" name="new_productalbum_name" id="ColorId_<?php echo $m; ?>">
                                    <?php
                                    foreach ($Colorlist as $color) {
                                        ?>
                                        <option value="<?php echo $color->color_name; ?>"<?php
                                        if ($color->color_name == $orderinfo->productalbum_name) {
                                            echo ' selected';
                                        }
                                        ?>><?php echo $color->color_name; ?></option>
                                                <?php
                                            }
                                            ?>										 
                                </select>
                            </td>
                            <td>
                                <select class="form-control" name="new_prosize_name" id="SizeId_<?php echo $m; ?>">
                                    <?php
                                    $Sizelist = ManageOrderController::GetSizeListByProductId($orderinfo->product_id, $orderinfo->productalbum_name);
                                    foreach ($Sizelist as $Size) {
                                        ?>
                                        <option value="<?php echo $Size->productsize_size; ?>"<?php
                                        if ($Size->productsize_size == $orderinfo->prosize_name) {
                                            echo ' selected';
                                        }
                                        ?>><?php echo $Size->productsize_size; ?></option>
                                                <?php
                                            }
                                            ?>
                                </select>
                            </td>
                            <td>
                                <select class="form-control" name="new_shoppinproduct_quantity" id="QtyId_<?php echo $m; ?>">
                                    <?php
                                    $Qtys = ManageOrderController::GetQtyProductId($orderinfo->product_id, $orderinfo->productalbum_name, $orderinfo->prosize_name);
                                    for ($q = 1; $q <= $Qtys + 1; $q++) {
                                        ?>
                                        <option value="<?php echo $q; ?>" <?php
                                        if ($q == $orderinfo->shoppinproduct_quantity) {
                                            echo ' selected';
                                        }
                                        ?>><?php echo $q; ?></option>
                                            <?php }
                                            ?>
                                </select>
                            </td>
                            <td>{{$orderinfo->product_price}}</td>
                            <td>
                                <input type="hidden" name="product_id"  id="product_id_<?php echo $k; ?>" value="{{$orderinfo->product_id}}">
                                <input type="hidden" name="shoppinproduct_id"  id="shoppinproduct_id_<?php echo $k; ?>" value="{{$orderinfo->shoppinproduct_id}}">
                                <input type="hidden" name="productalbum_name"  id="Color_<?php echo $k; ?>" value="{{$orderinfo->productalbum_name}}">
                                <input type="hidden" name="prosize_name"  id="ProSize_<?php echo $k; ?>" value="{{$orderinfo->prosize_name}}">
                                <input type="hidden" name="shoppinproduct_quantity"  id="ProQty_<?php echo $k; ?>" value="{{$orderinfo->shoppinproduct_quantity}}">
                                <input type="submit"  id="updatechange_<?php echo $m; ?>" class="btn-sm btn-primary exchange" value="Exchanged">
                            </td>
                        </tr>
                    </form>
                    <?php $m++; ?>								
                    @endforeach
                    </tbody>
                </table>
                <input type="hidden" name="total_loop" id="update_total_loop" value="<?php echo $m - 1; ?>">	
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-8">
                <p>
                    <span>Shipping Destination:</span><span class="lead"> <?php echo $shippingdestnation; ?></span> </p>
                <p>
                    <span>Delivery Mode:</span> 
                    <span class="lead">
                        <?php
                        if ($dmselect == 'cs') {
                            echo 'Cash on delivery';
                        } else {
                            echo 'bKash';
                        }
                        ?>
                    </span>
                </p>
                <p >
                    <span>Requested Delivery Notes:</span>
                    <span class="lead"><?php echo $shipping_address_details->conforder_deliverynotes; ?></span></p>
            </div>
            <!-- /.col -->
            <?php if($shipping_address_details->employee_status==1){
                $subtotal=$subtotal-($subtotal*20/100);
            } ?>
            <div class="col-xs-4">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td>Tk {{ $subtotal }}</td>
                        </tr>
                        <tr>
                            <th>
                                <?php
                                if ($subtotal >= 3000) {
                                    echo "Shipping Charge";
                                } else {
                                    if ($shipping_charge == 70) {
                                        echo "Shipping Charge(Inside Dhaka): ";
                                    } else {
                                        echo "Shipping Charge(Outside Dhaka): ";
                                    }
                                }
                                ?>
                            </th>
                            <td>
                                <?php
                                if ($subtotal >= 3000) {
                                    $shipping_charge = 0;
                                    echo 'Tk ' . $shipping_charge;
                                } else {
                                    if ($shipping_charge == 70) {
                                        echo 'Tk ' . $shipping_charge;
                                    } else {
                                        echo 'Tk ' . $shipping_charge;
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            
                            
                            
                            <?php 
         
                            if($shipping_address_details->used_promo==1){ ?>
                            <th>Total(Promo):</th>
                            <td>Tk <?php echo $totalamount=$shipping_address_details->shoppingcart_total; ?></td>
                            <?php }elseif($shipping_address_details->get_offer==1){ ?>
                                <th>Total (10% Discount) :</th>
                                <td>Tk <?php $totalamount=$subtotal + $shipping_charge; $discount=$totalamount*10/100; echo $totalamount=$totalamount-$discount; ?></td>
                            <?php }else{ ?>
                            <th>Total:</th>
                            <td>Tk <?php echo $totalamount=$subtotal + $shipping_charge; ?></td>
                            <?php } ?>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <form name="singleform" action="{{url('/update_order_status')}}" method="POST">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <input type="hidden" name="conforder_id" value="<?php echo $shipping_address_details->conforder_id; ?>" />
                    <input type="hidden" name="total_amount" value="<?php echo $totalamount; ?>" />
                    <div class="form-group">
                        <label>Order Status:</label>
                        <div class="input-group">
                            <?php $orderstatus = $shipping_address_details->conforder_status; ?>
                            <select name="ddlstatus" class="form-control" id="order_status">
                                <option value="<?php echo $orderstatus ?>"><?php echo $orderstatus ?></option>
                                <option value="Invalidate">Invalidate</option>
                                <option value="Pending_Dispatch">Verified</option>
                                <option value="Dispatched">Confirm Dispatch</option>
                                <option value="Payment_Pending">Delivered</option>
                                <option value="Closed">Payment Received</option>
                                <option value="Bkash_Payment_Receive">Bkash Payment Received</option>
                                <option value="Cancelled">Cancelled</option>
                                <option value="Exchange_Pending">Exchange Pending</option>
                                <option value="Exchange_Dispatched">Exchange Dispatched</option>
                                <option value="Exchanged">Exchanged</option>
                                <option value="Returned">Returned</option>
                            </select>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <div class="form-group hidden" id="expenses_amount">
                        <label>Expense Amount</label>
                        <input type="number" class="form-control" name="expenses_amount" value="<?php if(isset($conforder->amount)) echo $conforder->amount; ?>"/>
                    </div>
            </div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group col-xs-6">
                    <label>3pl Status</label>
                    <div class="input-group"><?php $threepldlvshow = $shipping_address_details->order_threepldlv; ?>
                        <select name="ddlthreepldlv" id="TPL" class="form-control">
                            <option value="<?php echo $threepldlvshow; ?>">
                                <?php echo $threepldlvshow; ?>
                            </option>
							<option value="eCourier">eCourier</option>
                            <option value="Pathao">Pathao</option>
                            <option value="Bidduyt">Bidduyt</option>
                            <option value="Sundorbon">Sundorbon</option>
                            <option value="Hand Delivery">Hand Delivery</option>
                        </select> 
                    </div>
                </div>
                <div class="form-group col-xs-6">
                    <input type="hidden" name="tplname" id="TPLNAME" value="" />
                    <label>Delivery date</label>
                    <input type="date" class="form-control" name="delivery_date" id="delivery_date" value="<?php echo isset($shipping_address_details->conforder_deliverydate)?date('Y-m-d',strtotime($shipping_address_details->conforder_deliverydate)):''; ?>"/>
                </div>
                <div class="form-group col-md-12 col-xs-12">
                <div class="input-group hidden" id="delivered_person">
                    <label>Delivered by</label>
                    <select name="delivered_by" class="form-control" id="delivered_by">
                        <option value="">--Select--</option>
                        @foreach($riders as $rider)
                        <option value="{{ $rider->id }}" <?php if(isset($delivery_by->delivery_by) && ($rider->id == $delivery_by->delivery_by)) echo 'selected'; ?>>{{ $rider->name }}</option>
                        @endforeach
                        
                    </select>
                  </div>
                 </div>   
            </div>
            <div class="col-md-4 col-xs-4">
                <div class="form-group">
                    <label>Order Status(Notes Optional):</label>
                        <textarea name="orderstatusnote" class="form-control"><?php echo $shipping_address_details->conforder_statusdetails;?></textarea><br />
                    <!-- /.input group -->
                </div>
                <div class="form-group">
                    <label>Admin comment for this user:</label>
                        <textarea name="admin_comment" class="form-control">{{ $shipping_address_details->admin_comment or '' }}</textarea>
                        <input type="hidden" name="registeruser_id" value="{{ $shipping_address_details->registeruser_id or '' }}" />
                </div>
            </div>
        </div>
        <div class="row no-print">
            <div class="col-xs-12">
                 <button type="submit" class="btn bg-navy btn-flat margin pull-left">Update</button>
        </form>
                <a href="{{url("/invoice-print/{$order_id}")}}" target="_blank" class="btn btn-default pull-right">Generate Print</a> 
                <!--                <button type="button" class="btn btn-success pull-right">Submit Payment</button> 
                                <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">Generate PDF</button></div>-->
            </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>

<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li>
            <a href="#control-sidebar-home-tab" data-toggle="tab"></a>
        </li>
        <li>
            <a href="#control-sidebar-settings-tab" data-toggle="tab"></a>
        </li>
    </ul>
</aside>
<?php
$conforder_id = $shipping_address_details->conforder_id;
?>
<script>
    var confoorderid =<?php echo $conforder_id; ?>;
    $(document).ready(function () {
        var total_loop = $("#total_loop").val();
        var update_total_loop = $("#update_total_loop").val();
        for (var m = 1; m <= total_loop; m++) {
            $('#updatechange' + '_' + m).on('click', function () {
                var rownowithid = $(this).attr('id');
                var rowno = rownowithid.split('_');
                var field_no = rowno[1];
            });
        }

        for (var j = 1; j <= update_total_loop; j++) {
            $('#btnexchange' + '_' + j).on('click', function () {
                var rownowithid = $(this).attr('id');
                var rowno = rownowithid.split('_');
                var field_no = rowno[1];
                $("#ViewTable").hide();
                $("#EditTable").show();
                $("#rowId_" + field_no).show();
                //   alert(pre_Qty);
                rowidpass(field_no);
            });
        }
        function rowidpass(field_no) {
            $('#SizeId' + '_' + field_no).on('change', function () {
                var pre_color = $("#ColorId_" + field_no + " option:selected").val();
                var pre_Size_ = $("#SizeId_" + field_no + " option:selected").val();
                var pre_Qty = $("#QtyId_" + field_no + " option:selected").val();
                var product_id = $("#product_id_" + field_no).val();
                //  alert(product_id);
                getQtyByColorSize(product_id, pre_color, pre_Size_, field_no)
            });

            $('#ColorId' + '_' + field_no).on('change', function () {
                var pre_color = $("#ColorId_" + field_no + " option:selected").val();
                var product_id = $("#product_id_" + field_no).val();
                getSizeByColor(product_id, pre_color, field_no);
                //  alert(product_id)
            });
        }

        function  getQtyByColorSize(productid, ProColor, ProSize, field_no) {
            var url_op = base_url + "/ajaxcall-getQuantityByColor/" + productid + '/' + ProSize + '/' + ProColor;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                    var qty = html[0].SizeWiseQty;
                    if (qty >= 1) {
                        $('#QtyId_' + field_no).find('option').remove();
                        for (var v = 1; v <= qty; v++) {
                            $('#QtyId_' + field_no).append('<option value="' + v + '">' + v + '</option>');
                        }
                    } else {
                        $('#QtyId_' + field_no).find('option').remove();
                        $('#QtyId_' + field_no).append('<option value="' + 0 + '">' + 0 + '</option>');
                    }
                }
            });
        }

        function getSizeByColor(product_id, color_name, field_no) {
            var url_op = base_url + "/getSizeByColor/" + product_id + '/' + color_name;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data)
                {
                    //  alert(data);
                    $("#SizeId_" + field_no).empty();
                    $("#SizeId_" + field_no).append('<option value="">Select Size</option>');
                    $.each(data, function (index, subcatobj) {
                        $("#SizeId_" + field_no).append('<option value="' + subcatobj.productsize_size + '">' + subcatobj.productsize_size + '</option>');
                    });
                }
            });
        }

        for (var i = 1; i <= total_loop; i++) {
            $('#cancel' + '_' + i).on('click', function () {
                var rownowithid = $(this).attr('id');

                var rowno = rownowithid.split('_');
                var field_no = rowno[1];

                var ProSize = $('#ProSize' + '_' + rowno[1]).val();
                var ProQty = $('#ProQty' + '_' + rowno[1]).val();
                var ProColor = $('#Color' + '_' + rowno[1]).val();
                var productid = $('#product_id' + '_' + rowno[1]).val();
                var shoppinproduct_id = $('#shoppinproduct_id' + '_' + rowno[1]).val();
                var url_op = base_url + "/cancel_indivisual_product_qty/" + productid + "/" + ProColor + "/" + ProSize + "/" + ProQty + "/" + shoppinproduct_id;
                $.ajax({
                    url: url_op,
                    type: 'GET',
                    success: function (data) {
                        if (data == 1) {
                            location.reload();
                        } else {

                        }
                    }
                });

            });

        }


      /*  $("#order_status").on('change', function () {
            var order_status = $(this).val();
           // var url_op = base_url + "/update_order_status/" + confoorderid + "/" + order_status;
            $.ajax({
                url: url_op,
                type: 'GET',
                success: function (data) {
               //     location.reload();
                }
            });
        });
        $("#TPL").on('change', function () {
            var threeplselectedid = $(this).val();
            // alert(threeplselectedid);
        //    var url = base_url + "/update_threepl_status/" + confoorderid + "/" + threeplselectedid;
            $.ajax({
                url: url,
                type: 'GET',
                success: function (data) {
                    //   alert(data);
                 //   location.reload();
                }
            });
        }); */
        $("#cancel").on("click", function () {
            var shoppinproduct_id = $("#shoppinproduct_id").val();
            alert(shoppinproduct_id);

        });
        var arr = [];
        $('#ColorId_1 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_2 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_3 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_4 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_5 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_6 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_7 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });

    });
</script>

<script>
    /*
        Showing delivered_by dropdown
    */
    (function() {
        document.getElementById('TPL').onchange = showRiders;
        document.getElementById('order_status').onchange = expensesAmount;
        if(document.getElementById('TPL').value == 'Hand Delivery') {
            document.getElementById('delivered_person').classList.remove('hidden');
            document.getElementById('delivered_person').classList.add('show');
        }
        if(document.getElementById('order_status').value == 'Closed') {
            document.getElementById('expenses_amount').classList.remove('hidden');
            document.getElementById('expenses_amount').classList.add('show');
        }
    })();
    function showRiders() {
        if(document.getElementById('TPL').value == 'Hand Delivery') {
            document.getElementById('delivered_person').classList.remove('hidden');
            document.getElementById('delivered_person').classList.add('show');
        } else {
            document.getElementById('delivered_person').classList.remove('show');
            document.getElementById('delivered_person').classList.add('hidden');
        }
    }
    function expensesAmount() {
        if(document.getElementById('order_status').value == 'Closed') {
            document.getElementById('expenses_amount').classList.remove('hidden');
            document.getElementById('expenses_amount').classList.add('show');
        } else {
            document.getElementById('expenses_amount').classList.remove('show');
            document.getElementById('expenses_amount').classList.add('hidden');
        }
    }
</script>
<script>
    /*
        Showing delivery route dropdown
    */
    (function() {
        $('#show-route-form').click(function() {
            showRouteForm();
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
         $('#save-route-form').click(function() {
            var data = {
                route_city: $('#route-city').val(),
                shoppingcart_id: {{ $shipping_address_details->shoppingcart_id }},
            };
            $.ajax({
                type: 'POST',
                url: base_url+"/update-city-in-product-details",
                data: data,
                success: function(d) {
                    var route_city_element = document.getElementById('route-city');
                    var selectedText = route_city_element.options[route_city_element.selectedIndex].text;
                    document.getElementById('route-text-default').innerHTML =selectedText;
                    hideRouteForm();
                },
                error:function(e) {
                }
            });
        });
        function showRouteForm() {
            document.getElementById('route-text').classList.remove('show');
            document.getElementById('route-text').classList.add('hidden');
            document.getElementById('route-form').classList.remove('hidden');
            document.getElementById('route-form').classList.add('show');
        }
        function hideRouteForm() {
            document.getElementById('route-text').classList.remove('hidden');
            document.getElementById('route-text').classList.add('show');
            document.getElementById('route-form').classList.remove('show');
            document.getElementById('route-form').classList.add('hidden');
        }
    })();
</script>
<script>
    $(document).ready(function () {
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $('select#route-region').on('change', function () {
            var RegionId = this.value;
            var url_op = base_url + "/citylist/" + RegionId;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    $('#route-city').empty();
                    $('#route-city').append('<option value="">--- Select Region ---</option>');
                    $.each(data, function (index, cityobj) {
                        $('#route-city').append('<option value="' + cityobj.CityId + '">' + cityobj.CityName + '</option>');
                    });
                }
            });
        });
    });
</script>
@endsection