@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<!-- Content Header (Page header) -->
<style>
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
</style>
<section class="content-header">
    <h1>
        Dashboard 
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{$total_incomplete_order}}</h3>
                    <h4>Incomplete Orders</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="{{url('/pride-admin/manage-incomplete-order')}}" class="small-box-footer">Go to incomplete orders page<i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$totalorder}}</h3>
                    <h4>Total Orders</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$totalregisteruser}}</h3>
                    <h4>User Registrations</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $total_sale_product }}</h3>
                    <h4>Total sale</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$totalcompleteorder}}</h3>
                    <h4>Complete Orders</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="" class="small-box-footer">Go to incomplete orders page<i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box small-box bg-red">
                <div class="inner">
                    <h3>{{number_format($totalbouncedorderrate, 2) }}
                        <sup style="font-size: 20px">%</sup>
                    </h3>
                    <h4>Bounce Rate</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$totalbounceorder}}</h3>

                    <h4>Total Bounce Orders</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{$total_qty - $total_sale_product}}</h3>
                    <h4>Remaining Stock</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->


    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Orders</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped dataTable no-footer">
                            <thead>
                                <tr>
                                    <th width="5%">SL</th>
                                    <th>Customer Name</th>
                                    <th>Order NO</th>                        	
                                    <th>Order Placed</th>                                                        
                                    <th width="15%">Order Status</th> 
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                ?>
                                @foreach($total_incomplete_order_info as $orderinfo)
                                <tr>                        	
                                    <td  width="5%">{{$i++}}</td>
                                    <td>{{$orderinfo->Shipping_txtfirstname}} {{$orderinfo->Shipping_txtlastname}}</td>
                                      <td>
                                        <a href='{{url("/pride-admin/order-details/{$orderinfo->conforder_id}")}}' target="__blank" class="info">{{$orderinfo->conforder_tracknumber}}</a>
                                    </td>
                                    <td>
                                        <?php $date=strtotime($orderinfo->conforder_placed_date); echo date('l, M d, Y',$date);?>
                                    </td>																		
                                    <td>
                                        <?php if ($orderinfo->conforder_status == "Order_Verification_Pending") { ?>
                                            <span class="label label-danger" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Pending_Dispatch") {
                                            ?>
                                            <span class="label label-warning" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Dispatch") {
                                            ?>
                                            <span class="label label-success" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                            <?php
                                        } else if($orderinfo->conforder_status == "Invalidate" || $orderinfo->conforder_status == "Cancelled"){ ?>
                                            <span class="label label-danger" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                    <?php }elseif($orderinfo->conforder_status == "Bkash_Payment_Receive"){ ?>
                                     <span class="label label-success" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                    <?php  }else{  ?>
                                       <span class="label label-default" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                    <?php } ?>
                                        </td>
                                        <td><a href='{{url("/invoice-print/{$orderinfo->conforder_id}")}}' target="__blank" class="btn btn-default btn-sm">Print Invoice</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="{{url('/pride-admin/manage-incomplete-order')}}" class="btn btn-sm btn-info btn-flat pull-left">Go to incomplete order page</a>
                    <a href="{{url('/pride-admin/manage-all-order')}}" class="btn btn-sm btn-default btn-flat pull-right">Go to all orders page</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-7 connectedSortable">
                <div class="form-group">
                    <label>Select Year</label>
                   <select id="SelectedYear" class="form-control" style="width:50%;">
                      <?php
                         for($i=date("Y");$i>=2017;$i--){?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option> 
                        <?php }
                       ?>
                   </select>
                 </div> 
                    &nbsp;
                    <!-- Custom tabs (Charts with tabs)-->
                    <div id="linechart_container"></div>
                    <button class="btn btn-sm btn-info btn-flat margin" id="plain">Plain</button>
                    <button class="btn btn-sm btn-info btn-flat margin" id="inverted">Inverted</button>
                   <!-- <button id="polar">Polar</button>-->
                    <!-- /.nav-tabs-custom -->
                </div>
                <div class="col-md-5 connectedSortable" style="padding-top: 38px;">
                    <div class="form-group">
                    <label></label>
                   
                 </div> 
                  &nbsp;&nbsp;
                    
                    
                     <div class="box box-default">
                <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                    
                    
                    <!-- solid sales graph -->
                   <!-- <div class="box box-solid bg-teal-gradient">
                        <div class="box-header">
                            <i class="fa fa-th"></i>

                            <h3 class="box-title">Sales Graph</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body border-radius-none">
                            <div class="chart" id="line-chart" style="height: 250px;"></div>
                        </div>
                        <div class="box-footer no-border">
                            <div class="row">
                                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                    <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60"
                                           data-fgColor="#39CCCC">

                                    <div class="knob-label">Mail-Orders</div>
                                </div>
                                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                    <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60"
                                           data-fgColor="#39CCCC">

                                    <div class="knob-label">Online</div>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60"
                                           data-fgColor="#39CCCC">

                                    <div class="knob-label">In-Store</div>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
   <!-- <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Monthly Sales Report</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <div class="btn-group">
                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-wrench"></i></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <p class="text-center">
                                <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                            </p>

                            <div class="chart">
                                <canvas id="salesChart" style="height: 180px;"></canvas>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p class="text-center">
                                <strong>Goal Completion</strong>
                            </p>

                            <div class="progress-group">
                                <span class="progress-text">Add Products to Cart</span>
                                <span class="progress-number"><b>160</b>/200</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                </div>
                            </div>
                            <div class="progress-group">
                                <span class="progress-text">Complete Purchase</span>
                                <span class="progress-number"><b>310</b>/400</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                                </div>
                            </div>
                            <div class="progress-group">
                                <span class="progress-text">Visit Premium Page</span>
                                <span class="progress-number"><b>480</b>/800</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                                </div>
                            </div>
                            <div class="progress-group">
                                <span class="progress-text">Send Inquiries</span>
                                <span class="progress-number"><b>250</b>/500</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                                <h5 class="description-header">$35,210.43</h5>
                                <span class="description-text">TOTAL REVENUE</span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                                <h5 class="description-header">$10,390.90</h5>
                                <span class="description-text">TOTAL COST</span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                                <h5 class="description-header">$24,813.53</h5>
                                <span class="description-text">TOTAL PROFIT</span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block">
                                <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                                <h5 class="description-header">1200</h5>
                                <span class="description-text">GOAL COMPLETIONS</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 
    <div class="row">
        <div class="col-md-8">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Visitors Report</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- 
                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
                            <div class="pad">
                                <div id="world-map-markers" style="height: 325px;"></div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <div class="pad box-pane-right bg-green" style="min-height: 280px">
                                <div class="description-block margin-bottom">
                                    <div class="sparkbar pad" data-color="#fff">90,70,90,70,75,80,70</div>
                                    <h5 class="description-header">8390</h5>
                                    <span class="description-text">Visits</span>
                                </div>
                                <div class="description-block margin-bottom">
                                    <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                                    <h5 class="description-header">30%</h5>
                                    <span class="description-text">Referrals</span>
                                </div>
                                <div class="description-block">
                                    <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                                    <h5 class="description-header">70%</h5>
                                    <span class="description-text">Organic</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Recently Added Products</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <!--
                        <div class="box-body">
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                    <div class="product-img">
                                        <img src="resource/images/logo.png" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title">Samsung TV
                                            <span class="label label-warning pull-right">$1800</span></a>
                                        <span class="product-description">
                                            Samsung 32" 1080p 60Hz LED Smart HDTV.
                                        </span>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="product-img">
                                        <img src="resource/images/logo.png" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title">Bicycle
                                            <span class="label label-info pull-right">$700</span></a>
                                        <span class="product-description">
                                            26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                                        </span>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="product-img">
                                        <img src="resource/images/logo.png" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title">Xbox One <span class="label label-danger pull-right">$350</span></a>
                                        <span class="product-description">
                                            Xbox One Console Bundle with Halo Master Chief Collection.
                                        </span>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="product-img">
                                        <img src="resource/images/logo.png" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title">PlayStation 4
                                            <span class="label label-success pull-right">$399</span></a>
                                        <span class="product-description">
                                            PlayStation 4 500GB Console (PS4)
                                        </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="box-footer text-center">
                            <a href="javascript:void(0)" class="uppercase">View All Products</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- 

        <div class="col-md-4">
            <!-- 
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Inventory</span>
                    <span class="info-box-number">5,200</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        50% Increase in 30 Days
                    </span>
                </div>
            </div>
            <!-- 
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Mentions</span>
                    <span class="info-box-number">92,050</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 20%"></div>
                    </div>
                    <span class="progress-description">
                        20% Increase in 30 Days
                    </span>
                </div>
            </div>
            <!--
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Downloads</span>
                    <span class="info-box-number">114,381</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
                        70% Increase in 30 Days
                    </span>
                </div>
            </div>
            <!-- 
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Direct Messages</span>
                    <span class="info-box-number">163,921</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 40%"></div>
                    </div>
                    <span class="progress-description">
                        40% Increase in 30 Days
                    </span>
                </div>
            </div>
            <!-- /.info-box -->

     <!-- <div class="box box-default">
                <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>-->
                <script>
                    $(document).ready(function () {
                        
                        
                        
                    /*    var chart = Highcharts.chart('linechart_container', {
                            title: {
                                text: 'Pride Limited'
                            },
                            subtitle: {
                                text: 'Monthly Sales Graph'
                            },
                            xAxis: {
                                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                            },
                            series: [{
                                    type: 'column',
                                    colorByPoint: true,
                                    data: [100000, 50000, 60000, 30000, 45000, 90000, 75000, 55000, 44000, 68000, 69000, 88000],
                                    showInLegend: false
                                }]

                        });


                        $('#plain').click(function () {
                            chart.update({
                                chart: {
                                    inverted: false,
                                    polar: false
                                },
                                subtitle: {
                                    text: 'Plain'
                                }
                            });
                        });

                        $('#inverted').click(function () {
                            chart.update({
                                chart: {
                                    inverted: true,
                                    polar: false
                                },
                                subtitle: {
                                    text: 'Inverted'
                                }
                            });
                        });

                        $('#polar').click(function () {
                            chart.update({
                                chart: {
                                    inverted: false,
                                    polar: true
                                },
                                subtitle: {
                                    text: 'Polar'
                                }
                            });
                        });*/
                   
                     var url_op = base_url + "/pride-admin/gebrowserdata";
                        $.ajax({
                            url: url_op,
                            type: 'GET',
                            dataType: 'json',
                            data: '',
                            success: function (response)
                            {
                                browserData(response);
                            }
                        });
                        
                       
                    var sdyear =   $("#SelectedYear option:selected").val(); 
                    $("#SelectedYear").change(function(){
                     sdyear =   $("#SelectedYear").val(); 
                        var url_op1 = base_url + "/pride-admin/getmonthlysalesdata/"+sdyear;
                        $.ajax({
                            url: url_op1,
                            type: 'GET',
                            dataType: 'json',
                            data: '',
                            success: function (response)
                            {
                               monthlysalesData(response,sdyear);
                            }
                        }); 
                     });
                     var url_op1 = base_url + "/pride-admin/getmonthlysalesdata/"+sdyear;
                        $.ajax({
                            url: url_op1,
                            type: 'GET',
                            dataType: 'json',
                            data: '',
                            success: function (response)
                            {
                               // alert(response);
                               monthlysalesData(response,sdyear);
                            }
                        }); 
                        
                        
                        
                        
                    });
                    
   

                    function browserData(response) {
                        var pieColors = (function () {
                            var colors = [],
                                    base = Highcharts.getOptions().colors[0],
                                    i;
                            for (i = 0; i < 10; i += 1) {
                                colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                            }
                            return colors;
                        }());
                        Highcharts.chart('container', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: 'Browsers use in Pride Limited website, 2018'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    colors: pieColors,
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                                        distance: -50,
                                        filter: {
                                            property: 'percentage',
                                            operator: '>',
                                            value: 4
                                        }
                                    }
                                }
                            },
                            series: [{
                                    name: 'Browser',
                                    data: response
                                }]
                        });

                    }
                    
                    
                    
                    function  monthlysalesData(response,sdyear){

                        var chart = Highcharts.chart('linechart_container', {
                            title: {
                                text: 'Pride Limited'
                            },
                            subtitle: {
                                text: 'Monthly Sales Graph:' + sdyear
                            },
                            xAxis: {
                                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','July','Aug','Sep','Oct','Nov','Dec']
                            },
                            series: [{
                                    type: 'column',
                                    colorByPoint: true,
                                    data:  $.parseJSON(response),
                                    showInLegend: false
                                }]

                        });


                        $('#plain').click(function () {
                            chart.update({
                                chart: {
                                    inverted: false,
                                    polar: false
                                },
                                subtitle: {
                                    text: 'Plain'
                                }
                            });
                        });

                        $('#inverted').click(function () {
                            chart.update({
                                chart: {
                                    inverted: true,
                                    polar: false
                                },
                                subtitle: {
                                    text: 'Inverted'
                                }
                            });
                        });

                        $('#polar').click(function () {
                            chart.update({
                                chart: {
                                    inverted: false,
                                    polar: true
                                },
                                subtitle: {
                                    text: 'Polar'
                                }
                            });
                        });
                      
                     }
                </script>               
          <!--  </div>-->
            <!-- /.box -->
            <!-- PRODUCT LIST -->
          <!--  <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Members</h3>

                    <div class="box-tools pull-right">
                        <span class="label label-danger">8 New Members</span>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="users-list clearfix">
                        <li>
                            <img src="resource/images/logo.png" alt="User Image">
                            <a class="users-list-name" href="#">Alexander Pierce</a>
                            <span class="users-list-date">Today</span>
                        </li>
                        <li>
                            <img src="resource/images/logo.png" alt="User Image">
                            <a class="users-list-name" href="#">Norman</a>
                            <span class="users-list-date">Yesterday</span>
                        </li>
                        <li>
                            <img src="resource/images/logo.png" alt="User Image">
                            <a class="users-list-name" href="#">Jane</a>
                            <span class="users-list-date">12 Jan</span>
                        </li>
                        <li>
                            <img src="resource/images/logo.png" alt="User Image">
                            <a class="users-list-name" href="#">John</a>
                            <span class="users-list-date">12 Jan</span>
                        </li>
                        <li>
                            <img src="resource/images/logo.png" alt="User Image">
                            <a class="users-list-name" href="#">Alexander</a>
                            <span class="users-list-date">13 Jan</span>
                        </li>
                        <li>
                            <img src="resource/images/logo.png" alt="User Image">
                            <a class="users-list-name" href="#">Sarah</a>
                            <span class="users-list-date">14 Jan</span>
                        </li>
                        <li>
                            <img src="resource/images/logo.png" alt="User Image">
                            <a class="users-list-name" href="#">Nora</a>
                            <span class="users-list-date">15 Jan</span>
                        </li>
                        <li>
                            <img src="resource/images/logo.png" alt="User Image">
                            <a class="users-list-name" href="#">Nadia</a>
                            <span class="users-list-date">15 Jan</span>
                        </li>
                    </ul>
                </div> 
                <div class="box-footer text-center">
                    <a href="javascript:void(0)" class="uppercase">View All Users</a>
                </div> -->
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
