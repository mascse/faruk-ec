@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<style>
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Incomplete Order(s)</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped dataTable no-footer">
                            <thead>
                                <tr>
                                    <th width="5%" style="color:black;">SL</th>
                                    <th style="color:black;">Customer Name</th>
                                    <th style="color:black;">Order NO#</th>                        	
                                    <th style="color:black;">Order Placed</th>                                                        
                                    <th width="25%" style="color:black;">Order Status</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                ?>
                                @foreach($total_incomplete_order_info as $orderinfo)
                                <tr>                        	
                                    <td style="color:black" width="5%">{{$i++}}</td>
                                    <td style="color:black">{{$orderinfo->registeruser_firstname}} {{$orderinfo->registeruser_lastname}}</td>
                                    <td style="color:black">
                                        <a href="{{url("/pride-admin/order-details/{$orderinfo->conforder_id}")}}" target="_self" class="info">{{$orderinfo->conforder_tracknumber}}</a>
                                    </td>
                                    <td style="color:black">
                                         <?php $date=strtotime($orderinfo->conforder_placed_date); echo date('l, M d, Y',$date);?>
                                    </td>																		
                                    <td>
                                        <?php if ($orderinfo->conforder_status == "Order_Verification_Pending") { ?>
                                            <span class="label label-primary" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Pending_Dispatch") {
                                            ?>
                                            <span class="label label-warning" style=""><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Dispatch") {
                                            ?>
                                            <span class="label label-success" style=""><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                            <?php
                                        } else if($orderinfo->conforder_status == "Invalidate" || $orderinfo->conforder_status == "Cancelled"){ ?>
                                            <span class="label label-danger" style=""><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php     }else{  ?>
                                           <span class="label label-default" style=""><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } ?>
										</td>
										<td><a href='{{url("/invoice-print/{$orderinfo->conforder_id}")}}' target="__blank" class="btn btn-default btn-sm">Print Invoice</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    <div class="row">
                        <div class="col-sm-12">
                            <style>
                                .pagination {
                                    display: inline-block;
                                    padding-left: 15px;
                                    margin: 20px 0;
                                    border-radius: 4px;
                                }
                            </style>
                            <div class="font-alt">                         
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Go to incomplete order page</a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">Go to all orders page</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
    @endsection


