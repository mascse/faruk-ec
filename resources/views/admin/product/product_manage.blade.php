@extends('admin.layouts.app')
@section('title', 'Product Add Form')
@section('content')
<section class="content-header">
    <h1>
        Product 
        <small>Manage Product</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Manage Product</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Browse All Product </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <center>
                        @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('delete'))
                        <div class="alert alert-danger">
                            {{ session('delete') }}
                        </div>
                        @endif
                    </center>
                    <form name="singleform" action="{{url('/product-manage')}}" method="GET">
                        <div class="form-group" style="width:50%;">
                            <label>Select Category</label>
                            <div class="input-group">
                                <select name="scid" id="scid"  class="form-control select2" style="width: 100%;">
                                    <?php
                                    foreach ($sub_category as $sub) {
                                        ?>
                                        <option  value="<?php echo $sub->subprocat_id; ?>"><?php echo "(" . $sub->procat_name . ") " . $sub->subprocat_name; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <div class="input-group-addon">
                                    <input style="background-color: white; border: none;" type="submit" name="btnsubmit" value="Search"  /> 
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                    </form>    
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Name</th>
                                <!--<th>Style Code</th> --->
                                <th>Category - Sub Category</th>
                                <th style="text-align:center;">Image</th>
                                <th style="text-align:center;">Manage</th>
                                <th style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($product_list as $product) {
                                $i++;
                                ?>
                                <tr>
                                    <td style="width:1%;text-align:center;"><?php echo $i; ?></td>
                                    <td style="width:15%;"><?php echo $product->product_name; ?></td>
                                    <td style="width:15%;"><?php echo $product->procat_name . " - " . $product->subprocat_name; ?></td>
                                    <?php if ($product->product_active_deactive == 0) { ?>
                                        <td style="width:10%;text-align:center;">
                                            <img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_tiny; ?>" alt=""  />
                                        </td>
                                        <td>
                                            <a class="btn bg-navy btn-flat btn-sm margin tdata" href="{{url("product-album-manage/{$product->product_id}/{$product->subprocat_id}")}}">Manage Product Gallery</a>                                             
                                            <a class="btn btn-danger btn-flat btn-sm margin tdata" href="{{url("product-deactive-active/{$product->procat_id}/{$product->subprocat_id}/{$product->product_id}/{$product->product_active_deactive}")}}">Deactive</a>
                                        </td>
                                        <td>
                                            <a class="btn bg-olive btn-flat btn-sm margin tdata" href="{{url("/edit-product/{$product->product_id}")}}">Edit</a> 
                                            <a class="btn btn-danger btn-flat btn-sm margin tdata" href="#" onClick="confirm_delete('{{url("/product-in-trash/{$product->product_id}/{$product->subprocat_id}")}}')">Delete</a>
                                            <a class="btn bg-orange btn-flat btn-sm margin tdata" href="{{url("/add-product-style/{$product->product_id}")}}">Add More</a>
											<a class="btn bg-navy btn-flat btn-sm margin tdata" href="{{url("/add-related-product/{$product->product_id}")}}">Add Related Product</a>
                                        </td>
                                    <?php } else { ?>
                                        <td><img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_tiny; ?>" /></td>
                                        <td>
                                            <a class="btn bg-navy btn-flat btn-sm margin tdata" href="{{url("product-album-manage/{$product->product_id}/{$product->subprocat_id}")}}">Manage Product Gallery</a> 
                                            <a class="btn bg-olive btn-flat btn-sm margintdata" href="{{url("product-deactive-active/{$product->procat_id}/{$product->subprocat_id}/{$product->product_id}/{$product->product_active_deactive}")}}">Active</a>
                                        </td>
                                        <td>
                                            <a  class="btn bg-olive btn-flat btn-sm margin tdata" href="{{url("/edit-product/{$product->product_id}")}}">Edit Product</a> 
                                            <a class="btn btn-danger btn-flat btn-sm margin tdata" href="#" onClick="confirm_delete({{url("/product-in-trash/{$product->product_id}/{$product->subprocat_id}")}})">Delete</a>
                                        </td>
                                    <?php }
                                    ?>
                                </tr>
                            <?php } ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>SL</th>
                                <th>Name</th>
                                <th>Category - Sub Category</th>
                                <th style="text-align:center;">Image</th>
                                <th style="text-align:center;">Manage</th>
                                <th style="text-align:center;">Action</th>
                            </tr>
                        </tfoot> 
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this product ?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
    document.getElementById("scid").value = "<?php echo $sub_pro_selected; ?>";
</script>
<!--- Ajax modal end ---->
@endsection