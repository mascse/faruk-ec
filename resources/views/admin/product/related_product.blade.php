@extends('admin.layouts.app')
@section('title', 'Product Add Form')
@section('content')
<style>
    input:invalid {
        border-color: #00c0ef;
    }
    .osh-msg-box.-success {
        border-color: #00a65a;
        color: #00a65a;
    }
    .osh-msg-box.-danger {
        border-color: red;
        color: red;
    }
    .osh-msg-box {
        border-width: 1px;
        border-style: solid;
        padding: 2px 5px;
        margin: 10px 0;
    }
    @-webkit-keyframes blinker {
        from {opacity: 1.0;}
        to {opacity: 0.0;}
    }
    .blink{
        color: red;
        font-weight:bold;
        text-decoration: blink;
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 0.8s;
        -webkit-animation-iteration-count:infinite;
        -webkit-animation-timing-function:ease-in-out;
        -webkit-animation-direction: alternate;
    }
</style>
<section class="content-header">
    <h1>
        Product
        <small>Related</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li>Add</li>
		<li class="active">Related</li>
    </ol>
</section>
<form name="add_product" id="myform" action="{{url('/related-product-save')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
	<input type="hidden" name="product_id" value="<?php echo $product_info->product_id;?>" />
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('save'))
                <center>
                    <div class=" osh-msg-box -success">{{session('save')}}
                    </div>
                </center>
                @endif    
                @if (session('error'))
                <center>
                    <div class=" osh-msg-box -danger">
                        <span class="blink">Error :: </span>&nbsp;&nbsp;
                        {{session('error')}}
                    </div>
                </center>
                @endif  
                <div class="panel panel-default">
                    <div class="panel-heading">Related Product</div>
                    <div class="panel-body">
					<div class="row">
					<div class="col-md-4">
					   <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $product_info->productimg_img_medium; ?>" class="img-responsive" style="height: 193px;border: 1px solid #eee;"/>
					</div>
					<div class="col-md-8">
						<table class="table table-striped">
							<tr>
								<th>Product Details</th>
								<th>&nbsp;</th>
							</tr>
							<tr>
								<td>Product Name : </td>
								<td><?php echo $product_info->product_name; ?></td>
							</tr>
							<tr>
								<td>Product Code : </td>
								<td><?php echo $product_info->product_styleref; ?></td>
							</tr>
							<tr>
								<td>Product Category : </td>
								<td> <?php echo $product_info->procat_name; ?></td>
							</tr>
							<tr>
								<td>Product Sub Category : </td>
								<td><?php echo $product_info->subprocat_name; ?></td>
							</tr>
							<tr>
								<td>Related Products: </td>
								<td>
								    <?php
								    if(count($related_product) > 0){
								    foreach($related_product as $r_product){ ?>
								       <img src="http://localhost/pride_new_v/storage/app/public/pgallery/{{$r_product->productimg_img}}" style="width: 100px;height: auto;border: 1px solid #eee;">
								    <?php }
								    }else{
								        echo "<span style='color:red;'>No related product selected</span>";
								    }?>
								</td>
							</tr>
						</table>
					  </div>
					  </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group {{ $errors->has('related_pro_id') ? ' has-error' : '' }}">
                                <label>Select Category</label>
                                <select type="text"  name="related_pro_id" id="related_pro_id" class="form-control select2" required>
                                    <option value="">-- select category --</option>
                                    @foreach($procat_list as $procat)
                                    <option value="{{$procat->procat_id}}">{{$procat->procat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group {{ $errors->has('related_sub_id') ? ' has-error' : '' }}">
                                <label>Select Sub Category</label>
                                <select type="text" name="related_sub_id" id="related_sub_id" class="form-control select2" required>
                                    <option value=""> -- select sub category -- </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group {{ $errors->has('related_sub_id') ? ' has-error' : '' }}">
                                <label>Related Product</label>
                                <div id="related_product"></div>
                            </div>
                        </div>
                        <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right" style="height: 3.5rem;">Add Related Product</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<link rel="stylesheet" href="{{asset('assets_admin/previewForm.css')}}">
<script src="{{asset('assets_admin/previewForm.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('assets_admin/dynamic_select_box.js')}}"></script> --->
<script>
$(document).ready(function () {
    $("#related_pro_id").change(function () {
        var rel_procate = $("#related_pro_id").val();
        //alert(rel_procate);
        var url_op = base_url + "/subpro-list/" + rel_procate;
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (data)
            {
                $("#related_sub_id").empty();
                $("#related_sub_id").append("<option value=''> -- Select Sub Category -- </option>");
                $.each(data, function (index, subpro_obj) {
                    $("#related_sub_id").append('<option value="' + subpro_obj.subprocat_id + '">' + subpro_obj.subprocat_name + '</option>');
                });

                $("#related_sub_id").change(function () {
                    var subpro_id = $("#related_sub_id").val();
                    //alert(subpro_id);
                    var url_op = base_url + "/get-product-by-subpro/" + subpro_id;
                    $.ajax({
                        url: url_op,
                        type: 'GET',
                        data: '',
                        success: function (data) {
                            //alert(data);
                            $("#related_product").empty();
                            $("#related_product").append(data);
                            //$.each(data, function (index, product_obj){
                            //	$("#related_product").append('<option value="' + product_obj.product_id + '" style="background-image:url("'+ base_url+'"/storage/app/public/pgallery/"'+product_obj.productimg_img+'")">'+product_obj.productimg_img+'"></option>');
                            //	});
                        }
                    });
                });
            }
        });
    });
});
</script>
@endsection