@extends('admin.layouts.app')
@section('title','Add Album')
@section('content')
<section class="content-header">
    <h1>
        Album
        <small>Add </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">ALBUM FORM</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <form name="add_album" action="{{url('/save-album')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <center>
                            @if (session('save'))
                            <div class="alert alert-success">
                                {{ session('save') }}
                            </div>
                            @endif
                        </center>
                        <center>
                            @if (session('error'))
                            <div class="alert alert-success">
                                {{ session('error') }}
                            </div>
                            @endif
                        </center>
                        <div class="form-group">
                            <label>Color Style Name</label>
                            <input type="text" class="form-control" name="txtsubcategoryname"/>
                        </div>
                        <div class="form-group">
                            <label>Color Style Order</label>
                            <input type="number" class="form-control" name="txtorder" min="1" max="100" value="100"/>
                            <span class="help-block" style="color:#f39c12;">Only Numbers</span>
                        </div>
                        <div class="form-group">
                            <label>Image(Thumbnail)</label>
                            <input id="input-upload-img1" type="file" class="file" name="filename" data-preview-file-type="text">
                            <span class="help-block" style="color:#f39c12;">only .jpg image is allowed Size (Width: 118px X Height: 143px)</span>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Add Album"/>
                </div>
            </div>
        </form>    
    </div>
</section>
@endsection