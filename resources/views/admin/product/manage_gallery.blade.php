@extends('admin.layouts.app')
@section('title', 'Product Gallery')
@section('content')
<section class="content-header">
    <h1>
        Manage Product
        <small>Gallery </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product</li>
        <li class="active">Manage Product Gallery</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Manage All Images for Product: '<?php echo $product_name->product_name; ?>' | Color Style:'<?php echo $album_name->productalbum_name; ?>'</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <center>
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                    </center>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th width="25%">Image Tag</th>
                                <th width="35%">Image</th>                            
                                <th>Image Order</th>                            
                                <th width="20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($product_img as $img) {
                                ?>                       
                                <!-- data -->
                                <tr>
                                    <td width="5%"><?php echo $i; ?></td>
                                    <td><?php echo $img->productimg_title; ?></td>                            
                                    <td><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $img->productimg_img_tiny; ?>" alt="Banner" /></td>							
                                    <td><?php echo $img->productimg_order; ?></td>
                                    <td width="20%">
                                        <a class="tdata" 
                                           href="{{url("/edit-album-image/{$img->productimg_id}/{$pid}/{$albumid}/{$i}")}}">Edit</a> 
                                        &nbsp; | &nbsp; 
                                        <a class="tdata" onclick="confirm_delete('{{url("/delete_album_image/{$albumid}/{$pid}/{$img->productimg_id}")}}')" href="#">Delete</a>
                                    </td>
                                </tr>
                                <!-- data -->
                                <?php
                                $i++;
                            } // end of while	
                            ?> 

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
@endsection