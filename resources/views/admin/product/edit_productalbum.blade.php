@extends('admin.layouts.app')
@section('title','Add Album')
@section('content')
<section class="content-header">
    <h1>
        Album
        <small>Add </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">ALBUM FORM</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <form name="add_album" action="{{url('/save-edit-swatch')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" class="form-control" name="productalbum_id" value="<?php echo $album_info->productalbum_id;?>"/>
            <input type="hidden" class="form-control" name="product_id" value="<?php echo $album_info->product_id;?>"/>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <center>
                            @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                            @endif
                        </center>
                        <center>
                            @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                            @endif
                        </center>
                        <div class="form-group">
                            <label>Productalbum Name</label>
                            <input type="text" class="form-control" name="productalbum_name" value="<?php echo $album_info->productalbum_name;?>" required/>
                        </div>
                        <div class="form-group">
                            <label>Productalbum Order</label>
                            <input type="number" class="form-control" name="productalbum_order" min="1" max="100" value="<?php echo $album_info->productalbum_order;?>"/>
                            <span class="help-block" style="color:#f39c12;">Only Numbers</span>
                        </div>
                        <div class="form-group">
                            <label>Current Image</label>
                            <div class="mb10">
                                <span class="file-input">
                                    <div class="file-preview">
                                        <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                        <div class="file-preview-thumbnails">
                                            <div class="file-preview-frame" id="preview">
                                                <img class="img-responsive" src="{{url('/')}}/storage/app/public/pgallery/<?php echo $album_info->productalbum_img; ?>" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>   
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Image(Thumbnail)</label>
                            <input id="input-upload-img1" type="file" class="file" name="filename" data-preview-file-type="text">
                            <span class="help-block" style="color:#f39c12;">only .jpg image is allowed Size (Width: 81px X Height: 62px)</span>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Update Swatch"/>
                </div>
            </div>
        </form>    
    </div>
</section>
@endsection