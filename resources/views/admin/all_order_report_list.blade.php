@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all"/>
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" media="all"/>
<style>
    fieldset {
    padding: .75em .825em .85em;
    margin: 0 2px;
    border: 1px solid silver;
}
fieldset {
    display: block;
    -webkit-margin-start: 2px;
    -webkit-margin-end: 2px;
    -webkit-padding-before: 0.35em;
    -webkit-padding-start: 0.75em;
    -webkit-padding-end: 0.75em;
    -webkit-padding-after: 0.625em;
    min-width: -webkit-min-content;
    border-width: 2px;
    border-style: groove;
    border-color: threedface;
    border-image: initial;
}
a:link, span.MsoHyperlink {
    mso-style-noshow: yes;
    mso-style-priority: 99;
    text-decoration: none !important;
    text-underline: single;
}

.margin_new {
    margin: 2px;
}
td, th {
    padding: 5px;
}
.text-design{
        color:red;
    }
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">All Order(s)</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div style="padding-bottom:15px;">
                         <fieldset><legend>Search Order</legend>
                                   <form name="filtering"  action="{{url('/search-order')}}" method="POST">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <table>
                							<tr>
                								<td class="field_label">
                									Form Date:
                								</td>
                								<td>
                								    <input type="date" name="form_date" id="date" value="<?php echo $form_date;?>" autocomplete="off" style="width:230px;"/>
                									(yyyy-m-d)&nbsp;
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                									To Date :
                								</td>
                								<td>
                									<input type="date" name="to_date" id="ToDate"  value="<?php echo $to_date;?>" autocomplete="off" style="width:230px;"/>
                									(yyyy-m-d)&nbsp;
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                									Tracking Number :
                								</td>
                								<td>
                									<input type="text" name="tracking_number"  id="TNO"  value="<?php echo $tracking_number;?>" autocomplete="off" style="width:230px;"/>
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                									Order Status :
                								</td>
                								<td>
                									<select name="order_status" id="OS" style="width:230px;">
                                                        <option value=""> -- select -- </option>
                                                        <option value="Invalidate">Invalidate</option>
                                                        <option value="Pending_Dispatch">Verified</option>
                                                        <option value="Dispatched">Confirm_Dispatch</option>
                                                        <option value="Payment_Pending">Delivered</option>
                                                        <option value="Closed">Payment_Received</option>
                                                        <option value="Cancelled">Cancelled</option>
                                                        <option value="Exchange_Pending">Exchange_Pending</option>
                                                        <option value="Exchange_Dispatched">Exchange_Dispatched</option>
                                                        <option value="Exchanged">Exchanged</option>
                                                        <option value="Returned">Returned</option>
                                                    </select>
                									&nbsp;
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                								</td>
                								<td>
                								
                								</td>
                							</tr>
                							<tr>
                								<td class="field_label">
                								</td>
                								<td>
                								   <input type="submit" value="Search"/>&nbsp;
                								   <input type="checkbox" name="match_type" value="1"/>Match all criteria
                								</td>
                							</tr>
                        			</table>
                        		</form>
                        </fieldset>
                    </div>
                    
                    <div class="col-md-12">
                        <?php if($form_date != ''){ ?><p style="font-weight: 500;color: #28d028;">Search form date <?php echo $form_date;?> to <?php echo $to_date;?></p><?php } ?>
                    </div>
                        <table id="example" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">SL</th>
                                    <th>Customer Name</th>
                                    <th>Order NO</th>                        	
                                    <th>Order Placed</th>                                                        
                                    <th width="25%">Order Status</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                ?>
                                @foreach($all_order_info as $orderinfo)
                                <tr>                        	
                                    <td width="5%">{{$i++}}</td>
                                    <td>{{$orderinfo->registeruser_firstname}} {{$orderinfo->registeruser_lastname}}</td>
                                    <td>
                                        <a href="{{url("/pride-admin/order-details/{$orderinfo->conforder_id}")}}" target="__blank" class="info">{{$orderinfo->conforder_tracknumber}}</a>
                                    </td>
                                    <td>
                                       <?php $date=strtotime($orderinfo->conforder_placed_date); echo date('l, M d, Y',$date);?>
                                    </td>																		
                                    <td>
                                        <?php if ($orderinfo->conforder_status == "Order_Verification_Pending") { ?>
                                            <span class="label label-primary" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Pending_Dispatch") {
                                            ?>
                                            <span class="label label-warning" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Dispatch") {
                                            ?>
                                            <span class="label label-success" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                            <?php
                                        } else if($orderinfo->conforder_status == "Invalidate" || $orderinfo->conforder_status == "Cancelled"){ ?>
                                            <span class="label label-danger" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php     }else if($orderinfo->conforder_status == "Closed"){  ?>
                                           <span class="label label-success" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php }else{ ?>
                                        <span class="label label-default" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } ?>
                                        </td>
                                        <td><?php echo number_format($orderinfo->shoppingcart_subtotal,2);?> Tk</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Go to incomplete order page</a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">Go to all orders page</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script>
        document.getElementById("OS").value="<?php echo $order_status;?>"
    </script>
    <script>
        $(document).ready(function() {
        $('#example1').DataTable();
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $( "#Date" ).datepicker({
           dateFormat: 'mm-dd-yy',
           changeMonth: true,
           changeYear: true
       });
    });
    </script>
    @endsection


