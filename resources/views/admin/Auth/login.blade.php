<!DOCTYPE html>
<html>
    <head>
        <title>Ecommerce| Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="{{asset('assets_admin/images/logo.png')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('assets_admin/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('assets_admin/login_asset/fonts/iconic/css/material-design-iconic-font.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets_admin/login_asset/vendor/animate/animate.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets_admin/login_asset/vendor/css-hamburgers/hamburgers.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets_admin/login_asset/vendor/animsition/css/animsition.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets_admin/login_asset/vendor/select2/select2.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets_admin/login_asset/vendor/daterangepicker/daterangepicker.css')}}">
        <!--custom css-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets_admin/login_asset/css/util.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets_admin/login_asset/css/main.css')}}">
        <style>
            .msg {
                font-family: Poppins-Regular;
                font-size: 15px;
                color: #ef5e5e;
                line-height: 1.2;
                display: block;
                width: 100%;
                height: 40px;
                background: transparent;
                padding: 0 5px;
            }
        </style>
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <form class="login100-form validate-form" name="loginform" method="post"  action="{{url('/pride-login-check')}}">
                        {{ csrf_field() }}
                        <span class="login100-form-title p-b-26">
                            ADMIN LOGIN
                        </span>
                        <span class="login100-form-title p-b-48">
                          <!--- <i class="zmdi zmdi-lock"></i>  --->
                            <center>
                                <img class="img-responsive" src="{{url('/')}}/storage/app/public/img/logo.png" height="50" width="120"/>
                            </center>
                        </span>
                        @if (session('error'))
                        <span class="msg">{{ session('error') }}</span>
                        @endif
                        <div  class="wrap-input100 validate-input" data-validate = "Enter user name">
                            <input class="input100" type="text" name="txtusername">
                            <span class="focus-input100" data-placeholder="User Name"></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <!---	<span class="btn-show-pass">
                                        <i class="zmdi zmdi-eye"></i>
                                </span> --->
                            <input class="input100" type="password" name="txtpassword" autocomplete="off">
                            <span class="focus-input100" data-placeholder="Password"></span>
                        </div>

                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <input class="button login100-form-btn" name="btnsubmit" type="submit" value="Login">
                            </div>
                        </div>

                        <div class="text-center p-t-115">

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="dropDownSelect1"></div>
        <script src="{{asset('assets_admin/login_asset/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assets_admin/login_asset/vendor/animsition/js/animsition.min.js')}}"></script>
        <script src="{{asset('assets_admin/login_asset/vendor/bootstrap/js/popper.js')}}"></script>
        <script src="{{asset('assets_admin/login_asset/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets_admin/login_asset/vendor/select2/select2.min.js')}}"></script>
        <script src="{{asset('assets_admin/login_asset/vendor/daterangepicker/moment.min.js')}}"></script>
        <script src="{{asset('assets_admin/login_asset/vendor/daterangepicker/daterangepicker.js')}}"></script>
        <script src="{{asset('assets_admin/login_asset/vendor/countdowntime/countdowntime.js')}}"></script>
        <script src="{{asset('assets_admin/login_asset/js/main.js')}}"></script>

    </body>
</html>