
<html>
    <head>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
</head>
    <body ng-app="crudApp" ng-controller="crudController" >
		
		<div class="container" ng-init="fetchData()">
			<br />
				<h3 align="center">AngularJS PHP CRUD (Create, Read, Update, Delete) using Bootstrap Modal</h3>
			<br />
			<div class="alert alert-success alert-dismissible" ng-show="success" >
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			</div>
			<div align="right">
				<button type="button" name="add_button" ng-click="addData()" class="btn btn-success">Add</button>
			</div>
			<br />
			<div class="table-responsive" style="overflow-x: unset;">
				<table datatable="ng" dt-options="vm.dtOptions" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="name in namesData">
							<td>{{name.first_name}}></td>
							<td>{{name.last_name}}></td>
							<td><button type="button" ng-click="fetchSingleData(name.id)" class="btn btn-warning btn-xs">Edit</button></td>
							<td><button type="button" ng-click="deleteData(name.id)" class="btn btn-danger btn-xs">Delete</button></td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>
	</body>
    
    
    
 
    
    
   <!-- <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Add User</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!--
        <div class="box-body">
            <div class="row">
                <div class="col-md-2"></div>
                <form action="<?php url('/create-admin-user')?>" method="post" enctype="multipart/form-data" >
                    <?php csrf_field() ?>
                    <div class="col-md-5">
                        @if(session('save'))
                        <div class="alert alert-success" role="alert">
                            <?php session('save') ?>
                        </div>
                        @endif
                        @if(session('error'))
                        <div class="alert alert-success" role="alert">
                            <?php session('error') ?>
                        </div>
                        @endif
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="first_name" value="<?php old('first_name') ?>" class="form-control">
                            <span class="text-danger"><?php $errors->first('first_name') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="<?php ('email') ?>" class="form-control">
                            <span class="text-danger"><?php $errors->first('email') ?></span>
                        </div>
                        <div class="form-group">
                            <label>User Name</label>
                            <input type="text" name="user_name" value="<?php old('user_name')?>" class="form-control">
                            <span class="text-danger"><?php $errors->first('user_name') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone_number" value="<?php old('user_name')?>" class="form-control">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea type="text" name="address" class="form-control" rows="3"></textarea>
                            <span class="text-danger"><?php $errors->first('address') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password"  class="form-control">
                            <span class="text-danger"><?php $errors->first('password') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="c_password" class="form-control">
                            <span class="text-danger"><?php $errors->first('c_password')?></span>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <select type="text" name="role_id" class="form-control">
                                <option value="">---- select role ----</option>
                                @foreach($role as $role_list)
                                <option value="<?php //$role_list->id ?>"> <?php //$role_list->role_name ?></option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Profile Image</label>
                            <input type="file" name="photo" class="form-control">
                            <span class="text-danger"><?php $errors->first('photo') ?></span>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
            </div>
        </div>
        <div class="box-footer">
            <input type="submit" name="btn" class="btn btn-success" value="Add User">
        </div>
        <form>*/-->

</html>
 
 
 <script>

var app = angular.module('crudApp');

app.controller('crudController', function($scope, $http){
    
     $scope.namesData = [
                            {id: '1', first_name: 'product1', last_name: 1000},
                            {id: '4', first_name: 'product4', last_name: 4000}

                        ];
	$scope.success = false;
	$scope.error = false;
	$scope.fetchData = function(){
		$http.get('fetch_data.php').success(function(data){
			$scope.namesData = data;
		});
	};

	$scope.openModal = function(){
		var modal_popup = angular.element('#crudmodal');
		modal_popup.modal('show');
	};

	$scope.closeModal = function(){
		var modal_popup = angular.element('#crudmodal');
		modal_popup.modal('hide');
	};

	$scope.addData = function(){
		$scope.modalTitle = 'Add Data';
		$scope.submit_button = 'Insert';
		$scope.openModal();
	};

	$scope.submitForm = function(){
		$http({
			method:"POST",
			url:"insert.php",
			data:{'first_name':$scope.first_name, 'last_name':$scope.last_name, 'action':$scope.submit_button, 'id':$scope.hidden_id}
		}).success(function(data){
			if(data.error != '')
			{
				$scope.success = false;
				$scope.error = true;
				$scope.errorMessage = data.error;
			}
			else
			{
				$scope.success = true;
				$scope.error = false;
				$scope.successMessage = data.message;
				$scope.form_data = {};
				$scope.closeModal();
				$scope.fetchData();
			}
		});
	};

	$scope.fetchSingleData = function(id){
		$http({
			method:"POST",
			url:"insert.php",
			data:{'id':id, 'action':'fetch_single_data'}
		}).success(function(data){
			$scope.first_name = data.first_name;
			$scope.last_name = data.last_name;
			$scope.hidden_id = id;
			$scope.modalTitle = 'Edit Data';
			$scope.submit_button = 'Edit';
			$scope.openModal();
		});
	};

	$scope.deleteData = function(id){ 
		if(confirm("Are you sure you want to remove it?"))
		{
			$http({
				method:"POST",
				url:"insert.php",
				data:{'id':id, 'action':'Delete'}
			}).success(function(data){
				$scope.success = true;
				$scope.error = false;
				$scope.successMessage = data.message;
				$scope.fetchData();
			});	
		}
	};

});

</script>    
