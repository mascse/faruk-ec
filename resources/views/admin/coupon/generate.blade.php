@extends('admin.layouts.app')
@section('title','Promo Codes')
@section('content')
<section class="content-header">
    <h1>
        Promo Codes
        <small>Add </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Promo Codes</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Promo Codes Generate</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <form name="add_subpro" action="{{url('/save-promcodes')}}" method="post" id="coupon_form">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
			<div class="col-md-12">
				<h2>PRIDE - Promo Code Generator</h2>
					<table class="table table-striped">
						<tr>
							<th>Parameter</th>
							<th>Type</th>
							<th>Default</th>
							<th>Custom value</th>
						</tr>
						<tr>
							<th>Number of coupons</th>
							<td>number</td>
							<td>1</td>
							<td><input class="form-control" type="number" name="no_of_coupons" value="1" min="1"/></td>
						</tr>
						<tr>
							<th>Length</th>
							<td>number</td>
							<td>6</td>
							<td><input class="form-control" type="number" name="length" value="6" min="1" /></td>
						</tr>
						<tr>
							<th>Prefix</th>
							<td>string</td>
							<td></td>
							<td><input class="form-control" type="text" name="prefix" value="PRIDE-" /></td>
						</tr>
						<tr>
							<th>Suffix</th>
							<td>string</td>
							<td></td>
							<td><input class="form-control" type="text" name="suffix" value="" /></td>
						</tr>
						<tr>
							<th>Showroom</th>
							<td>Select Show room</td>
							<td>all</td>
							<td>
							<select class="form-control">
							   <option value=""> -- select show room </option>
							</select>
							</td>
						</tr>
					</table>
					<div class="col-md-offset-8 col-md-4">
						<button type="submit" class="btn btn-success pull-right">Generate</button>
						<br/><br/>
					</div>
					
			</div>
		</div>
	</div>
</section>
@endsection