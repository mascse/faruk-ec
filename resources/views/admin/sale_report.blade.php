@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')

<?php 
// use App\Http\Controllers\Admin\ReportController;
?>
<style>
    fieldset {
    padding: .75em .825em .85em;
    margin: 0 2px;
    border: 1px solid silver;
}
fieldset {
    display: block;
    -webkit-margin-start: 2px;
    -webkit-margin-end: 2px;
    -webkit-padding-before: 0.35em;
    -webkit-padding-start: 0.75em;
    -webkit-padding-end: 0.75em;
    -webkit-padding-after: 0.625em;
    min-width: -webkit-min-content;
    border-width: 2px;
    border-style: groove;
    border-color: threedface;
    border-image: initial;
}
a:link, span.MsoHyperlink {
    mso-style-noshow: yes;
    mso-style-priority: 99;
    text-decoration: none !important;
    text-underline: single;
}

.margin_new {
    margin: 2px;
}
td, th {
    padding: 5px;
}
.text-design{
        color:red;
    }
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
</style>
<!-- Content Header (Page header) -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Sales Report</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div style="padding-bottom:15px;">
                        <fieldset><legend>Search Order</legend>
                            <form name="filtering"  action="{{ route('pride_admin.sale_reports') }}" method="GET">
                                
                                <table>
                                    <tr>
                                        <td class="field_label">
                                            Form Date:
                                        </td>
                                        <td>
                                            <input type="date" name="from" id="date" value="<?php echo isset($from)?date('Y-m-d',strtotime($from)):''; ?>" autocomplete="off" style="width:230px;"/>
                                            (mm/dd/yyyy)&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_label">
                                            To Date :
                                        </td>
                                        <td>
                                            <input type="date" name="to" id="ToDate"  value="<?php echo isset($to)?date('Y-m-d',strtotime($to)):''; ?>" autocomplete="off" style="width:230px;"/>
                                            (mm/dd/yyyy)&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_label">
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_label">
                                        </td>
                                        <td>
                                           <input type="submit" value="Search"/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </fieldset>
                    </div>
                    <div id="print_div">
                        <table id="exam" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Order Track Number</th>
                                    <th>Customer Name</th>
                                    <th>Order Placed</th>                        	
                                    <th>Delivery charge</th>
                                    <th>Sub Total</th>
                                    <th>Total Amount</th>
                                    <th>Discounted Price</th>                
                                </tr>
                            </thead>
                            @php($total_amount = 0)
                            @php($total_delivery = 0)
                            @php($subtotal = 0)
                            @php($discounted_total = 0)
                            @php($i = 1)
                            <tbody>
                                @foreach($sale_reports as $report)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $report->conforder_tracknumber }}</td>
                                    <td>{{ $report->registeruser_firstname }} {{ $report->registeruser_lastname }}</td>
                                    <td><?php echo date('M j, Y', strtotime($report->conforder_placed_date)); ?></td>
                                    <td>{{ $report->Shipping_Charge }}</td>
                                    <td>{{ $report->shoppingcart_subtotal }}</td>
                                    <td>{{ $report->Shipping_Charge + $report->shoppingcart_subtotal }}</td>
                                    <td>
                                    <?php $offer=$report->get_offer;
                                    $total=$report->shoppingcart_total;
                                    if($offer==1){
                                        $dis=$total*10/100; 
                                        $distotal=$total-$dis;
                                    }else{
                                        $distotal=$total; 
                                    }
                                    echo $distotal;
                                    $discounted_total +=$distotal;
                                    ?>
                                    </td>
                                    @php($total_delivery += $report->Shipping_Charge)
                                    @php($subtotal += $report->shoppingcart_subtotal)
                                    @php($total_amount= $total_delivery+$subtotal)
                                    @php($i++)
                                </tr>
                               
                                @endforeach
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="font-weight: bold">Total Amount</td>
                                    <td style="font-weight: bold">{{ $total_delivery }}</td>
                                    <td style="font-weight: bold">{{ $subtotal }}</td>
                                    <td style="font-weight: bold">{{ $total_amount }}</td>
                                    <td style="font-weight: bold">{{ $discounted_total }}</td>  
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="#" class="btn btn-sm btn-info btn-flat pull-left" onclick="printDiv()">Print Sales Report</a>
                    <a href="{{url('/utadmin/manage-all-order')}}" class="btn btn-sm btn-default btn-flat pull-right">Go to all orders page</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
<!-- /.content -->
<script type="text/javascript">
    function printDiv() {    
    var printContents = document.getElementById('print_div').innerHTML;
    var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
    }
</script>

@endsection