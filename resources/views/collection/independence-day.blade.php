<?php

use App\Http\Controllers\product\ProductController;
?>
@extends('layouts.app')
@section('title','Indepandance Day | Collection')
@section('content')

<style>
    .nav-block {
        position: relative;
        left: 0;
        right: 0;
        top: 100%;
        background: #fff;
        -webkit-box-shadow: 0 0 1px 0 rgba(127, 127, 127, 0.2);
        box-shadow: 0 0 1px 0 rgba(127, 127, 127, 0.2);
    }
    .icon-down:before {
        content: "\f107";
        font-family: FontAwesome;
        font-size: 15px;
    }
    .product-items .product-item-photo .product-image-wrapper {
        display: block;
        overflow: hidden;
        position: relative;
        border: 0px solid #00000012;
    }
    .custom-carousel {
        position:relative;
        display:inline-block;
        width:100%;
        padding-bottom:150%;
        border: 1px solid #eee;
    }
    .custom-carousel .item{
        opacity:0;
        -webkit-transition:opacity 1s;
        transition:opacity 1s;
        position:absolute;
        width:100%;
        height:100%;
        left:0;
    }
    .custom-carousel .item.active {
        opacity:1;
    }
    /*Active filering */
    .sidebar .filter-holder ul li .m-filter-item-list li a.active:after {
        background:#000;
    }
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item">
                                <a href="#" title=""><?php echo $title; ?></a>
                            </li>
                            <li class="item">
                                <a href="#" title="">Independence Day Collection</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="category-view">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">    
                                <div class="category-cms">
                                    <section class="gallery-page">
                                        <header class="head"></header>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="gallery-holder">
                                                    <div class="img"><a href="{{url('/offers')}}"><img src="https://www.khaadi.com/media/wysiwyg/Gallery/KhaadiPKGallery18032019.jpg" alt=""></a>
                                                        <div class="info">
                                                            <h2>Catalogues</h2>
                                                            <a class="btn" href="{{url('/offers')}}">View Offers</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-banner">
                        <div class="block-content">
                            <a href="#" class="opener-filter">filters</a>
                            <div class="filter-holder">
                                <div class="filter-content">
                                    <ul class="list-inline">
                                        <li class="active">
                                            <a class="opener-cate" href="#"> Category <span class="icon-down"></span></a> 
                                            <div class="filter-block">
                                                @if(trim($title) == 'woman')
                                                <strong><a href="{{url("/signature/signature-sari/9/signature")}}" ><span>Pride Signature</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 16 && $subcategory == 72) echo 'active'; ?>" href="{{url("/signature/kameez/digital_print/16/72")}}" ><span>Kameez Set</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 12 && $subcategory == 66) echo 'active'; ?>" href="{{url("/signature/unstitched_three_piece/12/66")}}" ><span>Unstitched Three Piece</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 13) echo 'active'; ?>" href="{{url("/signature-sari/dupatta/5/13")}}" ><span>Dupatta</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 55) echo 'active'; ?>" href="{{url("/signature/cotton-sari/9/55")}}" ><span>Cotton Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 56) echo 'active'; ?>" href="{{url("/signature/taat-silk-sari/9/56")}}" ><span>Taat/Silk Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 58) echo 'active'; ?>" href="{{url("/signature/half-silk-sari/9/58")}}" ><span>Half Silk Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 60) echo 'active'; ?>" href="{{url("/signature/muslin-sari/9/60")}}" ><span>Muslin Silk Sari</span></a></li>
                                                </ol>
                                                <strong><a href="" ><span>Pride Classic</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 7 && $subcategory == 40) echo 'active'; ?>" href="{{url("/classic/classic-sari/7/40")}}" ><span>Classic Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 7 && $subcategory == 77) echo 'active'; ?>" href="{{url("/classic/unstitched-three-piece/7/77")}}" ><span>Unstitched Three Piece</span></a></li>
                                                </ol>
                                                <strong><a href="{{url("/pride-girls/all/5/pride-girls")}}" ><span>Pride Girls</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 14) echo 'active'; ?>" href="{{url("/pride-girls/formal/5/14")}}" ><span>Formal</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 15) echo 'active'; ?>" href="{{url("/pride-girls/semi-formal/5/15")}}" ><span>Semi Formal</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 16) echo 'active'; ?>" href="{{url("/pride-girls/casual/5/16")}}" ><span>Casual</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 50) echo 'active'; ?>" href="{{url("/pride-girls/bottoms/5/50")}}" ><span>Bottoms</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 13) echo 'active'; ?>" href="{{url("/pride-girls/dupatta/5/13")}}" ><span>Dupatta</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 78) echo 'active'; ?>" href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewelry</span></a></li>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="active" href="{{url('/independence-day/woman/9')}}" ><span>Independence Day</span></a></li>
                                                    <!--<li  class="item"><a  href="{{url('/amar-ekushay-collection-2019/woman/9')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                    <li  class="item"><a  href="{{url('/falgun-collection-2019/woman/9')}}" ><span>Spring 2019</span></a></li>
                                                    <!--<li  class="item"><a class="#" href="#" ><span>Boishakh 1425</span></a></li>
                                                    <li  class="item"><a class="#" href="#" ><span>Puja 18</span></a></li>-->
                                                </ol>
                                                @endif
                                                @if(trim($title) == 'pride-girls')
                                                <strong><a href="{{url("/signature/signature-sari/9/signature")}}" ><span>Pride Signature</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 16 && $subcategory == 72) echo 'active'; ?>" href="{{url("/signature/kameez/digital_print/16/72")}}" ><span>Kameez Set</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 12 && $subcategory == 66) echo 'active'; ?>" href="{{url("/signature/unstitched_three_piece/12/66")}}" ><span>Unstitched Three Piece</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 13) echo 'active'; ?>" href="{{url("/signature-sari/dupatta/5/13")}}" ><span>Dupatta</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 55) echo 'active'; ?>" href="{{url("/signature/cotton-sari/9/55")}}" ><span>Cotton Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 56) echo 'active'; ?>" href="{{url("/signature/taat-silk-sari/9/56")}}" ><span>Taat/Silk Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 58) echo 'active'; ?>" href="{{url("/signature/half-silk-sari/9/58")}}" ><span>Half Silk Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 60) echo 'active'; ?>" href="{{url("/signature/muslin-sari/9/60")}}" ><span>Muslin Silk Sari</span></a></li>
                                                </ol>
                                                <strong><a href="" ><span>Pride Classic</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 7 && $subcategory == 40) echo 'active'; ?>" href="{{url("/classic/classic-sari/7/40")}}" ><span>Classic Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 7 && $subcategory == 77) echo 'active'; ?>" href="{{url("/classic/unstitched-three-piece/7/77")}}" ><span>Unstitched Three Piece</span></a></li>
                                                </ol>
                                                <strong><a href="{{url("/pride-girls/all/5/pride-girls")}}" ><span>Pride Girls</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 14) echo 'active'; ?>" href="{{url("/pride-girls/formal/5/14")}}" ><span>Formal</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 15) echo 'active'; ?>" href="{{url("/pride-girls/semi-formal/5/15")}}" ><span>Semi Formal</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 16) echo 'active'; ?>" href="{{url("/pride-girls/casual/5/16")}}" ><span>Casual</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 50) echo 'active'; ?>" href="{{url("/pride-girls/bottoms/5/50")}}" ><span>Bottoms</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 13) echo 'active'; ?>" href="{{url("/pride-girls/dupatta/5/13")}}" ><span>Dupatta</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 78) echo 'active'; ?>" href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewelry</span></a></li>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="active" href="{{url('/independence-day/woman/9')}}" ><span>Independence Day</span></a></li>
                                                    <!--<li  class="item"><a  href="{{url('/amar-ekushay-collection-2019/woman/9')}}" ><span>Amar Ekushay 2019</span></a></li> --->
                                                    <li  class="item"><a  href="{{url('/falgun-collection-2019/woman/9')}}" ><span>Spring 2019</span></a></li>
                                                    <!--<li  class="item"><a class="#" href="#" ><span>Boishakh 1425</span></a></li>
                                                    <li  class="item"><a class="#" href="#" ><span>Puja 18</span></a></li>-->
                                                </ol>
                                                @endif
                                                @if(trim($title) == 'man')
                                                <strong><a href="{{url("/athenic-men/panjabi/17/all-panjabi")}}"><span>Panjabi</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 17 && $subcategory == 'all-panjabi') echo 'active'; ?>" href="{{url("/athenic-men/panjabi/17/all-panjabi")}}" ><span>All</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 17 && $subcategory == 73) echo 'active'; ?>" href="{{url("/athenic-men/long-panjabi/regular-fit/17/73")}}" ><span>Regular Fit Panjabi</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 18 && $subcategory == 76) echo 'active'; ?>" href="{{url("/athenic-men/short-panjabi/slim-fit/18/76")}}" ><span>Slim Fit Panjabi</span></a></li>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="active" href="{{url('/independence-day/man/17')}}" ><span>Independence Day</span></a></li>
                                                    <!--<li  class="item"><a  href="{{url('/amar-ekushay-collection-2019/man/17')}}" ><span>Amar Ekushay 2019</span></a></li> -->
                                                    <li  class="item"><a href="{{url('/falgun-collection-2019/man/17')}}" ><span>Spring 2019</span></a></li>
                                                   <!-- <li  class="item"><a class="<?php if ($procat_id == 'ethnic-menswear' && $subcategory == 17) echo 'active'; ?>" href="{{url('/eid-collection-201/ethnic-menswear/17')}}" ><span>Eid Collection 18</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 'ethnic-menswear' && $subcategory == 17) echo 'active'; ?>" href="{{url('/puja-collection-2018/ethnic-menswear/17')}}" ><span>Puja Collection</span></a></li>-->
                                                </ol>
                                                @endif
                                                @if(trim($title) == 'kids')
                                                <strong><a href="{{url("/kids/girls/6/all-girls")}}" ><span>Girls</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 52) echo 'active'; ?>" href="{{url('/kids/girls/6/52')}}" ><span>Dresses</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 81) echo 'active'; ?>" href="{{url('/kids/girls/6/81')}}" ><span>Tunics</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 82) echo 'active'; ?>" href="{{url('/kids/girls/6/82')}}" ><span>Tops</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 83) echo 'active'; ?>" href="{{url('/kids/girls/6/83')}}" ><span>Bottoms</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 84) echo 'active'; ?>" href="{{url('/kids/girls/6/84')}}" ><span>Clothing Sets</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 85) echo 'active'; ?>" href="{{url('/kids/girls/6/85')}}" ><span>Rompers</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 89) echo 'active'; ?>" href="{{url('/kids/girls/6/89')}}" ><span>Jewelry</span></a></li>
                                                </ol>
                                                <strong><a href="{{url('/kids/boys/6/39')}}"><span>Boys</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 39) echo 'active'; ?>" href="{{url('/kids/boys/6/39')}}" ><span>Panjabi</span></a></li>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="active" href="{{url('/independence-day/kids/6')}}" ><span>Independence Day</span></a></li>
                                                     <!-- <li  class="item"><a class="active" href="{{url('/amar-ekushay-collection-2019/kids/6')}}" ><span>Amar Ekushay 2019</span></a></li> -->
                                                    <li  class="item"><a href="{{url('/falgun-collection-2019/kids/6')}}" ><span>Spring 2019</span></a></li>
                                                   <!--<li class="item"><a class="" href="#" ><span>Boishakh 1425</span></a></li>
                                                   <li class="item"><a class="" href="{{url('/eid-collection-201/pride-kids/6')}}" ><span>Eid Collection</span></a></li>
                                                   <li class="item"><a class="" href="{{url('/boishakh-1425/pride-kids/6')}}" ><span>Boishakh 1425</span></a></li>-->
                                                </ol>
                                                @endif
                                                @if(trim($title) == '')
                                                <strong><a href="#"><span>Independence Day</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a  href="{{url('/independence-day/woman/9')}}" ><span>Woman </span></a></li>
                                                    <li  class="item"><a  href="{{url('/independence-day/man/17')}}" ><span>Mens</span></a></li>
                                                    <!--<li  class="item"><a  href="{{url('/independence-day/kids/6')}}" ><span>Kids</span></a></li> --->
                                                </ol>
                                                <!--<strong><a href="#"><span>Amar Ekushay 2019</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a  href="{{url('/amar-ekushay-collection-2019/woman/9')}}" ><span>Woman </span></a></li>
                                                    <li  class="item"><a  href="{{url('/amar-ekushay-collection-2019/man/17')}}" ><span>Mens</span></a></li>
                                                    <li  class="item"><a  href="{{url('/amar-ekushay-collection-2019/kids/6')}}" ><span>Kids</span></a></li>
                                                </ol> -->
                                                <strong><a href="#"><span>Falgun Collection' 19</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a  href="{{url('/falgun-collection-2019/woman/9')}}" ><span>Woman </span></a></li>
                                                    <li  class="item"><a  href="{{url('/falgun-collection-2019/man/17')}}" ><span>Mens</span></a></li>
                                                    <li  class="item"><a  href="{{url('/falgun-collection-2019/kids/6')}}" ><span>Kids</span></a></li>
                                                </ol>
                                                @endif
                                            </div>
                                        </li>
                                        <!--<li>
                                            <a class="opener-cate" href="#"> Price <span class="icon-down"></span></a> 
                                            <div class="filter-block">

                                                <ol class="m-filter-item-list">
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 500</span> - <span class="price">Tk 1,000</span>                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 1,000</span> - <span class="price">Tk 1,500</span>                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 1,500</span> - <span class="price">Tk 2,000</span>                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 2,000</span> - <span class="price">Tk 2,500</span>                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 2,500</span> and above                                    </a>
                                                    </li>
                                                </ol>
                                            </div>
                                        </li> --->
                                        <!--<li>
                                            <a class="opener-cate" href="#"> Color <span class="icon-down"></span></a> 
                                            <div class="filter-block">

                                                <ol class="m-filter-item-list">
                                                    <li class="item">
                                                        <a href="#"> Black</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">Blue</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            Red                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            White                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            Pink                                    </a>
                                                    </li>
                                                </ol>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <input name="form_key" type="hidden" value="" />
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
                </div>
                <style>
                    .product-items .product-item-photo .product-image-wrapper {
                        display: block;
                        overflow: hidden;
                        position: relative;
                        border: 0px solid #00000012;
                    }
                </style>
                <div class="products wrapper grid products-grid">
                    <ol class="products list items product-items">
                        @foreach($product_list as $product)
                        <?php
                        $product_name = str_replace(' ', '-', $product->product_name);
                        $product_url = strtolower($product_name);
                        $data = ProductController::GetProductColorAlbum($product->product_id);
                        // dd($data);
                        $sold_out = ProductController::ProductWiseQty($product->product_id);
                        foreach ($data as $pro_album) {
                            $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                        }
                        ?>
                        <li class="item product product-item">    
                            <?php
                            date_default_timezone_set('Asia/Dhaka');
                            $today = date('d-m-Y');
                            $insert_date = $product->product_insertion_date;
                            $datetime1 = new DateTime($today);
                            $datetime2 = new DateTime($insert_date);
                            $interval = $datetime1->diff($datetime2);
                            $date_difference = $interval->format('%a');
                            $color_album = str_replace('/', '-', $product->productalbum_name);
                            if ($sold_out <= 0) {
                                ?>
                                                             <!--<span class="sprice-tag">Sold Out</span> --->
                                <span class="sold-out">Sold Out</span>
                            <?php } else if ($product->product_pricediscounted > 1) { ?>
                                <span class="sprice-tag"><?php echo $product->product_pricediscounted; ?>% Off</span>
                            <?php } if ($date_difference < 65) { ?>
                                <div class="tag_container round_tag_lt bg_red t_white"><span class="ttl_header">New</span></div> 
                            <?php } ?>
                            <div class="product-item-info" data-container="product-grid">
                                <a href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}">
                                    <span class="product-image-container">
                                        <span class="product-image-wrapper" >
                                            <span class="custom-carousel" onmouseenter="fadeImages(this)" onmouseleave="removeTimer(this)">
                                                <?php $images = ProductController::productImages($pro_album->productalbum_id); ?>
                                                @php($i = 0)
                                                @foreach($images as $image)
                                                <img class="item large_img<?php if ($i == 0) echo ' active'; ?>" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $image->productimg_img }}" alt="No Image Found"/>
                                                @php($i++)
                                                @endforeach
                                            </span>
                                        </span>
                                    </span>
                                </a>
                                <div class="product details product-item-details">
                                    <div class="info-holder">
                                        <strong class="product name product-item-name">
                                            <a class="product-item-link"
                                               href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}">
                                                {{$product->product_name}}
                                            </a>
                                        </strong>

                                    </div>
                                    <div class="info-holder">
                                        <?php if ($product->product_pricediscounted < 1) { ?>
                                            <div class="price-box price-final_price" data-role="priceBox">
                                                <span class="price-container price-final_price tax weee">
                                                    <span class="price-label">Regular Price</span>
                                                    <span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
                                                        <span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
                                                </span>
                                            </div>
                                        <?php } else { ?>
                                            <div class="price-box price-final_price" data-role="priceBox" data-product-id="84726" data-price-box="product-id-84726">
                                                <span class="normal-price">
                                                    <span class="price-container price-final_price tax weee">
                                                        <span id="product-price-84726" data-price-amount="600" data-price-type="finalPrice" class="price-wrapper ">
                                                            <span class="price">Tk &nbsp;{{$product->discount_product_price}}</span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </div>                                                            
                                            <div class="price-box price-final_price" data-role="priceBox">
                                                <span class="old-price">
                                                    <span class="price-container price-final_price tax weee">
                                                        <span class="price-label">Regular Price</span>
                                                        <span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
                                                            <span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
                                                    </span>
                                                </span>
                                            </div>
                                        <?php } ?>                                                      
                                        <div class="product-item-inner">
                                            <div class="product actions product-item-actions">
                                                <div class="actions-primary">
                                                    <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}" class="action tocart primary"><span>Shop Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                </div>
                <div class="pages">
                    <center>  {{{ $product_list->links() }}} </center>
                </div>
                <div class="toolbar toolbar-products" data-mage-init='{""}'>
                    <div class="modes"></div>
                    <p class="toolbar-amount" id="toolbar-amount">
                        Items <span class="toolbar-number">1</span>-<span class="toolbar-number">18</span> of <span class="toolbar-number">42</span>    </p>
                    <div class="pages">
                        <center></center>
                        <strong class="label pages-label" id="paging-label">Page</strong>
                        <!--<ul class="items pages-items" aria-labelledby="paging-label">
                            <li class="item current">
                                <strong class="page">
                                    <span class="label">You're currently reading page</span>
                                    <span>1</span>
                                </strong>
                            </li>
                            <li class="item">
                                <a href="" class="page">
                                    <span class="label">Page</span>
                                    <span>2</span>
                                </a>
                            </li>
                            <li class="item">
                                <a href="" class="page">
                                    <span class="label">Page</span>
                                    <span>3</span>
                                </a>
                            </li>
                            <li class="item pages-item-next">
                                <a class="action  next" href="" title="Next">
                                    <span class="label">Page</span>
                                    <span>Next</span>
                                </a>
                            </li>
                        </ul> --->
                    </div>
                    <!--  <div class="field limiter">
                        <label class="label" for="limiter">
                            <span>Show</span>
                        </label>
                        <div class="control">
                            <select id="limiter" data-role="limiter" class="limiter-options">
                                <option value="9">
                                    9                </option>
                                <option value="18"                    selected="selected">
                                    18                </option>
                                <option value="45">
                                    45                </option>
                            </select>
                        </div>
                        <span class="limiter-text">per page</span>
                    </div>

                    <div class="toolbar-sorter sorter">
                        <label class="sorter-label" for="sorter">Sort By</label>
                        <select id="sorter" data-role="sorter" class="sorter-options">
                            <option value="">Sort By</option>
                            <option value="position"
                                    >
                                Position            </option>
                            <option value="name"
                                    >
                                Product Name            </option>
                            <option value="price"
                                    >
                                Price            </option>
                            <option value="created_at"
                                    selected="selected"
                                    >
                                Date            </option>
                        </select>
                        <a title="Set Ascending Direction" href="#" class="action sorter-action sort-desc" data-role="direction-switcher" data-value="asc">
                            <span>Set Ascending Direction</span>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    var timer1;
    var timer2;
    var counter = 1;
    var running = false;
    (function () {

    })();
    function updateActive(items, i) {
        i = i % (items.length);
        items[i > 0 ? i - 1 : items.length - 1].classList.remove('active');
        items[i].classList.add('active');
        counter++;
        if (!running) {
            timer2 = setInterval(function () {
                updateActive(items, counter);
            }, 1500);
            running = true;
            clearInterval(timer1);
        }
    }
    function fadeImages(element) {
        var items = element.getElementsByClassName('item');
        for (var i = 0; i < items.length; i++) {
            items[i].classList.remove('active');
        }
        items[1].classList.add('active');
        timer1 = setInterval(function () {
            updateActive(items, counter);
        }, 100);
    }
    function removeTimer(element) {
        clearInterval(timer1);
        clearInterval(timer2);
        counter = 1;
        var items = element.getElementsByClassName('item');
        for (var i = 0; i < items.length; i++) {
            items[i].classList.remove('active');
        }
        items[0].classList.add('active');
        running = false;
    }

    /* Open filter category */
    (function () {
        //  document.getElementById('filter-category').classList.add('active');
    })();
</script>
@endsection