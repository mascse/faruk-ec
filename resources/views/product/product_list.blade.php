@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<?php

use App\Http\Controllers\product\ProductController;
use App\Productalbum;
?>
<style>
    .nav-block {
        position: relative;
        left: 0;
        right: 0;
        top: 100%;
        background: #fff;
        -webkit-box-shadow: 0 0 1px 0 rgba(127, 127, 127, 0.2);
        box-shadow: 0 0 1px 0 rgba(127, 127, 127, 0.2);
    }
    .icon-down:before {
        content: "\f107";
        font-family: FontAwesome;
        font-size: 15px;
    }
    .product-items .product-item-photo .product-image-wrapper {
        display: block;
        overflow: hidden;
        position: relative;
        border: 0px solid #00000012;
    }
    /* Pagination style */
    .pagination>li>a, .pagination>li>span {
        border-radius:50%;
        margin-left:20px;
        border:1px solid #FFF;
        color:#000;
    }
    .pagination>li:first-child>a, .pagination>li:first-child>span {
        border-radius:50%;
        border:1px solid #000;
    }
    .pagination>li:last-child>a, .pagination>li:last-child>span {
        border-radius:50%;
        border:1px solid #000;
    }
    .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
        background-color:#291d88;
        border-color:#291d88;
    }
    @media (max-width: 766px) {
        .pagination>li>a, .pagination>li>span {
            border-radius: 50%;
            margin-left: 4px;
            border: 1px solid #fff;
            color: #000;
        }
        .pagination>li>a, .pagination>li>span {
            position: relative;
            float: left;
            padding: 6px 10px;
            line-height: 1.33;
            text-decoration: none;
            color: #1f1f1f;
            background-color: #fff;
            border: 1px solid #ddd;
            margin-left: 6px;
        }
    }
</style>
<style>
    .custom-carousel {
        position:relative;
        display:inline-block;
        width:100%;
        padding-bottom:150%;
        border: 1px solid #eee;
    }
    .custom-carousel .item{
        opacity:0;
        -webkit-transition:opacity 1s;
        transition:opacity 1s;
        position:absolute;
        width:100%;
        height:100%;
        left:0;
    }
    .custom-carousel .item.active {
        opacity:1;
    }
    /*Active filering */
    .sidebar .filter-holder ul li .m-filter-item-list li a.active:after {
        background:#000;
    }
    div[class*=round_tag] {
        width: 45px;
        height: 45px;
        border-radius: 50%;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        -ms-border-radius: 50%;
        -o-border-radius: 50%;
        letter-spacing: -.03em;
    }
    .round_tag_lt, .tag_lt {
        top: -10px;
        left: -10px;
    }
    .tag_container {
        position: absolute;
        display: table;
        text-align: center;
        line-height: 115%;
        z-index: 9;
    }
    .bg_red {
        background: #f5a004;
        border: 1px solid #ddd;
    }
    .t_white {
        color: white;
        font-size: 9pt;
    }
    .tag_container>span {
        display: table-cell;
        vertical-align: middle;
        letter-spacing: 1px;
    }
    .swatch {
        border: 2px solid #eee;
        padding: 1px;
    }
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="itemhome">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item">
                                <a href="#" title="Main category"><?php echo $main_cate; ?></a>
                            </li>
                            <li class="item">
                                <a href="#" title="Sub category"><?php echo str_replace('-', '/', $title); ?></a>
                            </li>
                            <li class="item">
                                <a href="#" title="Child sub category"><?php echo str_replace('-', '/', $last); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-banner">
                        <div class="block-content">
                            <a href="#" class="opener-filter">filters</a>
                            <div class="filter-holder">
                                <div class="filter-content">
                                    <ul class="list-inline">
                                        <!--<li id='filter-fabric'>
                                            <a class="opener-cate" href="#"> Fabrics <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <ol class="m-filter-item-list">
                                        <?php
                                        if (!isset($fabrics))
                                            $fabrics = [];
                                        if (!isset($fabric))
                                            $fabric = null;
                                        ?>
                                                    @foreach($fabrics as $single)
                                                    @if($single !== null)
                                                    <li class="item"><a class="<?php if ($fabric == $single) echo 'active'; ?>" href="{{ url("product-by-fabric/{$procat_id}/{$subcategory}") }}/{{ $single }}">{{ $single }}</a></li>
                                                    @endif
                                                    @endforeach
                                                </ol>
                                            </div>
                                        </li>-->
                                        <li id='filter-category'>
                                            <a class="opener-cate" href="#"> Category <span class="icon-down"></span></a> 
                                            <div class="filter-block">
                                                @if(trim($main_cate) == 'Woman')
                                                <strong><a href="{{url("/signature/signature-sari/9/signature")}}" ><span>Pride Signature</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 16 && $subcategory == 72) echo 'active'; ?>" href="{{url("/signature/kameez/digital_print/16/72")}}" ><span>Kameez Set</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 12 && $subcategory == 66) echo 'active'; ?>" href="{{url("/signature/unstitched_three_piece/12/66")}}" ><span>Unstitched Three Piece</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 13) echo 'active'; ?>" href="{{url("/signature-sari/dupatta/5/13")}}" ><span>Orna</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 55) echo 'active'; ?>" href="{{url("/signature/cotton-sari/9/55")}}" ><span>Cotton Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 56) echo 'active'; ?>" href="{{url("/signature/taat-silk-sari/9/56")}}" ><span>Taat/Silk Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 58) echo 'active'; ?>" href="{{url("/signature/half-silk-sari/9/58")}}" ><span>Half Silk Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 9 && $subcategory == 60) echo 'active'; ?>" href="{{url("/signature/muslin-sari/9/60")}}" ><span>Muslin Silk Sari</span></a></li>
                                                </ol>
                                                <strong><a href="" ><span>Pride Classic</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 7 && $subcategory == 40) echo 'active'; ?>" href="{{url("/classic/classic-sari/7/40")}}" ><span>Classic Sari</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 7 && $subcategory == 77) echo 'active'; ?>" href="{{url("/classic/unstitched-three-piece/7/77")}}" ><span>Unstitched Three Piece</span></a></li>
                                                </ol>
                                                <strong><a href="{{url("/pride-girls/all/5/pride-girls")}}" ><span>Pride Girls</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 14) echo 'active'; ?>" href="{{url("/pride-girls/formal/5/14")}}" ><span>Formal</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 15) echo 'active'; ?>" href="{{url("/pride-girls/semi-formal/5/15")}}" ><span>Semi Formal</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 16) echo 'active'; ?>" href="{{url("/pride-girls/casual/5/16")}}" ><span>Casual</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 50) echo 'active'; ?>" href="{{url("/pride-girls/bottoms/5/50")}}" ><span>Bottoms</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 13) echo 'active'; ?>" href="{{url("/pride-girls/dupatta/5/13")}}" ><span>Orna</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 5 && $subcategory == 78) echo 'active'; ?>" href="{{url("/pride-girls/jewelry/5/78")}}" ><span>Jewelry</span></a></li>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="#" href="{{url('/falgun-collection-2019/woman/9')}}" ><span>Falgun Collection 18</span></a></li>
                                                    <!--<li  class="item"><a class="#" href="#" ><span>Boishakh 1425</span></a></li>
                                                    <li  class="item"><a class="#" href="#" ><span>Puja 18</span></a></li>-->
                                                </ol>
                                                @endif
                                                @if(trim($main_cate) == 'Mens')
                                                <strong><a href="{{url("/athenic-men/panjabi/17/all-panjabi")}}"><span>Panjabi</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 17 && $subcategory == 'all-panjabi') echo 'active'; ?>" href="{{url("/athenic-men/panjabi/17/all-panjabi")}}" ><span>All</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 17 && $subcategory == 73) echo 'active'; ?>" href="{{url("/athenic-men/long-panjabi/regular-fit/17/73")}}" ><span>Regular Fit Panjabi</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 18 && $subcategory == 76) echo 'active'; ?>" href="{{url("/athenic-men/short-panjabi/slim-fit/18/76")}}" ><span>Slim Fit Panjabi</span></a></li>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 'man' && $subcategory == 17) echo 'active'; ?>" href="{{url('/falgun-collection-2019/man/17')}}" ><span>Falgun Collection' 19</span></a></li>
                                                   <!-- <li  class="item"><a class="<?php if ($procat_id == 'ethnic-menswear' && $subcategory == 17) echo 'active'; ?>" href="{{url('/eid-collection-201/ethnic-menswear/17')}}" ><span>Eid Collection 18</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 'ethnic-menswear' && $subcategory == 17) echo 'active'; ?>" href="{{url('/puja-collection-2018/ethnic-menswear/17')}}" ><span>Puja Collection</span></a></li>-->
                                                </ol>
                                                @endif
                                                @if(trim($main_cate) == 'Kids')
                                                <strong><a href="{{url("/kids/girls/6/all-girls")}}" ><span>Girls</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 52) echo 'active'; ?>" href="{{url('/kids/girls/6/52')}}" ><span>Dresses</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 81) echo 'active'; ?>" href="{{url('/kids/girls/6/81')}}" ><span>Tunics</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 82) echo 'active'; ?>" href="{{url('/kids/girls/6/82')}}" ><span>Tops</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 83) echo 'active'; ?>" href="{{url('/kids/girls/6/83')}}" ><span>Bottoms</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 84) echo 'active'; ?>" href="{{url('/kids/girls/6/84')}}" ><span>Clothing Sets</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 85) echo 'active'; ?>" href="{{url('/kids/girls/6/85')}}" ><span>Rompers</span></a></li>
                                                    <li  class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 89) echo 'active'; ?>" href="{{url('/kids/girls/6/89')}}" ><span>Jewelry</span></a></li>
                                                </ol>
                                                <strong><a href="{{url('/kids/boys/6/39')}}"><span>Boys</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li class="item"><a class="<?php if ($procat_id == 6 && $subcategory == 39) echo 'active'; ?>" href="{{url('/kids/boys/6/39')}}" ><span>Panjabi</span></a></li>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <li  class="item"><a class="<?php if ($procat_id == 'kids' && $subcategory == 6) echo 'active'; ?>" href="{{url('/falgun-collection-2019/kids/6')}}" ><span>Falgun Collection' 19</span></a></li>
                                                    <!--<li class="item"><a class="" href="#" ><span>Boishakh 1425</span></a></li>
                                                    <li class="item"><a class="" href="{{url('/eid-collection-201/pride-kids/6')}}" ><span>Eid Collection</span></a></li>
                                                    <li class="item"><a class="" href="{{url('/boishakh-1425/pride-kids/6')}}" ><span>Boishakh 1425</span></a></li>-->
                                                </ol>
                                                @endif
                                            </div>
                                        </li>
                                        <li id='filter-price'>
                                            <a class="opener-cate" href="#"> Price <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <?php
                                                if (!isset($lower_price))
                                                    $lower_price = 0;
                                                if (!isset($upper_price))
                                                    $upper_price = 0;
                                                ?>
                                                <ol class="m-filter-item-list">
                                                    <li class="item"><a class="<?php if ($lower_price == 500 && $upper_price == 1000) echo 'active'; ?>" href="{{ url("product-by-price/{$procat_id}/{$subcategory}/500/1000") }}"><span>Tk 500</span> - <span>Tk 1,000</span></a></li>
                                                    <li class="item"><a class="<?php if ($lower_price == 1000 && $upper_price == 1500) echo 'active'; ?>" href="{{ url("product-by-price/{$procat_id}/{$subcategory}/1000/1500") }}"><span>Tk 1,000</span> - <span>Tk 1,500</span></a></li>
                                                    <li class="item"><a class="<?php if ($lower_price == 1500 && $upper_price == 2000) echo 'active'; ?>" href="{{ url("product-by-price/{$procat_id}/{$subcategory}/1500/2000") }}"><span>Tk 1,500</span> - <span>Tk 2,000</span></a></li>
                                                    <li class="item"><a class="<?php if ($lower_price == 2000 && $upper_price == 2500) echo 'active'; ?>" href="{{ url("product-by-price/{$procat_id}/{$subcategory}/2000/2500") }}"><span>Tk 2,000</span> - <span>Tk 2,500</span></a></li>
                                                    <li class="item"><a class="<?php if ($lower_price == 2500 && $upper_price == 0) echo 'active'; ?>" href="{{ url("product-by-price/{$procat_id}/{$subcategory}/2500") }}"><span>Tk 2,500</span> and above</a></li>
                                                </ol>
                                            </div>
                                        </li>
                                        <li id='filter-size'>
                                            <a class="opener-cate" href="#"> Size <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <?php
                                                if (trim($main_cate) == 'Mens') {
                                                    if (!isset($size))
                                                        $size = null;
                                                    ?>
                                                    <ol class="m-filter-item-list">
                                                        <li class="item"><a class="<?php if ($size == '38') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/38") }}"><span>38</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == '40') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/40") }}"><span>40</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == '42') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/42") }}"><span>42</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == '44') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/44") }}"><span>44</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == '46') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/46") }}"><span>46</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == '48') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/48") }}"><span>48</span></a></li>
                                                    </ol>
                                                    <?php
                                                }else {
                                                    if (!isset($size))
                                                        $size = null;
                                                    ?>
                                                    <ol class="m-filter-item-list">
                                                        <li class="item"><a class="<?php if ($size == 'XS') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/XS") }}"><span>XS</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == 'S') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/S") }}"><span>S</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == 'M') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/M") }}"><span>M</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == 'L') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/L") }}"><span>L</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == 'XL') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/XL") }}"><span>XL</span></a></li>
                                                        <li class="item"><a class="<?php if ($size == 'XXL') echo 'active'; ?>" href="{{ url("product-by-size/{$procat_id}/{$subcategory}/XXL") }}"><span>XXL</span></a></li>
                                                    </ol>
                                                <?php } ?>
                                            </div>
                                        </li>
                                        <li id='filter-color'>
                                            <a class="opener-cate" href="#"> Color <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <?php
                                                if (!isset($color_name))
                                                    $color_name = null;
                                                if (!isset($color_names))
                                                    $color_names = [];
                                                ?>
                                                <ol class="m-filter-item-list">
                                                    @foreach($color_names as $color)
                                                    <li class="item"><a class="<?php if ($color_name == $color) echo 'active'; ?>" href="{{ url("product-by-color/{$procat_id}/{$subcategory}/{$color}") }}"><span>{{ $color }}</a></li>
                                                    @endforeach
                                                </ol>
                                            </div>
                                        </li>
                                    </ul>
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <input name="form_key" type="hidden" value="" />
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
                </div>



                <style>
                    .product-items .product-item-photo .product-image-wrapper {
                        display: block;
                        overflow: hidden;
                        position: relative;
                        border: 0px solid #00000012;
                    }
                </style>
                <div class="products wrapper grid products-grid">
				<?php if($subcategory==13){ ?>
                    <ol class="products list items product-items">
                        @foreach($product_list as $product)
                        <?php
                        $product_name = str_replace(' ', '-', $product->product_name);
                        $product_url = strtolower($product_name);
                       // $pro_album = Productalbum::where('product_id',$product->product_id)->orderBy('productalbum_id','DESC')->first();
                        // dd($data);
                        $sold_out = ProductController::ProductWiseQty($product->product_id);
                        ?>
                        <li class="item product product-item">    
                            <?php
                            date_default_timezone_set('Asia/Dhaka');
                            $today = date('d-m-Y');
                            $insert_date = $product->product_insertion_date;
                            $datetime1 = new DateTime($today);
                            $datetime2 = new DateTime($insert_date);
                            $interval = $datetime1->diff($datetime2);
                            $date_difference = $interval->format('%a');
                            $color_album = str_replace('/', '-', $product->productalbum_name);
                            if ($sold_out <= 0) {
                                ?>
                                                 <!--<span class="sprice-tag">Sold Out</span> --->
                                <span class="sold-out">Sold Out</span>
                            <?php } else if ($product->product_pricediscounted > 1) { ?>
                                <span class="sprice-tag"><?php echo $product->product_pricediscounted; ?>% Off</span>
                            <?php } if ($date_difference < 65) { ?>
                                <div class="tag_container round_tag_lt bg_red t_white"><span class="ttl_header">New</span></div> 
                            <?php } ?>
                            <div class="product-item-info" data-container="product-grid">
                                <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}">
                                    <span class="product-image-container">
                                        <span class="product-image-wrapper" >
                                            <span class="custom-carousel" onmouseenter="fadeImages(this)" onmouseleave="removeTimer(this)">
                                                <?php $images = ProductController::productImages($product->productalbum_id); ?>
                                                @php($i = 0)
                                                @foreach($images as $image)
                                                <img class="item large_img<?php if ($i == 0) echo ' active'; ?>" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $image->productimg_img }}" alt="No Image Found"/>
                                                @php($i++)
                                                @endforeach
                                            </span>
                                        </span>
                                    </span>
                                </a>
                                <div class="product details product-item-details">
                                    <div class="info-holder">
                                        <strong class="product name product-item-name">
                                            <a class="product-item-link"
                                               href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}">
                                                {{$product->product_name}}
                                            </a>
                                        </strong>

                                    </div>
                                    <div class="info-holder">
                                        <?php if ($product->product_pricediscounted < 1) { ?>
                                            <div class="price-box price-final_price" data-role="priceBox">
                                                <span class="price-container price-final_price tax weee">
                                                    <span class="price-label">Regular Price</span>
                                                    <span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
                                                        <span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
                                                </span>
                                            </div>
                                        <?php } else { ?>
                                            <div class="price-box price-final_price" data-role="priceBox" data-product-id="84726" data-price-box="product-id-84726">
                                                <span class="normal-price">
                                                    <span class="price-container price-final_price tax weee">
                                                        <span id="product-price-84726" data-price-amount="600" data-price-type="finalPrice" class="price-wrapper ">
                                                            <span class="price">Tk &nbsp;{{$product->discount_product_price}}</span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </div>                                                            
                                            <div class="price-box price-final_price" data-role="priceBox">
                                                <span class="old-price">
                                                    <span class="price-container price-final_price tax weee">
                                                        <span class="price-label">Regular Price</span>
                                                        <span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
                                                            <span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
                                                    </span>
                                                </span>
                                            </div>
                                        <?php } ?>                                                      
                                        <div class="product-item-inner">
                                            <div class="product actions product-item-actions">
                                                <div class="actions-primary">
                                                    <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}" class="action tocart primary"><span>Shop Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
				<?php }else{ ?>
                    <ol class="products list items product-items">
                        @foreach($product_list as $product)
                        <?php
                        $product_name = str_replace(' ', '-', $product->product_name);
                        $product_url = strtolower($product_name);
                       // $data = ProductController::GetProductColorAlbum($product->product_id);
                       $pro_album = Productalbum::where('product_id',$product->product_id)->orderBy('productalbum_id','DESC')->first();
                        // dd($data);
                        $sold_out = ProductController::ProductWiseQty($product->product_id);
                      //  foreach ($data as $pro_album) {
                        //    $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                       // }
                        ?>
                        <li class="item product product-item">    
                           <?php
                            date_default_timezone_set('Asia/Dhaka');
    						$today=date('d-m-Y');
    						$insert_date=$product->product_insertion_date;
    						$datetime1 = new DateTime($today);
    						$datetime2 = new DateTime($insert_date);
    						$interval = $datetime1->diff($datetime2);
    						$date_difference=$interval->format('%a');
    						$color_album=str_replace('/','-',$pro_album->productalbum_name);
                           if($sold_out <= 0){ ?>
                             <!--<span class="sprice-tag">Sold Out</span> --->
                             <span class="sold-out">Sold Out</span>
                           <?php }else if($product->product_pricediscounted > 1){ ?>
							   <span class="sprice-tag"><?php echo $product->product_pricediscounted;?>% Off</span>
						   <?php  } if($date_difference < 31){ ?>
                             <div class="tag_container round_tag_lt bg_red t_white"><span class="ttl_header">New</span></div> 
                           <?php } ?>
                            <div class="product-item-info" data-container="product-grid">
                                <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}">
                                    <span class="product-image-container">
                                        <span class="product-image-wrapper" >
                                            <span class="custom-carousel" onmouseenter="fadeImages(this)" onmouseleave="removeTimer(this)">
                                                <?php $images = ProductController::productImages($pro_album->productalbum_id); 
												//dd($images);
												?>
                                                @php($i = 0)
                                                @foreach($images as $image)
                                                <img class="item large_img<?php if($i==0) echo ' active';?>" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $image->productimg_img_medium }}" alt="No Image Found"/>
                                                @php($i++)
                                                @endforeach
                                            </span>
                                        </span>
                                    </span>
                                </a>
                                <div class="product details product-item-details">
                                    <div class="info-holder">
                                        <strong class="product name product-item-name">
                                            <a class="product-item-link"
                                               href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}">
                                                {{$product->product_name}}
                                            </a>
                                        </strong>

                                    </div>
                                    <div class="info-holder">
                                       <?php if ($product->product_pricediscounted < 1) { ?>
										<div class="price-box price-final_price" data-role="priceBox">
											<span class="price-container price-final_price tax weee">
												<span class="price-label">Regular Price</span>
												<span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
													<span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
											</span>
										</div>
										<?php }else{?>
										<div class="price-box price-final_price" data-role="priceBox" data-product-id="84726" data-price-box="product-id-84726">
											<span class="normal-price">
												<span class="price-container price-final_price tax weee">
														<span id="product-price-84726" data-price-amount="600" data-price-type="finalPrice" class="price-wrapper ">
															<span class="price">Tk &nbsp;{{$product->discount_product_price}}</span>
														</span>
												</span>
											</span>
										</div>                                                            
										<div class="price-box price-final_price" data-role="priceBox">
												<span class="old-price">
													<span class="price-container price-final_price tax weee">
														<span class="price-label">Regular Price</span>
														<span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
															<span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
													</span>
												</span>
										</div>
									<?php } ?>                                                      
                                        <div class="product-item-inner">
                                            <div class="product actions product-item-actions">
                                                <div class="actions-primary">
                                                    <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}" class="action tocart primary"><span>Shop Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                    <?php } ?>
                </div>
                <div class="pages">
                    <center>  {{{ $product_list->links() }}} </center>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection
@section('appended.script')
<script>
    var timer1;
    var timer2;
    var counter = 1;
    var running = false;
    (function () {

    })();
    function updateActive(items, i) {
        i = i % (items.length);
        items[i > 0 ? i - 1 : items.length - 1].classList.remove('active');
        items[i].classList.add('active');
        counter++;
        if (!running) {
            timer2 = setInterval(function () {
                updateActive(items, counter);
            }, 1500);
            running = true;
            clearInterval(timer1);
        }
    }
    function fadeImages(element) {
        var items = element.getElementsByClassName('item');
        for (var i = 0; i < items.length; i++) {
            items[i].classList.remove('active');
        }
        items[1].classList.add('active');
        timer1 = setInterval(function () {
            updateActive(items, counter);
        }, 100);
    }
    function removeTimer(element) {
        clearInterval(timer1);
        clearInterval(timer2);
        counter = 1;
        var items = element.getElementsByClassName('item');
        for (var i = 0; i < items.length; i++) {
            items[i].classList.remove('active');
        }
        items[0].classList.add('active');
        running = false;
    }

    /* Open filter category */
    (function () {
<?php if (!((isset($lower_price) && $lower_price != 0) || (isset($upper_price) && $upper_price != 0) || (isset($size) && $size != null) || (isset($color_name) && $color_name != null))) { ?>
            document.getElementById('filter-category').classList.add('active');
    <?php
} elseif ((isset($lower_price) || isset($upper_price)) && ($lower_price != 0) && ($upper_price != 0)) {
    echo 'console.log("' . $lower_price . ' and ' . $upper_price . '");';
    ?>
            document.getElementById('filter-price').classList.add('active');
<?php } elseif (isset($size) && $size != null) { ?>
            document.getElementById('filter-size').classList.add('active');
<?php } elseif (isset($color_name) && $color_name != null) { ?>
            document.getElementById('filter-color').classList.add('active');
<?php } elseif (isset($fabric) && $fabric != null) { ?>
            document.getElementById('filter-fabric').classList.add('active');
<?php } ?>
    })();
</script>
@endsection