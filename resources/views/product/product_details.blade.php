<?php

use App\Http\Controllers\cart\LastseenController;
?>
@extends('layouts.app')
@section('title','Product List')
@section('content')
<style>
    .quantity {
        position: relative;
        margin-right: 15px;
        float: left;
    }
    .quantity input[type=number] {
        -moz-appearance: textfield;
    }
    .quantity input {
        width: 85px;
        height: 45px;
        line-height: 1.65;
        float: left;
        display: block;
        padding: 0;
        margin: 0;
        text-align: center;
        border: 1px solid #e8e8e8;
    }
    .quantity-nav {
        float: left;
        position: relative;
        height: 45px;
    }
    .quantity-button {
        position: relative;
        cursor: pointer;
        border-left: 1px solid #eee;
        border-right: 1px solid #eee;
        width: 20px;
        text-align: center;
        color: #333;
        font-size: 13px;
        line-height: 1.7;
        -webkit-transform: translateX(-100%);
        transform: translateX(-100%);
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
        user-select: none;
        z-index: 999;
        background-color: #fff;
    }
    .quantity-button.quantity-up {
        position: absolute;
        height: 50%;
        top: 0;
        border-top: 1px solid #eee;
        border-bottom: 1px solid #eee
    }
    .quantity-button.quantity-down {
        position: absolute;
        bottom: 0px;
        height: 50%;
        border-bottom: 1px solid #eee
    }
    .btn.btn-lg {
        padding: 12px 45px;
        font-size: 13px;
    }
    .btn.btn-b {
        background: #291d88;
        color: #fff;
        text-transform: uppercase;
    }
</style>
<main class="main shop-page">
    <div class="container">
        <?php
        $i = 0;
        LastseenController::AddLastSeen($product_id, $product_color);
        ?>
        <form action="{{url('/cart/add-to-cart')}}" method="POST">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="product-main row" id="color-image">
                <div class="product-main__left col-12 col-md-6 col-lg-7">
                    <div class="product-main__slider swiper-container js-product-main-slider mb-3">
                        <div class="swiper-wrapper">
                            @foreach($singleproductmultiplepic as $smplist)
                            <div class="product-main__slide swiper-slide position-relative">
                                <a class="product-main__slide-img js-product-zoom" href="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img}}" title="{{$singleproduct->product_name}}" data-source="Pride Limited" data-scale="2.5">
                                    <img class="img-fluid invisible" src="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img}}" alt="#">
                                </a>
                            </div>
                            @endforeach

                        </div>
                        <div class="product-main__slider-control"><a class="product-main__arrow prev js-product-main-prev" href="#"><i class="ion-ios-arrow-left"></i></a><a class="product-main__arrow next js-product-main-next" href="#"><i class="ion-ios-arrow-right"></i></a></div>
                    </div>
                    <div class="product-main__thumbs">
                        <div class="product-main__thumbs-slider swiper-container js-product-main-thumbs">
                            <div class="swiper-wrapper">
                                @foreach($singleproductmultiplepic as $smplist)
                                <div class="product-main__thumb swiper-slide pointer"><img class="img-fluid" src="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img_tiny}}" alt="#">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="product-main__thumbs-control"><a class="product-main__thumb-arrow prev js-product-thumb-prev" href="#"><i class="ion-ios-arrow-left"></i></a><a class="product-main__thumb-arrow next js-product-thumb-next" href="#"><i class="ion-ios-arrow-right"></i></a></div>
                    </div>
                </div>
                <div class="product-main__right col-12 col-md-6 col-lg-5">
                    <div class="product-main__title mb-4">
                        <h3 class="h3 text-uppercase bold mb-2">{{$singleproduct->product_name}}</h3>
                        <div class="item-price mb-3"><span class="currency">Tk </span>{{$singleproduct->product_price}}</div>
                        <!--  <div class="product-rating">
                            <div class="product-rating__stars mr-2 mb-2">
                              <select class="js-product-rating">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4" selected="selected">4</option>
                                <option value="5">5</option>
                              </select>
                            </div>
                            <div class="product-rating__stat mb-0"><span class="text-uppercase">number of ratings</span><span class="d-inline-block align-top px-2">|</span><span class="value">340</span></div>
                          </div> ---->
                    </div>
                    <div class="product-main__intro">
                        <p>{{$singleproduct->product_description}}</p>
                    </div>
                    <ul class="product-main__props">
                        <li class="d-flex"><span class="title mr-2 text-uppercase">Product Code:</span><span>{{$singleproduct->product_code}}</span></li>
                        <li class="d-flex"><span class="title mr-2 text-uppercase">Color:</span>
                            <div class="product-main__props-colors">
                                <select class="form-control" name="productcolor" id="colorName">
                                    @foreach($product_color_image as $color)
                                    <option value="{{$color->productalbum_name}}" <?php
                                    if ($color->productalbum_name == $select_color) {
                                        echo 'selected';
                                    }
                                    ?>>{{$color->productalbum_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                        <li class="d-flex"><span class="title mr-2 text-uppercase">Size:</span>
                            <div class="product-main__props-colors">
                                <select class="form-control" name="productsize" id="ProductSizeList">
                                    @foreach($product_sizes as $size)
                                    <option value="{{$size->productsize_size}}">{{$size->productsize_size}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                    </ul>
                    <div class="product-share">
                        <!--                        <div class="qty">
                                                    <button class="qty__btn pointer js-btn-qty" type="button" data-qty="increment"><i class="fa fa-angle-up"></i></button>
                                                    <input class="qty__input js-input-qty" name="productqty" min="1" type="text" value="1"/>
                                                    <button class="qty__btn pointer js-btn-qty" type="button" data-qty="decrement"><i class="fa fa-angle-down"></i></button>
                                                </div>-->
                        <div class="quantity">
                            @foreach($product_qty as $tqty)
                            <input type="number" min="1" name="productqty" max="{{$tqty->totalqty}}" step="1" value="1" id="pqty"><div class="quantity-nav">
                                <div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div>                                 
                            </div>
                            @endforeach
                        </div>
                        <input class="btn btn-lg btn-block btn-round btn-b" id="add_to_cart" type="submit" name="add" value="add to cart"/>
                        <?php $cart_image = array_reverse($singleproductmultiplepic, true); ?>
                        @foreach($cart_image as $orimage)
                        <input type="hidden" name="productid" id="productId" value="{{$singleproduct->product_id}}">
                        <input type="hidden"  id="selectcolor" value="{{$product_color}}">
                        <input type="hidden" name="productimage" id="productImage" value="{{$orimage->productimg_img_medium}}">
                        @endforeach
                    </div>
                    </form>
                    <div class="product-main__tabs">                
                        <ul class="nav nav-tabs mb-3 js-details-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" id="productDesc-tab" href="#productDesc" data-toggle="tab" role="tab" aria-controls="product Desc" aria-selected="true">description</a></li>
                            <li class="nav-item"><a class="nav-link" id="productReview-tab" href="#productReview" data-toggle="tab" role="tab" aria-controls="productReview" aria-selected="false">review<span> (2)</span></a></li>
                            <li class="nav-item"><a class="nav-link" id="productPayment-tab" href="#productPayment" data-toggle="tab" role="tab" aria-controls="productPayment" aria-selected="false">payment and delivery</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="productDesc" role="tabpanel" aria-labelledby="productDesc-tab">
                                <p>{{$singleproduct->product_description}}</p>
                            </div>
                            <div class="tab-pane fade" id="productReview" role="tabpanel" aria-labelledby="productReview-tab">
                                <section class="comments" id="comments">

                                </section>
                                <form class="order-form js-comment-form mb-0" action="#" method="POST" data-toggle="validator">
                                    <div class="row">
                                        <div class="form-group col-12 col-lg-6">
                                            <input class="form-control" type="text" name="field_name" placeholder="Name" required>
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <input class="form-control" type="email" name="field_email" placeholder="E-mail" required>
                                        </div>
                                        <div class="form-group col-12">
                                            <textarea class="form-control" rows="5" name="field_message" placeholder="Message" required></textarea>
                                        </div>
                                        <div class="form-group col-12 mt-2 mb-4">
                                            <select class="js-rating-select" name="field_raiting">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">5</option>                                                                                                                                                                                                       >4</option>                                                                                                                                                                                                      e="5">5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="comment-form__                                                                                                                                                                                                        footer">
                                        <button class="comment-form__submit btn btn-shop d-block mx-auto" type="submit">Add comment</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="productPayment" role="tabpanel" aria-labelledby="productPayment-tab">
                                <div class="swiper-container js-review-payments-slider">
                                    <div class="swiper-wrapper">
                                        <a class="swiper-slide" href="">Bkash</a>
                                        <a class="swiper-slide" href="">Hand Cash</a>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <section class="shop-products py-0">
                    <div class="container">
                        <div class="shop-st-header text-center">
                            <h2 class="h2 text-uppercase">you recently  <span class="text-second">watched</span>
                            </h2>
                            <h4 class="h4">income in the last 24 hours</h4>
                        </div>
                        <div class="swiper-container js-shop-items">
                            <div class="swiper-wrapper">
                                <?php
                                $i = 0;
                                $lastseen = Cart::instance('lastseen')->content();

                                // $rev_lastseen = krsort($lastseen);
                                //   dd($rev_lastseen);
                                foreach ($lastseen as $row) :
                                    $i++;
                                    $name = $row->name;
                                    $pro_name = str_replace(' ', '-', $name);
                                    $id = $row->id;
                                    $color = $row->options->color;
                                    ?>
                                    <div class="product-grid swiper-slide">
                                        <div class="product-item row js-shop-item-product">
                                            <div class="product-item__grid col-5 col-sm-5 col-lg-4 mb-4">
                                                <div class="product-item__img"><a class="product-item__img-link js-shop-img-flip d-block" href="{{url("shop/{$pro_name}/color-{$color}/{$id}")}}">
                                                        <span class="front"><img class="img-fluid" src="{{ URL::to('') }}/storage/app/pgallery/<?php echo ($row->options->has('product_image') ? $row->options->product_image : ''); ?>" alt="#" title="<?php echo $row->name; ?>"/></span>
                                                        <span class="back"><img class="img-fluid" src="{{ URL::to('') }}/storage/app/pgallery/<?php echo ($row->options->has('product_image') ? $row->options->product_image : ''); ?>" alt="#" title="<?php echo $row->name; ?>"/></span></a>
                                                </div>
                                            </div>
                                            <div class="product-item__bottom">
                                                <div class="product-item__title h4 text-uppercase mb-2"><a href="{{url("shop/{$pro_name}/color-{$color}/{$id}")}}"><?php echo $row->name; ?></a></div>
                                                <div class="product-item__price mb-0"><span class="currency">Tk</span><span> <?php echo $row->price; ?></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endforeach;
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
                </main>
                <span id="display" style="display: none;">{{session('view_bag')}}</span>
                <!----- Modal for View Bag ----->
                <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="view_bag" 
                     style="color:#ffffff; margin-top:20px;">
                    <div class="modal-dialog" style="width: 28%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <center>  <h4 class="modal-title" style="color:black;">{{session('cart_product_name')}}</h4></center>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <style>
                                    .cart_img{
                                        height: 209px !important;
                                    }
                                </style>
                                <div class="bodyrow"><img class="cart_img" src="{{ URL::to('') }}/storage/app/pgallery/{{session('cart_product_image')}}" alt="product name"  height="400px" /><br>
                                    <div class="clear"></div
                                </div> 
                                <span class="font-alt" style="color:black"><div class="panel-body"><strong>Price: {{session('cart_product_price')}}Tk</strong></div></span>
                            </div>
                            <div class="modal-footer">
                                <div class="footerrow">
                                    <a href="{{url('/shop-cart')}}" class="btn btn-sm btn-info" id="delete_link">VIEW BAG</a>
                                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(document).ready(function () {

                    var product_id = $("#productId").val();
                    var color_name = $("#selectcolor").val();
                    // alert(color_name);
                    // getSizeByColor(color_name);

                    $('#colorName').on('change', function (e) {
                        var color_name = e.target.value;
                        // alert(color_name);
                        getSizeByColor(color_name);
                        getColorbyImage(product_id, color_name);
                    });

                    $('#pqty').on('change', function (e) {
                        var pqty = e.target.value;
                        $('#productQty').val(pqty);
                    });
                    $('#ProductSizeList').on('change', function (e) {
                        var productSize = e.target.value;
                        $('#productSize').val(productSize);
                        getQtyByColorSize(product_id, color_name, productSize);
                    });
                    $('#colorName').on('change', function (e) {
                        var productColor = e.target.value;
                        $('#productColor').val(productColor);
                    });

                    function getSizeByColor(color_name) {
                        var url_op = base_url + "/getSizeByColor/" + product_id + '/' + color_name;
                        $.ajax({
                            url: url_op,
                            type: 'GET',
                            dataType: 'json',
                            data: '',
                            success: function (data)
                            {
                                //   alert(data);
                                $('#ProductSizeList').empty();
                                $('#ProductSizeList').append('<option value="">Select Size</option>');
                                $.each(data, function (index, subcatobj) {
                                    //   $('#ProductSizeList').append('<option value="' + subcatobj.productsize_id + '">' + subcatobj.productsize_size + '</option>');

                                    $('#ProductSizeList').append('<option value="' + subcatobj.productsize_size + '">' + subcatobj.productsize_size + '</option>');
                                });
                            }
                        });
                    }

                    function  getQtyByColorSize(product_id, color_name, productSize) {
                        var url_op = base_url + "/ajaxcall-getQuantityByColor/" + product_id + '/' + productSize + '/' + color_name;
                        $.ajax({
                            url: url_op,
                            type: 'GET',
                            dataType: 'json',
                            data: '',
                            success: function (html) {
                                //  alert(html);
                                var qty = html[0].SizeWiseQty;
                                //  alert(qty);
                                if (qty > 0) {
                                  //  alert(qty);
                                   // $('#pqty').val(qty);
                                    $('#pqty').val(1);
                                    $('#pqty').attr({"max": qty});
                                    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
                                    jQuery('.quantity').each(function () {
                                        var spinner = jQuery(this),
                                                input = spinner.find('input[type="number"]'),
                                                btnUp = spinner.find('.quantity-up'),
                                                btnDown = spinner.find('.quantity-down'),
                                                min = input.attr('min'),
                                                max = input.attr('max');

                                        btnUp.click(function () {
                                            var oldValue = parseFloat(input.val());
                                            if (oldValue >= max) {
                                                var newVal = oldValue;
                                            } else {
                                                var newVal = oldValue + 1;
                                            }
                                            spinner.find("input").val(newVal);
                                            spinner.find("input").trigger("change");
                                        });

                                        btnDown.click(function () {
                                            var oldValue = parseFloat(input.val());
                                            if (oldValue <= min) {
                                                var newVal = oldValue;
                                            } else {
                                                var newVal = oldValue - 1;
                                            }
                                            spinner.find("input").val(newVal);
                                            spinner.find("input").trigger("change");
                                        });

                                    });
                                    document.getElementById("add_to_cart").disabled = false;
                                } else {
                                    $('#pqty').val(0);
                                   // $('#pqty').attr({"max": 0});
                                    document.getElementById("add_to_cart").disabled = true;
                                    //  alert('Not enough stock');
                                }
                            }
                        });
                    }
                    function getColorbyImage(product_id, color_name) {
                        var url_op = base_url + "/getImageByColor/" + product_id + '/' + color_name;
                        $.ajax({
                            url: url_op,
                            type: 'GET',
                            dataType: 'json',
                            success: function (data)
                            {
                                //   alert(data);
                                $.each(data, function (index, subimg) {
                                    $('#color-image').append('<img src="' + base_url + '"/storage/app/pgallery/"' + subimg.productimg_img + '/>');
                                });
                                //   $("#color-image").html(data);

                            }
                        });
                    }
                    
                });
            </script>
            @endsection