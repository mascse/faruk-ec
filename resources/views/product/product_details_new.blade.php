<?php

use App\Http\Controllers\cart\LastseenController;
use App\Http\Controllers\product\ProductController;
?>
@extends('layouts.app')
@section('title',$title)
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/product_details.css')}}?10">
<style>
    .dropdown-menu{
        display:none;
    }
    a.simple.button {
        margin-bottom:15px;
    }
    .product-shop {
    margin: 0 0 15px;
    padding: 0 19px;
    padding-top: 16px; 
}
.dropdown-menu>li:first-child {
    border: 1px solid #ddd!important;
    padding: 11px;
}
.swatch {
    border: 2px solid #eee;
    padding: 1px;
}
.selected-color {
    border: 3px solid #291e88!important;
    padding: 2px;
}
@media (max-width: 1000px){
.dropdown-menu > li {
    border: 1px solid #ddd !important;
    padding: 11px !important;
}
.dropdown-menu>li {
    /* border: none; */
    /* padding: 18px 8px 1px 12px; */
}
}
#loading-image {
    position:fixed;
    left:0;
    top:0;
    width:100vw;
    height:100vh;
    background:#333;
    opacity:0.8;
    margin:0;
    z-index:999;
}
#loading-image img {
    position:absolute;
    left:50%;
    top:50%;
    width:auto;
    height:auto;
    transform:translate(-50%, -50%);
    opacity:1;
}
</style>
<!-- Magic Zoom Plus Magento 2 module version v1.5.20 [v1.6.64:v5.2.4] -->
<link type="text/css" href="{{asset('assets/MagicZoomPlus/magiczoomplus.css')}}" rel="stylesheet" media="screen" />
<link type="text/css" href="{{asset('assets/MagicZoomPlus/magiczoomplus.module.css')}}" rel="stylesheet" media="screen" />
<script type="text/javascript" src="{{asset('assets/MagicZoomPlus/magiczoomplus.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/MagicZoomPlus/magictoolbox.utils.js')}}"></script>
<script type="text/javascript">
var mzOptions = {
    'zoomWidth': 'auto',
    'zoomHeight': 'auto',
    'zoomPosition': 'right',
    'zoomDistance': 15,
    'selectorTrigger': 'click',
    'transitionEffect': true,
    'lazyZoom': false,
    'rightClick': true,
    'zoomMode': 'zoom',
    'zoomOn': 'hover',
    'upscale': true,
    'smoothing': true,
    'variableZoom': false,
    'zoomCaption': 'off',
    'expand': 'window',
    'expandZoomMode': 'zoom',
    'expandZoomOn': 'click',
    'expandCaption': true,
    'closeOnClickOutside': true,
    'cssClass': '',
    'hint': 'once',
    'textHoverZoomHint': 'Hover to zoom',
    'textClickZoomHint': 'Click to zoom',
    'textExpandHint': 'Click to expand',
    'textBtnClose': 'Close',
    'textBtnNext': 'Next',
    'textBtnPrev': 'Previous'
}
</script>
<script type="text/javascript">
    var mzMobileOptions = {
        'zoomMode': 'zoom',
        'textHoverZoomHint': 'Touch to zoom',
        'textClickZoomHint': 'Double tap to zoom',
        'textExpandHint': 'Tap to expand'
    }
</script>

<main id="maincontent" class="page-main">
    <div v-if="loading_image" id="loading-image">
        <img src="{{ url('/') }}/storage/app/public/loader.gif" Alt="Loading..." />
    </div>
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="breadcrumbs">
                        <ul class="items">
                            <!--<li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>-->
                            <li class="item">
                                <a href="#" title="Main category"><?php echo $main_cate; ?></a>
                            </li>
                            <li class="item">
                                <a href="#" title="Sub Category"><?php echo $category_name; ?></a>
                            </li>
                            <li class="item">
                                <a href="#" title="Child sub category"><?php echo $subcategory_name; ?></a>
                            </li>
                            <!--<li class="item">
                                <a href="" title=""><?php echo $title; ?></a>
                            </li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" id="vue-product">
        <div class="row">
            <div class="col-md-12">
                <input name="form_key" type="hidden" value="WOMjPmq3TsCweB3O" />
                <div class="product-detail">
                    <div class="row">
                        <div class="col-sm-7  col-lg-5">
                            <div class="gallery-detail">
                                <a id="gallery-prev-area" tabindex="-1"></a>
                                <div class="action-skip-wrapper"><a class="action skip gallery-next-area" href="#gallery-next-area"><span>Skip to the end of the images gallery</span></a>
                                </div><div class="action-skip-wrapper"><a class="action skip gallery-prev-area" href="#gallery-prev-area"><span>Skip to the beginning of the images gallery</span></a>
                                </div><a id="gallery-next-area" tabindex="-1"></a>
                                <div class="MagicToolboxContainer selectorsRight minWidth" data-mage-init='{"magicToolboxThumbSwitcher": {"playIfBase":0,"showRelated":0,"videoAutoRestart":0,"tool":"magiczoomplus","switchMethod":"click","productId":"90363"}}'>
                                    <div class="MagicToolboxMainContainer">
                                        <div id="mtImageContainer" style="display: block;">
                                            <div>
                                                <?php
                                                $i = 0;
                                                foreach ($singleproductmultiplepic as $smplist) {
                                                    $i++;
                                                    if ($i == 1) {
                                                        ?>
                                                        <a id="MagicZoomPlusImage-product-90363"  class="MagicZoom" href="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" title="{{$singleproduct->product_name}}" data-options="lazyZoom:true;">
                                                            <img itemprop="image" src="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}"   alt="{{$singleproduct->product_name}}" />
                                                        </a>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div id="mt360Container" style="display: none;"></div>
                                        <div id="mtVideoContainer" style="display: none;"></div>    
                                    </div>
                                    <div class="MagicToolboxSelectorsContainer" style="flex-basis: 88px; width: 88px;">
                                        <div id="MagicToolboxSelectors90363" class="">
                                            <!--                                            active-selector-->
                                            @foreach($singleproductmultiplepic as $smplist)	
                                            <a class="mt-thumb-switcher  " data-zoom-id="MagicZoomPlusImage-product-90363" href="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img}}"  data-image="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img}}" title="{{$singleproduct->product_name}}">
                                                <img src="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img_tiny}}"  alt="{{$singleproduct->product_name}}" />
                                            </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    if (window.matchMedia("(max-width: 767px)").matches) {
                                        $scroll = document.getElementById('MagicToolboxSelectors90363');
                                        if ($scroll && typeof $scroll != 'undefined') {
                                            $attr = $scroll.getAttribute('data-options');
                                            if ($attr !== null) {
                                                $scroll.setAttribute('data-options', $attr.replace(/orientation *\: *[a-zA-Z]{1,}/gm, 'orientation:horizontal'));
                                            }
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                        <div class="col-sm-5 col-lg-4">
                            <div class="product-info-main">
                                <div class="page-title-wrapper product">
                                    <div class="page-title-wrapper">
                                        <div>
                                            <h1 class="page-title">
                                                <span class="base" data-ui-id="page-title-wrapper" >{{$singleproduct->product_name}}</span>
                                            </h1>
                                        </div>
                                        <?php if ($category_name == 'Pride Girls') { ?>
                                            <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_girl.png" class="brand-img" style="width:175px;height:auto;"/>
                                            </div>
                                        <?php } elseif ($category_name == 'Pride Classic') { ?>
                                            <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_classic.png" class="brand-img" style="width:175px;height:auto;"/>
                                            </div>
                                        <?php } elseif ($category_name == 'Pride Ethnic Menswear Panjabi RF' || $category_name =='Pride Ethnic Menswear Panjabi SF') { ?>
                                            <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_menswear.png" class="brand-img" style="width:175px;height:auto;"/>
                                            </div>
                                        <?php }elseif($category_name =='Pride Kids'){ ?>
                                         <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_kids.png" class="brand-img" style="width:175px;height:auto;"/>
                                          </div>
                                         <?php }elseif($category_name=='Signature Kameez' || $category_name =='Signature Three Piece Suit' || $category_name=='Signature Sari'){ ?>
                                         <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                 <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_signature.png" class="brand-img" style="width:175px;height:auto;"/>
                                          </div>
                                         <?php } ?>
                                    </div>
                                </div>
                                <div class="product-info-price">
                                    <div class="product-info-stock-sku">
                                        <div class="product attribute sku">
                                            <strong class="type">Code</strong>    
                                            <div class="value" itemprop="code">{{$singleproduct->product_code}}</div>
                                             <div class="font-alt product_barcode" style="display:none;font-size:8pt;text-transform: none;padding-top:5px;"><span> Barcode : </span> <span id="barcode"></span></div>
                                        </div>
                                    </div>
                                    <div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$singleproduct->product_id}}" data-price-box="product-id-{{$singleproduct->product_id}}">
                                        <span class="price-container price-final_price tax weee"
                                              itemprop="offers" itemscope itemtype="#">
                                            <span  id="product-price-{{$singleproduct->product_price}}" data-price-amount="{{$singleproduct->product_price}}"
                                                   data-price-type="finalPrice"
                                                   class="price-wrapper "
                                                   itemprop="price">
                                                <span class="price"> 
                                                <?php
                                            if ($singleproduct->product_pricediscounted < 1) {
                                                echo 'Tk' . ' ' . $singleproduct->product_price . '<br>';
                                            } else {
                                                echo '<div class="FColor">' . 'Tk' . ' ' . $singleproduct->discount_product_price . '</div>';
                                                ?>
                                                                                <!--<div class="sale-flash"><?php $singleproduct->discount_product_price . '%'; ?>  Off*</div>-->
                                                <?php
                                                echo '<div class="offer-rate"><del> Tk' . $singleproduct->product_price . '</del>'; echo '&nbsp;&nbsp;'.$singleproduct->product_pricediscounted . '% OFF</div>'; 
                                            }
                                            ?>
                                                
                                                </span>   
                                                   
                                            </span>
                                            <meta itemprop="priceCurrency" content="TK" />
                                        </span>
                                    </div>
                                </div>
                                <div class="cartForm">
                                    <style>
                                        .product-info-main .sizechart { display : none; }
                                    </style>
                                    <div class="product-add-form">
                                        <form action="{{url('/cart/add-to-cart')}}" method="post" id="product_addtocart_form">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="productid" value="{{$singleproduct->product_id}}" />
                                            <input type="hidden" name="productcolor" id="selectcolor" value="{{$product_color}}">
                                            <input type="hidden" name="productimage" id="productImage" value="{{$cart_image->productimg_img_thm}}">
                                            <div class="product-options-wrapper" id="product-options-wrapper" data-hasrequired="* Required Fields">
                                                <div class="fieldset" tabindex="0">
                                                    <div class="swatch-opt" data-role="swatch-options">
                                                        <div class="swatch-attribute size" attribute-code="size" attribute-id="142" option-selected="273">
                                                            <span id="option-label-size-142" class="swatch-attribute-label">Color</span>
                                                            <span class="swatch-attribute-selected-option">{{ $product_color }} </span>
                                                            <?php
                                                            $product_name = str_replace(' ', '-', $singleproduct->product_name);
                                                            $product_url = strtolower($product_name);
                                                            ?>
                                                            <?php
                                                            $i = 0;
                                                            foreach ($product_color_image as $color) {
                                                                $swatch_color_album=str_replace('/','-',$color->productalbum_name);
                                                                $i++;
                                                                ?>
                                                                <?php
                                                                if ($i == 5) {
                                                                    echo "</br></br>";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>
                                                                <a class="pull-right"  style="overflow:hidden; width:30px; height:30px; border-radius:50%; display:inline-block;margin: 0 5px 6px 0px;" href='{{url("shop/{$product_url}/color-{$swatch_color_album}/{$singleproduct->product_id}")}}'>
                                                                    <img style="width:30px; height:30px; display:inline-block; border-radius:50%" src="{{ URL::to('') }}/storage/app/public/pgallery/{{$color->productalbum_img}}" class="img-responsive swatch <?php if ($product_selected_color->productalbum_img == $color->productalbum_img) echo 'selected-color'; ?>"/>
                                                                </a>
                                                               &nbsp;&nbsp;
                                                            <?php } ?>
                                                            <input class="swatch-input super-attribute-select" type="text" value="{{$product_color}}" data-selector="productsize" data-validate="{required: true}" aria-required="true" aria-invalid="false" data-attr-name="size">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-options-wrapper" id="product-options-wrapper" data-hasrequired="* Required Fields">
                                                <div class="fieldset" tabindex="0">
                                                    <div class="swatch-opt" data-role="swatch-options">
                                                        <div class="swatch-attribute size" attribute-code="size" attribute-id="142" option-selected="273">
                                                            <span id="option-label-size-142" class="swatch-attribute-label">Size</span>
                                                            <span id="product-size-text" class="swatch-attribute-selected-option">{{$product_sizes[0]->productsize_size}}</span>
                                                            <div aria-activedescendant="option-label-size-142-item-273" tabindex="0" aria-invalid="false" aria-required="true" role="listbox" aria-labelledby="option-label-size-142" class="swatch-attribute-options clearfix">
                                                                <!--selected-->
                                                                @php($i = 0)
                                                                @foreach($product_sizes as $size)
                                                                <div class="swatch-option text <?php
																	if ($size->SizeWiseQty < 1)
																		echo 'disabled-size';
																	elseif ($i == 0)
																		echo 'active';
																	?>" aria-checked="false" v-on:click="swatchoption($event)" v-bind:value="{ size_input: {{$size->productsize_size}} }" id="{{$size->productsize_size}}" aria-label="{{$size->productsize_size}}" option-tooltip-thumb="" option-tooltip-value="{{ $size->productsize_size }}" role="option">
																	{{ $size->productsize_size }}
															    </div>
                                                                @php($i++)
                                                                @endforeach
                                                            </div>
															
                                                            <input id="product-size-input" class="swatch-input super-attribute-select" name="productsize" type="text" value="{{$product_sizes[0]->productsize_size}}" data-selector="productsize" data-validate="{required: true}" aria-required="true" aria-invalid="false" data-attr-name="size">
                                                       
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-tocart">
                                                <div class="fieldset">
                                                    <div class="field qty">
                                                        <label class="label" for="qty"><span>Quantity</span></label>
                                                        <div class="control ">
                                                            <div class="inputnumber">
                                                                <a href="#" class="plusminus" id="plus" v-on:click="addQty($event)"><span>+</span></a>
                                                                <input type="number" name="productqty" id="qty" min="1" max="{{$product_qty}}" value="1" title="Qty" class="input-text qty"/>
                                                                <a href="#" class="plusminus" id="minus" v-on:click="minusQty($event)"><span>-</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="color:red;font-weight:700;padding-bottom:10px;" id="total_sold_out_msg"></div>
                                                     <div style="color:red;font-weight:700;padding-bottom:10px;" id="sold_out_msg"></div>
                                                    <div class="actions">
                                                        <button onClick="startLoading()" id="add-cart" type="submit"
                                                                title="Add to Cart"
                                                                class="action primary tocart"
                                                                id="product-addtocart-button">
                                                            <span class="pride.add_to_cart">Add to Cart</span>
                                                        </button>
                                                        <div id="instant-purchase" data-bind="scope:'instant-purchase'">
                                                        </div>
                                                        <div class="size-popup">
                                                            <div class="popup">
                                                                <div class="video-area">
                                                                    <div class="video-holder">
                                                                        <div class="video-frame">
                                                                            <div class="size-chart-tabel"><h1>CLOTHING SIZE CHART GIRLS</h1> 
                                                                                <table width="100%" border="1"> 
                                                                                    <tr class="even"> 
                                                                                        <th scope="col">GIRLS</th> 
                                                                                        <th scope="col">MEASURE YOUR</th> 
                                                                                        <th scope="col">XS</th> 
                                                                                        <th scope="col">S</th> 
                                                                                        <th scope="col">M</th> 
                                                                                        <th scope="col">L</th> 
                                                                                        <th scope="col">XL</th> 
                                                                                        <th scope="col">XXL</th> 
                                                                                        <th scope="col">XXXL</th> 
                                                                                    </tr> 
                                                                                    <tr > 
                                                                                        <th scope="row">REGULAR FIT TUNIC</th> 
                                                                                        <th scope="row">CHEST</th> 
                                                                                        <td></td> 
                                                                                        <td>38"</td> 
                                                                                        <td>39.5"</td> 
                                                                                        <td>41"</td> 
                                                                                        <td>42.5"</td> 
                                                                                        <td>44"</td> 
                                                                                        <td>45.5"</td>  
                                                                                    </tr> 
                                                                                    <tr class="even"> 
                                                                                        <th scope="row">TAILORED FIT TUNIC</th> 
                                                                                        <th scope="row">CHEST</th> 
                                                                                        <td></td> 
                                                                                        <td>36"</td> 
                                                                                        <td>37.5"</td> 
                                                                                        <td>39"</td> 
                                                                                        <td>40.5"</td> 
                                                                                        <td>42"</td> 
                                                                                        <td>43.5"</td> 
                                                                                    </tr> 
                                                                                    <tr > 
                                                                                        <th scope="row">PANT</th>
                                                                                        <th scope="row">WAIST</th> 
                                                                                        <td>26"</td> 
                                                                                        <td>28"</td> 
                                                                                        <td>30"</td> 
                                                                                        <td>32"</td> 
                                                                                        <td>34"</td> 
                                                                                        <td>36"</td> 
                                                                                        <td>38"</td> 
                                                                                    </tr> 
                                                                                    <tr class="even"> 
                                                                                        <th scope="row">LEGGINGS</th> 
                                                                                        <th scope="row">WAIST</th> 
                                                                                        <td></td> 
                                                                                        <td>26"</td> 
                                                                                        <td>27"</td> 
                                                                                        <td>28"</td> 
                                                                                        <td>29"</td> 
                                                                                        <td>33"</td> 
                                                                                        <td>31"</td> 
                                                                                    </tr> 
                                                                                </table> 
                                                                                <span class="note">*Measurements in inches</span> 
                                                                            </div>                                        
                                                                            <a class="close icon-delete-outline" href="#"><span>Close</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <a class="sizechart open" href="#popup1">size chart</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!--<script type="text/javascript">
                                                jQuery('#plus').click(function (e) {
													alert(e);
                                                    var currentval = jQuery('#qty').val();
                                                    var newValue = parseInt(currentval) + 1;
                                                    jQuery('#qty').val(newValue);
                                                    e.preventDefault();
                                                });
                                                jQuery('#minus').click(function (e) {
                                                    var currentval1 = jQuery('#qty').val();
                                                    var newValue1 = currentval1 - 1;
                                                    if (newValue1 > 0) {
                                                        jQuery('#qty').val(newValue1);
                                                    }
                                                    e.preventDefault();
                                                });
                                            </script> --->

                                        </form>
                                    </div>
                                </div>
                               <div id="find-store-section" style="position:relative; min-height:50px;">
									<a class="simple button" href="#" v-on:click="findinstore($event)" title="{{$singleproduct->product_name}}">
										<span class="sprite-left store-locater sprite-left"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
										Find In Store
									</a>
									<span class="find_store_close" v-on:click="findinstoreclose($event)">X</span>
								</div> 
                                <div class="searchstore">
                                    <div id="preferred-store-panel">
										<div class="dropdown">
											<button id="select-district" class="btn btn-primary dropdown-toggle"  style="width:100%; background-color:#291e88; border-color:#291e88" type="button" data-toggle="dropdown">Select District <span class="caret"></span></button>
											<ul class="dropdown-menu" style="width:100%" id="find_in_city">
												<!--<li style="cursor:pointer" v-on:click="getStore()" class="list-group-item">Dhaka</li> --->
												<li v-for="d in city_list" style='cursor:pointer' v-on:click='getStore' class='list-group-item'>@{{d.district}}</li>
											</ul>
										</div>
                                        <span class="stockmsg">Please note availability may change</span>
                                    </div>
                                    <div  class="searchresults">
                                        <div id="store-list-loading" style="text-align:center; padding-bottom:10px; display:none"><img style="width:30px; margin:auto" src="{{url('/')}}/storage/app/public/loading.gif" /></div>
                                        <div class="store-list-container" id="store-list-container">
                                            
                                        </div>
                                        <div class="store-list-pagination">
                                        </div></div>
                                </div>
                                <div class="product-shop">    
                                    <div class="product info detailed">
                                        <div class="content">
                                            <div class="product data items" data-mage-init='{"tabs":{"openedState":"active"}}'>
                                                <div class="data item title"
                                                     aria-labeledby="tab-label-product.info.overview-title"
                                                     data-role="collapsible" id="tab-label-product.info.overview">
                                                    <a class="data switch"
                                                       tabindex="-1"
                                                       data-toggle="switch"
                                                       href="#product.info.overview"
                                                       id="tab-label-product.info.overview-title">
                                                        Description                    
                                                    </a>
                                                </div>
                                                <div class="data item content" id="product.info.overview" data-role="content">
                                                    <div class="product attribute overview">
                                                        <div style="font-size:14px" class="value" itemprop="description">{{ $singleproduct->product_description }}</div>
                                                    </div>
                                                </div>
                                                <!--<div class="data item title"
                                                     aria-labeledby="tab-label-additional-title"
                                                     data-role="collapsible" id="tab-label-additional">
                                                    <a class="data switch"
                                                       tabindex="-1"
                                                       data-toggle="switch"
                                                       href="#additional"
                                                       id="tab-label-additional-title">
                                                        Details                   
                                                    </a>
                                                </div> --->
                                                <div class="data item content" id="additional" data-role="content">
                                                    <div class="additional-attributes-wrapper table-wrapper">
                                                        <table class="data table additional-attributes" id="product-attribute-specs-table">
                                                            <!--<caption class="table-caption"></caption>-->
                                                            <tbody>
                                                                <tr>
                                                                    <th class="col label" style="font-size:16px" scope="row">Care</th>
                                                                    <td class="col data" data-th="Material">{{ $singleproduct->product_care }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social-networks">
                                    <strong class="label">share this product</strong>
                                    <ul class="social-icons">
                                        <?php $current_url=URL::current();?>
                                        <li><a class="tw icon-social-twitter" href="javascript:window.open('https://twitter.com/share?url={{$current_url}}&text={{$singleproduct->product_name}}', 'twitter', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Tweet"><span>Twitter</span></a></li> 
                                        <li><a class="fb icon-facebook"  href="javascript:window.open('https://www.facebook.com/dialog/share?app_id=258246005086458&display=popup&href={{$current_url}}&redirect_uri={{$current_url}}', 'facebook', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Share on Facebook"><span>facebook</span></a></li>
                                        <!-- li><a class="pn icon-pinterest-p" href="javascript:window.open('https://pinterest.com/pin/create/button/?url=&media=&description=MA18406', 'pinterest', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Pin it"><span>Pinterest</span></a></li -->
                                        <!--<li> <a class="wa fa fa-whatsapp" href="whatsapp://send?text=https://www.pride-limited.com/ma18406-beige-3pc.html" data-action="share/whatsapp/share"><span>Share via Whatsapp</span></a></li> 
                                        https://www.facebook.com/dialog/share?app_id=258246005086458&display=popup&href={{$singleproduct->product_id}}&redirect_uri=https://pride-limited.com/
                                        --->
                                    </ul>
                                </div>
                            </div></div><div class="col-sm-12 col-lg-3"><div class="match-with-sec">


                                <div class="block related" data-mage-init='{"relatedProducts":{"relatedCheckbox":".related.checkbox"}}' data-limit="3" data-shuffle="1">
                                    <div class="block-title title">
                                        <strong id="block-related-heading" role="heading" aria-level="2">Top Picks</strong>
                                    </div>
                                    <div class="block-content content" aria-labelledby="block-related-heading">
                                        <div class="products wrapper grid products-grid products-related">
                                            <ol class="products list items product-items">
                                                <?php
												$CartItems = Cart::instance('products')->content();
                                                $top_pick = LastseenController::TopPicks($main_cate, $product_id, $sub_cate_id);
                                                foreach ($top_pick as $top) {
                                                $product_name = str_replace(' ', '-', $top->product_name);
                                                $product_url = strtolower($product_name);
                                                $top_color_album=str_replace('/','-',$top->productalbum_name);
                                                 $medium_image = ProductController::GetProductImage($top->product_id);
                                                    ?>
                                                    <li class="item product product-item">                                
                                                        <div class="product-item-info related-available">
                                                            <!-- related_products_list-->                    
                                                            <a href="{{url("shop/{$product_url}/color-{$top_color_album}/{$top->product_id}")}}" class="product photo product-item-photo">
                                                                <span class="product-image-container">
                                                                    <span class="product-image-wrapper">
                                                                        <img class="product-image-photo"
                                                                             src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $medium_image->productimg_img_medium; ?>" alt=" {{$top->product_name}}"/></span>
                                                                </span>
                                                            </a>
                                                            <div class="product details product-item-details">
                                                                <strong class="product name product-item-name">
                                                                    <a class="product-item-link" title="{{$top->product_name}}" href="{{url("shop/{$product_url}/color-{$top_color_album}/{$top->product_id}")}}">
                                                                        {{$top->product_name}}
                                                                    </a>
                                                                </strong>
                                                                <div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$top->product_id}}" data-price-box="product-id-{{$top->product_id}}">
                                                                    <span class="price-container price-final_price tax weee">
                                                                        <span  id="product-price-{{$top->product_price}}" data-price-amount="{{$top->product_price}}" data-price-type="finalPrice" class="price-wrapper ">
                                                                            <span class="price">Tk {{$top->product_price}} </span>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                <div class="field choice related">
                                                                    <input type="checkbox" class="checkbox related" id="related-checkbox75234" name="related_products[]" value="{{$top->product_id}}" />
                                                                    <label class="label" for="related-checkbox75234"><span>Shop Now</span></label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ol>
                                        </div>
                                       <!-- <a class="btn-next" href="javascript:void(0);">next</a>
                                        <a class="btn-prev" href="javascript:void(0);">prev</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block upsell" data-mage-init='{"upsellProducts":{}}' data-limit="4" data-shuffle="1">
                    <div class="block-title title">
                        <strong id="block-upsell-heading" role="heading" aria-level="2">You may also like</strong>
                    </div>
                    <div class="block-content content" aria-labelledby="block-upsell-heading">
                        <div class="products wrapper grid products-grid products-upsell">
                            <ol class="products list items product-items">
                                <?php
                                $may_like = LastseenController::YouMayLike($main_cate, $product_id, $sub_cate_id);
                                foreach ($may_like as $product) {
                                    $product_name = str_replace(' ', '-', $product->product_name);
                                    $product_url = strtolower($product_name);
                                    $data = ProductController::GetProductColorAlbum($product->product_id);
                                    $color_album=str_replace('/','-',$product->productalbum_name);
                                    // dd($data);
                                    $sold_out = ProductController::ProductWiseQty($product->product_id);
                                    foreach ($data as $pro_album) {
                                        $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                                    }
                                     $images_last = ProductController::productImages($pro_album->productalbum_id); 
                                  //   dd($images);
                                    ?>
                                    <li class="item product product-item">                                
                                        <div class="product-item-info ">
                                            <!-- upsell_products_list-->                    
                                            <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}" class="product photo product-item-photo">
                                                <span class="product-image-container">
                                                    <span class="product-image-wrapper">
                                                        <span class="custom-carousel" onmouseenter="fadeImages(this)" onmouseleave="removeTimer(this)">
                                                            @php($i = 0)
                                                            @foreach($images_last as $image_l)
                                                            <img class="item large_img<?php if($i==0) echo ' active';?>" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $image_l->productimg_img_medium }}" alt="No Image Found"/>
                                                            @php($i++)
                                                            @endforeach
                                                        </span>
                                                        </span>
                                                </span>
                                            </a>
                                            <div class="product details product-item-details">
                                                <strong class="product name product-item-name"><a class="product-item-link" title="{{$product->product_name}}" href="{{url("shop/{$product_url}/color-{$color_album}/{$product->product_id}")}}">
                                                        {{$product->product_name}}</a>
                                                </strong>
                                                <div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$product->product_id}}" data-price-box="product-id-{{$product->product_id}}">
                                                    <span class="price-container price-final_price tax weee">
                                                        <span  id="product-price-{{$product->product_price}}" data-price-amount="{{$product->product_price}}" data-price-type="finalPrice" class="price-wrapper ">
                                                            <span class="price">Tk {{$product->product_price}} </span>    
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ol>
                        </div>
                        <a class="btn-next" href="javascript:void(0);"><img style="width:34px" src="{{url('/')}}/storage/app/public/icon_next.svg"/>Next</a>
                        <a class="btn-prev" href="javascript:void(0);"> <img style="width:34px" src="{{url('/')}}/storage/app/public/icon_prev.svg"/>prev</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Modal -->
<div class="modal fade" id="proceed-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="alertCartModal"><?php $check_stock=session('stock_out_message'); if($check_stock==1){ echo 'Your product has been added to the cart !';}else{ ?>
                       <span style="color:red;">{{session('stock_out_message')}}</span>
                    <?php } ?> </div>
                    <div class="col-sm-6 col-md-4 col-xs-5">
                        <img style="border: 1px solid #eee;" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $singleproductmultiplepic[0]->productimg_img }}" alt="" />
                    </div>
                    <div class="col-sm-6 col-md-8 col-xs-7">
                        <div class="page-title-wrapper product">
                            <div class="page-title-wrapper">
                                <div>
                                    <h1 class="page-title">
                                        <span class="base title_mobile" data-ui-id="page-title-wrapper" >{{$singleproduct->product_name}}</span>
                                    </h1>
                                </div>
                            </div>
                        </div>
                         <div class="product-info-price">
                            <div class="product-info-stock-sku">
                                <div class="product attribute sku" style="color: black;font-size:11px;">
                                    <strong class="type">Style Code</strong>    
                                    <div class="value" itemprop="sku">{{$singleproduct->product_code}}</div>
                                    <!--<br><strong class="type">Barcode</strong>    
                                    <div class="value" itemprop="sku">{{session('cart_product_barcode')}}</div> -->
                                </div>
                            </div>
                         </div>
                        <p><span></span><span style="font-weight: 600;">Tk {{session('cart_product_price')}}</span></p>
                        <p><span>Color - </span><span style="font-weight: 600;">{{session('cart_product_color')}}</span></p>
                        <p><span>Size - </span><span style="font-weight: 600;">{{session('cart_product_size')}}</span></p>
                    </div>
                    <!--<div class="col-xs-6" style="position:absolute;bottom:20px;right:0">
                        <a style="color:#291e88" href="{{ url('shop-cart') }}">Checkout</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="text-danger" data-dismiss="modal">Close</a>
                    </div> -->
                </div>
                <!--<table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th>Color</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$singleproduct->product_name}}</td>
                            <td><img style="width:50px" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $singleproductmultiplepic[0]->productimg_img }}" alt="" /></td>
                            <td><img src="{{ URL::to('') }}/storage/app/public/pgallery/{{$product_selected_color->productalbum_img}}" style="width:30px; height:30px; display:inline-block; border-radius:50%;"/></td>
                        </tr>
                    </tbody>
                </table>-->
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="{{ url('shop-cart') }}" style="color:#1d0d9e;border-bottom:1px solid #1d0d9e;">Checkout</a> &nbsp;&nbsp; | &nbsp;&nbsp;
                <a href="#" data-dismiss="modal" style="color:red;border-bottom:1px solid #1d0d9e;">Cancel</a>
            </div>
        </div>
    </div>
</div>
</div>
<script>
  var product_price = "{{$singleproduct->product_price}}";
  var product_name = "{{$product_url}}";
  var product_id = "{{$product_id}}";
  var color_name = "{{$product_color}}";
  var product_code = "{{$singleproduct->product_code}}";
  var cart_items={!! $CartItems !!};
  console.log(cart_items);
</script>
<script src="{{asset('assets/js/vue-product.js')}}?1"></script>
<script>
function amountToCart() {
        var size = jQuery(".swatch-option.text.active").text();
	    @php($reduction_amount = 0)
        @php($CartItems = Cart::instance('products')->content())
        @foreach($CartItems as $item)
        <?php $color=($item->options->has('color') ? $item->options->color : '');?>
        <?php $size=($item->options->has('size') ? $item->options->size : ''); ?>
        @if($item->id == $singleproduct->product_id && $color == $product_color)
        if(size == "{{ $size }}")
        @php($reduction_amount = $item->qty)
        return {{ $reduction_amount }};
        @endif
        @endforeach
        return 0;
	}
    (function(){
        document.getElementById("loading-image").outerHTML='<div id="loading-image-removed"></div>';
    })();
    function startLoading(){
        var amount=document.getElementById("qty").getAttribute("max");
        if(amount - document.getElementById("qty").value >= 0)
           document.getElementById("loading-image-removed").outerHTML='<div id="loading-image"><img src="https://pride-limited.com/storage/app/public/loader.gif" Alt="Loading..." /></div>';
    }
</script>
@endsection