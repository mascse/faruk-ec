<?php

use App\Http\Controllers\product\ProductController;
?>
@extends('layouts.app')
@section('title','Pohela Boishak 1425')
@section('content')
<!--- slider here ---->
<style>
    .img-fluid{
        height:383px !important;
    }
    .shop-sidebar__category-list {
    margin-bottom: 1.5625rem;
    font-size: 14px;
    font-weight: 400;
    font-family:myriad-pro; 
    color: #666;
    line-height: 1.75;
    white-space: nowrap;
}
.shop-sidebar .h3, .shop-sidebar .h4 {
    color: #291d88;
    font-size: 14px;
}
.shop-sidebar__category-list .item-link:hover {
    color: #291d88;
     font-size: 14px;
    font-weight: 400;
}
sup{
    border: 1px solid #0000007d;
    top: -0.6em;
    border-radius: 50%; 
}
</style>
<div id="search_result">
<!--- slider end here ---->
<?php if ($procat_id == 5) { ?>
<div class="container">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img  src="{{ URL::to('') }}/storage/app/public/img/shop_banner/pride_girls.jpg" class="img-responsive" alt="First slide">
                </div>
            </div>
        </div>
    </div>
   <?php }elseif($procat_id == 9){ ?>
      <div class="container">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img  src="{{ URL::to('') }}/storage/app/public/img/shop_banner/home page banner 3.jpg" class="img-responsive" alt="First slide">
                </div>
            </div>
        </div>
    </div>
   <?php }elseif($procat_id == 6){ ?>
   <div class="container">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img  src="{{ URL::to('') }}/storage/app/public/img/shop_banner/home page banner 5.jpg" class="img-responsive" alt="First slide">
                </div>
            </div>
        </div>
    </div>
   <?php }elseif($procat_id == 17){  ?>
     <div class="container">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img  src="{{ URL::to('') }}/storage/app/public/img/shop_banner/mens wear banner.jpg" class="img-responsive" alt="First slide">
                </div>
            </div>
        </div>
    </div>
   <?php } ?>
<main class="main shop-page">
    <div class="container">
        <div class="row align-items-start">
            <div class="col-12 d-flex justify-content-end d-lg-none">
            </div>
                        <div class="col-12 col-lg-2 collapse d-lg-block" id="shopSidebar">
                            <aside class="shop-sidebar">
                                <section class="shop-sidebar__category">
                                    <h4 class="h4 text-uppercase">category</h4>
                                    <ul class="shop-sidebar__category-list js-collapse-menu">
                                        <li><a class="item-link" href="{{url('boishakh-1425/pride-girls/5')}}">Ecommerce<sup> R </sup>  &nbsp;Girls</a>
                                        </li>
                                         <li><a class="item-link" href="{{url('/boishakh-1425/pride-signature/9')}}">Ecommerce<sup> R </sup>  &nbsp;Signature </a>
                                        </li>
                                         <li><a class="item-link" href="{{url('/boishakh-1425/ethnic-menswear/17')}}">Ecommerce<sup> R </sup>  &nbsp;Ethnic Menswear</a>
                                        </li>
                                         <li><a class="item-link" href="{{url('/boishakh-1425/pride-kids/6')}}">Ecommerce<sup> R </sup>  &nbsp;Kids</a>
                                        </li>
                                    </ul>
                                </section>
                            </aside>
                        </div>
            <div class="col-12 col-lg-10">
                <!-- <div class="sortbar">
                     <div class="sortbar__row row align-items-center">
                         <div class="col-12 d-flex justify-content-between col-lg-4 col-xl-4">
                             <div class="sortbar__search">showing<span> 9</span> of<span> 100</span> results</div>
                         </div>
                         <div class="col-lg-3 col-xl-3 d-flex align-items-center">
                             <div class="sortbar__label">show</div>
                             <div class="select-dropdown sortbar__select sortbar__select--show js-sortbar-show">
                                 <select class="form-control js-select js-sort-show" name="sort_show">
                                     <option value="9">9</option>
                                     <option value="8">8</option>
                                     <option value="7">7</option>
                                     <option value="6">6</option>
                                     <option value="5">5</option>
                                     <option value="4">4</option>
                                     <option value="3">3</option>
                                 </select>
                             </div>
                         </div>
                         <div class="col-lg-3 col-xl-3 d-flex align-items-center">
                             <div class="sortbar__label">price</div>
                             <div class="select-dropdown sortbar__select sortbar__select--price js-sortbar-price">
                                 <select class="form-control select-dropdown js-select js-sort-price" name="sort_price">
                                     <option value="asc">asc</option>
                                     <option value="descending">descending</option>
                                 </select>
                             </div>
                         </div>
                         <div class="sortbar__col-grid col-lg-2 col-xl-2 text-right">
                             <ul class="sortbar__gridlist">
                                 <li><a class="btn-toggle-grid js-toggle-grid active" href="#" data-grid="grid" data-cols="col-6 col-lg-4" data-toggle="tooltip" data-placement="top" title="Grid"><span></span><span></span><span></span></a></li>
                                 <li><a class="btn-toggle-list js-toggle-grid" href="#" data-grid="list" data-cols="list col-12" data-toggle="tooltip" data-placement="top" title="List"><span></span><span></span></a>
                                 </li>
                             </ul>
                         </div>
                     </div>
                 </div> --->
                <div class="row">
                    @foreach($product_list as $product)
                    <?php
                    $product_name = str_replace(' ', '-', $product->product_name);
                    $product_url = strtolower($product_name);
                    $data = ProductController::GetProductColorAlbum($product->product_id);
                    $sold_out = ProductController::ProductWiseQty($product->product_id);
                    ?>
                    <div class="product-grid col-6 col-md-4">
                        <div class="product-item row js-shop-item-product">
                            <div class="product-item__grid col-5 col-sm-5 col-lg-4 mb-4">
                                <div class="product-item__img">
                                    <a class="product-item__img-link js-shop-img-flip d-block" href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}">
                                        <?php
                                        foreach ($data as $pro_album) {
                                            $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                                        }
                                        ?>
                                        <span class="front">
                                            <img class="img-fluid"  
                                                 src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_medium; ?>" 
                                                 onmouseover="this.src = base_url + '/storage/app/public/pgallery/<?php echo $colorwiseimg->productimg_img_medium; ?>'" 
                                                 onmouseout="this.src = base_url + '/storage/app/public/pgallery/<?php echo $product->productimg_img_medium; ?>'" border="0" alt=""/>
<!--                                            <img class="img-fluid" src="{{ URL::to('') }}/storage/app/pgallery/{{$product->productimg_img_medium}}" alt="#" height="383px"/>-->
                                        </span>
                                        <span class="back"><img class="img-fluid" src="{{ URL::to('') }}/storage/app/public/img/product/294_detailimg_2.13.jpg" alt="#"/></span>
                                    </a>
                                    <?php  if($sold_out <= 0){ ?>
                                       <div class="product-item__sale">Sold Out</div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="product-item__list collapse js-list-show col-7 col-sm-7 col-lg-8">
                                <div class="product-item__title h4 text-uppercase mb-4">{{$product->product_name}}</div>
                                <div class="product-item__price"><span class="currency">Tk</span><span> {{$product->product_price}}</span></div>
                                <div class="product-rating mb-3">
                                    <div class="product-rating__stars mr-2 mb-2">
                                        <select class="js-product-rating">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4" selected="selected">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                    <div class="product-rating__stat mb-2"><span class="text-uppercase">number of ratings</span><span class="d-inline-block align-top px-2">|</span><span class="value">340</span></div>
                                </div>
                                <div class="product-item__list-desc">
                                    <p></p>
                                </div>
                                <div class="product-share">
                                    <button class="product-share__btn-cart btn btn-shop" type="button">add to cart</button>
<!--                                    <button class="product-share__btn pointer" type="button" data-toggle="tooltip" data-placement="top" title="View"><i class="ion-ios-eye-outline"></i></button>
                                    <button class="product-share__btn pointer" type="button" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="ion-android-favorite-outline"></i></button>-->
                                </div>
                            </div>
                            <div class="product-item__bottom js-grid-show">
                                <div class="product-item__title h4 text-uppercase mb-2"><a href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}">{{$product->product_name}}</a></div>
                                <div class="product-item__price mb-0"><span class="currency">Tk</span><span> {{$product->product_price}}</span></div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <nav aria-label="Page navigation example">
                    
                </nav>
            </div>
        </div>

    </div>
 </main>
</div>
@endsection