<ul class="store-list">
	<input type="hidden" class="totalcount" value="13">
	<input type="hidden" class="instockcount" value="3">
	<?php foreach($showroom_stock as $stock){ ?>
	<li class="store-tile 10200003">
		<div class="store-address">
			<a href="#"><span class="heading-name"><?php echo $stock->SalesPointName;?></span></a><br>
			<?php echo $stock->SPAddress;?><br>
			<?php echo $stock->city;?>
		</div>
		<div class="store-details">
		  <?php if($stock->CurrentStock <= 0){ ?>
			  <span class="outofstock" data-status="1">not available</span><br>
			  <span class="stockmessage outofstock-msg hide">Ship to FREE for when you <br> select 'in Store Pick Up' at checkout (selected stores only)</span>
		  <?php }elseif($stock->CurrentStock <= 2){ ?>
		    <span class="lowstock findstore" data-status="2">low stock</span><br>
			<span class="hide stockmessage instock-msg">Availability at store is not guaranteed. Inventory status refreshed hourly.</span><br>
		  <?php }else{ ?>
			<span class="instock" data-status="3">available (<?php echo $stock->CurrentStock;?>)</span><br>
			<span class="hide stockmessage instock-msg">Availability at store is not guaranteed. Inventory status refreshed hourly.</span><br>
		  <?php } ?>
			
			<span class="opening"><?php echo $stock->OpeningTime;?> - <?php echo $stock->ClosingTime;?></span>
		</div>
	</li>
	<?php } ?>	
	<span class="morestore more">More</span>
	<div class="more-store-details hide">
		<li class="store-tile 10200075">
			<div class="store-address">
				<a href="#"><span class="heading-name">UNIQLO Jersey Gardens</span></a><br>
				651 Kapkowski Road, Space 2016 <br>
				Elizabeth, New Jersey
			</div>
			<div class="store-details">
				<span class="outofstock" data-status="1">not available</span><br>
				<span class="hide stockmessage outofstock-msg">Ship to FREE for when you <br> select 'in Store Pick Up' at checkout (selected stores only)</span><br>
				<span class="opening">10am - 9pm</span>
			</div>
		</li>
		
	</div>
</ul>