<?php
use App\Http\Controllers\product\ProductController;
?>
@extends('layouts.app')
@section('title','Falgun Collection')
@section('content')

<style>
    .nav-block {
        position: relative;
        left: 0;
        right: 0;
        top: 100%;
        background: #fff;
        -webkit-box-shadow: 0 0 1px 0 rgba(127, 127, 127, 0.2);
        box-shadow: 0 0 1px 0 rgba(127, 127, 127, 0.2);
    }
    .icon-down:before {
        content: "\f107";
        font-family: FontAwesome;
        font-size: 15px;
    }
    .product-items .product-item-photo .product-image-wrapper {
        display: block;
        overflow: hidden;
        position: relative;
        border: 0px solid #00000012;
    }
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item">
                                <a href="" title=""><?php echo $title;?></a>
                            </li>
                            <li class="item">
                                <a href="" title="">Falgun Collection</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-banner">
                        <div class="block-content">
                            <a href="#" class="opener-filter">filters</a>
                            <div class="filter-holder">
                                <div class="filter-content">
                                    <ul class="list-inline">
                                        <li>
                                            <a class="opener-cate" href="#"> Category <span class="icon-down"></span></a> 
                                            <div class="filter-block">

                                                <ol class="m-filter-item-list">
                                                    <?php if ($title == 'woman') { ?>
                                                        <li class="item">
                                                            <a href="#">
                                                                Signature
                                                            </a>
                                                        </li>
                                                        <li class="item">
                                                            <a href="#">
                                                                Classic 
                                                            </a>
                                                        </li>
                                                        <li class="item">
                                                            <a href="#">
                                                                Pride Girls 
                                                            </a>
                                                        </li>
                                                    <?php } elseif ($title == 'man') { ?>
                                                        <li class="item">
                                                            <a href="#">
                                                                Ethnic Menswear
                                                            </a>
                                                        </li>
                                                    <?php } elseif ($title == 'kids') { ?>
                                                        <li class="item">
                                                            <a href="#">
                                                                Boys
                                                            </a>
                                                        </li>
                                                        <li class="item">
                                                            <a href="#">
                                                                Girls
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ol>
                                            </div>
                                        </li>
                                        <li>
                                            <a class="opener-cate" href="#"> Price <span class="icon-down"></span></a> 
                                            <div class="filter-block">

                                                <ol class="m-filter-item-list">
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 500</span> - <span class="price">Tk 1,000</span>                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 1,000</span> - <span class="price">Tk 1,500</span>                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 1,500</span> - <span class="price">Tk 2,000</span>                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 2,000</span> - <span class="price">Tk 2,500</span>                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            <span class="price">Tk 2,500</span> and above                                    </a>
                                                    </li>
                                                </ol>
                                            </div>
                                        </li>
                                        <!--<li>
                                            <a class="opener-cate" href="#"> Color <span class="icon-down"></span></a> 
                                            <div class="filter-block">

                                                <ol class="m-filter-item-list">
                                                    <li class="item">
                                                        <a href="#"> Black</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">Blue</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            Red                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            White                                    </a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="#">
                                                            Pink                                    </a>
                                                    </li>
                                                </ol>
                                            </div>
                                        </li> -->

                                    </ul>
                                </div>   
                            </div>



                        </div>
                    </div>



                </div>
            </div>
            <div class="col-md-10">
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
                </div>



                <style>
                    .product-items .product-item-photo .product-image-wrapper {
                        display: block;
                        overflow: hidden;
                        position: relative;
                        border: 0px solid #00000012;
                    }
                </style>
                <div class="products wrapper grid products-grid">
                    <ol class="products list items product-items">
                        @foreach($product_list as $product)
                        <?php
                        $product_name = str_replace(' ', '-', $product->product_name);
                        $product_url = strtolower($product_name);
                        $data = ProductController::GetProductColorAlbum($product->product_id);
                        // dd($data);
                        $sold_out = ProductController::ProductWiseQty($product->product_id);
                        foreach ($data as $pro_album) {
                            $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                        }
                        ?>
                        <li class="item product product-item">                                
                            <div class="product-item-info" data-container="product-grid">
                                <a href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}" class="product photo product-item-photo" tabindex="-1">
                                    <span class="product-image-container">
                                        <!--                                        style="border: 0.6px solid #00000012;"-->
                                        <span class="product-image-wrapper" >
                                            <span>
                                                <span class="product-image-container">
                                                    <!--                                                    style="border: 0.6px solid #00000012;"-->
                                                    <span class="product-image-wrapper" >
                                                        <img class="product-image-photo" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img; ?>" alt="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img; ?>"/>
                                                    </span>
                                                </span>
                                            </span>
                                            <span class="back">
                                                <span class="product-image-container">
                                                    <span class="product-image-wrapper">
                                                        <img class="product-image-photo"
                                                             src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $colorwiseimg->productimg_img; ?>"
                                                             alt="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $colorwiseimg->productimg_img; ?>"/>
                                                    </span>
                                                </span>
                                            </span>
                                        </span>
                                    </span>                     

                                </a>

                                <div class="product details product-item-details">
                                    <div class="info-holder">
                                        <strong class="product name product-item-name">
                                            <a class="product-item-link"
                                               href="{{url("shop/{$product_url}/color-{$product->productalbum_name}/{$product->product_id}")}}">
                                                {{$product->product_name}}
                                            </a>
                                        </strong>

                                    </div>
                                    <div class="info-holder">
                                        <div class="price-box price-final_price" data-role="priceBox" data-product-id="79644" data-price-box="product-id-79644">
                                            <span class="normal-price">
                                                <span class="price-container price-final_price tax weee"
                                                      >
                                                    <!--        <span class="price-label">As low as</span>-->
                                                    <span  id="product-price-79644"                data-price-amount="{{$product->product_price}}"
                                                           data-price-type="finalPrice"
                                                           class="price-wrapper "
                                                           >
                                                        <span class="price">Tk,&nbsp;{{$product->product_price}}</span>    </span>
                                                </span>
                                            </span>
                                        </div>                                                        
                                        <div class="product-item-inner">
                                            <div class="product actions product-item-actions">
                                                <div class="actions-primary">
                                                    <form data-role="tocart-form" action="#" method="post">
                                                        <input type="hidden" name="product" value="">
                                                        <input type="hidden" name="uenc" value="">
                                                        <input name="form_key" type="hidden" value="" />                                                
                                                        <a href="#" class="action tocart primary"><span>Shop Now</a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                </div>
                <div class="toolbar toolbar-products" data-mage-init='{""}'>
                    <div class="modes"></div>
                    <p class="toolbar-amount" id="toolbar-amount">
                        Items <span class="toolbar-number">1</span>-<span class="toolbar-number">18</span> of <span class="toolbar-number">42</span>    </p>
                    <div class="pages">
                        <center></center>
                        <strong class="label pages-label" id="paging-label">Page</strong>
                        <ul class="items pages-items" aria-labelledby="paging-label">
                            <li class="item current">
                                <strong class="page">
                                    <span class="label">You're currently reading page</span>
                                    <span>1</span>
                                </strong>
                            </li>
                            <li class="item">
                                <a href="" class="page">
                                    <span class="label">Page</span>
                                    <span>2</span>
                                </a>
                            </li>
                            <li class="item">
                                <a href="" class="page">
                                    <span class="label">Page</span>
                                    <span>3</span>
                                </a>
                            </li>
                            <li class="item pages-item-next">
                                <a class="action  next" href="" title="Next">
                                    <span class="label">Page</span>
                                    <span>Next</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="field limiter">
                        <label class="label" for="limiter">
                            <span>Show</span>
                        </label>
                        <div class="control">
                            <select id="limiter" data-role="limiter" class="limiter-options">
                                <option value="9">
                                    9                </option>
                                <option value="18"                    selected="selected">
                                    18                </option>
                                <option value="45">
                                    45                </option>
                            </select>
                        </div>
                        <span class="limiter-text">per page</span>
                    </div>

                    <div class="toolbar-sorter sorter">
                        <label class="sorter-label" for="sorter">Sort By</label>
                        <select id="sorter" data-role="sorter" class="sorter-options">
                            <option value="">Sort By</option>
                            <option value="position"
                                    >
                                Position            </option>
                            <option value="name"
                                    >
                                Product Name            </option>
                            <option value="price"
                                    >
                                Price            </option>
                            <option value="created_at"
                                    selected="selected"
                                    >
                                Date            </option>
                        </select>
                        <a title="Set Ascending Direction" href="#" class="action sorter-action sort-desc" data-role="direction-switcher" data-value="asc">
                            <span>Set Ascending Direction</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection