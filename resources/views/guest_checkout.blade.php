@extends('layouts.app')
@section('title','Checkout')
@section('content')
<style>
    .font-alt{
        font-weight: bold;
        color: green;
    }
    .form-control:disabled, .form-control[readonly] {
        background-color: #ffffff;
        opacity: 1;
    }
    .order-total__list {
        margin-bottom: 2.5rem;
        font-weight: 700;
        font-size: 1.1875rem;
        line-height: 1.26316;
        color: #999;
        text-transform: capitalize;
    }
    label {
        display: inline-block;
        margin-bottom: .5rem;
        color: black;
        font-weight: 600;
    }
    .form-nav { margin-bottom:40px; display:inline-block; width:100%; }
    .form-nav-item { position:relative; float:left; width:20%; text-align:center; font-size:13px; color:#9bb0bb; font-weight:700; }
    .form-nav-item span {
        display:inline-block;
        background:#291d88;
        color:white;
        width:35px;
        height:35px;
        text-align:center;
        border-radius:100%;
        font-size:15px;
        padding-top:7px;
        margin-bottom:7px;
        box-shadow:0px 0px 20px rgba(0, 0, 0, 0.07);
        position:relative;
        z-index:10;
    }

    .form-nav-item:after {
        content:'';
        width:120px;
        height:4px;
        display:block;
        background:white;
        position:absolute;
        right:-60px;
        top:17px;
    }
    .multi-steps > li.is-active:before, .multi-steps > li.is-active ~ li:before {
        content: counter(stepNum);
        font-family: inherit;
        font-weight: 700;
    }
    .multi-steps > li.is-active:after, .multi-steps > li.is-active ~ li:after {
        background-color: #ededed;
    }

    .multi-steps {
        display: table;
        table-layout: fixed;
        width: 100%;
    }
    .multi-steps > li {
        counter-increment: stepNum;
        text-align: center;
        display: table-cell;
        position: relative;
        color: #291d88;
    }
    .multi-steps > li:before {
        content: '\f00c';
        content: '\2713;';
        content: '\10003';
        content: '\10004';
        content: '\2713';
        display: block;
        margin: 0 auto 4px;
        background-color: #fff;
        width: 36px;
        height: 36px;
        line-height: 32px;
        text-align: center;
        font-weight: bold;
        border-width: 2px;
        border-style: solid;
        border-color: #291d88;
        border-radius: 50%;
    }
    .multi-steps > li:after {
        content: '';
        height: 2px;
        width: 100%;
        background-color: #291d88;
        position: absolute;
        top: 16px;
        left: 50%;
        z-index: -1;
    }
    .multi-steps > li:last-child:after {
        display: none;
    }
    .multi-steps > li.is-active:before {
        background-color: #fff;
        border-color: #291d88;
    }
    .multi-steps > li.is-active ~ li {
        color: #808080;
    }
    .multi-steps > li.is-active ~ li:before {
        background-color: #ededed;
        border-color: #ededed;
    }
    .has-error {
        border-bottom: 1px solid red;
    }
    button {
        width: 100%;
        height: 30px;
        font-size: 10px;
        line-height: 28px;
        color: #fff;
        text-transform: uppercase;
        letter-spacing: 2px;
        position: relative;
        border: none;
        background: rgb(41, 30, 136);
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .checkout-container .step-title:before {
        background: #291e88;
        border-radius: 100%;
        color: #fff;
        font-size: 18px;
        height: 30px;
        left: 0;
        line-height: 30px;
        position: absolute;
        text-align: center;
        top: 2px;
        width: 30px;
    }
    .checkout-container .opc-block-summary span.title:before {
        background: #291e88;
        border-radius: 100%;
        color: #fff;
        font-size: 18px;
        height: 30px;
        left: 0;
        content: '4';
        line-height: 30px;
        position: absolute;
        text-align: center;
        top: 2px;
        width: 30px;
    }
    .table-caption{
        font-weight: 600;
        color: black;
    }
    .payment-option-title.field.choice {
        cursor: pointer;
    }
    .items-in-cart{
        cursor: pointer;
    }
    .has-error {
        border: 1px solid red !important;
        /* color: red; */
    }
    .page.messages .message-success {
        padding: 5px;
        color: #a94442;
        margin: 10px 0 0;
        letter-spacing: 2px;
        font-weight: 500;
        background: #f1c7c7;
        font-size: 14px;
    }
    /* Tooltip container */

    /* Tooltip text */
    .tooltip-container .tooltiptext {
        visibility: hidden;
        background-color: #EEE;
        color: #000;
        text-align: center;
        padding: 10px 5px;
        border-radius: 6px;

        /* Position the tooltip text - see examples below! */
        position: absolute;
        z-index: 1;
        box-shadow:0 0 5px #AAA;
    }

    /* Show the tooltip text when you mouse over the tooltip container */
    .tooltip-container:hover .tooltiptext {
        visibility: visible;
    }
    /* List fixing */
    .numbered.list-group {
        list-style: decimal inside;
    }
</style>
<script>
    jQuery('button.open-close').click(function () {
    jQuery(this).parent(this).toggleClass('open-close-icon');
    });</script>
<div id="order-preview-page">
    <div id="search_result">
        <main id="maincontent" class="page-main" style="padding-top: 60px;">
            <a id="contentarea" tabindex="-1"></a>
            @if (session('payment-msg'))
            <div class="beadcumarea">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page messages" style="display:block;">
                                <div data-placeholder="messages"></div>
                                <div data-bind="scope: 'messages'">
                                    <div data-bind="foreach: { data: cookieMessages, as: 'message' }" class="messages"></div>
                                    <div data-bind="foreach: { data: messages().messages, as: 'message' }" class="messages"></div>
                                </div>
                                <div class="messages">
                                    <div class="message message-success success">
                                        <div data-ui-id="checkout-cart-validationmessages-message-success">{{ session('payment-msg') }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="authenticationPopup" style="display: none;">
                        </div>
                        <div id="checkout" class="checkout-container">
                            <div class="checkout-header">
                                <h1 class="title">Checkout</h1>
                                <div class="description">Please enter your details below to complete your purchase</div>
                            </div>
                            <div class="opc-wrapper layout-3-columns">
                                <form class="form" action="{{url('/guest-order')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="checkout-column opc">
                                        <div class="checkout-block">
                                            <li id="shipping" class="checkout-shipping-address">
                                                <div class="step-title" data-role="title">Name &amp; Address</div>
                                                <div id="checkout-step-shipping" class="step-content" data-role="content">

                                                    <div id="shipping-new-address-form" class="fieldset address">
                                                        <div class="field _required">
                                                            <label class="label">
                                                                <span>First Name</span>
                                                            </label>
                                                            <div class="control {{ $errors->has('firstname') ? ' has-error' : '' }}">
                                                                <input class="input-text" type="text" name="firstname" value="" required/>
                                                                @if ($errors->has('firstname'))
                                                                <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                                    <div id="password-strength-meter" class="password-strength-meter">
                                                                        {{ $errors->first('firstname') }}
                                                                        <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="field _required"  name="shippingAddress.lastname">
                                                            <label class="label">
                                                                <span>Last Name</span>
                                                            </label>
                                                            <div class="control {{ $errors->has('lastname') ? ' has-error' : '' }}">
                                                                <input class="input-text" type="text" value="" name="lastname" required/>
                                                                @if ($errors->has('lastname'))
                                                                <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                                    <div id="password-strength-meter" class="password-strength-meter">
                                                                        {{ $errors->first('lastname') }}
                                                                        <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
														<div class="field _required"  name="shippingAddress.telephone">
                                                        <label class="label">
                                                            <span>Email</span>
                                                        </label>

                                                        <div class="control _with-tooltip {{ $errors->has('email') ? ' has-error' : '' }}">
                                                            <input class="input-text" type="text"  name="email" />
                                                            <div class="field-tooltip toggle">
                                                                <span class="field-tooltip-action action-help" tabindex="0" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false" role="button"></span>
                                                                <div class="field-tooltip-content" data-target="dropdown" aria-hidden="true">For delivery questions.</div>
                                                            </div>
                                                            @if ($errors->has('email'))
                                                            <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                                <div id="password-strength-meter" class="password-strength-meter">
                                                                    {{ $errors->first('email') }}
                                                                    <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                        <div class="field _required"  name="shippingAddress.telephone">
                                                            <label class="label">
                                                                <span>Mobile Number</span>
                                                            </label>

                                                            <div class="control _with-tooltip {{ $errors->has('mobile_no') ? ' has-error' : '' }}">
                                                                <input class="input-text" type="text" value="" name="mobile_no" required/>
                                                                <div class="field-tooltip toggle">
                                                                    <span class="field-tooltip-action action-help" tabindex="0" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false" role="button"></span>
                                                                    <div class="field-tooltip-content" data-target="dropdown" aria-hidden="true">For delivery questions.</div>
                                                                </div>
                                                                @if ($errors->has('mobile_no'))
                                                                <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                                    <div id="password-strength-meter" class="password-strength-meter">
                                                                        {{ $errors->first('mobile_no') }}
                                                                        <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <fieldset class="field street admin__control-fields required">
                                                            <legend class="label">
                                                                <span>Street Address</span>
                                                            </legend>
                                                            <div class="control">
                                                                <div class="field _required"  name="shippingAddress">
                                                                    <label class="label">
                                                                    </label>
                                                                    <div class="control {{ $errors->has('address') ? ' has-error' : '' }}">
                                                                        <input class="input-text" type="text"  value="" name="address" required/>
                                                                        @if ($errors->has('address'))
                                                                        <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                                            <div id="password-strength-meter" class="password-strength-meter">
                                                                                {{ $errors->first('address') }}
                                                                                <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                        <div class="field _required"  name="shippingAddress.city">
                                                            <label class="label">
                                                                <span>City</span>
                                                            </label>
                                                            <div class="control {{ $errors->has('address') ? ' has-error' : '' }}">
                                                                <input class="input-text" type="text"  name="registeruser_city" required/>
                                                                @if ($errors->has('registeruser_city'))
                                                                <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                                    <div id="password-strength-meter" class="password-strength-meter">
                                                                        {{ $errors->first('registeruser_city') }}
                                                                        <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="field" name="shippingAddress.country_id">
                                                            <label class="label">
                                                                <span>Country</span>
                                                            </label>
                                                            <div class="control">
                                                                <select class="select" name="country_id">
                                                                    <option value="Bangladesh" selected>Bangladesh</option>
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="field">
                                                            <label class="label">
                                                                <span>Zip/Postal Code</span>
                                                            </label>
                                                            <div class="control">
                                                                <input class="input-text" type="text" name="registeruser_zipcode"  value="" />
                                                            </div>
                                                        </div>
                                                        <div class="field choice" style="display: block;">
                                                            <input type="checkbox" class="checkbox" id="shipping-save-in-address-book">
                                                            <label class="label" for="shipping-save-in-address-book">
                                                                <span>Save in address book</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="checkout-column opc">
                                        <div class="checkout-block">
                                            <li id="opc-shipping_method" class="checkout-shipping-method">
                                                <div class="checkout-shipping-method">
                                                    <div class="step-title">Shipping Area</div>
                                                    <div class="shipping-policy-block field-tooltip" style="display: none;">
                                                        <span class="field-tooltip-action" tabindex="0" data-toggle="dropdown">
                                                            <span>See our Shipping Policy</span>
                                                        </span>
                                                        <div class="field-tooltip-content">
                                                            <span>shipping Policy Content</span>
                                                        </div>
                                                    </div>
                                                    <div id="checkout-step-shipping_method" class="step-content">
                                                        <div id="checkout-shipping-method-load">
                                                            <table class="table-checkout-shipping-method">
                                                                <thead>
                                                                    <tr class="row">
                                                                        <th class="col col-price">City</th>
                                                                        <th class="col col-method">Region</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="col col-method">
                                                                            <label>Select City</label>
                                                                            <div class="control {{ $errors->has('RegionList') ? ' has-error' : '' }}">
                                                                                <select v-on:change = "changeCity" name="RegionList" id="RegionList" required>
                                                                                    <option value="" selected>-- Select City --</option>
                                                                                    <option value="1" >Dhaka</option> 
                                                                                    <option value="2" >Chittagong</option>
                                                                                    <option value="3" >Barisal</option>
                                                                                    <option value="4" >Khulna</option> 
                                                                                    <option value="5" >Mymensingh</option>
                                                                                    <option value="6" >Rajshahi</option>
                                                                                    <option value="7" >Rangpur</option> 
                                                                                    <option value="8" >Sylhet</option>
                                                                                </select>
                                                                                @if ($errors->has('RegionList'))
                                                                                <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                                                    <div id="password-strength-meter" class="password-strength-meter">
                                                                                        {{ $errors->first('RegionList') }}
                                                                                        <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                        <td class="col col-price">
                                                                            <label>Select Region</label>
                                                                            <div class="control {{ $errors->has('region') ? ' has-error' : '' }}">
                                                                                <select  name="region" id="CityList" v-model="city_list" v-on:change = "changeArea" required>
                                                                                    <option value="">-- Select Region --</option>
                                                                                </select>
                                                                                @if ($errors->has('region'))
                                                                                <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                                                    <div id="password-strength-meter" class="password-strength-meter">
                                                                                        {{ $errors->first('region') }}
                                                                                        <span id="password-strength-meter-label" data-role="password-strength-meter-label" >
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div id="onepage-checkout-shipping-method-additional-load">
                                                        </div>
                                                        <div class="actions-toolbar" id="shipping-method-buttons-container">
                                                            <div class="primary">
                                                                <button data-role="opc-continue" type="submit" class="button action continue primary">
                                                                    <span><span>Next</span></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </div>
                                        <div class="checkout-block">
                                            <li id="payment" role="presentation" class="checkout-payment-method">
                                                <div id="checkout-step-payment" class="step-content">
                                                    <fieldset class="fieldset">
                                                        <legend class="legend">
                                                            <span>Payment Information</span>
                                                        </legend><br>
                                                        <div id="checkout-payment-method-load" class="opc-payment">
                                                            <div class="items payment-methods">
                                                                <div class="payment-group" data-repeat-index="0">
                                                                    <div class="step-title">Payment Method:</div>
                                                                    <div class="payment-method">
                                                                        <div class="payment-method-title field choice">
                                                                            <input :disabled="outside_dhaka" type="radio" @click="bKash_selected = false; iPay_selected=false; ssl_selected = false" v-model="delivery_method" value="cDelivery">
                                                                            <label class="label" for="cs">
                                                                                <span>Cash On Delivery</span></label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="payment-method">
                                                                        <div class="payment-method-title field choice">
                                                                            <input type="radio" @click="bKash_selected = true; iPay_selected=false; ssl_selected = false" v-model="delivery_method" value="bKash" id="bKash">
                                                                            <label class="label" for="bKash">
                                                                                <span>bKash - Digital Wallet Mobile Account</span></label>
                                                                        </div>
                                                                        <div class="payment-method-content" id="TID" style="display:none;">
                                                                            <div class="field">
                                                                                <label class="label" for="giftcard-code">
                                                                                    <span>Transaction Code</span>
                                                                                </label>
                                                                                <span id="bkash-message"></span>
                                                                                <div class="control">
                                                                                    <input class="input-text" type="text" id="TransactionId" name="TransactionId" placeholder="Enter Transaction Id">
                                                                                </div>
                                                                            </div>
                                                                            <div class="actions-toolbar">
                                                                                <div class="primary">
                                                                                    <button class="action action-add primary" type="button" id="apply_transcode" value="Apply">
                                                                                        <span><span>Apply</span></span>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                            <div class="help-block with-errors"></div>
                                                                            <strong>bKash Payment Instruction:</strong>
                                                                            <ol class="custom-control-description position-static mb-2" style="font-weight: 500;font-size: 14px;color: #333;">
                                                                                <li>1. Go to bKash Menu by dialing <strong>*247#</strong>.</li>
                                                                                <li>2. Choose <strong>payment</strong>.</li>
                                                                                <li>3. Enter Merchant bKash Wallet No <strong>01990409336</strong>.</li>
                                                                                <li>4. Enter the amount of your order value.</li>
                                                                                <li>5. Enter 1 as a reference No: or you can mention the purpose of the transaction in one word. e.g. Bill.</li>
                                                                                <li>6. Enter 1 as Counter No: .</li>
                                                                                <li>7. Enter your Menu PIN to confirm.</li>
                                                                                <li>8. Done! You will receive a confirmation SMS.</li>
                                                                            </ol>
                                                                        </div>
                                                                    </div>

                                                                    <div class="payment-method">
                                                                        <div class="payment-method-title field choice">
                                                                            <!--<input type="radio" name="dmselect" class="radio" id="ssl" value="ssl"> -->
                                                                            <input type="radio" @click="bKash_selected = false; iPay_selected=false; ssl_selected = true" v-model="delivery_method" value="ssl">
                                                                            <label class="label">
                                                                                <span>Credit/ Debit Card</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="payment-method">
                                                                        <div class="payment-method-title field choice">
                                                                            <!--<input type="radio" name="dmselect" class="radio" id="ipay" value="ipay"> -->
                                                                            <input type="radio" @click="bKash_selected = false; iPay_selected=true; ssl_selected = false" v-model="delivery_method" value="iPay">
                                                                            <label class="label">
                                                                                <span>iPay</span>
                                                                            </label>
                                                                        </div

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="payment-option _collapsible opc-payment-additional giftcardaccount " id="giftcardaccount-placer">
                                                                <div class="payment-option-title field choice">
                                                                    <span class="action action-toggle" id="block-giftcard-heading" role="heading" aria-level="2">
                                                                        <span class="table-caption">Apply Promotion Card</span>
                                                                    </span>
                                                                </div>
                                                                <div class="payment-option-content" data-role="content" role="tabpanel" aria-hidden="true" style="display: block;">
                                                                    <div data-role="checkout-messages" class="messages" style="color:green;">
                                                                    </div>
                                                                    <div class="payment-option-inner">
                                                                        <div class="field">
                                                                            <label class="label" for="giftcard-code">
                                                                                <span>Enter the Promotion code</span>
                                                                            </label>
                                                                            <div class="control">
                                                                                <input class="form-control input-text" type="text" id="promo_code_new" name="promo_code" v-model="promo_code"  placeholder="Enter the Promotion code">
                                                                                <div id="password-strength-meter-container" data-role="password-strength-meter">
                                                                                    <div id="password-strength-meter" class="password-strength-meter">
                                                                                        <span id="promo_check" data-role="password-strength-meter-label" >
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="actions-toolbar">
                                                                        <div class="primary">
                                                                            <button class="action action-add primary" type="button" id="ApplyCoupon" @click="promoCodeCheck()" value="Apply">
                                                                                <span><span>Apply</span></span>
                                                                            </button>
                                                                        </div>
                                                                        <!--<div class="secondary">
                                                                            <button class="action action-check" type="button"  value="See Balance">
                                                                                <span><span>See Balance</span></span>
                                                                            </button>
                                                                        </div> --->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </fieldset>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="checkout-column opc">
                                        <div class="checkout-block">
                                            <div class="opc-block-summary">
                                                <span class="title">Order Summary</span>
                                                <div class="block items-in-cart">
                                                    <div class="title" id="giftcardaccount-placer">
                                                        <strong role="heading">
                                                            <span><?php
                                                                $i = 0;
                                                                foreach (Cart::instance('products')->content() as $row) : $i++;
                                                                    ?>
                                                                <?php endforeach; ?>
                                                                {{ $i }}</span>
                                                            <span>Item<?php
                                                                if ($i == 1) {
                                                                    echo '';
                                                                } else {
                                                                    echo 's';
                                                                }
                                                                ?> in Cart</span>
                                                        </strong>
                                                    </div>
                                                    <div class="content minicart-items"  style="display:block;">
                                                        <div class="minicart-items-wrapper overflowed">
                                                            @if (Cart::instance('products')->content())
                                                            @php($CartItems = Cart::instance('products')->content())
                                                            <?php
                                                            $total = 0;
                                                            $shipping = 70;
                                                            foreach (Cart::instance('products')->content() as $row) :
                                                                ?>
                                                                <input name="product_id[]" id="product_id" type="hidden" value="<?php echo $row->id; ?>"/>
                                                                <input name="product_barcode[]" id="product_barcode" type="hidden" value="<?php echo ($row->options->has('barcode') ? $row->options->barcode : ''); ?>"/>
                                                                <input type="hidden" name="image_link[]" value="<?php echo $row->options->product_image; ?>">
                                                                <input type="hidden" name="product_name[]" value="<?php echo $row->name; ?>">
                                                                <ol class="minicart-items">
                                                                    <li class="product-item">
                                                                        <div class="product">
                                                                            <span class="product-image-container" style="height: 150px; width: 150px;">
                                                                                <span class="product-image-wrapper">
                                                                                    <img  src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo ($row->options->has('product_image') ? $row->options->product_image : ''); ?>" width="150" height="150" alt="<?php echo $row->name; ?>" title="<?php echo $row->name; ?>">
                                                                                </span>
                                                                            </span>
                                                                            <div class="product-item-details">
                                                                                <div class="product-item-inner">
                                                                                    <div class="product-item-name-block">
                                                                                        <strong class="product-item-name"><?php echo $row->name; ?></strong>
                                                                                        <strong class="product-item-name">Size : <?php echo ($row->options->has('size') ? $row->options->size : ''); ?></strong>
                                                                                        <input class="form-control" type="hidden" name="product_size[]" value="<?php echo ($row->options->has('size') ? $row->options->size : ''); ?>">
                                                                                        <strong class="product-item-name">Color : <?php echo ($row->options->has('color') ? $row->options->color : ''); ?></strong>
                                                                                        <input class="form-control" type="hidden" name="productalbum_name[]" value="<?php echo ($row->options->has('color') ? $row->options->color : ''); ?>">
                                                                                        <div class="details-qty">
                                                                                            <span class="label"><span>Qty</span></span>
                                                                                            <input class="form-control" type="hidden" name="shoppinproduct_quantity[]"  value="<?php echo $row->qty; ?>">
                                                                                            <span class="value"><?php echo $row->qty; ?></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="subtotal">
                                                                                        <span class="price-excluding-tax">
                                                                                            <span class="cart-price">
                                                                                                <input name="product_price[]" id="product_price" type="hidden" value="<?php echo $row->price; ?>"/>
                                                                                                <span class="price">Tk <?php
                                                                                                    echo $row->price * $row->qty;
                                                                                                    $total += $row->price * $row->qty;
                                                                                                    ?></span>
                                                                                            </span>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ol>
                                                            <?php endforeach; ?>
                                                            @endif
                                                            <?php if (Cart::count() == 0) { ?>
                                                                <center style="color:red;">Sorry! your shopping cart is empty!</center>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="additional-options">
                                                <div>
                                                    <div class="font-alt"  id="greterThreeThousand" style="color:green;padding-bottom:15px;display:none;">Congratulations ! You have unlocked FREE SHIPPING!&nbsp;&nbsp;</div>
                                                </div>
                                                <div>
                                                    <span class="table-caption">Choose a delivery option</span>
                                                    <div style="padding: 10px;" id="delivery-options">
                                                        <div  v-if="!outside_dhaka"><input style="line-height:16px" type="radio" name="delivery_type" v-model="delivery_type" value="0"> <span style="color:#000; font-weight:500">Standard delivery (BDT 70)</span></div>
                                                        <div  v-if="!outside_dhaka"><input style="line-height:16px" type="radio" name="delivery_type" v-model="delivery_type" value="3"> <span style="color:#000; font-weight:500">One Day Delivery (BDT 85)</span></div>
                                                        <div  v-if="!outside_dhaka"><span class="tooltip-container"><input type="radio" name="delivery_type" v-model="delivery_type" value="1" :disabled="hour>=11"><span v-if="hour>=11" class="tooltiptext">Only for orders placed before 11am</span></span> <span style="color:#000; font-weight:500">Same day delivery (BDT 130)</span></div>
                                                        <div  v-if="outside_dhaka"><input style="line-height:16px" type="radio" name="delivery_type" v-model="delivery_type" value="2"> <span style="color:#000; font-weight:500">Delivery outside Dhaka (BDT 150)</span></div>
                                                    </div>
                                                    <!--                                                <label class="custom-control custom-radio">
                                                                                                        <input class="custom-control-input" type="radio" name="shippingmethod" id="home" value="in" checked="">
                                                                                                        <span class="d-inline-block mr-2 custom-control-indicator"></span>
                                                                                                        <span class="custom-control-description">Cash on Delivery, Delivery Charge TK 70 (Inside Dhaka City)</span>
                                                                                                    </label>
                                                                                                    <label class="custom-control custom-radio">
                                                                                                        <input class="custom-control-input" type="radio" name="shippingmethod" id="OutSideDhaka" value="out">
                                                                                                        <span class="d-inline-block mr-2 custom-control-indicator"></span>
                                                                                                        <span class="custom-control-description"> Full Advance Payment, Delivery Charge TK 150 (Outside Dhaka City)</span>
                                                                                                    </label>-->
                                                </div>
                                                <table class="data table totals">
                                                    <caption class="table-caption">Total</caption>
                                                    <tbody>
                                                        <tr class="totals sub">
                                                            <th colspan="1" scope="row">Subtotal</th>
                                                            <td class="amount" data-th="Subtotal">
                                                                <span class="price">
                                                                    <input type="hidden" name="shoppingcart_subtotal" id="subtotal" value="<?php echo $total; ?>">
                                                                    Tk	@{{ subTotal }}</span>
                                                            </td>
                                                        </tr>
                                                        <tr class="totals sub">
                                                            <th colspan="1" scope="row">Shipping</th>
                                                            <td class="amount" data-th="shipping_charge">
                                                                <div class="shippingmethod" id="InsideDhaka">
                                                                    Tk @{{ getShippingCharge }} 
                                                                </div>
                                                                <div class="shippingmethod" id="OutsideDhaka" style="display:none;">
                                                                    Tk @{{ getShippingCharge }} 
                                                                </div>
                                                                <div id="shipping_zero"  style="display:none;">
                                                                    Tk @{{ getShippingCharge }} 
                                                                </div>
                                                                <!--<input type="hidden" id="shipping_charge" name="Shipping_Charge" value="70" readonly/> --->
                                                                <input name="Shipping_Charge" type="hidden" v-model="getShippingCharge"/>
                                                            </td>
                                                        </tr>
                                                    <input type="hidden" id="offer_check" value="0"/>
                                                    
                                                        <tr class="grand totals">
                                                            <th colspan="1" scope="row">
                                                                <strong>Order Total</strong>
                                                            </th>
                                                            <td class="amount" data-th="Order Total">
                                                                <strong>
                                                                    <div class="grandtotal">
                                                                        Tk	@{{ total }} 
    <!--                                                                        <input type="hidden" name="shoppingcart_total" id="total_amount"  value=" $total_amount; ?>"/>-->
                                                                        <input type="hidden" name="used_promo" id="used_promo"  value="0"/>
                                                                        <input name="shoppingcart_total" type="hidden" v-model="total"/>
                                                                        <input type="hidden" name="used_promo" id="used_promo"  value="0"/>
                                                                        <input name="CityList" type="hidden" v-model="city_list"/>
                                                                        <input name="dmselect" type="hidden" v-model="delivery_method"/>
                                                                    </div>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                  
                                                    </tbody>
                                                </table>
                                                <div class="field">
                                                    <label class="label" >
                                                        <span>Order Comment</span>
                                                    </label>
                                                    <div class="control">
                                                        <textarea class="admin__control-textarea" name="conforder_deliverynotes" cols="15" rows="2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkout-payment-method submit">
                                                <div class="payment-methods">
                                                    <div class="actions-toolbar">
                                                        <div id="buttonoverlay" style="position: absolute; display: none; width: 29%; height: 50px; background-color: #000; color: #fff; padding: 14px; text-align: center; z-index: 99;">Please wait....</div>
                                                        <?php if (Cart::count() != 0) { ?>
                                                            <button class="action primary checkout" type="submit"  title="Place Order">
                                                                <span>Place Order</span>
                                                            </button>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            </form>
                        </div>
                        <div class="checkout-page-notification">
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
<!--- Delete modal ----->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <h6 style="text-align:center;">Are you sure to removed this item ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Remove</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
    var cart_items = {!! $CartItems !!};
    var offer = 1;
    var hour_now = {{ date('H') }};
    var item_number = {{ Cart::count() }};
    var urls = ["{{url('/orderpreview-submit')}}"];</script>
<script type = "text/javascript" src = "{{ asset('assets/js/vue-orderpreview.js') }}?{{ time() }}"></script>
<script>

    jQuery(function () {
    jQuery(".payment-option-title").on('click', function () {
    jQuery(".payment-option-content").toggle();
    });
    jQuery(".items-in-cart").on('click', function () {
    jQuery(".content").toggle();
    });
    });
</script>
@endsection